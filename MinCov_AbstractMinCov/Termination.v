(**************************************************************)
(*   Copyright (c) 2024 Thibault Hilaire                      *)
(*                      David Ilcinkas                        *)
(*                      Jerome Leroux                         *)                           
(*   Authors : Thibault Hilaire [*]                           *)
(*             David Ilcinkas [+,*]                           *)
(*             Jerome Leroux  [+,*]                           *)
(*                                                            *)
(*                             [*] Affiliation Univ. Bordeaux *)
(*                             [+] Affiliation CNRS           *)
(**************************************************************)
(*      This file is distributed under the terms of the       *)
(*                      MIT License                           *)
(**************************************************************)


From mathcomp Require Import all_ssreflect zify.
From Coq Require Import Relations.
From AlmostFull.PropBounded Require Import AlmostFull AFConstructions.
Import Order.TTheory.
From Petri_Nets_in_Coq.Karp_Miller Require Import orderext  afext petrinet pnkmaccel.
From Petri_Nets_in_Coq.Tools Require Import wbrP wbr_tree Utils.
From Petri_Nets_in_Coq.MinCov_AbstractMinCov Require Import New_transitions KMTrees AbstractMinCov.
From Coq Require Import Arith Lia Wellfounded FunInd Recdef.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope order_scope.
Local Open Scope petri_net_scope.

Section Well_foundness.


Variable pn : petri_net.


Notation place := (place pn).
Notation marking := (marking pn).
Notation markingc := (markingc pn).
Notation transition := (transition pn).
Notation transitionc:= (transitionc pn).
Notation omega_transition:= (omega_transition pn).
Notation abstraction := (abstraction pn).
Notation acceleration := (acceleration pn).
Notation nil_abstraction :=(nil_abstraction pn).
Notation KMTree := (KMTree pn).
Notation Rel_small_step:=(@Rel_small_step pn).
Notation omegaR:=(@omegaR pn).


Notation vsv := ((@val _ _ _) \o sval ).
Notation vsa := (vsv \o val) .

Notation "l .histo" := (l.1.1.1) (at level 2, format "l .histo").
Notation "l .omega" := (l.1.1.2) (at level 2, format "l .omega").
Notation "l .front" := (l.1.2) (at level 2, format "l .front").
Notation "l .saturated" := (l.2) (at level 2, format "l .saturated").

Section label.

Definition label := ((((seq markingc*nat)*bool)%type)*bool)%type.



(*
  We define two well-founded relations labelR and labelMod on labels:
  - LabelBr will be use to say that the branches of our KMTree are "bad", by this we mean that
    they don't have any increasing pair according to <= on the markings that is a wqo
    because <= is one on nat
  - LabelMod is a well founded relation that is here to explicit the "ressources" we loose each time
    we rewrite a node in Rel_small_step, it is mostly the number of places of a marking where there isn't
    an omega yet
*)

Definition HistoR (l1 l2 : seq markingc) : Prop := Bad_extension  Order.le l1 l2.

Lemma wf_HistoR : well_founded HistoR.
Proof.
  apply wf_bad_extension_from_af.
  apply: (@af_strengthen _ (finfun_lift (option_lift leq))).
    by apply: af_finfun; apply: af_option; apply: af_leq.
  move => mc mc'; rewrite /finfun_lift /option_lift => H.
  apply/forallP => p; move/(_ p) : H .
  by case: (mc p); case:(mc' p).
Qed.

Definition labelBr (l1 l2 : label) : Prop := HistoR l1.histo l2.histo.


Theorem wf_labelBr : well_founded labelBr.
Proof.
  apply (wf_inverse_image label _ HistoR).
  apply wf_HistoR.
Qed.


Lemma Bad_antichain (l : seq markingc) :
  antichain l -> Bad Order.le l.
Proof.
  elim : l => //= [_| a l Hind /andP [Hal Hl]] .
    by apply badEmpty.
  apply (badExtend).
    by apply Hind.
  move/allP : Hal; move : Hind Hl a => _ _.
  elim : l => //= [ a Hl| mc l Hind a Hal].
    by rewrite /neverLarger List.Forall_nil_iff.
  rewrite neverLargerE; split.
    apply Hind => x Hin.
    by apply Hal; rewrite in_cons Hin orbT.
  move/(_ mc): Hal.
  rewrite in_cons eqxx orTb.
  move/(_ isT)/(negP) => Hneq Hmc.
  apply Hneq.
  by rewrite Hmc orbT.
Qed.



Inductive Pre_labelMod : nat -> nat -> bool -> bool -> bool -> bool -> Prop :=
| sat n1 n2 b1 b2 s1 s2:
    n1 < n2 ->
    Pre_labelMod n1 n2 b1 b2 s1 s2
| true_true n :
    Pre_labelMod n n true true false true
|true_false n b :
    Pre_labelMod n n true false false b
| false_true n b:
    Pre_labelMod n n false true b true
| false_false n :
    Pre_labelMod n n false false false true.

Definition labelMod (l1 l2: label) : Prop := Pre_labelMod l1.omega l2.omega l1.front l2.front l1.saturated l2.saturated.


Theorem wf_labelMod : well_founded labelMod.
Proof.
  suff: (forall n l, l.omega = n -> Acc labelMod l).
    move => H [] [] [] s1 mc1 b1 A1.
    by apply: H.
  move => n1.
  elim:(lt_wf n1) => {}n1 _ Hind [] [] [] h1 _ f1 s1 /= ->.
  constructor; move => [] [] [] h2 n2 f2 s2.
  inversion 1 as [ n n' b b' s s' Hlt Hn Hn' Hf1 Hf2 Hb Hb' |
  n Hn Heq Hf1 Hf2 Hs1 Hs2 |
  n b Hn Heq Hf1 Hf2 Hs1 Hb  |
  n b Hn Heq Hf1 Hf2 Hs2 Hb  |
  n Hn Heq Hf1 Hf2 Hs1 Hs2];subst.
  - by apply: (Hind n2); move: Hlt => //=; lia.
  - simpl in Heq, Hf1, Hf2, Hs1, Hs2;subst.
    constructor; move => [] [] [] h3 n3 f3 s3.
    inversion 1 as [n n' b b' s s' Hlt Hn Hn' Hf1 Hf2 Hb Hb'| | | |];subst.
    by apply: (Hind n3); move: Hlt => //=; lia.
  - simpl in Heq, Hf1, Hf2, Hs1; subst .
    constructor; move => [] [] [] h3 n3 f3 s3.
    inversion 1 as [n n' b b' s s' Hlt Hn Hn' Hf1 Hf2 Hb Hb'| | | |];subst.
    by apply: (Hind n3); move: Hlt => //=; lia.
  - simpl in Heq, Hf1, Hf2, Hb; subst .
    constructor; move => [] [] [] h3 n3 f3 s3.
    inversion 1 as [ n n' b b' s s' Hlt Hn Hn' Hf1 Hf2 Hb Hb'|
     |n b Hn Heq Hf1 Hf2 Hs1 Hb | | n Hn Heq Hf1 Hf2 Hs1 Hs2
    ];subst.
    + by apply: (Hind n3); move: Hlt => //=; lia.
    1,2: clear Hf2.
    + simpl in Heq, Hf1, Hs1; subst .
      constructor; move => [] [] [] h4 n4 f4 s4.
      inversion 1 as [n n' b b' s s' Hlt Hn Hn' Hf1 Hf2 Hb Hb'| | | |];subst.
      by apply: (Hind n4); move: Hlt => //=; lia.
    + simpl in Heq, Hf1, Hs1, Hs2; subst.
      constructor; move => [] [] [] h4 n4 f4 s4.
      inversion 1 as [ n n' b b' s s' Hlt Hn Hn' Hf1 Hf2 Hb Hb'|
      | n b Hn Heq Hf1 Hf2 Hs1 Hb  | |];subst.
        by apply: (Hind n4); move: Hlt => //=; lia.
      simpl in Heq, Hf1, Hf2, Hs1; subst; clear Hf2.
      constructor; move => [] [] [] h5 n5 f5 s5.
      inversion 1 as [n n' b b' s s' Hlt Hn Hn' Hf1 Hf2 Hb Hb'| | | |];subst.
      by apply: (Hind n5); move: Hlt => //=; lia.
  - simpl in Heq, Hf1, Hf2 , Hs1, Hs2; subst.
    constructor; move => [] [] [] h3 n3 f3 s3.
    inversion 1 as [ n n' b b' s s' Hlt Hn Hn' Hf1 Hf2 Hb Hb'
    | | n b Hn Heq Hf1 Hf2 Hs1 Hb | |];subst.
      by apply: (Hind n3); move: Hlt => //=; lia.
    simpl in Heq, Hf1, Hf2, Hs1; subst; clear Hf2.
    constructor; move => [] [] [] h4 n4 f4 s4.
    inversion 1 as [n n' b b' s s' Hlt Hn Hn' Hf1 Hf2 Hb Hb'| | | |];subst.
    by apply: (Hind n4); move: Hlt => //=; lia.
Qed.


Definition add_histo_label (mc : markingc) (l: label) := (rcons l.histo mc, l.omega, l.front, l.saturated).

Fixpoint add_histo (mc : markingc) (T : Tree label)  := match T with
  | Node a l => Node (add_histo_label mc a) (map (add_histo mc) l )
end.

Lemma add_histo_hdC (mc : markingc) (T: Tree label) :
  hd (add_histo mc T) = add_histo_label mc (hd T).
Proof. by case: T. Qed.

Lemma foldr_add_histoC (s : seq markingc) (T: Tree label):
  hd (foldr add_histo T s) = foldr add_histo_label (hd T) s.
Proof.
  elim : s T => //= mc s Hind T.
   by rewrite add_histo_hdC Hind.
Qed.


End label.

(*
  We cast KMTree into Trees defined with sequences in order to use our well-founded rewriting relation on them.
  The idea is to put labels on nodes that are:
  - the branch and the last node of the branch
  - a boolean saying whether the last node of the branch is front
  - a boolean saying whether the last node of the branch is saturated
*)

Section KM_to_label.

Fixpoint from_KM_to_label (T: KMTree) (Acc: seq acceleration): Tree label := match T with
| Br mc b f => Node ((nil,(omega_possible mc)),b,(saturated_markingc mc Acc))
    [seq (add_histo mc (from_KM_to_label (f t) Acc)) | t <- enum transition & ~~EmptyTree (f t)]
| Empty => Node ((nil, 0),true, false) nil
end.

Lemma from_KM_to_label_Br mc b f Acc:
  from_KM_to_label (Br mc b f) Acc
    =
  Node ((nil,(omega_possible mc)),b,(saturated_markingc mc Acc))
  [seq (add_histo mc (from_KM_to_label (f t) Acc)) | t <- enum transition & ~~EmptyTree (f t)].
Proof. by []. Qed.


Lemma foldr_add_histoE a l l':
  (foldr add_histo (Node a l) (rev l') = Node (foldr add_histo_label a (rev l'))
  (map (fun x => foldr add_histo x (rev l') ) l )) .
Proof.
  elim : l' a l => //= [a l //=|mc l' Hind a l].
    by rewrite map_id.
  rewrite rev_cons foldr_rcons /(add_histo mc) -/(add_histo) (Hind (add_histo_label mc a) _).
  apply f_equal2.
    by rewrite foldr_rcons.
  rewrite -map_comp.
  apply eq_map => t //=.
  by rewrite foldr_rcons.
Qed.

Lemma foldr_add_histo_labelE (a : label) l :
  foldr add_histo_label a (rev l) = ((a.histo)++l, a.omega ,a.front, a.saturated).
Proof.
  elim : l a => //= [a | mc l Hind a].
    rewrite cats0.
    by move: a => [] [] [] .
  by rewrite rev_cons foldr_rcons Hind /add_histo_label //= cat_rcons.
Qed.


Lemma foldr_add_nilE mc b s A :
  foldr add_histo_label ([::],mc, b, A) s = (rev s, mc,b, A ).
Proof.
  elim: s mc b => //= m s Hind mc b.
  by rewrite Hind /add_histo_label //= rev_cons.
Qed.

(*
  We show that if our KMTree respects some properties, then they are going to be good
  for a well-founded relation that is a prerequesite for our rewriting relation to be well-founded
*)

Lemma good_labelBr_sub_histo l1 l2 T A:
  Front_leaves T ->
  antichain (rev (l1++l2)++ Markings_not_front_of_T T) ->
  good labelBr (foldr add_histo (from_KM_to_label T A) (rev (l1++l2))) ->
  good labelBr (foldr add_histo (from_KM_to_label T A) (rev l1)).
Proof.
  elim : T l1 l2 =>  [ l1 l2 _ _ _ | mc  b f Hind l1 l2 Hfl Hac].
    all: rewrite !foldr_add_histoE -/from_KM_to_label.
    exact: good_leaf.
  rewrite -!map_comp.
  elim : (enum transition) => //= [_|t s Hind'].
    exact: good_leaf.
  move Heq: (f t) => [ //= | //= mc0 b0 f0] .
  generalize dependent b.
  case  => [|Hfl Hac Hind'].
    move/Front_leaves_invariant => Hlf.
    by rewrite Hlf in Heq.
  move => Hgood;apply good_br.
  - by apply Hind'; inversion Hgood.
  - rewrite foldr_add_histoC Heq from_KM_to_label_Br -foldr_rcons !foldr_add_nilE  rev_rcons //=.
    apply extra.
    apply Bad_antichain.
    rewrite revK.
    move/rev_antichain : Hac.
    rewrite rev_cat revK Markings_not_Front_Br rev_cons cat_rcons.
    apply antichain_subseq.
    apply (@cat_subseq _ nil (mc::l1) _ _ ).
      exact: sub0seq.
    by rewrite prefix_subseq.
  - rewrite -foldr_rcons -rev_cons.
    apply (Hind t (mc::l1) l2).
    + by move/Front_leaves_invariant2 : Hfl.
    + move : Hac; rewrite !rev_cat rev_cons -!catA cat_rcons Markings_not_Front_Br.
      apply (antichain_subseq).
      rewrite !subseq_cat2l subseqE subseq_flatten //.
      exact: codom_f.
    + inversion_clear Hgood as [| a T s0 Hg Hlabel Hgc Hhd].
       by move : Hgc; rewrite -foldr_rcons !rev_cat rcons_cat rev_cons .
Qed.


Lemma good_labelBr_add_histo l T A:
  Front_leaves T ->
  antichain ( (rev l) ++ Markings_not_front_of_T T) ->
  good labelBr (from_KM_to_label T A) ->
  good labelBr ( foldr add_histo (from_KM_to_label T A) (rev l)).
Proof.
  elim : T l => [//= l _ _ _| mc b f Hind l Hfl Hac].
    rewrite foldr_add_histoE //=.
    exact:good_leaf.
  rewrite from_KM_to_label_Br  foldr_add_histoE /=.
  elim : (enum _) => [//= _| t0 l0 Hind'].
    exact:good_leaf.
  move Heq: (f t0) =>  [| mc0  b0 f0] //=.
  all: rewrite Heq //=.
  generalize dependent b.
  case  => [|Hfl Hac Hind'].
    move/Front_leaves_invariant => Hlf.
    by rewrite Hlf in Heq.
  move => Hgood;apply good_br.
  - apply Hind'.
    by inversion Hgood.
  - rewrite -(foldr_rcons) foldr_add_histoC Heq from_KM_to_label_Br //= -rev_cons /labelBr !foldr_add_nilE !revK //=.
    apply extra.
    apply Bad_antichain.
    move/rev_antichain : Hac.
    rewrite rev_cat revK Markings_not_Front_Br rev_cons cat_rcons.
    apply antichain_subseq.
    by rewrite (subseq_cat2r _ nil _) sub0seq.
  - rewrite -(foldr_rcons) -(rev_cons).
    apply (Hind t0 _ (Front_leaves_invariant2 Hfl t0)) => //=; last first.
      inversion_clear Hgood as [| a T s0 Hg Hlabel Hgc Hhd].
      move : Hgc; apply (@good_labelBr_sub_histo nil [::mc]).
        by move/Front_leaves_invariant2: Hfl.
      move : Hac => //; rewrite cat0s rev_cons cat_rcons.
      apply antichain_subseq.
      apply cat_subseq.
        by rewrite subseq_rev sub0seq.
      rewrite Markings_not_Front_Br subseqE subseq_flatten //.
      exact: codom_f.
    move : Hac;apply antichain_subseq.
    rewrite rev_cons cat_rcons cat_subseq //.
    rewrite Markings_not_Front_Br subseqE subseq_flatten //.
    exact: codom_f.
Qed.

Lemma Good_from_KM_to_label:
  forall T A, Front_leaves T  ->
  Not_Front_Antichain T ->
  good labelBr (from_KM_to_label T A) .
Proof.
  elim =>  [ A _ _|mc b f Hind A Hfl Hnfa].
    exact: good_leaf.
  rewrite from_KM_to_label_Br.
  elim : (enum _) => [| t0 s Hind2] /=.
    exact: good_leaf.
  move Heq: (f t0) =>  [| mc0 b0 f0] //=.
  apply: good_br => //.
    rewrite add_histo_hdC  Heq /=.
    by repeat constructor.
  apply (@good_labelBr_add_histo [::mc]).
  - exact: (Front_leaves_invariant2 Hfl).
  - move: Hfl Heq Hnfa.
    case b => [|_ _].
      by move/Front_leaves_invariant => ->.
    rewrite /rev /catrev cat1s /Not_Front_Antichain Markings_not_Front_Br.
    apply antichain_subseq.
    rewrite subseqE subseq_flatten //.
    exact: codom_f.
  - apply (Hind t0 A (Front_leaves_invariant2 Hfl t0)).
    exact: (NFA_invariant Hfl Hnfa).
Qed.

End KM_to_label.

(*
  We will now show with some lemmas that if two KMTrees with their sequences of accelerations are
  in relation according to Rel_small_step, then their image by from_KM_to_label are in relation
  according to Rt, that is well-founded on good trees, hence showing that when our KMTrees are Front_leaves
  and have the Not_Front_Antichain property, then Rel_small_step is well-founded
 *)

Definition RT (T1 T2 : KMTree) (A1 A2 : seq acceleration) :=
  Rt labelBr labelMod (from_KM_to_label T1 A1) (from_KM_to_label T2 A2).
Definition RT' (T1 T2 : KMTree) (A1 A2 : seq acceleration):=
  Rt' labelMod (from_KM_to_label T1 A1) (from_KM_to_label T2 A2).

Lemma RT'0 T :
  Rt' labelMod T (Node ([::], O, true, false) [::]) -> False.
Proof.
  inversion 1 as
  [ a a' s s' Hmod  HT [Ha Hs] |
    a s1 s2  Hsize |
    a s1 s2 Hseq  HT [Ha Hs]
  ];subst.
  - move : Hmod; rewrite /labelMod //=.
    by inversion 1.
  - by inversion Hsize.
  - by inversion Hseq.
Qed.

Lemma add_histo_Rt' T mc a1 a2 l1 l2:
  T = Node a1 l1 ->
  Rt' labelMod (Node a1 l1) (Node a2 l2) ->
  Rt' labelMod (add_histo mc (Node a1 l1)) ((add_histo mc (Node a2 l2))).
Proof.
  elim : T mc a1 a2 l1 l2 => a l Hind mc a1 a2 l1 l2 [Ha Hl]; subst => HR.
  inversion HR as
  [a a' s s' Hr [Ha Hs] [Ha' Hs']|
   a s1 s2  Hsize [Ha Hs1] [Ha1 Hs2] |
   a s1 s2 Hr [Ha1 Hl1] [Ha2 Hl2]].
  1,2: rewrite /add_histo -/add_histo.
  - by apply Rt'_mod.
  - by subst; apply Rt'_del; rewrite !size_map.
  - subst;apply Rt'_child => //.
    rewrite -/add_histo.
    have Hl1 : (forall t t', List.In t l1 -> Rt' labelMod t t' ->
    Rt' labelMod (add_histo mc t) (add_histo mc t')).
      move => [a l] [a' l'] Hin Hhyp.
      by apply (Hind (Node a l)).
    clear Hind HR a2.
    elim : l1 l2 mc Hl1 Hr => // [ l2 mc _ | t1 l1 Hind [| t2 l2] mc Hrec HR ].
    + by inversion 1.
    + by inversion HR.
    + inversion_clear HR as [ T1 T2 s s' Hhere | T1 T2 s |
      T s1 s2 Hfurther [HT Hl1] [Heq Hl2]].
      * apply:One_here.
          by apply:Hrec => //=; left.
        by apply: Hind => //= t t' Hin; apply Hrec; right.
      * by apply No_more, Hrec => //=; left.
      * by apply Only_further, Hind => //= t t' Hin; apply Hrec; right.
Qed.


Lemma  add_histo_RT' mc T1 T2 A1 A2:
  RT' T1 T2 A1 A2 ->
  ~~ EmptyTree T1 ->
  Rt' labelMod (add_histo mc (from_KM_to_label T1 A1) )
                      (add_histo mc (from_KM_to_label T2 A2)).
Proof.
  move: T1 T2 => [| mc1 b1 f1] [|mc2 b2 f2] // .
    by move/RT'0.
  move => HRT _.
  by apply (@add_histo_Rt' (from_KM_to_label (Br mc1 b1 f1) A1)) .
Qed.

Lemma  Rt'_acc_suffix T A' A :
  (from_KM_to_label T (A' ++ A)) = (from_KM_to_label T A) \/
  Rt' labelMod  (from_KM_to_label T (A' ++ A)) (from_KM_to_label T A) .
Proof.
  elim: T A' A => [|mc [] f Hind] A' A.
  - by left.
  all:rewrite !from_KM_to_label_Br.
  all: case Heq: ((saturated_markingc mc A)== saturated_markingc mc (A'++A)); last first.
  1,3:move/eqP: Heq.
  1,2:case Hsat: (saturated_markingc mc (A'++A)).
  1,3:move/saturated_markingc_suffix: Hsat => -> //=.
  1,2:case: saturated_markingc => //= _.
  1,2:right;apply Rt'_mod.
  - exact: true_true.
  - exact: false_false.
  all: move/eqP: Heq => ->.
  all: case: (@fun_R_seq_aux transition transition (Tree label)
   (fun t => add_histo mc (from_KM_to_label (f t) (A' ++ A)))
   (fun t => add_histo mc (from_KM_to_label (f t) A))
   (enum transition) (fun t => ~~ EmptyTree (f t)) (fun t => ~~ EmptyTree (f t)) (Rt' labelMod)).
  1,5: by [].
  1,4: move => t _; case: (Hind t A' A).
  1,3: by move => ->; left.
  1,2: case Hft0: (f t) =>[ //=| mc0 b0 f0].
  1,3:by left.
  1,2:by move => HRt'; right; apply: @add_histo_RT'.
  2,4: move => ->.
  2,3: by left.
  1,2: by move => HR; right; apply Rt'_child.
Qed.


Lemma Rt'_child_case mc (f : {ffun transition -> KMTree}) (t: transition) (f': transition -> KMTree) A A':
  RT' (f' t) (f t) (A'++A) A ->
  (f t) <> (Empty pn) ->
  (f' t) <> (Empty pn) ->
  RT'  (Br mc false [ffun t0 => if t0 == t then (f' t0) else (f t0)]) (Br mc false f) (A'++A) A.
Proof.
  case Hft: (f t) =>[ //=| mc1 b1 f1].
  case Hf't: (f' t) => [//=| mc2 b2 f2].
  move => Hchild _ _ .
  rewrite /RT' !from_KM_to_label_Br.
  case Heq: ((saturated_markingc mc A)== saturated_markingc mc (A'++A)); last first.
    move/eqP: Heq.
    case Hsat: (saturated_markingc mc (A'++A)).
      move/saturated_markingc_suffix: Hsat => -> //=.
    case: saturated_markingc => //= _.
    by apply Rt'_mod, false_false.
  move/eqP: Heq => ->.
  apply: Rt'_child.
  apply: (@fun_R_seq _ transition _ _ _ t).
  4: by rewrite Hft.
  3: by rewrite ffunE eqxx Hf't.
  3: by apply (@add_histo_RT' mc); rewrite ffunE eqxx Hf't ?Hft.
  2: move => t0; rewrite ffunE; case Heq: (t0 == t) => //=.
  2: by move/eqP: Heq Hf't Hft => -> -> ->.
  - move => t0 ; rewrite ffunE; case : (t0==t) => //= _.
    case: (@Rt'_acc_suffix (f t0) A' A) => [ ->|].
      by left.
    case Hft0: (f t0) =>[ //=| mc0 b0 f0].
      by left.
    move => HRt'; right.
    by apply: @add_histo_RT'.
Qed.

Lemma aux_Rt'_del_case (mc : markingc) f (t : transition) (l : seq transition) A:
  (forall t', t' \in l -> t' != t) ->
  [seq add_histo mc (from_KM_to_label ([ffun t1 => if t1 == t then remove_add (f t1) [::]else f t1] t0) A)
  | t0 <- l & ~~ EmptyTree ([ffun t1 => if t1 == t then remove_add (f t1) [::] else f t1] t0)]
  =
  [seq add_histo mc (from_KM_to_label (f t0) A)| t0 <- l & ~~ EmptyTree (f t0)].
Proof.
  move => Heq.
  rewrite (@eq_in_filter _ _ (fun t0 => ~~ EmptyTree ( f t0)) l); last first.
    by move => t0; move/(Heq t0); rewrite ffunE; move/negbTE => ->.
  apply eq_in_map => t0.
  rewrite mem_filter ffunE => /andP [ _ Hin].
  by move/negbTE: (Heq t0 Hin) => ->.
Qed.

Lemma Rt'_del_case :
  forall (ad : seq transition) (T2 : KMTree) A' A,
  Front_leaves T2 ->
  forall mc : markingc, Some mc = m_from_add T2 ad ->
  ad <> [::] ->
  Rt' labelMod (from_KM_to_label (remove_add T2 ad) (A'++A)) (from_KM_to_label T2 A) .
Proof.
  elim => [//=| t ad Hind] [//=| mc0 b0 f0] A' A Hfl mc Hmc _.
  have Hb:=((Not_Front_root2 Hfl Hmc)); subst.
  rewrite remove_add_Br !from_KM_to_label_Br.
  (* First we need to know whether the labels are the same or not because if they are not we must
  decrease with labelMod *)
  case Heq: (saturated_markingc mc0 (A'++A)== saturated_markingc mc0 A); last first.
    case Heq': (saturated_markingc mc0 (A'++A)) Heq.
      by move/saturated_markingc_suffix: Heq' => ->.
    case: (saturated_markingc mc0 A) => //= _.
    by apply Rt'_mod, false_false.
  generalize dependent ad.
  (* Then we need to know if we use Rt'_del or Rt'_child *)
  case => [|t' ad' ] Hind Hmc.
    move/eqP: Heq => ->.
    move:(Dec_enum  t) => [l1 [l2 [Hl1 [Hl2 ->]]]].
    rewrite -cat1s !filter_cat !map_cat /= !ffunE eqxx remove_nil //=.
    rewrite (@aux_Rt'_del_case mc0 f0 _ _  _ Hl1) (@aux_Rt'_del_case mc0 f0 _ _ _ Hl2).
    apply: Rt'_del; rewrite !size_cat !size_map.
    case Hempty: (EmptyTree ( f0 t)) => //=.
      by move/EmptyTreeT: Hempty Hmc; rewrite m_from_add_Br => ->.
    by lia.
  apply (@Rt'_child_case mc0 f0 t) => //.
    + apply: (Hind _ _ _ _   mc ) => //=.
      by move/Front_leaves_invariant2: Hfl.
    + by move => Hft; move: Hmc; rewrite m_from_add_Br Hft.
    + by move/remove_Empty; move : Hmc; rewrite m_from_add_Br => Hmc' Hft; rewrite Hft in Hmc'.
Qed.


Lemma Br_if (mc : markingc) b t f1 f2 f3 :
  (forall t0, t0 != t -> f2 t0 = f3 t0) ->
  Br mc b  [ ffun t0 => if t0 == t then f1 t0 else f2 t0] =
  Br mc b  [ ffun t0 => if t0 == t then f1 t0 else f3 t0].
Proof.
  move => Heq.
  apply f_equal3 => //=.
  apply/ffunP => t0; rewrite !ffunE.
  case Ht0:( t0 == t) => //=.
  rewrite Heq => //=.
  by apply/negbT.
Qed.

Lemma inc_Rel_small_step_Rt:
  forall T1 T2 A1 A2, Front_leaves T2 ->
  Not_Front_Antichain T2 ->
  Rel_small_step (T1,A1) (T2,A2) ->
  RT T1 T2 A1 A2.
Proof.
  move => T1 T2 A1 A2 Hfl2 Hnf2 HR.
  apply Rt'_to_Rt.
  all: move: (Rel_small_step_keep_fl HR Hfl2) (Rel_small_step_keep_antichain HR Hfl2 Hnf2).
    apply Good_from_KM_to_label.
  inversion_clear HR as
  [ T A S ad mc acc mc' _ _ Hmc Hmc' Hneq |
    T A S ad HFront _ Hcover |
    T A S ad HnFront HnSat  |
    T A S ad _ ad' mc' _ _ Hmc' _ Hprefix |
    T A S ad HFront Hsat Hac ] => Hfl1 Hnf1.
  all: clear T1 A1.
  (* Now we begin the case of acceleration a front node *)
  - move: ad T2 A2  Hfl2 {Hnf2} acc mc mc' Hmc Hmc' Hneq {Hfl1 Hnf1}.
    elim => [//| t ad Hind] [//=| mc b f] A Hfl a mc0 mc1 Hmc0 Hmc1 Hneq.
      move: Hmc0 => [] Heq ; subst.
      rewrite /saturate_a_little -Hmc1.
      rewrite (negbTE Hneq).
      apply Rt'_mod, sat => //=.
      suff :(omegaR mc1 mc).
        by rewrite /omegaR; lia.
      apply:(@acc_inc_omega pn mc mc1 a) => //=.
      by rewrite eq_sym.
    (* Now we begin the child case of RT' *)
    have Hb:=((Not_Front_root2 Hfl Hmc0)); subst.
    rewrite saturate_a_little_Br.
    apply (@Rt'_child_case mc f t ) => //.
      + apply: (Hind _ _ _  a mc0 mc1) => //=.
        by move/Front_leaves_invariant2: Hfl.
      + by move => Hft; move: Hmc0; rewrite m_from_add_Br Hft.
      + by apply (@saturate_none_empty _ _ _ _ mc0); rewrite Hmc0 m_from_add_Br.
  (* Now we begin the case of removing a node covered by the chosen front node  *)
  3: have Had':(ad' <> nil).
  3: by move => Had'; rewrite Had' prefix0s in Hprefix.
  3:move: ad ad' mc' Hprefix Had' Hmc' Hfl1 Hnf1 Hnf2 => _ ad mc _ Had Hmc _ _ _ .
  have Had:(ad <> nil).
    move => Had; subst; move : Hcover; rewrite /ad_covered_not_front.
    move: Hfl2 Hnf2 Hnf1 Hfl1  HFront => _  _ _ _.
    by case: T2 => // mc b f /eqP ->.
  move/inv_ad_covered_not_front: Hcover => [] mc []mc' []ad' [Hmc _].
  clear Hnf2 HFront Hfl1 Hnf1.
  1,3 :move : ad T2  S A2 Hfl2 mc Hmc Had; apply Rt'_del_case.
  (* Now we begin the acceleration discovered case *)
  - move/not_saturated_node_inv: HnSat => [] mc [] Hmc.
    move: ad T2 A2 S Hfl2 {Hnf2 Hfl1 Hnf1} HnFront mc Hmc.
    elim => [//| t ad Hind] [//=| mc b f] A2 S Hfl HnFront mc0 Hmc0 HnSat.
      move: Hmc0 => [] Heq ; subst.
      rewrite /to_Front !from_KM_to_label_Br.
      move/negbTE: HnSat => ->.
      move: HnFront => //=.
      case b => //= _.
      by apply Rt'_mod, true_false.
    (* Now we begin the child case of RT'*)
    have Hb:=((Not_Front_root2 Hfl Hmc0)); subst.
    rewrite to_Front_Br  (@Br_if mc false t _ _ f); last first.
      move => t0 //=.
    apply (@Rt'_child_case mc f t ) => //.
    + apply: (Hind _ _ _ _ _ mc0) => //=.
      by move/Front_leaves_invariant2: Hfl.
    + by move => Hft; move: Hmc0; rewrite m_from_add_Br Hft.
    + move: Hmc0; rewrite m_from_add_Br.
      case: (f t) => //= mc' b' f'.
      by case ad.
  (* Now we begin the exploring case *)
  - move : ad T2 A2 Hfl2 {Hnf2 } HFront Hsat {Hac Hfl1 Hnf1}.
    elim => [//| t ad Hind] [//=| mc b f] A Hfl HFront .
      rewrite /Front_extension //= => ->.
      move: HFront => //= /eqP ->.
      by apply Rt'_mod, false_true.
    (* Now we begin the child case of RT' *)
    have Hb:=((Not_Front_root Hfl HFront)); subst.
    rewrite Front_extension_Br /saturated_node => Hsat.
    apply (@Rt'_child_case mc f t ) => //.
    + apply: (Hind) => //=.
      by move/Front_leaves_invariant2: Hfl.
    2:move/Front_extension_none_empty.
    all:by move => Hft; move: HFront; rewrite Is_front_Br Hft.
Qed.


Theorem wf_Rel_small_step: forall (T : KMTree) (A : seq acceleration),
  Front_leaves T ->
  Not_Front_Antichain T ->
  Acc Rel_small_step (T,A).
Proof.
  pose Rel_small_step'(T1 T2 : KMTree * (seq acceleration)):=Rt labelBr labelMod (from_KM_to_label T1.1 T1.2) (from_KM_to_label T2.1 T2.2).
  move => T A Hgood Hnf.
  have Hacc:(Acc Rel_small_step' (T,A)).
    rewrite /Rel_small_step'.
    apply (Acc_inverse_image _ _ (Rt labelBr labelMod) (fun (X: KMTree * (seq acceleration)) => from_KM_to_label (X.1) (X.2) )).
    apply: Acc_good_Rt.
    - exact: wf_labelBr.
    - exact: wf_labelMod.
    - exact: Good_from_KM_to_label.
  pose Good (T: KMTree * (seq acceleration)):=Front_leaves T.1.
  pose NFA (T: KMTree * (seq acceleration)):= Not_Front_Antichain T.1.
  have HGood: Good (T,A).
    by apply Hgood.
  have HNF : NFA (T,A).
    by apply Hnf.
  move : Hgood Hnf HGood HNF => _ _.
  elim : Hacc => [[{}T {}A] _]  Hind Hfl Hnf.
  constructor => [[T' A'] HR].
  apply Hind.
  - exact: inc_Rel_small_step_Rt .
  - by apply (@Rel_small_step_keep_fl _ T' T A' A).
  - by apply (@Rel_small_step_keep_antichain _ T' T A' A).
Qed.


End Well_foundness.