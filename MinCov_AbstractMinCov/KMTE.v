(**************************************************************)
(*   Copyright (c) 2024 Thibault Hilaire                      *)
(*                      David Ilcinkas                        *)
(*                      Jerome Leroux                         *)                           
(*   Authors : Thibault Hilaire [*]                           *)
(*             David Ilcinkas [+,*]                           *)
(*             Jerome Leroux  [+,*]                           *)
(*                                                            *)
(*                             [*] Affiliation Univ. Bordeaux *)
(*                             [+] Affiliation CNRS           *)
(**************************************************************)
(*      This file is distributed under the terms of the       *)
(*                      MIT License                           *)
(**************************************************************)


From mathcomp Require Import all_ssreflect zify.
From Coq Require Import Relations.
Import Order.TTheory.
From Petri_Nets_in_Coq.Karp_Miller Require Import orderext monad petrinet pnkmaccel. (* Petri Nets formalization *)
From Petri_Nets_in_Coq.Tools Require Import Utils. (* Tools *)
From Petri_Nets_in_Coq.MinCov_AbstractMinCov Require Import New_transitions KMTrees AbstractMinCov.
From Coq Require Import Arith Lia Wellfounded.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope order_scope.
Local Open Scope petri_net_scope.

Section Rel.

Variable pn : petri_net.


Notation place := (place pn).
Notation marking := (marking pn).
Notation markingc := (markingc pn).
Notation transition := (transition pn).
Notation transitionc:= (transitionc pn).
Notation omega_transition:= (omega_transition pn).
Notation abstraction := (abstraction pn).
Notation acceleration := (acceleration pn).
Notation nil_abstraction :=(nil_abstraction pn).
Notation apply_transitionc :=(@apply_transitionc pn).
Notation t_to_abstraction := (@t_to_abstraction pn).
Notation good_compoc := (@good_compoc pn).
Notation KMTree := (KMTree pn).
Notation Empty := (Empty pn).
Notation Br := (KMTrees.Br).
Notation address := (address pn).
Notation Rel_small_step := (@Rel_small_step pn).

Definition vsv : abstraction -> transitionc := fun a: abstraction => val (sval a).
Notation vsa := (vsv \o val) .


(*
  We define the type KMTE that will be our cover tree for our petri nets:
  - the markingc is the congiguration of the node
  - the acceleration sequence is the sequence of accelerations we use with potentially
    a transition (in all cases but for the root) to obtain this markingc from the parent one
  - the boolean is here to denote specific nodes, we call them Front nodes and they are
    the nodes from wich we can extend the tree
  - the function f is here to define the children of the current node
*)

Unset Elimination Schemes.

Inductive KMTE :=
| Empty_E
| Br_E of markingc & (seq acceleration) &  bool & {ffun transition -> KMTE}.




Lemma KMTE_ind P (PDLf : P Empty_E)
      (PBr: forall mc a b (f : {ffun transition -> KMTE}),
          (forall t, P (f t)) -> P (Br_E mc a b f)) : forall t, P t.
Proof.
  fix IHf 1 => -[|mc a b f]; [exact: PDLf|].
  by apply:PBr => t; apply: IHf.
Qed.

Set Elimination Schemes.

(*
  We define a function to go from KMTE to KMTree.
  Like this we don't have to redefine
  everything as both structures are very similar
*)

Fixpoint Anonymisation (T: KMTE) :KMTree := match T with
  | Empty_E => Empty
  | Br_E mc acc b f => Br mc b [ ffun t => Anonymisation (f t)]
end.


Notation "A> T" :=(Anonymisation T) (at level 70, T at next level): petri_net_scope.
Coercion KMTE_to_Kmtree := Anonymisation : KMTE -> KMTree.

(* We define a function that extract all accelerations used in a KMTE *)

Fixpoint Acceleration_of_T (T: KMTE) : seq acceleration := match T with
| Br_E mc s b f => s ++ (flatten (codom (Acceleration_of_T \o f)))
| _ => nil
end.

Lemma Acceleration_of_Br mc acc b f:
  Acceleration_of_T (Br_E mc acc b f) = acc ++ (flatten (codom (Acceleration_of_T \o f)))  .
Proof.
  by [].
Qed.


(*
  We redefine the notion of consistency of KMTree because here
  we have accelerations embeded into the edges of our trees.
  Hence we have to use them and not just know that there exists accelerations
  that works.
*)
Section consistentE_tree.


Definition consistentE_head (mc0 : markingc) (T : KMTE) : bool := match T with
| Empty_E => true
| Br_E mc acc b f => Some mc == apply_transitionc  mc0  (abstraction_compo_seq (map val acc))
end.

Definition consistentE_child (mc : markingc) (f : {ffun transition -> KMTE}) (t : transition) : bool :=
match nextmc mc t with
| None => match (f t) with
            | Empty_E => true
            | _ => false
          end
| Some mc1 => match (f t) with
              | Empty_E => true
              | Br_E mc2 a b f' => foldm apply_transitionc  mc1  (map vsa a)  == Some mc2
              end
end.

Fixpoint consistentE_tree (T: KMTE) : bool := match T with
| Empty_E => true
| Br_E mc acc b f => [forall t, (consistentE_child mc f t)&&( consistentE_tree (f t))]
end.

Lemma consistentE_tree_Br mc acc b f:
  consistentE_tree (Br_E mc acc b f) -> (forall t, (consistentE_child mc f t) /\ consistentE_tree (f t)).
Proof.
  move/forallP => H t.
  by move/(_ t): H; rewrite -/consistent_tree; move/andP.
Qed.

Lemma Consistent_head_anonym m T A:
  consistentE_head (embedm m) T ->
  All_Acc A (Acceleration_of_T T) ->
  consistent_head A m T.
Proof.
  case: T A => [//=| mc acc b f ] A /= Hmc /List.Forall_app [] Hacc _.
  exists acc; split => //=.
Qed.


Lemma Consistent_anonym T A:
  consistentE_tree T ->
  All_Acc A (Acceleration_of_T T) ->
  consistent_tree A T.
Proof.
  elim: T A => [//=| mc acc b f Hind] A.
  move/consistentE_tree_Br => Hcon.
  rewrite Acceleration_of_Br => /List.Forall_app //= [] Hall_acc Hall_ind t.
  move: Hall_ind.
  rewrite ffunE codomE.
  move:(Dec_enum t) => [] s1 [] s2 [] _ [] _ ->.
  rewrite map_cat map_cons flatten_cat //=.
  move/List.Forall_app => [] _ /List.Forall_app [] Hall_ind _.
  split; last first.
    apply:Hind => //=.
    by move/(_ t): Hcon => [].
  rewrite /consistent_child.
  move/(_ t): Hcon => [] Hchild _.
  move: Hchild; rewrite /consistentE_child.
  case: nextmc => [mc'|//=]; last first.
  all:rewrite ffunE.
  all:case Hft:(f t) =>[| mc1 acc1 b1 f1] //.
  rewrite /Anonymisation => Hmc1.
  exists acc1; split.
    by move: Hft Hall_ind => -> //= /List.Forall_app [].
  apply/useful_compoc; move/eqP: Hmc1 => <-.
  apply: f_equal2 => //.
  by rewrite map_comp.
Qed.

Fixpoint compE_acc_seq (T : KMTE) (a1 a2 : address) (t: option transition) (hasStarted: bool):
seq (transition * seq acceleration) :=match T,a2,a1,t,hasStarted with
| Br_E mc s b f, t2::a2', t1::a1', t, h => compE_acc_seq (f t2) a1' a2' t h
| Br_E mc s b f, t2::a2', nil, _, false => compE_acc_seq (f t2) nil a2' (Some t2) true
| Br_E mc s b f, t2::a2', nil, Some t,  true  => (t, s) :: (compE_acc_seq (f t2) nil a2' (Some t2) true)
| Br_E mc s b f, nil, _, Some t, true  => [::(t,s)]
| _,_,_,_,_=> nil
end.


Definition compE_acc_aux (T : KMTE) (a1 a2 : address) :=
  flatten [seq (t_to_abstraction i.1)::(map val i.2) |i <- (compE_acc_seq T a1 a2 None false)].

Definition computingE_acceleration (T : KMTE) (a1 a2 : address): acceleration :=
  abs_to_acc (abstraction_compo_seq (compE_acc_aux T a1 a2 )).


Lemma consistentE_cpt_acc T ad s mc mc':
  consistentE_tree T ->
  Some mc = m_from_add T ad ->
  Some mc' = m_from_add T (ad ++ s) ->
  Some mc' = apply_transitionc mc (abstraction_compo_seq (compE_acc_aux T ad (ad++s) )) .
Proof.
  (* we want to know where to start computing*)
  elim : ad s T mc mc' => [| t ad Hind]; last first.
    move => s [//=|mc0 b0 acc0 f0] mc mc'.
    move/consistentE_tree_Br/(_ t) => [_ Hcon].
    rewrite cat_cons !m_from_add_Br /compE_acc_aux -/compE_acc_aux !ffunE.
    by apply Hind.
  case  => [| t s /=] [//=|mc0 b0 acc0 f0].
    move => mc mc' _ [] -> [] -> /=.
    cbn; symmetry; apply /eqP.
    exact : nil_transitionc_id.
  rewrite /compE_acc_aux -/compE_acc_aux.
  elim: s t mc0 acc0 b0 f0 => [t mc0 acc0 b0 f0 | t' s Hind t mc0 acc0 b0 f0 ] mc mc' /consistentE_tree_Br/(_ t) =>  /=.
  all: case Hf0: (f0 t) => [//=| mc1 acc1 b1 f1] [Hchild Hcon].
  all: move: Hchild; rewrite /consistentE_child !ffunE Hf0; case Hnext: (nextmc) => [mc2|]; last first.
  all: try by [].
  all: move/eqP : Hnext; move/next_eq_apply => /eqP Hmc2 /eqP Hmc1.
    move => [] -> [] ->.
    apply/eqP; apply: (@good_compoc _ _ mc0 mc2) .
      by rewrite Hmc2.
    apply useful_compoc.
    rewrite -Hmc1 -/cat cats0.
    apply f_equal2 => //.
    by rewrite map_comp.
  move => [] H; subst; rewrite m_from_add_Br => Hmc'.
  rewrite /compE_acc_seq -/compE_acc_seq map_cons.
  rewrite -cat1s flatten_cat.
  apply/eqP; apply abstraction_compo_seq_cat.
  apply:(@good_compoc  _ _ _ mc1).
    apply:(@good_compoc  _ _ _ mc2).
      by rewrite -Hmc2.
    rewrite -/cat cats0 //=.
    apply: useful_compoc.
    by rewrite -Hmc1 map_comp.
  apply/eqP.
  by apply: (Hind t' mc1 b1 acc1).
Qed.



Lemma cptE_acc_accelerate T ad ad' mc a:
  consistentE_tree T ->
  Possible_acceleration T ad (ad') ->
  a = computingE_acceleration T ad ad'->
  Some mc = m_from_add T ad ->
  exists mc', Some mc' = apply_transitionc mc a /\ mc < mc'.
Proof.
  move => Hcons.
  inversion_clear 1 as [mc1 mc2 Hmc1 Hmc2 Hlt Hprefix].
  move/prefixP: Hprefix => [] s H;subst.
  rewrite /computing_acceleration.
  move: (consistentE_cpt_acc Hcons Hmc1 Hmc2).
  remember (abstraction_compo_seq (compE_acc_aux T ad (ad++s) )) as b => Hb ->.
  rewrite -Hmc1; case => Heq.
  move: (abs_to_acc_inv Hb Hlt) => [mc' Hmc'].
  exists mc'; split.
    by subst.
  move : Hb Hmc'; rewrite Heq => H2 H'.
  move: (@abs_to_acc_increase pn mc1 mc2 mc' b).
  rewrite {}H2 {}H' => H2'.
  apply: (Order.POrderTheory.lt_le_trans Hlt).
  by apply H2'.
Qed.



End consistentE_tree.

(*
  We redefine the operations that we will use in order to define the
  relation corresponding to MinCov and we show their commutativity with the
  previous one with respect of A>
*)


Fixpoint removeE_add T a : KMTE := match T,a with
| _, nil => Empty_E
| Br_E mc acc b f, t'::a' =>
    Br_E mc acc b [ffun t => if t==t' then removeE_add (f t) a' else f t]
| _,_ => T
end.

Lemma removeE_add_Br mc acc b f t ad:
  removeE_add (Br_E mc acc b f) (t::ad) =
  Br_E mc acc b [ffun t0 => if t0 == t then removeE_add (f t0) ad else f t0]  .
Proof.
  by [].
Qed.

Lemma removeEC T ad:
  A> (removeE_add T ad) = remove_add (A>T) ad.
Proof.
  elim: T ad => [| mc acc b f Hind] [|t ad] //=.
  apply: f_equal.
  apply/ffunP => t0; rewrite !ffunE.
  by case: (t0==t).
Qed.

Fixpoint to_FrontE T ad : KMTE := match T,ad with
| Empty_E,_ => Empty_E
| Br_E mc acc b f, nil   => Br_E mc acc true [ffun t => Empty_E]
| Br_E mc acc b f, t0::l => Br_E mc acc b    [ffun t => if t==t0 then (to_FrontE (f t) l) else (f t)]
end.


Lemma to_FrontE_Br mc acc b f t ad:
  to_FrontE (Br_E mc acc b f) (t::ad) =
  Br_E mc acc b [ffun t0 => if t0 == t then to_FrontE (f t0) ad else f t0]  .
Proof.
  by [].
Qed.

Lemma to_frontEC T ad:
  A> (to_FrontE T ad) = to_Front (A>T) ad.
Proof.
  elim: T ad => [| mc acc b f Hind] [|t ad] //=.
  all:apply: f_equal.
  all:apply/ffunP => t0; rewrite !ffunE //=.
  by case: (t0==t).
Qed.


Fixpoint Front_extensionE T ad: KMTE := match T,ad with
| Empty_E,_ => Empty_E
| Br_E mc acc b f,nil => Br_E mc acc false [ffun t => match nextmc mc t with
                                   | None => Empty_E
                                   | Some mc' => Br_E mc' nil true [ffun => Empty_E]
                                   end]
| Br_E mc acc b f,t0::l => Br_E mc acc b [ffun t => if t==t0 then (Front_extensionE (f t) l) else (f t)]
end.

Lemma Front_extensionE_Br mc acc b f t ad:
  Front_extensionE (Br_E mc acc b f) (t::ad) =
  Br_E mc acc b [ffun t0 => if t0 == t then Front_extensionE (f t0) ad else f t0]  .
Proof.
  by [].
Qed.


Lemma Front_extensionEC T ad:
  A> (Front_extensionE T ad) = Front_extension (A>T) ad.
Proof.
  elim: T ad => [| mc acc b f Hind] [|t ad] //=.
  all:apply: f_equal.
  all:apply/ffunP => t0; rewrite !ffunE //=.
    case: (nextmc) => //= mc0.
    apply: f_equal.
    apply/ffunP => t1; rewrite !ffunE //=.
  by case: (t0==t).
Qed.

Section removeE_strict_covered.
(*
  We define a function that removes all subtrees of a KMTE whose root markingcs are strictly less
  than a specific one
*)

Fixpoint removeE_strict_covered T mc : KMTE := match T with
| Empty_E => Empty_E
| Br_E mc' acc b f => if mc'< mc then Empty_E
    else Br_E mc' acc b [ffun t => removeE_strict_covered (f t) mc]
end.

Lemma removeE_strict_covered_is_front (T:KMTE) ad mc:
  Is_Front T ad ->
  Some mc = m_from_add T ad ->
  No_Possible_acc T ad ->
  Is_Front (removeE_strict_covered T mc) ad.
Proof.
  elim : ad T mc => [//=|t ad Hind] [//=|mc0 acc0 b0 f0] mc.
    move => //= /eqP -> [] ->.
    by rewrite /Order.lt //= eqxx.
  rewrite //= !ffunE => Hfront Hmc Hnpos.
  case Hlt: (mc0 < mc) => //=; last first.
    rewrite !ffunE.
    apply:Hind => //= ad' mc1 mc2 Hmc1.
    rewrite -Hmc => [] [] -> Hprefix.
    by apply: (@Hnpos (t::ad')) => //=; rewrite ?ffunE ?Hprefix ?eqxx.
  move: (Hnpos nil mc0 mc Logic.eq_refl) => [] //=.
  by rewrite ffunE.
Qed.

Lemma Is_front_removeE_strict_covered T ad mc' mc:
  ~~Is_Front (removeE_strict_covered T mc) ad ->
  Some mc' = m_from_add (removeE_strict_covered T mc) ad ->
  ~~Is_Front T ad.
Proof.
  elim : ad T mc' mc => [//=|t ad Hind] [//=|mc0 acc0 b0 f0] mc' mc //=.
  all: case: (mc0 < mc) => //=.
  rewrite !ffunE.
  exact:Hind.
Qed.


Lemma removeE_strict_covered_fl (T:KMTE) mc:
  Front_leaves T ->
  Front_leaves (removeE_strict_covered T mc).
Proof.
  elim: T mc => [//=| mc0 acc0 [] f0 Hind] mc //=.
  all: case: (mc0 < mc) => //.
    move/Front_leaves_invariant => Hfl //=.
    apply/forallP => t.
    move/(_ mc t): Hfl;rewrite !ffunE.
    by case: (f0 t).
  move/forallP => Hfl //=.
  apply/forallP => t.
  move/(_ t): Hfl; rewrite !ffunE.
  exact: Hind.
Qed.

Lemma removeE_strict_covered_mfad (T:KMTE) ad mc:
  Some mc = m_from_add T ad ->
  No_Possible_acc T ad ->
  Some mc = m_from_add (removeE_strict_covered T mc) ad.
Proof.
  elim : ad T mc => [//=|t ad Hind] [//=|mc0 acc0 b0 f0] mc //=.
    move => [] -> .
    by rewrite /Order.lt //= eqxx.
  rewrite !ffunE => Hmc Hnpos.
  case Hlt: (mc0 < mc) => //=; last first.
    rewrite !ffunE.
    apply:Hind => //= ad' mc1 mc2 Hmc1.
    rewrite -Hmc => [] [] -> Hprefix.
    by apply: (@Hnpos (t::ad')) => //=; rewrite ?ffunE ?Hprefix ?eqxx.
  move: (Hnpos nil mc0 mc Logic.eq_refl) => [] //=.
  by rewrite ffunE.
Qed.

Lemma mfad_removeE_strict_covered T ad mc mc':
  Some mc' = m_from_add (removeE_strict_covered T mc) ad ->
  Some mc' = m_from_add T ad.
Proof.
  elim : ad T mc mc' => [//=|t ad Hind] [//=|mc0 acc0 b0 f0] mc mc' //=.
  all: case : (mc0 < mc) => //=.
  rewrite !ffunE.
  exact:Hind.
Qed.


Lemma removeE_strict_covered_M_n_front T mc:
  subseq (Markings_not_front_of_T (removeE_strict_covered T mc))
  (Markings_not_front_of_T T).
Proof.
  elim: T mc => [//=| mc0 acc0 [] f0 Hind] mc //.
  all: rewrite /removeE_strict_covered -/removeE_strict_covered.
  all:case: (mc0 < mc) => //.
  rewrite !Markings_not_Front_Br subseqE -/Anonymisation.
  apply: subseq_codom.
    by rewrite !size_codom.
  move => i.
  case Hsize: (size (enum transition) <= i).
    rewrite (nth_default) => //=.
    by rewrite sub0seq.
  by rewrite size_map.
  have Hlt:( i < size (enum transition))%N.
    rewrite ltnNge.
    by apply/negPf.
  move : Hsize Hlt; rewrite -cardE  => _ Hsize.
  rewrite !(nth_codom nil _ (Ordinal Hsize)) //= !ffunE.
  exact:Hind.
Qed.

Lemma removeE_strict_coveredF T ad mc mc':
  Some mc' = m_from_add (removeE_strict_covered T mc) ad ->
  ~(mc' < mc).
Proof.
  elim: ad T mc mc' => [|t ad Hind] [|mc0 acc0 b0 f0] mc mc' //=.
  all: case Hlt: (mc0 < mc) => //=.
    by move => [] ->; apply/negP/negbT.
  rewrite !ffunE.
  exact: Hind.
Qed.



End removeE_strict_covered.



Section Saturate_KMTree.

(*
  We define a function that takes a KMTE and saturates it according to
  a sequence of accelerations and a node in the KMTE
*)

Fixpoint saturate_KMTree  (A : seq acceleration) (T: KMTE) (ad :address): KMTE := match T,ad with
| Empty_E,_ => Empty_E
| Br_E mc acc b f, t'::ad' => Br_E mc acc b [ffun t => if t==t' then saturate_KMTree A (f t) ad' else f t]
| Br_E mc acc b f, nil =>   Br_E (saturate_markingc mc A acc).1 (saturate_markingc mc A acc).2  b f
end.

Lemma saturate_KMTree_Br A mc acc b f t ad:
  saturate_KMTree A (Br_E mc acc b f) (t::ad) =
  Br_E mc acc b [ffun t0 => if t0 == t then saturate_KMTree A (f t0) ad else f t0] .
Proof.
  by [].
Qed.

Lemma saturate_KMTree_mfa A (T:KMTE) ad mc:
  Some mc = m_from_add T ad ->
  exists mc', Some mc' = m_from_add (saturate_KMTree A T ad) ad.
Proof.
  elim: ad T A mc => [|t ad Hind] [|mc0 acc0 b0 f0] A mc //=.
    move => _.
    by eexists.
  rewrite !ffunE eqxx => /Hind.
  move/(_ A) => [] mc' //=.
Qed.


Lemma saturate_KMTree_Is_Front A (T:KMTE) ad ad':
  Is_Front T ad
  = Is_Front (saturate_KMTree A T ad') ad.
Proof.
  elim: ad T A ad' => [|t ad Hind] [|mc acc b f] A ad' //=.
    by case: ad'.
  case: ad' => //= t' ad'.
  rewrite !ffunE.
  by case: (t==t').
Qed.

Lemma saturate_KMTree_Fl A (T:KMTE) ad:
  Front_leaves T ->
  Front_leaves (saturate_KMTree A T ad).
Proof.
  elim: ad T A => [|t ad Hind] [|mc acc [] f] A //=.
    move/Front_leaves_invariant => Hfl.
    apply/forallP => t0.
    move: (Hfl mc t0).
    rewrite !ffunE.
    case: (f t0) => //=.
    by case (t0==t).
  move/forallP => Hfl.
  apply/forallP => t0.
  move: (Hfl t0).
  rewrite !ffunE.
  case: (t0 == t) => //=.
  exact: Hind.
Qed.

Lemma saturate_KMTree_nfa A (T:KMTE) ad:
  Is_Front T ad ->
  Not_Front_Antichain T ->
  Not_Front_Antichain (saturate_KMTree A T ad).
Proof.
  move => HFront.
  suff: (Markings_not_front_of_T T) = (Markings_not_front_of_T (saturate_KMTree A T ad)).
    by rewrite /Not_Front_Antichain => ->.
  elim: ad T A HFront => [|t ad Hind] [|mc acc [] f] A HFront //=.
  apply f_equal, f_equal.
  apply:eq_codom => t0 //=; rewrite !ffunE.
  case Heq: (t0 == t) => //=.
  apply: Hind.
  by move/eqP: Heq HFront; rewrite Is_front_Br -/Anonymisation ffunE => ->.
Qed.

Lemma saturated_node_saturate A T ad:
  saturated_node A (saturate_KMTree A T ad) ad.
Proof.
  elim: ad T A => [|t ad Hind] [|mc acc b f] A //=.
    case H:(saturate_markingc mc A acc) => [mc' s] //=.
    exact: (@saturate_marking_works _ mc mc' _ acc s).
  by rewrite !ffunE eqxx.
Qed.

Lemma saturate_KMTree_consistentE A (T:KMTE) ad:
  Is_Front T ad ->
  Front_leaves T ->
  consistentE_tree T ->
  consistentE_tree (saturate_KMTree A T ad).
Proof.
  elim: ad T A => [| t ad Hind] [| mc acc b f] A // .
    move/eqP => -> /Front_leaves_invariant.
    rewrite -/Anonymisation => Hfl //= /forallP Hcon.
    apply/forallP => t.
    apply/andP; split.
      rewrite /consistentE_child.
      case: nextmc.
    1,2,3: by move: (Hfl t); rewrite ffunE; case: (f t) .
  rewrite Is_front_Br saturate_KMTree_Br -/Anonymisation ffunE => Hfront.
  move/Front_leaves_invariant2; rewrite -/Anonymisation => Hfl //= /forallP Hcon.
  apply/forallP => t0; rewrite !ffunE.
  apply/andP; split; last first.
    case Heq: (t0 == t).
      move/eqP in Heq; rewrite Heq.
      apply: Hind => //=.
        by move:(Hfl t); rewrite ffunE.
      rewrite -Heq.
    1,2: by move/andP : (Hcon t0) => [].
  move/andP: (Hcon t0) => [] Hchild _.
  move: Hchild; rewrite /consistentE_child.
  case: nextmc => // [mc'|].
  all: rewrite !ffunE; case (t0==t) => //; case : (f t0) => [|mc1 acc1 b1 f1] //.
  move => Hmc1.
  case: (ad) => //.
  rewrite /saturate_KMTree.
  apply/eqP.
  case Hsat: (saturate_markingc) => [mc0 s].
  apply: saturate_markingc_computation.
    by rewrite //= -Hsat.
  by move/eqP: Hmc1.
Qed.

End Saturate_KMTree.

Section Operations_keeps_consistent.

Lemma removeE_keeps_consistentE_head (T : KMTE) (ad : address) mc :
  consistentE_head mc T -> consistentE_head mc (removeE_add T ad).
Proof.
  by elim : ad T mc => [| ad l Hind]//=; elim.
Qed.

Lemma removeE_keeps_consistentE (T : KMTE) (ad : address) :
  consistentE_tree T -> consistentE_tree (removeE_add T ad).
Proof.
  elim : ad T => [| ad l Hind]//=.
    by elim .
  elim  => [ //=| mc acc b f _ /consistentE_tree_Br Hcon].
  rewrite  removeE_add_Br /consistentE_tree -/consistentE_tree.
  apply/forallP => t.
  rewrite !ffunE.
  apply/andP.
  split; last first.
  all :move/( _ (f t)) in Hind.
  all: move/(_ t):Hcon => [H1 H2].
    case (t==ad) => //=.
    by apply Hind.
  move : H1 ;rewrite /consistent_child.
  rewrite /consistentE_child !ffunE => //=.
  case : (nextmc mc t) => //=; last first.
    by case: (t==ad); case: (f t) ; case l.
  move => m.
  case (t==ad) => //=.
  by elim l ; elim (f t).
Qed.

Lemma saturate_keeps_consistentE_head (T : KMTE) (ad : address) A mc :
  consistentE_head mc T -> consistentE_head mc (saturate_KMTree A T ad).
Proof.
  elim : ad T mc => [| ad l Hind]//=; elim => //= mc0 acc0 b0 f0 _ mc /useful_compoc2 Hmc0.
  remember (saturate_markingc mc0 A acc0).1 as mc3.
  remember (saturate_markingc mc0 A acc0).2 as s.
  apply/useful_compoc.
  have H: (mc3,s) =(saturate_markingc mc0 A acc0).
    rewrite Heqmc3 Heqs //=.
    by destruct saturate_markingc.
  move: (@saturate_markingc_computation _ mc mc0 mc3 A acc0 s H) => <- //=.
    by rewrite !map_comp.
  by rewrite -Hmc0 !map_comp.
Qed.


Lemma saturate_keeps_consistentE (T : KMTE) (ad : address) (A : seq acceleration):
  consistentE_tree T  ->
  Front_leaves T ->
  Is_Front T ad ->
  consistentE_tree (saturate_KMTree A T ad).
Proof.
  elim : ad T => [| ad l Hind]//=.
    elim => //= mc acc b f Hind Hcon Hfl.
    move/eqP => Hb; subst.
    apply/forallP => t.
    move/forallP/(_ t): Hfl.
    rewrite !ffunE /consistentE_child //=.
    by case: (f t); case: nextmc.
  case  => [ //=| mc acc [] f Hcons Hfl Hfront].
  all: rewrite saturate_KMTree_Br /consistentE_tree -/consistentE_tree.
  all: apply/forallP => t.
    move/Front_leaves_invariant/(_ t): Hfl.
    rewrite !ffunE /consistentE_child /consistentE_tree !ffunE.
    by case: (t == ad); case (f t); case nextmc.
  apply/andP.
  move/consistentE_tree_Br/(_ t): Hcons => [] Hcons_child Hcons_tree.
  case Ht:(t==ad);rewrite !ffunE /consistentE_child ffunE Ht -/consistentE_child //=; last first.
  split; last first.
    apply:Hind => //=.
      by move: Hfl => //= /forallP/(_ t); rewrite ffunE.
    by move:Ht Hfront => //= /eqP ->; rewrite ffunE.
  move: Hcons_child.
  rewrite /consistentE_child.
  case: nextmc; case: (f t) => //= mc0 acc0 b0 f0 m /eqP Hmc0 {Hind Hfront}.
  case : l => //=.
  remember (saturate_markingc mc0 A acc0).1 as mc3.
  remember (saturate_markingc mc0 A acc0).2 as s.
  have H: (mc3,s) =(saturate_markingc mc0 A acc0).
    rewrite Heqmc3 Heqs //=.
    by destruct saturate_markingc.
  move: (@saturate_markingc_computation _ m mc0 mc3 A acc0 s H) => <- //=.
  by rewrite -Hmc0 !map_comp.
Qed.

Lemma To_FrontE_keeps_consistentE_head (T : KMTE) (ad : address) mc :
  consistentE_head mc T -> consistentE_head mc (to_FrontE T ad).
Proof.
  elim : ad T mc => [| ad l Hind]//=.
    by case.
  by elim.
Qed.


Lemma To_FrontE_keeps_consistentE (T : KMTE) (ad : address) :
  consistentE_tree T -> consistentE_tree (to_FrontE T ad).
Proof.
  elim : ad T => [| ad l Hind]//=.
    case => //= m acc b f _  .
    apply/forallP => t.
    rewrite /consistentE_child ffunE //=.
    by case : (nextmc).
  elim => [//=| mc acc b f _ Hcon].
  rewrite /to_FrontE -/to_FrontE /consistentE_tree -/consistentE_tree.
  apply/forallP => t.
  rewrite !ffunE.
  apply/andP.
  case Ht:(t== ad) => // ; split.
  - 2: apply Hind.
  all : move/consistentE_tree_Br/(_ t) :Hcon => [Hchild Htree]  //.
  all: move: Hchild; rewrite /consistentE_child !ffunE Ht //=.
  - case : nextmc => // [mc0|].
    all: by case: ( f t); case l.
Qed.

Lemma Front_extensionE_keeps_consistentE_head (T : KMTE) (ad : address) mc :
  consistentE_head mc T -> consistentE_head mc (Front_extensionE T ad).
Proof.
  elim : ad T mc => [| ad l Hind]//=.
    by case.
  by elim.
Qed.


Lemma Front_extensionE_keeps_consistentE (T : KMTE) ( ad : address) :
  consistentE_tree T -> consistentE_tree ( Front_extensionE T ad).
Proof.
  elim : ad T => [| t ad Hind] [//=| mc acc b f] .
    move => _.
    rewrite /Front_extensionE /consistentE_tree.
    apply/forallP => t.
    apply/andP.
    rewrite !ffunE; split.
      rewrite /consistentE_child ffunE //=.
      by case : nextmc.
    case : nextmc => //= mc'.
    apply/forallP => t'.
    rewrite /consistentE_child !ffunE.
    by case : nextmc.
  move/consistentE_tree_Br =>  Hcon.
  rewrite /Front_extensionE -/Front_extensionE /consistentE_tree -/consistentE_tree.
  apply/forallP => t0.
  move/(_ t0): Hcon => [] Hchild Hcon.
  rewrite /consistentE_child ffunE.
  case Heq: (t0==t).
  all: rewrite ?ffunE ?Heq //=.
  all: apply/andP; split.
  2: exact:Hind.
  all: move: Hchild; rewrite /consistentE_child //=.
  case : nextmc => // [mc1|]; last first.
  all:by case: (f t0); case ad.
Qed.


Lemma removeE_strict_covered_keeps_consistentE_head (T : KMTE)  mc mc' :
  consistentE_head mc T -> consistentE_head mc (removeE_strict_covered  T mc').
Proof.
  case :T mc' mc => //= mc0 acc0 b0 f0 mc mc'.
  by case (mc0 < mc).
Qed.


Lemma removeE_strict_covered_keeps_consistentE (T : KMTE) mc :
  consistentE_tree T -> consistentE_tree ( removeE_strict_covered T mc).
Proof.
  elim : T mc => [//=| mc0 acc0 b0 f0 Hind] mc.
  rewrite /removeE_strict_covered -/removeE_strict_covered.
  case: (mc0 < mc) => //= /forallP Hcons.
  apply/forallP => t.
  apply/andP.
  move/(_ t)/andP: Hcons => [].
  rewrite /consistentE_child !ffunE => Hcons_child Hcons.
  split; last first.
    exact: Hind.
  move: Hcons_child.
  case: nextmc; case (f0 t)=> //= m s b f.
  by case : (m < mc).
Qed.

End Operations_keeps_consistent.

End Rel.