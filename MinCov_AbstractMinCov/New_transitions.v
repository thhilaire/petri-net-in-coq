(**************************************************************)
(*   Copyright (c) 2024 Thibault Hilaire                      *)
(*                      David Ilcinkas                        *)
(*                      Jerome Leroux                         *)                           
(*   Authors : Thibault Hilaire [*]                           *)
(*             David Ilcinkas [+,*]                           *)
(*             Jerome Leroux  [+,*]                           *)
(*                                                            *)
(*                             [*] Affiliation Univ. Bordeaux *)
(*                             [+] Affiliation CNRS           *)
(**************************************************************)
(*      This file is distributed under the terms of the       *)
(*                      MIT License                           *)
(**************************************************************)


From mathcomp Require Import all_ssreflect zify.
Import Order.TTheory.
From Petri_Nets_in_Coq.Karp_Miller Require Import ssreflectext orderext monad petrinet pnkmaccel.
From Petri_Nets_in_Coq.Tools Require Import Utils.
From Coq Require Import Arith Lia Wellfounded  Recdef FunInd.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope order_scope.
Local Open Scope petri_net_scope.

Section Extension_transition.

Variable pn : petri_net.

(* From Program/Utils.v*)
Notation dec := Sumbool.sumbool_of_bool.

Lemma dec_op (X : Type) (x : option X) : {x' | x = Some x'} + {x = None}.
Proof. by case: x => [x | ]; [left; exists x | right]. Defined.



Notation place := (place pn).
Notation marking := (marking pn).
Notation markingc := (markingc pn).
Notation transition := (transition pn).

Notation "tc .pre" := (tc.1) (at level 2, format "tc .pre").
Notation "tc .post" := (tc.2) (at level 2, format "tc .post").



Lemma ineq_marking (m1 m2 : marking) (p : place) :
  ~~(m1 p <= m2 p) -> (m1 <= m2)=false.
Proof.
  move => H.
  case Htl: (m1 <= m2).
  2: auto.
  move/forallP/(_ p) in Htl; by rewrite Htl in H.
Qed.



Lemma ineq_markingc (mc1 mc2 : markingc) (p : place) :
  ~~(mc1 p <= mc2 p) -> (mc1 <= mc2)=false.
Proof.
  move => H.
  case Htl: (mc1 <= mc2).
  2: auto.
  move/forallP/(_ p) in Htl; by rewrite Htl in H.
Qed.




Section transitionp.
(*
  We will want to be able to compose transitions here but
  because we will want to define a neutral element for the composition,
  that will be the empty transition that requires nothing and does nothing,
  and this neutral element may not be in the transition set of the network,
  we define a new type more general than the type of transition
*)


Definition transitionp := (marking * marking)%type.

Definition apply_transitionp (m : marking ) (tp : transitionp): option marking :=
 if tp.pre <= m then Some (m :-: tp.pre :+: tp.post) else None.

Variant apply_transitionp_spec (m : marking) (tp : transitionp) : option marking -> Prop :=
| NextmSome of tp.pre <= m
  : apply_transitionp_spec m tp (Some (m :-: tp.pre :+: tp.post))
| NextmNone p of ~~ (tp.pre p <= m p)
  : apply_transitionp_spec m tp None.

Lemma apply_transitionpP m tp : apply_transitionp_spec m tp (apply_transitionp m tp).
Proof.
  rewrite /apply_transitionp; case: ifPn; first by move=> H; constructor.
  by rewrite negb_forall; move/existsP=> [p H]; constructor 2 with p.
Qed.

Lemma apply_equal_nextm (m : marking ) (t: transition) :
  apply_transitionp m (pre t, post t) == nextm m t.
Proof. done. Qed.

(* Going into the composition of transitions *)

Definition pre_compo (a b :transitionp) : marking :=
b.pre :-: a.post :+: a.pre.

Definition post_compo ( a b : transitionp) : marking :=
a.post :-: b.pre :+: b.post.

Definition transitionp_compo a b : transitionp :=
(pre_compo a b, post_compo a b).

Lemma good_compo_transitionp (m : marking) (t t' : transitionp) :
  obind (apply_transitionp^~ t') (apply_transitionp m t) =
  apply_transitionp m (transitionp_compo t t').
Proof.
  (repeat case: apply_transitionpP => //=).
  1: move => Hprec Hpre' Hpre.
  1: f_equal; apply/ffunP => p.
  1: move: Hpre Hpre' Hprec => /forallP/(_ p)-Hpre /forallP/(_ p)-Hpre' /forallP/(_ p)-Hprec.
  2: move => p /negPf-Hneg /forallP/(_ p)-Hpre' /forallP/(_ p)-Hpre.
  3: move => Hprec p.
  3: move: Hprec => /forallP/(_ p)-Hprec /negPf-Hneg /forallP/(_ p)-Hpre.
  4: move => Hprec p.
  4: move: Hprec => /forallP/(_ p)-Hprec /negPf-Hneg.
  2,3,4: apply/eqP; cbn; rewrite -{}Hneg.
  all: try rewrite !ffunE; try rewrite !ffunE in Hprec; try rewrite !ffunE in Hpre'.
  1,2,3: move: Hpre.
  1,2: move: Hpre'.
  1,3,4: move: Hprec.
  all: move: (t.pre p) (t.post p) (t'.pre p) (t'.post p) (m p) => [] [] [] [] [] //=.
  all: try lia.
Qed.

Lemma good_compo (a b : transitionp ) (m1 m2 m3 : marking) :
  Some m2 == apply_transitionp m1 a ->
  Some m3 == apply_transitionp m2 b ->
  Some m3 == apply_transitionp m1 (transitionp_compo a b).
Proof.
  rewrite -good_compo_transitionp => /eqP<- //=.
Qed.

(*
  Extension of the composition to sequences of transitionp
  and lemmas about its validity
*)

Definition nil_transitionp : transitionp := ([ffun p: place => 0], [ffun p: place => 0]).

Lemma nil_transitionp_id (m : marking) :
  Some m == apply_transitionp m nil_transitionp.
Proof.
  case: apply_transitionpP => /=; last first.
    cbn; move => p /negPf<-.
    rewrite ffunE; lia.
  move => _ ; apply /eqP; f_equal.
  apply/ffunP => p.
  by rewrite !ffunE addn0 subn0.
Qed.



Definition seq_to_one ( s : seq transition ):=
  foldr transitionp_compo nil_transitionp (map (fun t:transition => (pre t, post t)) s).

Lemma usefull_compo (s : seq transition ) :
  forall m1 m2, m1 ={ s }> m2 -> Some m2 == apply_transitionp m1 (seq_to_one s).
Proof.
  elim: s => [m1 m2 /= <- | a s IHs].
    by have /eqP <- := nil_transitionp_id m1.
  move => m1 m3 /foldm_cons_some-[m2 [/eqP-H12 H23]].
  move : IHs H12 => /(_ m2 m3 H23 )-IHs.
  have /(_ (pre a, post a) m1)-H13 := good_compo _ IHs.
  rewrite /seq_to_one.
  have /eqP <- := apply_equal_nextm m1 a => /eqP-H12.
  apply H13; apply /eqP; done.
Qed.



Lemma usefull_compo2 (s : seq transition ) :
  forall m1 m2 : marking, Some m2 == apply_transitionp m1 (seq_to_one s) -> m1 ={ s }> m2 .
Proof.
  elim :s=> [m1 m2 /= H| a s IHs].
    have /eqP :=(nil_transitionp_id m1) => H'.
    rewrite -{}H' in H; symmetry; by apply/eqP.
  move => m1 m3 H.
  apply foldm_cons_some.
  rewrite /seq_to_one /= -/(seq_to_one s) in H.
  have H':=(good_compo_transitionp  m1 (pre a, post a) (seq_to_one s)  ).
  rewrite <- H'  in H; clear H'.
  have /eqP H':=(apply_equal_nextm m1 a).
  rewrite <- H'; clear H'.
  case :apply_transitionpP; last first.
    move => p Hneg.
    rewrite /apply_transitionp in H.
    case Htl: (pre a <= m1).
      move /forallP/(_ p) in Htl.
      by rewrite Htl in Hneg.
    by rewrite Htl /= in H.
  move => Ha_pre.
  exists (m1 :-: pre a :+: post a).
  split; auto; rewrite /apply_transitionp Ha_pre  in H.
  by apply IHs.
Qed.




Lemma fire_seq   (s : seq transition) :
  forall m1 : marking, (seq_to_one s).pre <= m1 -> exists m2,   m1 ={ s }> m2.
Proof.
  move => m1 Hpre.
  remember (apply_transitionp m1 (seq_to_one s)) as om.
  case : om Heqom => [m2|]; last first.
    case : apply_transitionpP => //=.
    move => p; move: Hpre => /forallP-/(_ p)-> //=.
  move => /eqP-H; exists m2.
  by apply usefull_compo2.
Qed.



Lemma seq_to_cat (s s' : seq transition):
  seq_to_one (s++s') == transitionp_compo (seq_to_one s) (seq_to_one s').
Proof.
  elim s => [ | a l IH].
    rewrite  /(seq_to_one [::])  /transitionp_compo /pre_compo /post_compo /=.
    apply/andP;split.
    1,2 : apply/eqP/ffunP => p; rewrite !ffunE //=.
    by rewrite subn0 addn0.
  move/eqP in IH.
  rewrite /seq_to_one /= -/(seq_to_one(l++s') ) IH -/(seq_to_one l) -/(seq_to_one s') /transitionp_compo /pre_compo /post_compo /=.
  apply/andP;split.
  1,2 : apply/eqP/ffunP => p; rewrite !ffunE //=; lia.
Qed.



Lemma iter_good (n: nat) (p :place) (s :seq transition) :
  (seq_to_one s).pre p <= (seq_to_one s).post p  ->
  ((seq_to_one (iter _ n (cat s) s)).pre p + (n.+1)*((seq_to_one s).post p) =
  (seq_to_one (iter _ n (cat s) s)).post p + (n.+1)*((seq_to_one s).pre p) )
  /\(seq_to_one (iter _ n (cat s) s)).pre p = (seq_to_one s).pre p  .
Proof.
  elim n => [H | m IHm H].
    split; auto.
  rewrite !mul1n addnC //=.
  split.
  all: assert (H1:  (seq_to_one (iter _ m (cat s) s)).pre p + (m.+1) * (seq_to_one s).post p =
  (seq_to_one (iter _ m (cat s) s)).post p + (m.+1) * (seq_to_one s).pre p /\
  (seq_to_one (iter _  m (cat s) s)).pre p = (seq_to_one s).pre p).
  1,3 : by apply IHm.
  all: clear IHm; destruct H1 as [H1 H2].
  2: rewrite -H2.
  all: rewrite /iter -/(iter _ m (cat s) s).
  all: have /eqP H':= seq_to_cat  s (iter _ m (cat s) s).
  all: rewrite {}H' /transitionp_compo /pre_compo /post_compo !ffunE /=; lia.
Qed.

End transitionp.




(*
  We define a new type of transitions that can have omega in it.
  The first element is the pre of this transition and the second is its post
*)

Section transitionc.



Definition transitionc := (markingc * markingc)%type .



(* Natural operation on nat extended to natc *)

Definition maxc ( m n : natc) : natc :=
match m,n with
| None,_ => None
| _, None => None
| Some m1, Some n1 => Some (maxn m1 n1)
end.



Definition addc (n m : natc) : natc := obind (addncn n) m.



Definition subc (n m : natc):=
match m with
| Some x => subncn n x
| None => Some 0
end.




Notation "nc1 '+ nc2" := (addc nc1 nc2) (at level 50, left associativity)
                      : petri_net_scope.
Notation "nc1 '- nc2" := (subc nc1 nc2) (at level 50, left associativity)
                        : petri_net_scope.
Notation "mc1 :'+: mc2" := (fflift2 addc mc1 mc2) (at level 50, left associativity)
                        : petri_net_scope.
Notation "mc1 :'-: mc2" := (fflift2 subc mc1 mc2) (at level 50, left associativity)
                        : petri_net_scope.




Lemma None_win_addl (n m : natc) :
  n = None -> n '+ m = None.
Proof.
  intro Hn.
  destruct (dec_op m).
  destruct s. all: rewrite Hn e; reflexivity.
Qed.

Lemma None_win_addr (n m : natc) :
  m = None -> n '+ m = None.
Proof.
  intro Hm.
  destruct (dec_op n).
  destruct s.
  all: rewrite Hm e; reflexivity.
Qed.



Lemma addcA (n1 n2 n3 : natc) :
  n1 '+ (n2 '+ n3) = n1 '+ n2 '+ n3.
Proof.
  destruct (dec_op n1), (dec_op n2), (dec_op n3) .
  1,2,3,4 : destruct s as [ x1 e1]; rewrite e1.
  1,2 : destruct s0 as [ x2 e2] ; rewrite e2.
  - destruct s1 as [x3 e3]; rewrite  e3; simpl; rewrite addnA; reflexivity.
  - rewrite e; cbn; reflexivity.
  - destruct s0 as [x3 e3]; rewrite  e e3; cbn; reflexivity.
  - rewrite  e e0; cbn; reflexivity.
  all : rewrite e; repeat rewrite None_win_addl; reflexivity.
Qed.

Lemma subcKC (m n : natc) : m <= n -> (n '- m) '+ m = n.
Proof.
  destruct (dec_op m) as [[m' e] |e], (dec_op n) as [[n' e'] |e'].
  all: rewrite e e' //=.
  move => H.
  apply f_equal.
  rewrite /Order.le //= in H.
  lia.
Qed.

(*
  The converse implication does not hold in general,
  for example if m3 and m1 are markingc full of None
  and m2 is the markingc with only 0
*)
Lemma leq_addc2r (m1 m2 m3 : markingc) :
  (m1 <= m2) -> (m1 :'+: m3 <= m2 :'+: m3).
Proof.
  move/forallP => Hleq.
  apply/forallP => p; rewrite !ffunE.
  move/(_ p): Hleq.
  move: (m1 p) (m2 p) (m3 p) => [n1|] [n2|] [n3|] => //=.
  rewrite /Order.le //=.
  lia.
Qed.

Lemma leq_addc2l (m1 m2 m3 : markingc) :
  (m1 <= m2) -> (m3 :'+: m1 <= m3 :'+: m2).
Proof.
  move/forallP => Hleq.
  apply/forallP => p; rewrite !ffunE.
  move/(_ p): Hleq.
  move: (m1 p) (m2 p) (m3 p) => [n1|] [n2|] [n3|] => //=.
  rewrite /Order.le //=.
  lia.
Qed.


Lemma leq_subc2r (m1 m2 m3 : markingc) :
  (m1 <= m2) -> (m1 :'-: m3 <= m2 :'-: m3).
Proof.
  move/forallP => Hleq.
  apply/forallP => p; rewrite !ffunE.
  move/(_ p): Hleq.
  move: (m1 p) (m2 p) (m3 p) => [n1|] [n2|] [n3|] => //=.
  rewrite /Order.le //=.
  lia.
Qed.


Definition apply_transitionc (mc : markingc ) ( a : transitionc): option markingc :=
 if a.pre <= mc then Some (mc :'-: a.pre :'+: a.post) else None.


 Variant apply_transitionc_spec (mc : markingc) (a : transitionc) : option markingc -> Prop :=
| NextmcSome of a.pre <= mc
  : apply_transitionc_spec mc a (Some (mc :'-: a.pre :'+: a.post))
| NextmcNone p of ~~ (a.pre p <= mc p)
  : apply_transitionc_spec mc a None.

Lemma apply_transitioncP mc a : apply_transitionc_spec mc a (apply_transitionc mc a).
Proof.
  rewrite /apply_transitionc; case: ifPn; first by move=> H; constructor.
  by rewrite negb_forall; move/existsP=> [p H]; constructor 2 with p.
Qed.


(* One-step transition on markingcs with transitionc.  *)
Notation "mc1 =1{' t '}> mc2" :=
  (apply_transitionc mc1 t = Some mc2)
    (at level 70, t at next level)
  : petri_net_scope.
(* Multi-step transition on markingcs with transitionc.  *)
Notation "mc1 ={' s '}> mc2" :=
  (foldm apply_transitionc mc1 s = Some mc2)
    (at level 70, s at next level)
  : petri_net_scope.

Lemma apply_transitioncM (mc1 mc2 mc1' : markingc) ( t :transitionc):
  mc1 =1{' t '}> mc2 ->
  mc1 <= mc1' ->
  exists mc2',
  mc1' =1{' t '}> mc2' /\ mc2 <= mc2'.
Proof.
  case : apply_transitioncP => //= Hpre []<- Hmc1.
  exists (mc1' :'-: t.pre :'+: t.post); split.
    by rewrite /apply_transitionc (Order.POrderTheory.le_trans Hpre Hmc1).
  by apply leq_addc2r, leq_subc2r.
Qed.

Lemma apply_seq_transitioncM (mc1 mc2 mc1' : markingc) ( s : seq transitionc):
  mc1 ={' s '}> mc2 ->
  mc1 <= mc1' ->
  exists mc2',
  mc1' ={' s '}> mc2' /\ mc2 <= mc2'.
Proof.
  elim : s mc1 mc2 mc1' => [| t s Hind] mc1 mc2 mc1'.
    move => []->.
    by exists mc1'.
  rewrite foldm_cons_some => [[m [Hm Hmc2]]] Hleq.
  move: (apply_transitioncM Hm Hleq) => [m' [Hm' Hleq']].
  move/(_ m mc2 m' Hmc2 Hleq') : Hind => [mc2' [Hmc2' Hleq'']].
  exists mc2'; split => //=.
  rewrite foldm_cons_some.
  by exists m'.
Qed.

(* Composition of transitionc *)

Definition pre_compoc ( a b : transitionc ) : markingc :=
b.pre :'-: a.post :'+: a.pre.


Definition post_compoc ( a b : transitionc ) : markingc :=
a.post :'-: b.pre :'+: b.post.



Definition transitionc_compo ( a b : transitionc ) : transitionc :=
(pre_compoc a b, post_compoc a b).


Lemma lenc'_subLRC (nc1 nc2 nc3 : natc) :
  (nc1 != None) || (nc2 == None) ->
  (nc1 '- nc3 <= nc2) = (nc1 <= nc2 '+ nc3).
Proof.
  case: nc1 nc2 nc3 => [n1|] [n2|] [n3|] //.
  by rewrite lenc_subLRC.
Qed.

Lemma lenc'_subRLC (nc1 nc2 nc3 : natc) :
  (nc3 != None) ->
  (nc3 <= nc2) ->
  (nc1 '+ nc3 <= nc2) = (nc1 <= nc2 '- nc3).
Proof.
  case: nc1 nc2 nc3 => [n1|] [n2|] [n3|] Hle //.
  rewrite /Order.le //=; lia.
Qed.


(* The functions that transform markings and transitions into a transitionc *)

Definition markings_to_transitionc (m1 m2 : marking) : transitionc :=
  (embedm m1, embedm m2).

Definition nil_transitionc :=
  markings_to_transitionc ([ffun => 0]) ([ffun => 0]).

Lemma nil_transitionc_id (mc : markingc) :
  apply_transitionc mc nil_transitionc == Some mc.
Proof.
  case: apply_transitioncP => /=; last first.
    move => p /negPf.
    rewrite !ffunE  /embedn .
    by destruct (mc p).
  move => _ ; apply /eqP; f_equal.
  apply/ffunP => p.
  rewrite !ffunE /embedn; destruct (mc p).
  all: simpl; f_equal; lia.
Qed.



Lemma compo_neutralR (a :transitionc) :
  transitionc_compo a nil_transitionc == a .
Proof.
  apply/andP;split.
  all: apply/eqP; apply/ffunP => p.
    rewrite  /pre_compoc !ffunE /embedn /=.
  2: rewrite /post_compoc !ffunE /embedn /=.
  all : destruct (dec_op (a.post p)) as [[ n1 e1] | e1].
  all: destruct (dec_op (a.pre p)) as [[ n2 e2] | e2].
  all: try rewrite e1 //=.
  all: try rewrite e2 //=.
  all: f_equal; lia.
Qed.

Lemma compo_neutralL (a :transitionc) :
  transitionc_compo  nil_transitionc a == a .
Proof.
  apply/andP;split.
  all: apply/eqP; apply/ffunP => p.
    rewrite  /pre_compoc !ffunE /embedn /=.
  2: rewrite /post_compoc !ffunE /embedn /=.
  all : destruct (dec_op (a.post p)) as [[ n1 e1] | e1].
  all: destruct (dec_op (a.pre p)) as [[ n2 e2] | e2].
  all: try rewrite e1 //=.
  all: try rewrite e2 //=.
  all: f_equal; lia.
Qed.


Definition t_to_transitionc (t : transition) :=
  markings_to_transitionc (pre t) (post t).


(* The extension of the composition of transitionc to sequences of transitionc *)


Definition seqc_to_one ( s : seq transitionc ):= foldr transitionc_compo nil_transitionc s.


Notation " '> t " := (t_to_transitionc t)(at level 70, t at next level)
  : petri_net_scope.



(*
  A lemma to express that from a seq of transitions, it is the same to compose
  them and then lift the result to transitionc as to do it in the other order
*)

Lemma commu_seq_to_one (s : seq transition ) :
  (embedm (seq_to_one s).pre , embedm (seq_to_one s).post ) == seqc_to_one ( map (t_to_transitionc) s ).
Proof.
  elim: s => [|a s Hind].
    apply/andP;split.
    1,2 : apply /eqP; apply ffunP => p; by rewrite !ffunE.
  apply /andP; split.
  1,2 : apply /eqP;move/eqP: Hind; rewrite /seqc_to_one /= => <-.
  1,2:  apply ffunP => p; rewrite -/(seq_to_one s).
  1:rewrite /pre_compo !ffunE /embedn /=; case Htl:((seq_to_one s).pre p < post a p).
  1,2: apply f_equal.
  3:rewrite /post_compo !ffunE /embedn /=; case Htl: ((seq_to_one s).pre p < post a p).
  3,4: apply f_equal.
  all: move: Htl.
  all: by move: ((seq_to_one s).pre p) ((seq_to_one s).post p)
             (post a p) (pre a p) => [] [] [] []; lia.
Qed.

Lemma t_to_transitionc_inv (m m': marking) ( t: transition):
  m =1{ t }> m' ->
  embedm m =1{' ('> t) '}> embedm m'.
Proof.
  case: nextmP => //= Hpre []<-.
  rewrite /apply_transitionc /= lemc_embed Hpre.
  apply f_equal; apply ffunP => p.
  by rewrite !ffunE.
Qed.


Lemma seqc_inv (m m' : marking ) (s : seq transition):
  m ={ s }> m' ->
  embedm m ={' [seq t_to_transitionc i | i <-s] '}> embedm m' .
Proof.
  elim : s m m' => [ m m' []-> //=| t s Hind m m'].
  rewrite !foldm_cons_some => [[m'' [Hm'' Hm']] ].
  exists (embedm m''); split; last first.
    exact: Hind.
  exact: t_to_transitionc_inv.
Qed.


Section omega_transition.

(* Definition of omega transitions that are a subtype of transitionc *)

Definition inv_omega_transition ( t :transitionc) :=
[forall p , (t.pre p == None ) ==> (t.post p == None)].


Definition omega_transition := { t | inv_omega_transition t }.


Coercion omega_to_c := val : omega_transition -> transitionc.



Lemma good_compo_transitionc (mc : markingc) (a a' : omega_transition) :
  obind (apply_transitionc^~ a') (apply_transitionc mc a) =
  apply_transitionc mc (transitionc_compo a a').
Proof.
  cbn; move :a a' => [ a Ha] [a' Ha'] /=.
  (repeat case: apply_transitioncP => //=).
  1: move => Hprec Hpre' Hpre.
  1: f_equal; apply/ffunP => p.
  1: move/forallP/(_ p): Hpre Hpre' Hprec.
  2: move => p /negPf-Hneg /forallP/(_ p)-Hpre' /forallP/(_ p)-Hpre.
  3: move => Hprec p.
  3: move: Hprec => /forallP/(_ p)-Hprec /negPf-Hneg /forallP/(_ p)-Hpre.
  4: move => Hprec p.
  4: move: Hprec => /forallP/(_ p)-Hprec /negPf-Hneg.
  all: move :Ha Ha' => /forallP/(_ p)/implyP-Ha /forallP/(_ p)/implyP-Ha'.
  2,3,4: apply/eqP; cbn; rewrite -{}Hneg.
  all: try rewrite !ffunE; try rewrite !ffunE in Hprec; try rewrite !ffunE in Hpre'.
  all: move : Ha Ha'.
  3,4 : move : Hprec.
  2,3 : move : Hpre.
  2 : move : Hpre'.
  all : destruct (dec_op(a.post p)) as [[n1 e1]|e1],
  (dec_op(a'.post p)) as [[n2 e2]| e2],
  (dec_op(a.pre p)) as [[n3 e3]| e3],
  (dec_op(a'.pre p)) as [[n4 e4]| e4],
  (dec_op(mc p)) as [[n5 e5]| e5].
  all: rewrite e1 e2 e3 e4 e5.
  all: cbn; auto ; rewrite /Order.le /=.
  all: try lia.
  move => H1 H2 H3 _ _; f_equal; lia.
Qed.

Lemma good_compoc (a b : omega_transition ) (mc1 mc2 mc3 : markingc) :
  Some mc2 == apply_transitionc mc1  a ->
  Some mc3 == apply_transitionc mc2  b ->
  Some mc3 == apply_transitionc mc1 (transitionc_compo a  b).
Proof.
  rewrite -good_compo_transitionc => /eqP<- //=.
Qed.


Lemma markings_is_an_omega (m1 m2 : marking) : inv_omega_transition (markings_to_transitionc m1 m2).
Proof.
  apply /forallP => p; rewrite /markings_to_transitionc !ffunE.
  apply/implyP; move/eqP => H.
  inversion  H.
Qed.



(*
  Definition of abstractions that are a subtype of omega transitions,
  to fire an abstraction is the same as to fire a sequence of transitions
*)

Section abstraction.

Definition inv_abstraction_aux (t : transitionc ) (y :marking*marking) (p : place) (n : nat) :=
mem_nc (t.pre p) (y.pre p)
/\ (t.post p != None -> t.post p `+ y.pre p <= t.pre p `+ y.post p )
/\ (t.post p == None -> y.pre p + n <= y.post p).



Definition inv_abstraction ( t : transitionc) :=
forall (n : nat), exists (o_n : seq transition) , forall (p : place ),
t.pre p != None -> (inv_abstraction_aux t (seq_to_one o_n) p n ).



Definition abstraction := { a : omega_transition | inv_abstraction a}.

Definition R_weak (t1 t2 : transitionc): Prop:=
  t2.pre <= t1.pre /\ t1.post :'+: t2.pre <= t1.pre :'+: t2.post.

Program Definition Weakening (t: omega_transition) (n: nat) : omega_transition := ( t.pre,
  [ffun p: place =>
  match t.post p with
    | None => t.pre p `+ n
    |_ => t.post p
  end]).
Next Obligation.
  move: t => [t Homega].
  apply/forallP => p //=.
  apply/implyP => Hpre.
  rewrite !ffunE.
  move/forallP/(_ p)/implyP/(_ Hpre)/eqP: Homega => ->.
  by move/eqP: Hpre => ->.
Qed.

Lemma abstraction_to_weakening (t: omega_transition) :
  inv_abstraction (val t) ->
  forall (n:nat), exists (o_n : seq transition) ,
   R_weak (Weakening t n) (markings_to_transitionc (seq_to_one o_n).pre (seq_to_one o_n).post).
Proof.
  move: t => [t Homega] //= Habs n.
  move/(_ n): Habs => [] s Habs.
  exists s.
  rewrite /R_weak //=; split.
  all:apply/forallP => p.
  2: rewrite !ffunE //=.
  all: move/(_ p): Habs; rewrite /inv_abstraction_aux.
  all :move: (t.pre p) => [] //= m /(_ isT)Habs.
  all :move: Habs => [] Hpre [] //=.
    by move =>_ _; rewrite ffunE.
  move: (t.post p) => [m'|_] //=.
    by move => /(_ isT)H .
  rewrite eqxx => /(_ isT)H.
  by rewrite /Order.le //=; lia.
Qed.

Lemma weakening_to_abstraction (t: omega_transition) :
  (forall (n:nat), exists (o_n : seq transition) ,
  R_weak (Weakening t n) (markings_to_transitionc (seq_to_one o_n).pre (seq_to_one o_n).post)) ->
  inv_abstraction (val t).
Proof.
  move: t => [t Homega] //= Hweak n.
  move/(_ n): Hweak => [] s Hweak.
  exists s => p.
  move: Hweak; rewrite /R_weak /inv_abstraction_aux //= => [] [] /forallP/(_ p) Hpre.
  move/forallP/(_ p); rewrite !ffunE //=.
  move: Hpre.
  move: (t.pre p) (t.post p) => [] //= m [m'|] //=.
  all: rewrite /Order.le  !ffunE //= => Hpre Heffect _; split => //=; split => //= _.
  by move: Heffect; lia.
Qed.


Lemma nil_is_an_abstraction : inv_abstraction (nil_transitionc).
Proof.
  move => n.
  exists nil => p .
  by rewrite /inv_abstraction_aux !ffunE.
Qed.

Lemma t_is_an_abstraction (t : transition) : inv_abstraction ('>t).
Proof.
  move => n.
  exists (t:: nil) =>p.
  rewrite /inv_abstraction_aux  !ffunE /=.
  move => _ ; split.
    rewrite /embedn /Order.le //=.
  split; move => _ //=.
  rewrite  /Order.le  /= ;lia.
Qed.


Program Definition t_to_abstraction (t : transition) : abstraction :=
'>t .
Next Obligation.
  apply markings_is_an_omega.
Defined.
Next Obligation.
  apply t_is_an_abstraction.
Defined.


Program Definition nil_abstraction : abstraction :=
nil_transitionc.
Next Obligation.
  apply markings_is_an_omega.
Defined.
Next Obligation.
  apply nil_is_an_abstraction.
Defined.



Definition vsv : abstraction -> transitionc := fun a: abstraction => val (sval a).

Coercion abst_to_omega := sval : abstraction -> omega_transition.


Lemma next_eq_apply (mc mc': markingc) (t : transition) :
  nextmc mc t == Some mc' ->
  apply_transitionc mc (t_to_abstraction t) == Some mc'.
Proof.
  case : nextmcP =>  Hpre H //=.
  cbn.
  move/eqP in H.
  inversion_clear H.
  rewrite /apply_transitionc /t_to_transitionc /markings_to_transitionc //=.
  have H:(embedm (pre t) <= mc).
    apply/forallP => p.
    move/forallP/(_ p) : Hpre.
    rewrite !ffunE /in_mem /mem /embedn //= /mem_nc.
    by destruct (mc p).
  rewrite H => //=.
  apply/eqP; apply ffunP => p.
  rewrite !ffunE //=.
Qed.

Lemma apply_eq_next (mc mc': markingc) (t : transition) :
  apply_transitionc mc (t_to_abstraction t) == Some mc' ->
  nextmc mc t == Some mc'.
Proof.
  case : apply_transitioncP =>  Hpre H //=.
  cbn.
  move/eqP in H.
  inversion_clear H.
  rewrite /nextmc mem_mcP Hpre  //=.
  apply/eqP; apply ffunP => p.
  rewrite !ffunE //=.
Qed.

(* Composition of abstractions *)

Lemma compo_inv_omega ( a b : abstraction ) :
  inv_omega_transition (transitionc_compo a b).
Proof.
  cbn;move : a b => [ [a Ha'] _]  [[b Hb'] _] /=.
  apply/forallP => p.
  apply/implyP .
  rewrite /transitionc_compo /= /post_compoc !ffunE => H.
  move/forallP/(_ p)/implyP in Ha'.
  move/forallP/(_ p)/implyP in Hb'.
  destruct (a.pre p) as [n1 |],
  (a.post p) as [n2 | ], (b.pre p) as [n3 |],
  (b.post p) as [n4 | ].
  all: cbn; auto.
Qed.



Lemma compo_inv_abstraction ( a b : abstraction ) :
  inv_abstraction (transitionc_compo a b).
Proof.
  cbn;move : a b => [ [a Ha'] /= Ha]  [[b Hb'] /= Hb ] .
  rewrite /inv_abstraction => n.
  set n'': nat := \max_(i | (b.post i == None) && (a.post i != None))
    (match a.pre i `+ n '- a.post i with Some m => m | None => 0 end).
  set n':nat := maxn n n''.
  move :Hb; rewrite /inv_abstraction; move/(_ n') => [o_n' Hb].
  set l: nat := \max_(i | (a.post i == None) && (a.pre i != None)) (((seq_to_one o_n').pre i) + maxn 0  (n - ((seq_to_one o_n').post i)) ).
  move :Ha; rewrite /inv_abstraction; move/(_ l) => [o_l Ha].
  exists (cat o_l o_n') => p.
  cbn in Hb, Ha.
  move : Ha' Hb' => /forallP/(_ p)-Ha' /forallP/(_ p)-Hb'.
  destruct (dec_op (a.pre p)) as [[n1 e1] |e1 ], (dec_op (a.post p)) as [[n2 e2] |e2 ]
  ,(dec_op (b.pre p)) as [[n3 e3] |e3 ], (dec_op (b.post p)) as [[n4 e4] |e4 ].
  all: rewrite e1 e2 in Ha'; rewrite e3 e4 in Hb'.
  all : try inversion Ha'; try inversion Hb' ; clear Ha' Hb'.
  all: move => H; rewrite /transitionc_compo /= !ffunE e1 e2 e3 /= in H; inversion_clear H.
  all: assert (H:(inv_abstraction_aux a (seq_to_one o_l) p l)).
  1,3,5,7,9 : move/(_ p) in Ha; apply Ha; by rewrite e1.
  1,2 : assert (H':(inv_abstraction_aux b (seq_to_one o_n') p n')).
  1,3 : move/(_ p) in Hb; apply Hb; by rewrite e3.
  all: clear Ha Hb.
  all: have /eqP H'':= (seq_to_cat o_l o_n' ).
  all: rewrite /inv_abstraction_aux /transitionc_compo {}H'' !ffunE e1 e2 e3 e4 /=.
  all: rewrite/ inv_abstraction_aux e1 e2 /Order.le /= in H; destruct H as [H1 [H2 H3]].
  1,2: rewrite/ inv_abstraction_aux e3 e4 /Order.le /= in H'; destruct H' as [H'1 [H'2 H'3]].
  - {repeat split; auto.
    lia. apply/implyP; rewrite implyTb /Order.le /=. lia. }
  - {repeat split; auto.
    lia. apply/implyP; rewrite implyTb /Order.le /=. move: H2 H'3.
    move/implyP; rewrite implyTb /Order.le /= => H2. move/implyP; rewrite implyTb => H'3.
    have H:=leq_maxr n n''; rewrite -/(n') in H. clear H3 H'2.
    have H': match a.pre p `+ n '- a.post p with | Some m => m | None => 0 end <= n''.
    apply leq_bigmax_cond with (F:= fun i => match a.pre i `+ n '- a.post i with | Some m => m | None => 0 end); by rewrite e2 e4.
    rewrite e1 e2 /= in H'. lia. }
  all: have H: ((seq_to_one o_n').pre p + maxn 0 (n - (seq_to_one o_n').post p)) <= l.
  1,3,5 : apply leq_bigmax_cond with (F:= fun i => ((seq_to_one o_n').pre i + maxn 0 (n - (seq_to_one o_n').post i))); by rewrite e2 e1.
  all: have H':=leq_maxr 0 (n - (seq_to_one o_n').post p).
  all: move/implyP in H3; rewrite implyTb in H3; clear H2.
  all: repeat split; auto; lia.
Qed.


Program Definition abstraction_compo (a b : abstraction) : abstraction :=
transitionc_compo a b.
Next Obligation.
  apply compo_inv_omega.
Defined.
Next Obligation.
  apply compo_inv_abstraction.
Defined.




Definition abstraction_compo_seq ( s : seq abstraction) : abstraction :=
foldr abstraction_compo nil_abstraction s .



Lemma abstraction_compo_seq1 (a : abstraction) (s1 : seq abstraction) :
 (abstraction_compo_seq (a::s1)) =(abstraction_compo a (abstraction_compo_seq s1)).
Proof.
  by [].
Qed.


Lemma abst_compo_neutralL (a :abstraction) (mc : markingc) :
  apply_transitionc mc (abstraction_compo nil_abstraction a) = apply_transitionc mc a .
Proof.
  rewrite /abstraction_compo.
  cbn.
  by move/eqP : (compo_neutralL (sval a)) => ->.
Qed.

Lemma abst_compo_neutralR (a :abstraction) (mc : markingc) :
  apply_transitionc mc (abstraction_compo a nil_abstraction ) = apply_transitionc mc a .
Proof.
  rewrite /abstraction_compo.
  cbn.
  by move/eqP : (compo_neutralR (sval a)) => ->.
Qed.

Lemma abstraction_compo_seq_cat (s1 s2 : seq abstraction) (mc mc': markingc) :
  Some mc' == apply_transitionc mc (abstraction_compo (abstraction_compo_seq s1) (abstraction_compo_seq s2)) ->
  Some mc' == apply_transitionc mc (abstraction_compo_seq (s1++s2)).
Proof.
  elim : s1 s2 mc mc' => [| a s1 Hind] s2 mc mc'.
    by rewrite cat0s abst_compo_neutralL.
  rewrite cat_cons !abstraction_compo_seq1.
  rewrite -!good_compo_transitionc.
  case  : (apply_transitioncP mc a) => [|//=] _.
  rewrite 1!good_compo_transitionc //=.
  by move/Hind.
Qed.

Lemma abstraction_cat_compo_seq (s1 s2 : seq abstraction) (mc mc': markingc) :
  Some mc' == apply_transitionc mc (abstraction_compo_seq (s1++s2)) ->
  Some mc' == apply_transitionc mc (abstraction_compo (abstraction_compo_seq s1) (abstraction_compo_seq s2)).
Proof.
  elim : s1 s2 mc mc' => [| a s1 Hind] s2 mc mc'.
    by rewrite cat0s abst_compo_neutralL.
  rewrite cat_cons !abstraction_compo_seq1.
  rewrite -!good_compo_transitionc.
  case  : (apply_transitioncP mc a) => [|//=] _.
  rewrite 1!good_compo_transitionc //=.
  by move/Hind.
Qed.

(* One-step transition on markingcs with an abstraction.  *)
Notation "mc1 =1{' a '}>> mc2" :=
  (apply_transitionc mc1 (vsv a) = Some mc2)
    (at level 70, a at next level)
  : petri_net_scope.
(* Multi-step transition on markingcs with an abstraction.  *)
Notation "mc1 ={' s '}>> mc2" :=
  (foldm apply_transitionc mc1 (map vsv s) = Some mc2)
    (at level 70, s at next level)
  : petri_net_scope.


Lemma useful_compoc (s : seq abstraction ) :
  forall mc1 mc2, mc1 ={' s '}>> mc2 ->
  Some mc2 == apply_transitionc mc1  (abstraction_compo_seq s).
Proof.
  elim :s=> [mc1 mc2 /= <-| a s IHs].
    by have /eqP <- := nil_transitionc_id mc1.
  move => mc1 mc3 /foldm_cons_some-[mc2 [/eqP-H12 H23]].
  move : IHs H12 => /(_ mc2 mc3 H23 )-IHs.
  have /(_  (sval a) mc1)-H13 := good_compoc _ IHs.
  rewrite /seq_to_one => H.
  apply H13; move/eqP in H; by rewrite H.
Qed.


Lemma useful_compoc2 (s : seq abstraction ) :
  forall mc1 mc2: markingc ,
  Some mc2 == apply_transitionc mc1 (abstraction_compo_seq s) ->
  mc1 ={' s '}>> mc2 .
Proof.
  elim :s=> [mc1 mc2 /= H| a s IHs].
    have /eqP :=(nil_transitionc_id mc1) => H'.
    rewrite {}H' in H; symmetry; by apply/eqP.
  move => mc1 mc3 H.
  apply foldm_cons_some.
  rewrite /abstraction_compo_seq /= -/ (abstraction_compo_seq s) -/(vsv a)  in H.
  rewrite -good_compo_transitionc in H.
  case :apply_transitioncP; last first.
    move => p Hneg.
    rewrite /apply_transitionc in H.
    apply ineq_markingc in Hneg.
    by  rewrite Hneg /= in H.
  move => Ha_pre.
  exists (mc1 :'-: (vsv a).pre:'+: (vsv a).post).
  split; auto; rewrite /apply_transitionc Ha_pre  in H.
  by apply IHs.
Qed.

(*
  We define the notion of acceleration, which is a subtype of abstraction that either doesn't change
  a place of a marking or bring it to omega
*)
Section acceleration.

Definition inv_accel (t : transitionc) :=
[forall p , (t.post p == None )|| (t.post p == t.pre p)].


Definition acceleration := { a : abstraction | inv_accel a}.

Definition abs_to_acc_pre ( a : transitionc ) : markingc := [ffun p: place =>
match a.post p, a.pre p with
  |Some n, Some m => if n<m then None else a.pre p
  |Some n, None => None
  |_,_ => a.pre p
end
].


Definition abs_to_acc_post ( a : transitionc ) : markingc := [ffun p: place =>
match a.post p, a.pre p with
  |None, None => a.pre p
  |Some n, Some m => if (n==m) then a.pre p else None
  |_,_ => None
end].



Definition abs_to_acc_aux (t : transitionc ) :=
(abs_to_acc_pre t, abs_to_acc_post t).




Lemma abs_to_acc_is_an_omega (a : abstraction) :
  inv_omega_transition (abs_to_acc_aux a).
Proof.
  cbn;move : a => [ [a Ha'] _] /=.
  rewrite /= /inv_omega_transition.
  apply/forallP => p.
  apply/implyP => H.
  rewrite /abs_to_acc_aux /= !ffunE.
  move/forallP/(_ p)/implyP in Ha'.
  rewrite /abs_to_acc_aux /=  !ffunE in H.
  destruct (a.pre p) as [n1 |],(a.post p) as [n2 | ].
  all:  auto.
  case Htl: (n2<n1).
  all:  try by rewrite Htl in H.
  apply ltn_eqF in Htl; by rewrite Htl.
Qed.



Lemma abs_to_acc_is_an_abstraction (a : abstraction) :
  inv_abstraction (abs_to_acc_aux  a).
Proof.
  cbn;move: a => [ [ a Ha'] /= Ha].
  rewrite /inv_abstraction => n.
  case n.
    { exists nil.
      move => p.
      destruct  (dec_op (a.pre p)) as [[n1 e1] |e1 ], (dec_op (a.post p)) as [[n2 e2] |e2 ].
      all : rewrite /abs_to_acc_aux /abs_to_acc_post /abs_to_acc_pre !ffunE e1 e2 //= .
        case Htl: (n2 < n1).
          by cbn.
      all : move => _.
      all: rewrite /inv_abstraction_aux !ffunE e1 e2 .
        rewrite Htl.
        case :(n2==n1).
      all: by [].
    }
  cbn in Ha.
  move => m; clear n Ha'.  (* n is m.+1 so I put n in the names  *)
  move/(_ m.+1): Ha  => [o_n Ho_n].
  exists (iter _ m (cat o_n ) o_n) => p.
  destruct  (dec_op (a.pre p)) as [[n1 e1] |e1 ], (dec_op (a.post p)) as [[n2 e2] |e2 ].
  all: move/(_ p) in Ho_n; move :Ho_n.
  all : rewrite /abs_to_acc_aux !ffunE e1 e2 //= => Ho_n.
    case Htl: (n2 < n1).
      by cbn.
  all : assert (inv_abstraction_aux a (seq_to_one o_n) p m.+1).
  1,3: by apply Ho_n.
  all: clear Ho_n; move => _.
  all: rewrite /inv_abstraction_aux e1 e2 /= in H.
  1: destruct H as [H1 [H2 _] ].
  2: destruct H as [H1 [_ H2]].
  all: move/implyP in H2; rewrite implyTb /Order.le /= in H2.
  all : assert (H:(seq_to_one (iter _ m (cat o_n) o_n)).pre p + (m.+1)*((seq_to_one o_n).post p) =
  (seq_to_one (iter _ m (cat o_n) o_n)).post p + (m.+1)*((seq_to_one o_n).pre p)
  /\(seq_to_one (iter _ m (cat o_n) o_n)).pre p = (seq_to_one o_n).pre p  ).
  1,3: apply iter_good; lia.
  all: destruct H as [Ho_n Ho_n'].
  all: rewrite /inv_abstraction_aux !ffunE e1 e2 //=; split; try split; auto.
  - rewrite Htl /mem_nc.
  1,4: rewrite Ho_n' //=.
  1,2 : case Htl': (n2 == n1); auto.
  all: move => _.
    rewrite Htl  /Order.le /=.
    move/eqP in Htl'; rewrite {}Htl' /Order.le /= in H2.
    rewrite /Order.le /= (leq_add2l n1 ((seq_to_one (iter _ m (cat o_n) o_n)).pre p) ((seq_to_one (iter _ m (cat o_n) o_n)).post p) ).
    have H:((seq_to_one o_n).pre p <= (seq_to_one o_n).post p) by lia.
  2,3: have H:( 1%nat <= (seq_to_one o_n).post p - (seq_to_one o_n).pre p) by lia .
  all: move/leq_mul : H ;repeat move/(_ m.+1); move/implyP ; rewrite ltnSn implyTb /=; lia.
Qed.

Lemma abs_to_acc_is_an_acceleration (a : abstraction) :
  inv_accel (abs_to_acc_aux a).
Proof.
  cbn;move : a => [ [a Ha'] _] /=.
  rewrite /= /inv_accel.
  apply/forallP => p.
  rewrite /abs_to_acc_aux /= !ffunE.
  rewrite /inv_omega_transition in Ha'.
  move/forallP/(_ p) in Ha'.
  destruct (a.pre p) as [n1 |],
  (a.post p) as [n2 | ].
  all: auto.
  case Htl: (n2==n1) => //=.
  move /eqP in Htl; rewrite Htl /Order.lt /= (ltnn n1) //=.
Qed.


Program Definition abs_to_acc (a  : abstraction) : acceleration :=
abs_to_acc_aux  a.
Next Obligation.
  apply abs_to_acc_is_an_omega.
Defined.
Next Obligation.
  apply abs_to_acc_is_an_abstraction.
Defined.
Next Obligation.
  apply abs_to_acc_is_an_acceleration.
Defined.

Coercion acc_to_abst := val : acceleration -> abstraction.




(* A notation to go from an acceleration to a transitionc *)
Definition vsa: acceleration -> transitionc := (vsv \o val).



(* One-step transition on markingcs with an acceleration.  *)
Notation "mc1 =1{' a '}>>> mc2" :=
  (apply_transitionc mc1 (vsa a) = Some mc2)
    (at level 70, a at next level)
  : petri_net_scope.
(* Multi-step transition on markingcs with an acceleration.  *)
Notation "mc1 ={' s '}>>> mc2" :=
  (foldm apply_transitionc  mc1 (map vsa s) = Some mc2)
    (at level 70, s at next level)
  : petri_net_scope.

(* Some lemma about how accelerations change markingc *)
Lemma abs_to_acc_increase mc mc1 mc2 a:
  mc =1{' a '}>> mc1 ->
  mc =1{' (abs_to_acc a) '}>>> mc2 ->
  mc1 <= mc2.
Proof.
  case : apply_transitioncP => [Hpre | //=].
  case => [] Hpost.
  case : apply_transitioncP => [Hpre'| //=].
  case => [] Hpost'; subst.
  apply/forallP => p.
  move/(forallP)/(_ p) : Hpre; move/(forallP)/(_ p) : Hpre' .
  rewrite !ffunE.
  destruct (dec_op((vsv a).post p)) as [[n1 e1]|e1],
    (dec_op( (vsv a).pre p)) as [[n2 e2]| e2],
    (dec_op(mc p)) as [[n3 e3]| e3]; rewrite e1 e2 e3 => //=.
  1,2:case Hlt:(n1<n2); case Heq:(n1 == n2) => //=.
    by move/eqP : Heq => ->.
  move/eqP : Heq Hlt => ->; lia.
Qed.

Lemma abs_to_acc_inv mc1 mc2 (a: abstraction):
  Some mc2 = apply_transitionc mc1 a ->
  mc1 < mc2 ->
  exists mc, Some mc = apply_transitionc mc1 (abs_to_acc a) .
Proof.
  move => Hmc2.
  case : (apply_transitioncP) => [_ |p].
    by exists (mc1 :'-: (vsa (abs_to_acc a)).pre :'+: (vsa (abs_to_acc a)).post).
  move/negP => Habs.
  move: Hmc2; case : apply_transitioncP => //= /forallP/(_ p) Hmc1.
  case => -> /andP [Hneq /forallP/(_ p) Hleq].
  exfalso; apply Habs; move : Hleq Hmc1.
  rewrite /abs_to_acc /= /abs_to_acc_pre /= !ffunE.
  destruct (dec_op ((vsv a).post p)) as [[n1 e1]|e1], (dec_op ((vsv a).pre p)) as [[n2 e2]|e2],
   (dec_op (mc1 p)) as [[n3 e3]|e3].
  all: rewrite {}e1 {}e2 {}e3 => //=.
  case Hlt: (n1 < n2) => //=.
  rewrite /Order.le /=.
  lia.
Qed.


Lemma acc_no_decrease (mc mc' : markingc) (a : acceleration) :
  Some mc' == apply_transitionc mc  a  ->  (mc <= mc').
Proof.
  cbn;move :a =>  [[[a Homega] Habst] Hacc] //=.
  cbn in Habst, Hacc.
  rewrite /inv_accel //= in Hacc.
  clear Habst.
  rewrite /inv_omega_transition //= in Homega.
  move/eqP.
  case : apply_transitioncP => //=.
  move/forallP : Hacc Homega => Hacc; move/forallP => Homega; move/forallP => Hpre H.
  inversion_clear H.
  apply/forallP => p; rewrite !ffunE.
  move/(_ p) in Hacc.
  elim : (orP Hacc); move/eqP => -> //=.
  move/(_ p) in Hpre.
  apply subcKC in Hpre; rewrite Hpre.
  case Htl:(mc p) => //=.
Qed.

Lemma acc_seq_no_decrease (mc mc': markingc) (acc: seq acceleration):
  mc ={' acc '}>>> mc' -> (mc <= mc').
Proof.
  elim: acc mc mc' => [|a acc Hind] mc mc'.
    by move => [] ->.
  move => //=.
  case Ha: (apply_transitionc) => [mc''|] //=.
  move/Hind.
  move/eqP: Ha.
  rewrite eq_sym; move/acc_no_decrease.
  exact: (Order.POrderTheory.le_trans).
Qed.


Lemma one_acc_is_enough (mc mc' : markingc) (a : acceleration) :
  mc =1{' a '}>>> mc' ->
  mc' =1{' a '}>>> mc'.
Proof.
  cbn;move :a =>  [[[a Homega] Habst] Hacc] //=.
  cbn in Habst, Hacc.
  rewrite /inv_accel //= in Hacc.
  clear Habst Homega.
  case : apply_transitioncP => //=.
  move/forallP => Hpre.
  inversion_clear 1.
  case : apply_transitioncP => //=.
    move => Hpre'; apply f_equal. rewrite -ffunP => p.
    move/forallP/(_ p) in Hacc.
    elim : (orP Hacc); move/eqP => H; move/forallP/(_ p): Hpre'; rewrite !ffunE {}H.
      by rewrite !None_win_addr.
    by apply subcKC.
  move => p; rewrite !ffunE.
  move/forallP/(_ p) in Hacc.
  elim : (orP Hacc); move/eqP => H; move/(_ p): Hpre; rewrite {}H.
    by rewrite !None_win_addr.
  move => Hpre.
  have H:=(subcKC Hpre ); by rewrite {}H Hpre.
Qed.


(* The proposition 1 of the paper of Finkel & al *)

Lemma coverability_preserved_by_abstraction (a : abstraction) (mc mc': markingc) (m0 : marking) :
  (forall m, m \in mc -> coverable m0 m) ->
  mc =1{' a '}>> mc'  ->
  forall m, m \in mc' -> coverable m0 m.
Proof.
  rewrite /coverable /reachable => Covermc Ha m Hm.
  set n: nat := \max_(i | mc' i == None) (m i).
  move :a Ha => [[a Ha] Ha'] /= abst.
  cbn in Ha', abst.
  move: Ha' => /(_ n)/=-[o_n Ho_n]; unfold inv_abstraction_aux in Ho_n.
  set l: nat :=  \max_(i | mc i == None) (((seq_to_one o_n).pre i) + maxn 0  (n - ((seq_to_one o_n).post i)) ).
  set m3: marking := [ ffun p => match (mc p) with
  | None => l
  | Some n => n
  end].
  (* We will show that from m3 we can fire o_n *)
  have Hm3' : exists m4,   m3 ={ o_n }> m4.
  {
    apply /fire_seq /forallP => p.
    destruct (dec_op (mc p)) as [[n1 e1]| e1 ].
    all: rewrite /m3 !ffunE e1; case: apply_transitioncP abst => //.
    all: move/forallP/(_ p) ; rewrite e1 => Ha_pre Ha_post.
    2:{
      assert (((seq_to_one o_n).pre p + maxn 0 (n - (seq_to_one o_n).post p))<=l).
        move/eqP in e1; by apply leq_bigmax_cond with (F:= fun i => ((seq_to_one o_n).pre i + maxn 0 (n - (seq_to_one o_n).post i))).
      apply: (leq_trans _ H); apply: leq_addr.
    }
    destruct (dec_op(a.pre p)) as [[n2 e2]| e2 ]; last first.
    all: rewrite e2 in Ha_pre.
      by inversion Ha_pre.
    move/(_ p) in Ho_n.
    assert (H:inv_abstraction_aux a (seq_to_one o_n) p n).
      by apply Ho_n; rewrite e2.
    destruct H as [H1 H2].
    move :H1 Ha_pre ; rewrite /mem_nc e2 => H1; rewrite /Order.le => //= Ha_pre.
    lia.
  }
  (* Now we will show that this m4 covers m *)
  move:Hm3'=>[m4 Hm4].
  assert (Hm':m <= m4).
  {
    have /eqP H:= usefull_compo Hm4.
    case : (apply_transitionpP m3 (seq_to_one o_n)); last first.
      move => p Hp; apply ineq_marking in Hp; by rewrite /apply_transitionp Hp in H.
    move => Hp; rewrite /apply_transitionp {}Hp in H.
    inversion_clear H.
    apply /forallP=> p.
    move /(_ p) in Ho_n.
    move:abst; case : apply_transitioncP => // Ha_pre /Some_inj-Ha_post.
    rewrite - Ha_post in Hm.
    move: Hm Ha; move/forallP/(_ p) => Hm;move/forallP/(_ p) => Ha.
    move/ffunP/(_ p) :Ha_post ;rewrite !ffunE => Ha_post.
    destruct (dec_op (mc p)) as [[n1 e1] |e1 ], (dec_op (a.pre p)) as [[n2 e2] |e2 ]
    , (dec_op (a.post p)) as [[n3 e3] |e3 ].
    all: move :Hm Ha_post Ho_n Ha;rewrite /m3 !ffunE e1 e2 e3 /= => Hm Ha_post Ho_n Ha.
    all: auto; clear Ha.
    all: move/forallP/(_ p) in Ha_pre; rewrite e1 e2 in Ha_pre; auto.
    1,2:destruct Ho_n as [H0 [H1 H2]]; auto.
    2: {
        assert (H: m p <= n ).
          apply leq_bigmax_cond.
          by rewrite Ha_post.
        apply (leq_trans H). move/implyP in H2; rewrite implyTb in H2. lia.
      }
    1:{
        move/implyP in H1; rewrite implyTb /Order.le /= in H1 .
        rewrite /in_mem /= in Hm. apply (leq_trans Hm). rewrite /Order.le /= in H1,Ha_pre.
        lia.
      }
    all: assert (H: m p <= n ).
    1,3,5 :apply leq_bigmax_cond; by rewrite Ha_post.
    all: apply (leq_trans H); assert (((seq_to_one o_n).pre p + maxn 0 (n - (seq_to_one o_n).post p))<=l).
    1,3,5 : move/eqP in e1; by apply leq_bigmax_cond with (F:= fun i => ((seq_to_one o_n).pre i + maxn 0 (n - (seq_to_one o_n).post i))).
    all: lia.
  }
  (* Now we will show that m4 is in the cover *)
  move/(_ m3) in Covermc.
  assert (H:m3 \in mc) .
    apply /forallP => p. destruct (dec_op (mc p)) as [[n' e]| e].
    1,2: by rewrite /m3 ffunE e /in_mem /=.
  move: Covermc => /(_ H)-[ m' [[s Hs] H']].
  have [ m2' [coverm' Hm2']] := nextm_seq_ucompat Hm4 H'.
  exists m2'.
  split; last first.
    apply /forallP => p.
    move: Hm' Hm2' => /forallP/(_ p)-Hm' /forallP/(_ p)-Hm2'.
    by apply (leq_trans Hm' Hm2').
  exists (cat s o_n).
  apply foldm_cat_some; by exists m'.
Qed.


(* A well founded relation on markingc *)
Section omegaR.

Definition omega_possible (mc: markingc) : nat :=  #|[set p | mc p != None]|.

Lemma omega_possible0:
  omega_possible ([ffun => None]) = 0 .
Proof.
  apply/eqP;rewrite /omega_possible cards_eq0.
  apply/eqP; apply eq_finset => p.
  by rewrite ffunE.
Qed.

Lemma leq_omega_possible mc mc':
  mc <= mc' ->
  omega_possible mc' <= omega_possible mc.
Proof.
  move/forallP => Hleq.
  apply: subset_leq_card.
  apply/subsetP => p.
  rewrite !in_set.
  move: (Hleq p).
  by case: (mc p); case: (mc' p).
Qed.

Definition omegaR mc mc' := lt (omega_possible mc) (omega_possible mc').

Lemma omegaR_trans mc mc' mc'' :
  omegaR mc mc' ->
  omegaR mc' mc'' ->
  omegaR mc mc''.
Proof.
  exact: Nat.lt_trans.
Qed.

Lemma wf_omegaR : well_founded omegaR .
Proof.
  apply (wf_inverse_image markingc nat lt omega_possible).
  apply lt_wf.
Qed.

Lemma lt_omegaR (mc1 mc2: markingc) :
  mc1 < mc2 ->
  omegaR mc2 mc1 ->
  topsmc mc1 \proper topsmc mc2.
Proof.
  rewrite /Order.lt //=.
  move/andP => [Hneq Hleq] Homega.
  rewrite /proper.
  apply/andP; split.
  apply /subsetP;rewrite /sub_mem /topsmc => p.
    move/forallP/(_ p) in Hleq.
    by rewrite !inE; move/eqP => H;by rewrite H in Hleq.
  apply /subsetP;rewrite /sub_mem /topsmc => Hnone.
  rewrite /omegaR !/omega_possible  in Homega.
  have Hlt:(#|[set p | mc2 p != None]| < #|[set p | mc1 p != None]|) by lia.
  clear Homega.
  enough ([set p | mc2 p != None] = [set p | mc1 p != None]).
    rewrite H in Hlt; lia.
  rewrite -setP /eq_mem => p.
  rewrite !inE.
  move /forallP/(_ p) in Hleq.
  move/(_ p):Hnone; rewrite !inE.
  destruct (dec_op (mc1 p)) as [[s1 e1]| e1], (dec_op (mc2 p)) as [[s2 e2]| e2].
  all: rewrite e1 e2 => //= .
  all: move/implyP => //=.
  by rewrite e1 e2 in Hleq.
Qed.




Lemma acc_inc_omega mc mc' (acc : acceleration) :
  mc =1{' acc '}>>> mc' ->
  mc != mc' ->
  omegaR mc' mc.
Proof.
  cbn;move : acc => [[[acc Homega]Habst] Hacc] //=.
  cbn in Hacc,Habst.
  move : Homega Habst Hacc => //= _ _ Hacc Hfire Hneq.
  rewrite /inv_accel in Hacc.
  move/eqP in Hneq.
  rewrite /apply_transitionc in Hfire.
  case Htl: (acc.1<=mc).
  all: rewrite Htl in Hfire.
  all: inversion Hfire; subst.
  rewrite /omegaR /omega_possible .
  enough ( [set p | fflift2 addc (fflift2 subc mc acc.1) acc.2 p != None] \proper [set p | mc p != None] ).
    rewrite properEcard in H; elim: (andP H) => _ H'; lia.
  rewrite properEneq. apply/andP; split.
    apply/eqP => H; apply Hneq.
    apply f_equal, f_equal, f_equal.
    apply/ffunP => p.
  2: apply/subsetP;rewrite/sub_mem => p.
  all: move/forallP/(_ p) in Htl; apply subcKC in Htl.
  all: move/forallP/(_ p) in Hacc.
  all: elim: (orP Hacc); move/eqP => Haccp.
  3,4: rewrite !inE.
  all: rewrite !ffunE Haccp //=.
    {
      destruct (dec_op (mc p)) as [[n e]| e].
      all: rewrite e => //=.
      have H': p \in  [set p | mc p != None].
      by rewrite !inE e.
      by rewrite -H !inE !ffunE Haccp //= in H'.
    }
  destruct (dec_op (acc.1 p)) as [[n e]| e].
  all: move: Htl;rewrite e => //= H; by rewrite H.
Qed.

Lemma sacc_inc_omega mc mc' (acc: seq acceleration) :
  mc ={' acc '}>>> mc' ->
  mc != mc' ->
  omegaR mc' mc.
Proof.
  move : mc mc'.
  elim : acc => [ mc mc'| a l Hind mc mc'].
    by inversion 1; rewrite eq_refl.
  destruct (dec_op (apply_transitionc mc (vsa a))) as [[m e]|e].
  all: rewrite  //= e => //=.
  case Htl:(mc ==m).
    move /eqP in Htl; subst;by apply Hind.
  case Htl': (m==mc').
    move => _; move/eqP in Htl'; subst; apply (acc_inc_omega e) .
    move/( _ m mc'): Hind => Hind H Hneq.
  apply (@omegaR_trans mc' m mc ).
    apply Hind => //=;by move/negbT in Htl'.
  apply (@acc_inc_omega _ _ a) => //=;by move/negbT in Htl.
Qed.

(*
  A definition and functions to saturate a markingc
  and lemmas to have properties on it
*)

Definition saturated_markingc (mc : markingc) (A: seq acceleration)  :=
all( fun (a: acceleration) => (Some mc == apply_transitionc mc a) || (None == apply_transitionc mc a)) A.

Lemma saturated_markingc_suffix mc s1 s2:
  saturated_markingc mc (s1++s2) ->
  saturated_markingc mc s2.
Proof.
  elim: s1 mc s2 => //= a s1 Hind mc s2 /andP [] _.
  exact:Hind.
Qed.

Lemma saturated_omega A:
  saturated_markingc [ffun=> None] A .
Proof.
  elim: A => //= a A Hind.
  apply/andP; split => //=.
  apply/orP; left.
  case: apply_transitioncP; last first.
    move => p.
    by case:((vsa a).pre p); rewrite ffunE.
  move => _.
  apply/eqP; apply f_equal; apply/ffunP => p.
  rewrite !ffunE.
  case Hapre: ((vsa a).pre p) => [n1 //=| //=].
    by case: ((vsa a).post p).
  suff: ((vsa a).post p) = None.
   by move => ->.
  have : (inv_accel a).
    by generalize dependent a; move => [a Hacc] /=.
  move/forallP/(_ p)/orP; move: Hapre => ->.
  by case => /eqP ->.
Qed.


Fixpoint accelerate_seq  mc  (a : seq acceleration) : seq acceleration:=
match a with
|nil => nil
|h::l => match apply_transitionc mc h with
        | None => accelerate_seq mc l
        | Some mc' => if mc'== mc then accelerate_seq mc l  else h::(accelerate_seq mc' l)
        end
  end.

Lemma accelerate_seq_In mc A a s:
  s= accelerate_seq mc A ->
  List.In a s ->
  List.In a A.
Proof.
  elim : A mc s a => //= [| a0 A Hind] mc s a .
    by move => ->.
  case: apply_transitionc; last first.
    by move => Hs Hin; right; apply:(Hind mc s).
  move => mc'.
  case: (mc' == mc).
    by move => Hs Hin; right; apply:(Hind mc s).
  move => -> //=.
  elim.
    by left.
  by move => Hin; right; apply: (Hind mc' (accelerate_seq mc' A)).
Qed.


Lemma apply_accelerate mc (a l : seq acceleration) :
  accelerate_seq mc a = l ->
  None != foldm (apply_transitionc) mc (map vsa l).
Proof.
  move : mc  l.
  elim :a => [| a a' IHa].
    rewrite /accelerate_seq => mc  l Hl.
    by rewrite -Hl => //=.
  move => mc l.
  rewrite /accelerate_seq.
  destruct (dec_op(apply_transitionc mc (vsa a))) as [[mc' e]|e].
  all: rewrite e -/accelerate_seq.
  2: by apply IHa.
  case Htl : (mc'==mc).
    apply IHa.
  move=> H.
  rewrite -H //= e .
  by apply IHa.
Qed.


Lemma accelerate_not_nil mc  (a l:seq acceleration) :
  accelerate_seq mc a = l ->
  (l <> nil) ->
  (exists mc', (mc ={' l '}>>> mc' /\ mc'>mc)) /\
  (forall s1 s2 s3 mc1 mc2, s1++s2++s3=l -> s2 <> nil -> mc={' s1 '}>>> mc1 ->
  mc1 ={' s2 '}>>> mc2 -> mc1 < mc2)  .
Proof.
  move : mc l.
  elim : a => [| a a' IHa].
    rewrite /accelerate_seq => mc   l Hl Hlneq.
    by rewrite Hl in Hlneq.
  move => mc  l.
  rewrite /accelerate_seq.
  destruct (dec_op(apply_transitionc mc (vsa a))) as [[mc'' e]|e].
  all: rewrite e -/accelerate_seq.
  2: by apply IHa.
  case Htl : (mc''==mc).
    by apply IHa.
  case : l .
    by [].
  move => a0 l0 Ha0 _.
  have Hmc'':(mc<mc'').
    rewrite /Order.lt //= Htl /relcw //=.
    apply (@acc_no_decrease  _ _ a); rewrite eq_sym; by apply/eqP.
  move : Ha0;case :l0.
    inversion 1 as [[Ha Hnil]]; subst; split; rewrite Hnil.
      by exists mc''; rewrite //= e.
    move =>  [ //=| a s1] s2 s3 mc1 mc2; last first.
      rewrite cat_cons; inversion 1 as [[Ha Hnil]].
      by move/nilP; rewrite !cat_nilp => /andP [] _ /andP [] /nilP ->.
    move => H.
    elim: (cat1 H); last first.
      by move=> [] ->.
    move => [] -> _ _ [] <- //=.
    by move: e => -> //= [] <-.
  move => a1 l1.
  inversion 1; subst.
  move/(_ mc'' (a1::l1)) in IHa.
  rewrite H1.
  apply IHa in H1.
  2: by [].
  move :H1 =>[] H1 Hprefix.
  move: H1 => [] mc' [] H Hmc'.
  split.
    exists mc'; split; auto; last first.
      apply (Order.POrderTheory.lt_trans Hmc'' Hmc').
    rewrite foldm_cons_some; exists mc''; rewrite  //= e  H //=.
  move => [//=| a s1]; last first.
    move => s2 s3 mc1 mc2.
    rewrite cat_cons; inversion 1 as [[Ha Hs]]; subst.
    move:Hs; rewrite map_cons //= e //=.
    exact: Hprefix.
  move => [//=|a s2] s3 mc1 mc2.
  rewrite cat_cons; inversion 1 as [[Ha Hs]]; subst => _ [] <-.
  rewrite map_cons //= e //=.
  generalize dependent s2.
  move => [//= _ _ [] <- //=| a s2] _ Hs2 Hmc2.
  apply: (Order.POrderTheory.lt_trans Hmc'').
  apply: (@Hprefix nil (a::s2) s3 mc'' mc2) => //=; by subst.
Qed.


Lemma accelerate_seq_consistency mc acc :
  accelerate_seq mc acc = nil ->
  saturated_markingc mc acc.
Proof.
  elim: acc mc => //= a acc Hind mc.
  case: apply_transitionc => [mc'|]; last first.
    by move/Hind => ->.
  case Heq: (mc' == mc).
    move/eqP: Heq => -> /Hind ->.
    by rewrite eqxx.
  by inversion 1.
Qed.

Lemma accelerate_seq_consistency2 mc acc:
  saturated_markingc mc acc ->
  accelerate_seq mc acc = nil.
Proof.
  elim : acc mc => //= a acc Hind mc /andP [] /orP.
  case => /eqP <-.
    rewrite eqxx.
  all: exact:Hind.
Qed.


Function saturate_markingc (mc : markingc) (a acc : seq acceleration) {wf omegaR mc} : markingc*(seq acceleration):=
match accelerate_seq mc a  with
| nil => (mc,acc)
| t::s => match foldm (apply_transitionc ) mc (map vsa (t::s)) with
      | None => (mc,acc)
      | Some mc' => saturate_markingc mc' a (acc++(t::s))
      end
end.
Proof.
  2: apply wf_omegaR.
  move => mc a acc a0 l0 Ha0 mc' H.
  eapply sacc_inc_omega.
    apply H.
  have H':=(@accelerate_not_nil mc a (a0::l0)).
  apply H' in Ha0 => //=.
  move: Ha0 => [] [] mc'' [] H0 H1 H2.
  rewrite /Order.lt //= in H1.
  elim :(andP H1) => Hneq _.
  rewrite H in H0; inversion H0; subst.
  by rewrite eq_sym.
Defined.

Lemma saturate_markingc_computation mc1 mc2 mc3 A acc s:
  (mc3,s) = saturate_markingc mc2 A acc ->
  mc1 ={' acc '}>>> mc2 ->
  mc1 ={' s '}>>> mc3.
Proof.
  functional induction (saturate_markingc mc2 A acc)
  as [mc A acc HA| mc A acc H H' HA HNone  | mc A acc H H' HA mc'' Hmc'' Hind].
  - by move => [] -> ->.
  - have: (None != foldm apply_transitionc mc [seq vsa i | i <- accelerate_seq mc A]).
      by apply: apply_accelerate.
    by rewrite HA HNone.
  - move/Hind => {}Hind Hmc.
    apply:Hind.
    by rewrite map_cat foldm_cat Hmc.
Qed.


Lemma saturate_markingc_leq mc mc' a acc acc':
  (mc',acc')= saturate_markingc mc a acc ->
  mc <= mc'.
Proof.
  functional induction (saturate_markingc mc a acc)
  as [mc a acc Hacc| mc a acc H H' Hacc HNone  | mc a acc H H' Hacc mc'' Hmc'' Hind].
  - by move => [] -> _ .
  - have: (None != foldm apply_transitionc mc [seq vsa i | i <- accelerate_seq mc a]).
      by apply: apply_accelerate.
    by rewrite Hacc HNone.
  - move/Hind; move/acc_seq_no_decrease: Hmc''.
    exact: (Order.POrderTheory.le_trans).
Qed.




Lemma saturate_marking_works mc mc' a acc acc':
  (mc', acc') = saturate_markingc mc a acc ->
  saturated_markingc mc' a.
Proof.
  functional induction (saturate_markingc mc a acc)
  as [mc a acc Hacc| mc a acc H H' Hacc HNone  | mc a acc H H' Hacc mc'' Hmc'' Hind].
  - move => [] -> _ .
    by apply: accelerate_seq_consistency.
  - have: (None != foldm apply_transitionc mc [seq vsa i | i <- accelerate_seq mc a]).
      by apply: apply_accelerate.
    by rewrite Hacc HNone.
  - by [].
Qed.

Lemma saturated_saturate_markingc mc acc acc' :
  saturated_markingc mc acc ->
  (mc, acc') =  saturate_markingc mc acc acc'.
Proof.
  functional induction (saturate_markingc mc acc acc')
  as [mc acc acc' Hacc| mc acc acc' H H' Hacc HNone  | mc acc acc' H H' Hacc mc'' Hmc'' Hind].
  - by [].
  - have: (None != foldm apply_transitionc mc [seq vsa i | i <- accelerate_seq mc acc]).
      by apply: apply_accelerate.
    by rewrite Hacc HNone.
  - by move/accelerate_seq_consistency2 => H'';rewrite H'' in Hacc.
Qed.


Lemma saturate_markingc_redondant mc mc' a acc acc':
  (mc', acc') = saturate_markingc mc a acc ->
  (mc', acc') = saturate_markingc mc' a acc'.
Proof.
  functional induction (saturate_markingc mc a acc)
  as [mc a acc Hacc| mc a acc H H' Hacc HNone  | mc a acc H H' Hacc mc'' Hmc'' Hind].
  3: by [].
  - move => [] -> -> .
    functional induction (saturate_markingc mc a acc)
    as [mc a acc Hacc'| mc a acc H H' Hacc' HNone  | mc a acc H H' Hacc' mc'' Hmc'' Hind] => //=.
    by rewrite Hacc in Hacc'.
  - have: (None != foldm apply_transitionc mc [seq vsa i | i <- accelerate_seq mc a]).
      by apply: apply_accelerate.
    by rewrite Hacc HNone.
Qed.


Lemma Saturate_no_decrease_omega mc mc' a acc acc':
  (mc', acc') = saturate_markingc mc a acc ->
  leq (omega_possible mc') (omega_possible mc).
Proof.
  functional induction (saturate_markingc mc a acc)
  as [mc a acc Hacc| mc a acc H H' Hacc HNone  | mc a acc H H' Hacc mc'' Hmc'' Hind].
  - by move => [] ->.
  - have: (None != foldm apply_transitionc mc [seq vsa i | i <- accelerate_seq mc a]).
      by apply: apply_accelerate.
    by rewrite Hacc HNone.
  - move/Hind.
    case Hacc_seq: (accelerate_seq mc a) => [|a' l].
      by rewrite Hacc_seq in Hacc.
    move:(accelerate_not_nil Hacc_seq) => [] // [] mc0.
    rewrite -Hacc_seq Hacc Hmc''=> [] [] [] ->.
    rewrite /Order.lt => //= /andP [] _.
    move/leq_omega_possible => Hleq2 Hprefix Hleq1.
    by apply: (leq_trans Hleq1).
Qed.

Lemma  not_saturated mc A:
  ~~ (saturated_markingc mc A) ->
  exists a mc', List.In a A /\
  Some mc' = apply_transitionc mc (vsa a) /\
  mc' != mc.
Proof.
  elim : A mc => [//=| a A Hind] mc //=.
  move/nandP.
  case; last first.
    move/Hind => [] a' [] mc' [] Hin [] Hmc' Hneq.
    exists a', mc'; split => //=; by right.
  move/norP.
  case Hmc': apply_transitionc => [mc'|//=]; last first.
    by inversion 1.
  move => [] .
  case Heq: (mc' == mc).
    by move/eqP : Heq => ->; rewrite eqxx.
  move => _ _; exists a, mc'.
  split => //=.
    by left.
  split => //=.
  by move/eqP/eqP: Heq.
Qed.

Lemma saturate_markingc_acc_seq mc mc' (A s s' s0: seq acceleration):
  (mc',s') = saturate_markingc mc A s ->
  exists s0,
  (mc ={' s0 '}>>> mc' /\
  (forall a, List.In a s0 -> List.In a A) /\
  forall s1 s2 s3 mc1 mc2, s1++s2++s3=s0 -> s2 <> nil -> mc={' s1 '}>>> mc1 ->
  mc1 ={' s2 '}>>> mc2 -> mc1 < mc2)  .
Proof.
  functional induction (saturate_markingc mc A s)
  as [mc a acc Hacc| mc a acc H H' Hacc HNone  | mc a acc H H' Hacc mc'' Hmc'' Hind].
  - move => [] -> _.
    exists nil => //=.
    split => //=; split => //= s1 s2 s3 mc1 mc2.
    by move/nilP; rewrite !cat_nilp => /andP [] _ /andP [] /nilP ->.
  - have: (None != foldm apply_transitionc mc [seq vsa i | i <- accelerate_seq mc a]).
      by apply: apply_accelerate.
    by rewrite Hacc HNone.
  - move/Hind => [] s1 [] Hs1 [] Hin_s1 Hprefix_s1 {Hind}.
    exists ((accelerate_seq mc a)++s1); split.
      by rewrite map_cat foldm_cat Hacc Hmc''.
    split.
      move => a0.
      rewrite List.in_app_iff.
      elim; last first.
        exact:Hin_s1.
      exact: (accelerate_seq_In Logic.eq_refl).
    move => s2 s3 s4 mc1 mc2.
    case Hacc_seq: (accelerate_seq mc a) => [|a' s].
      rewrite cat0s.
      by move: Hacc_seq Hacc => ->.
    move:(accelerate_not_nil Hacc_seq) => [] // [] mc3 [] Hmc3 Hlt Hprefix_acc_seq Hs.
    move: (cat_prefix Hs) => [] s5.
    elim => Hs5; last first.
      move: Hs5 Hs => <-; rewrite -!catA => /catL.
      rewrite map_cat foldm_cat -Hacc_seq Hacc Hmc'' //=.
      exact: Hprefix_s1.
    move: Hs; rewrite -Hs5 -catA => /catL Hs.
    move: (cat_prefix Hs) => [] s6.
    elim => Hs6.
      move:  Hs6 Hs5 => <- Hs6 Hs3 Hmc1 Hmc2.
      exact: (Hprefix_acc_seq s2 s3 s6).
    generalize dependent s6; move => [| a6 s6 Hs6].
      rewrite cats0 => Heq; subst s5.
      by apply: (Hprefix_acc_seq s2 s3 nil); rewrite cats0.
    move: Hs6 Hs => <-.
    rewrite -catA => /catL => H1 _ Hs2.
    move: Hmc'' ;rewrite -Hacc Hacc_seq -Hs5.
    move: s5 Hs5 => [|a5 s5] Hs5.
      rewrite cats0 cat0s Hs2 => [] [] ->.
      exact: (Hprefix_s1 nil _ s4).
    move => Hmc'' Hmc2.
    apply: (@Order.POrderTheory.lt_trans _ _ mc'').
      apply: (Hprefix_acc_seq s2 (a5::s5) nil) => //=.
        by rewrite -Hs5 cats0.
      by move: Hmc''; rewrite map_cat foldm_cat Hs2.
    apply: (Hprefix_s1 nil (a6::s6) s4) => //=.
    move: Hs2 Hmc'' Hmc2; rewrite !map_cat foldm_cat //= => -> //=.
    by case: (apply_transitionc mc1) => //= mc5; rewrite foldm_cat //= => ->.
Qed.


End omegaR.
End acceleration.
End abstraction.
End omega_transition.
End transitionc.
End Extension_transition.