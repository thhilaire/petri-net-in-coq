(**************************************************************)
(*   Copyright (c) 2024 Thibault Hilaire                      *)
(*                      David Ilcinkas                        *)
(*                      Jerome Leroux                         *)                           
(*   Authors : Thibault Hilaire [*]                           *)
(*             David Ilcinkas [+,*]                           *)
(*             Jerome Leroux  [+,*]                           *)
(*                                                            *)
(*                             [*] Affiliation Univ. Bordeaux *)
(*                             [+] Affiliation CNRS           *)
(**************************************************************)
(*      This file is distributed under the terms of the       *)
(*                      MIT License                           *)
(**************************************************************)


From mathcomp Require Import all_ssreflect zify.
From Coq Require Import Relations.
From AlmostFull.PropBounded Require Import AlmostFull AFConstructions.
Import Order.TTheory.
From Petri_Nets_in_Coq.Karp_Miller Require Import orderext petrinet pnkmaccel.
From Petri_Nets_in_Coq.Tools Require Import Utils.
From Petri_Nets_in_Coq.MinCov_AbstractMinCov Require Import New_transitions  KMTrees AbstractMinCov Completeness.
From Coq Require Import Arith Lia Wellfounded FunInd Recdef.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope order_scope.
Local Open Scope petri_net_scope.

Section Correction.


Variable pn : petri_net.


Notation place := (place pn).
Notation marking := (marking pn).
Notation markingc := (markingc pn).
Notation transition := (transition pn).
Notation transitionc:= (transitionc pn).
Notation omega_transition:= (omega_transition pn).
Notation abstraction := (abstraction pn).
Notation acceleration := (acceleration pn).
Notation apply_transitionc := (@apply_transitionc pn).
Notation t_to_abstraction := (@t_to_abstraction pn).
Notation good_compoc := (@good_compoc pn).
Notation nil_transitionc :=(nil_transitionc pn).
Notation nil_abstraction :=(nil_abstraction pn).
Notation address := (address pn).
Notation KMTree := (KMTree pn).
Notation Empty := (Empty pn).
Notation Rel_small_step:=(@Rel_small_step pn).




Definition clover (m0 : marking)  (l : seq markingc ) :=
  antichain l /\
  forall m : marking,
    coverable m0 m <-> exists mc:markingc, (mc \in l) && (m \in mc).

Definition Flattening_mc (mc : markingc) : marking:=
  [ ffun p => match (mc p) with
  | None => 0
  | Some n => n end].

(*
  We define the function that given an omega-marking, computes
  the maximal constant of this omega-marking
*)
Definition Max_cst_mc (mc : markingc) : nat :=
  \max_( p <- enum place ) ((Flattening_mc mc) p).

Lemma Max_cst_mcE (mc: markingc):
  forall p, (mc p == None) || (mc p <= Some (Max_cst_mc mc)).
Proof.
  move => p.
  have: (mc p == None \/ exists n:nat, mc p == Some n).
    move: (mc p) => [n|] //=.
      by right; exists n.
    by left.
  case => [ /eqP -> //=| [] n /eqP Hp ].
  rewrite Hp //= /Order.le //= /Max_cst_mc.
  have: Flattening_mc mc p = n.
    by rewrite /Flattening_mc ffunE Hp.
  move => <-.
  apply: leq_bigmax_seq => //=.
  exact: mem_enum.
Qed.

Definition Max_cst_seq_mc (s : seq markingc) : nat :=
  \max_( mc <- s) (Max_cst_mc mc).

Lemma Max_cst_seq_mcE (s: seq markingc):
  forall p mc, mc \in s -> (mc p == None) || (mc p <= Some (Max_cst_seq_mc s)).
Proof.
  move => p mc Hin.
  move/orP: (Max_cst_mcE mc p) => [-> //=|] Hleq .
  apply/orP; right.
  apply: (Order.POrderTheory.le_trans Hleq).
  rewrite /Order.le //=.
  exact: leq_bigmax_seq.
Qed.

Lemma In_ideal_seq (mc: markingc) (s : seq markingc):
  (forall m,m \in mc -> exists mc': markingc, (mc' \in s)&& (m \in mc') ) ->
  exists mc', (mc' \in s) && (mc <= mc') .
Proof.
  move => Hforall.
  remember (Max_cst_seq_mc s) as c.
  set m: marking := [ ffun p => match (mc p) with
  | None => c.+1
  | Some n => n
  end].
  move: (Hforall m) => [| mc' /andP [] Hin Hleq].
    apply/forallP => p.
    rewrite ffunE.
    case: (mc p) => //= n.
    by rewrite mem_ncP.
  exists mc'; rewrite Hin //=.
  apply/forallP => p.
  have: (mc p == None \/ exists n:nat, mc p == Some n).
    move: (mc p) => [n|] //=.
      by right; exists n.
    by left.
  move => [/eqP Hp|[] n /eqP Hp]; last first.
    move/forallP/(_ p):Hleq.
    by rewrite ffunE Hp mem_ncP.
  move/orP: (Max_cst_seq_mcE  p Hin) => [/eqP -> //=|].
  rewrite -Heqc.
  move/forallP/(_ p):Hleq.
  rewrite ffunE Hp mem_ncP //= => Hleq1 Hleq2.
  move: (mc' p) Hleq1 Hleq2 => [n|//=].
  rewrite /Order.le //= => Hleq1 Hleq2.
  by move: (leq_trans Hleq1 Hleq2); rewrite ltnn.
Qed.


Theorem clover_unique m0 (l1 l2: seq markingc) : clover m0 l1 -> clover m0 l2 -> perm_eq l1 l2.
Proof.
  move => [] Hac1 Hl1 [] Hac2 Hl2.
  apply/allP => mc.
  rewrite mem_cat => /orP [] Hin //=.
  all:rewrite eqn_leq; apply/andP; split.
  all:apply/leq_uniq_countP => //=.
  1,3,4,5: exact: antichain_uniq.
  all: move => _.
  all: generalize dependent l1; generalize dependent l2.
  1: move => lt Hac_lt Hlt ls Hac_ls Hls Hin.
  2: move => ls Hac_ls Hls Hin lt Hac_lt Hlt.
  all: move: (@In_ideal_seq mc lt) => [].
  1,3:move => m Hleq.
  1,2:by rewrite -(Hlt m) (Hls m); exists mc; rewrite Hin Hleq.
  all:move => mc2/andP [] Hin2 Hleq.
  all:move: (@In_ideal_seq mc2 ls) => [].
  1,3:move => m Hleq2.
  1,2:by rewrite -(Hls m) (Hlt m); exists mc2; rewrite Hin2 Hleq2.
  all:move => mc3 /andP [] Hin3 Hleq2.
  all:case Heq: (mc == mc3); last first.
  1,3:move/negP/negP in Heq.
  1,2:move: (In_same_antichain Hin Hin3 Heq Hac_ls).
  1,2:by move: (Order.POrderTheory.le_trans Hleq Hleq2) => ->.
  all:move/eqP in Heq; subst.
  all:have: (mc3 <= mc2 <= mc3).
  1,3:by apply/andP;split.
  all:by move/(Order.POrderTheory.le_anti) => ->.
Qed.



Lemma No_Front_Correctness T A (m0 : marking):
  clos_refl_trans_1n _ Rel_small_step (T,A) (KMTree_init m0) ->
  No_Front T ->
  clover m0 (Markings_of_T T).
Proof.
  move => HRel.
  move: (cls_Rel_small_step_keep_fl HRel (Front_leaves_init m0)) => Hfl.
  move: (cls_Rel_small_step_keep_antichain HRel (Nfa_init m0) (Front_leaves_init m0)) => Hnfa.
  move: (cls_Rel_small_step_keep_cons HRel (Cons_init nil m0) (Front_leaves_init m0)) => Hcons.
  move: (cls_Rel_small_step_keep_consistent_head HRel (Cons_head_init nil m0)) => Hcons_h HnoFront.
  rewrite No_front_Markings_of_T //=; split => //= m; split.
    move/(Rel_small_step_all_covered HRel HnoFront) => [] mc [] /inv_Markings_of [] ad Hmc Hleq.
    exists mc; apply/andP; split => //=.
    apply: inv2_Markings_not_front => //=.
      by rewrite Hmc.
    exact: No_Front_abs.
  move  => [] mc /andP [] /inv_Markings_not_front [] ad [] /inv2_Markings_of Hin Hnfl.
  exact: (cover_consistent_KMTree Hcons Hcons_h).
Qed.


Theorem Correctness T A (m0: marking):
  clos_refl_trans_1n _ Rel_small_step (T,A) (KMTree_init m0) ->
  (forall T' A', ~ Rel_small_step (T',A') (T,A)) ->
  clover m0 (Markings_of_T T).
Proof.
  move => HRel.
  move: (cls_Rel_small_step_keep_fl HRel (Front_leaves_init m0)) => Hfl.
  move: (cls_Rel_small_step_keep_antichain HRel (Nfa_init m0) (Front_leaves_init m0)) => Hnfa.
  move: (cls_Rel_small_step_keep_cons HRel (Cons_init nil m0) (Front_leaves_init m0)) => Hcons.
  move: (cls_Rel_small_step_keep_consistent_head HRel (Cons_head_init nil m0)) => Hcons_h HnoRel.
  move: (term_Rel_small_step_No_Front Hfl Hnfa Hcons HnoRel) => HnoFront.
  exact: (No_Front_Correctness HRel).
Qed.


End Correction.
