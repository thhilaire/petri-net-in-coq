(**************************************************************)
(*   Copyright (c) 2024 Thibault Hilaire                      *)
(*                      David Ilcinkas                        *)
(*                      Jerome Leroux                         *)                           
(*   Authors : Thibault Hilaire [*]                           *)
(*             David Ilcinkas [+,*]                           *)
(*             Jerome Leroux  [+,*]                           *)
(*                                                            *)
(*                             [*] Affiliation Univ. Bordeaux *)
(*                             [+] Affiliation CNRS           *)
(**************************************************************)
(*      This file is distributed under the terms of the       *)
(*                      MIT License                           *)
(**************************************************************)


From mathcomp Require Import all_ssreflect zify.
From Coq Require Import Relations.
Import Order.TTheory.
From Petri_Nets_in_Coq.Karp_Miller Require Import orderext monad petrinet pnkmaccel.
From Petri_Nets_in_Coq.Tools Require Import Utils.
From Petri_Nets_in_Coq.MinCov_AbstractMinCov Require Import New_transitions KMTrees AbstractMinCov KMTE.
From Coq Require Import Arith Lia Wellfounded.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope order_scope.
Local Open Scope petri_net_scope.

Section Rel.

Variable pn : petri_net.


Notation place := (place pn).
Notation marking := (marking pn).
Notation markingc := (markingc pn).
Notation transition := (transition pn).
Notation transitionc:= (transitionc pn).
Notation omega_transition:= (omega_transition pn).
Notation abstraction := (abstraction pn).
Notation acceleration := (acceleration pn).
Notation nil_abstraction :=(nil_abstraction pn).
Notation apply_transitionc :=(@apply_transitionc pn).
Notation t_to_abstraction := (@t_to_abstraction pn).
Notation good_compoc := (@good_compoc pn).
Notation KMTree := (KMTree pn).
Notation Empty := (Empty pn).
Notation Br := (KMTrees.Br).
Notation address := (address pn).
Notation Rel_small_step := (@Rel_small_step pn).
Notation KMTE := (KMTE pn).

Definition vsv : abstraction -> transitionc := fun a: abstraction => val (sval a).
Notation vsa := (vsv \o val) .
Notation "A> T" :=(Anonymisation T) (at level 70, T at next level): petri_net_scope.



(* So we define here the relation corresponding to MinCov *)

Variant Rel  : (KMTE * seq acceleration) -> ( KMTE * seq acceleration) -> Prop :=
| Rel_clean (T:KMTE) A ad T': Is_Front T ad
    -> T'= saturate_KMTree A T ad
    -> ad_covered_not_front T' ad
    -> Rel (removeE_add T' ad,A) (T,A)
| Rel_accel (T:KMTE) A ad T' ad' a: Is_Front T ad
    -> T'= saturate_KMTree A T ad
    -> ~~ ad_covered_not_front T' ad
    -> Possible_acceleration T' ad' ad
    -> a = computingE_acceleration T' ad' ad
    -> Rel (to_FrontE T' ad', a :: A) (T,A)
| Rel_explo (T:KMTE) A ad T' mc: Is_Front T ad
    -> T'= saturate_KMTree A T ad
    -> ~~ ad_covered_not_front T' ad
    -> No_Possible_acc T' ad
    -> Some mc = m_from_add T' ad
    -> Rel (Front_extensionE (removeE_strict_covered T' mc) ad, A) (T,A)
.


Lemma Rel_keep_consistent (T1 T2 : KMTE) A1 A2:
  Rel (T1,A1) (T2,A2) -> 
  consistentE_tree T2 ->
  Front_leaves T2 -> 
  consistentE_tree T1.
Proof.
  inversion_clear 1 as
  [ T A ad T' HFront HT' Had |
    T A ad T' ad' a HFront HT' Hncov Hacc Ha |
    T A ad T' mc HFront HT' Hncov Hnacc Hmc ] => Hcons Hfl.
  - apply: removeE_keeps_consistentE.
    rewrite HT'. 
    exact: saturate_keeps_consistentE.
  - apply: To_FrontE_keeps_consistentE.
    rewrite HT'. 
    exact: saturate_keeps_consistentE.
  - apply: Front_extensionE_keeps_consistentE.
    rewrite HT'.
    apply: removeE_strict_covered_keeps_consistentE. 
    exact: saturate_keeps_consistentE.
Qed.

Lemma Rel_keep_consistent_head (T1 T2 : KMTE) A1 A2 mc0:
  Rel (T1,A1) (T2,A2) -> 
  consistentE_head mc0 T2 ->
  Front_leaves T2 -> 
  consistentE_head mc0 T1.
Proof.
  inversion_clear 1 as
  [ T A ad T' HFront HT' Had |
    T A ad T' ad' a HFront HT' Hncov Hacc Ha |
    T A ad T' mc HFront HT' Hncov Hnacc Hmc ] => Hcons Hfl.
  - apply: removeE_keeps_consistentE_head.
    rewrite HT'. 
    exact: saturate_keeps_consistentE_head.
  - apply: To_FrontE_keeps_consistentE_head.
    rewrite HT'. 
    exact: saturate_keeps_consistentE_head.
  - apply: Front_extensionE_keeps_consistentE_head.
    rewrite HT'.
    apply: removeE_strict_covered_keeps_consistentE_head. 
    exact: saturate_keeps_consistentE_head.
Qed.



(*
  We will show now that Rel is simulated by our Rel_small_step, for this we will:
  - show that the saturating process can be simulated by some steps of Rel_small_steps
  - show that removeE_strict_covered can be simulated by some steps of Rel_small_steps
  - and then show that any other operation is just one step of Rel_small_step
*)

(* Saturation case *)

Inductive rel_sat : (KMTree * seq acceleration) -> ( KMTree * seq acceleration) -> Prop :=
| rel_small_sat T A A' ad mc acc mc': Is_Front T ad
    -> List.In acc A
    -> Some mc = m_from_add T ad
    -> Some mc' = apply_transitionc mc (vsa acc)
    -> mc' != mc
    -> rel_sat (saturate_a_little acc T ad, A'++A) (T,A)
.

Lemma Inc_sat_small_step :
  inclusion _ rel_sat Rel_small_step.
Proof.
  move => [] T' A' [] T A.
  inversion_clear 1 as [ T1 A1 A2 ad mc acc mc' Hfront Hacc Hmc Hmc' Hneq].
  exact: (@Rel_small_step_sat _ _ _ A2 ad mc acc mc').
Qed.




Lemma Rel_small_subtree t mc0 b0 (f0:{ffun transition -> KMTree})  T T' A A':
  rel_sat (T',A') (T,A) ->
  rel_sat (Br mc0 b0 [ffun t0 => if t0==t then T' else (f0 t0)], A')
                 (Br mc0 b0 [ffun t0 => if t0==t then T else (f0 t0)], A).
Proof.
  inversion_clear 1 as
  [ T1 A1 A2 ad mc acc mc' Hfront Hacc Hmc Hmc' Hneq ].
  remember [ffun t0 => if t0 == t then T else f0 t0] as f1.
  suff : Br mc0 b0 [ffun t0 => if t0 == t then saturate_a_little acc T ad else f0 t0] =
          saturate_a_little acc (Br mc0 b0 f1 ) (t::ad).
    move => ->.
    apply: (@rel_small_sat _ _ A2 (t::ad) mc acc mc') => //=.
    1,2: by rewrite Heqf1 ffunE eqxx.
  rewrite saturate_a_little_Br; apply f_equal.
  apply/ffunP => t0.
  rewrite !ffunE.
  case Heq: (t0==t).
  all: by rewrite Heqf1 !ffunE Heq.
Qed.

Lemma Clrt_Rel_small_subtree t mc0 b0 (f0:{ffun transition -> KMTree})  T A A':
  clos_refl_trans _ rel_sat (T,A') (f0 t,A) ->
  clos_refl_trans _ rel_sat (Br mc0 b0 [ffun t0 => if t0==t then T else (f0 t0)], A')
                 (Br mc0 b0 f0, A).
Proof.
  remember ((f0 t),A) as T1.
  remember (T,A') as T2.
  have :T = T2.1 /\ A' = T2.2 .
    by rewrite HeqT2.
  have: (f0 t) = T1.1 /\ A = T1.2.
    by rewrite HeqT1.
  have Hf0:([ffun t0 => if t0==t then f0 t else f0 t0] = f0).
    apply/ffunP => t0; rewrite ffunE.
    case Heq:(t0 == t) => //=.
    by move/eqP: Heq => ->.
  rewrite -{3}Hf0 => [] [] -> -> [] -> ->.
  clear HeqT1 HeqT2 Hf0.
  elim => [ [x1 A1] [x2 A2] HR | x | x y z Hxy Hindxy Hyz Hindyz].
  - by apply rt_step, Rel_small_subtree.
  - exact: rt_refl.
  - apply: rt_trans.
      apply: Hindxy.
    apply: Hindyz.
Qed.


Lemma Saturate_Rel_small_step (T:KMTE) A ad:
  Is_Front T ad ->
  (clos_refl_trans _ Rel_small_step) (A>saturate_KMTree A T ad,A) (A>T,A).
Proof.
  move => HFront.
  apply: (Rel_clos_trans_inc Inc_sat_small_step).
  elim: ad T HFront => [| t ad Hind] [ //=| mc acc b f]; last first.
  - rewrite saturate_KMTree_Br  //=.
    have:[ffun t0 => A> [ffun t1 => if t1 == t then saturate_KMTree A (f t1) ad else f t1] t0] =
    [ffun t0 => if t0 == t then A> saturate_KMTree A (f t0) ad else A> f t0].
      apply/ffunP => t0; rewrite !ffunE.
      by case (t0 ==t).
    move => ->.
    remember [ffun t0 => A> f t0] as f1.
    have :forall t,(A> (f t)) = f1 t.
      move => t0.
      by rewrite Heqf1 ffunE.
    move => Hf1.
    have :[ffun t0 => if t0 == t then A> saturate_KMTree A (f t0) ad else A> f t0] =
    [ffun t0 => if t0 == t then A> saturate_KMTree A (f t) ad else f1 t0].
      apply/ffunP => t0.
      rewrite !ffunE.
      case Heq: (t0==t) => //=.
      by move/eqP: Heq => ->.
    move => -> HFront.
    apply (@Clrt_Rel_small_subtree t mc b); rewrite -(Hf1 t) .
    apply:Hind.
    by move: HFront; rewrite ffunE .
  - rewrite /saturate_KMTree //=  => /eqP ->.
    remember (saturate_markingc mc A acc).1 as mc'.
    move: (@saturate_markingc_acc_seq _ mc mc'
    A acc (saturate_markingc mc A acc).2 ) => [] .
    - apply nil.
    - by rewrite Heqmc';move: (saturate_markingc mc A acc) => [].
    - move => s {Heqmc'}.
      elim: s mc' mc A acc b f => [| a s Hind]  mc' mc A acc b f.
        move => //= [] [] <- _.
        exact: rt_refl.
      rewrite map_cons //=.
      case Hmca: (apply_transitionc mc a) => [mca //=| //=]; last first.
        by move => [].
      move => [] Hsat [] Hall_in Hprefix.
      have Hmc_mca: mc < mca.
        apply:(Hprefix nil [::a] s mc mca) => //=.
        by rewrite Hmca.
      apply: (@rt_trans _ _ _ (Br mca true [ffun t => A> f t],A) ); last first.
        apply: rt_step.
        have: (Br mca true [ffun t => A> f t]) = (saturate_a_little a (Br mc true [ffun t => A> f t]) nil).
          rewrite /saturate_a_little Hmca.
          by move: Hmc_mca;rewrite /Order.lt //= => /andP [] /negbTE ->.
        move => ->.
        rewrite -{1}(cat0s A).
        apply:(rel_small_sat) => //.
        + by apply: Hall_in; left.
        + by rewrite Hmca.
        + by move: Hmc_mca;rewrite /Order.lt //= => /andP [] /negbTE ->.
      apply:Hind => //=.
      split => //=; split => //=.
        move => a0 Hin.
        by apply: Hall_in; right.
      move => s1 s2 s3 mc1 mc2 Hs Hs2 Hmc1 Hmc2.
      apply:(Hprefix (a::s1) s2 s3) => //=.
        by rewrite Hs.
      by rewrite Hmca.
Qed.

(* removeE_strict_covered case *)

Inductive rel_clean : (KMTree * seq acceleration) -> ( KMTree * seq acceleration) -> Prop :=
| rel_small_clean T A A' ad mc ad' (mc': markingc): Is_Front T ad
    -> Some mc = m_from_add T ad
    -> Some mc' = m_from_add T ad'
    -> mc' <= mc
    -> ~~ prefix ad' ad
    -> rel_clean (remove_add T ad', A'++A) (T,A) .

Lemma Inc_clean_small_step :
  inclusion _ rel_clean Rel_small_step.
Proof.
  move => [] T' A' [] T A.
  inversion_clear 1 as [ T1 A1 A2 ad mc acc mc' Hfront Hacc Hmc Hmc' Hneq].
  exact: (@Rel_small_step_cov _ _ _ A2 ad mc acc mc').
Qed.

(*
  We define a function that gives us the list of addresses of a KMTE T that
  will be deleted by removeE_strict_covered T mc
*)

Fixpoint Aux_seq_ad_lt_m T mc ad : seq address := match T with
  | Empty_E => nil
  | Br_E mc0 acc0 b0 f0 => if mc0 < mc
                           then [::ad]
                           else
                           flatten [ seq (Aux_seq_ad_lt_m (f0 t) mc (rcons ad t) ) | t <- (enum transition)]
end.

Definition Seq_ad_lt_m T mc := Aux_seq_ad_lt_m T mc nil.

Lemma Aux_seq_ad_lt_m_Br mc0 acc0 b0 f0 mc ad:
  Aux_seq_ad_lt_m (Br_E mc0 acc0 b0 f0) mc ad = (if mc0 < mc
  then  [::ad]
  else flatten [seq Aux_seq_ad_lt_m (f0 t) mc (rcons ad t) | t <- enum transition]) .
Proof.
  by [].
Qed.


Lemma Aux_seq_ad_lt_m_prefix T mc s ad:
  ad \in (Aux_seq_ad_lt_m T mc s) ->
  prefix s ad.
Proof.
  elim: T mc s ad => [//=| mc0 acc0 b0 f0 Hind] mc s ad.
  rewrite Aux_seq_ad_lt_m_Br.
  case: (mc0 < mc).
    rewrite in_cons => /orP.
    elim => //=.
    by move/eqP => ->; apply prefix_refl.
  move/In_flatten_codom => [] t /Hind /prefixP [] s' Hs'.
  apply/prefixP; exists (t::s').
  by rewrite Hs'-cat_rcons.
Qed.


Lemma Aux_seq_ad_lt_m_consistency T mc s ad:
  ad \in (Aux_seq_ad_lt_m T mc s) ->
  exists mc' ad' , Some mc' = m_from_add T ad'  /\
  mc' < mc /\
  (s++ad') = ad.
Proof.
  elim: T mc s ad => [//=| mc0 acc0 b0 f0 Hind] mc s [|t ad].
  all:rewrite Aux_seq_ad_lt_m_Br.
    case Hmc0: (mc0 < mc).
      rewrite in_cons => /orP.
      elim => //= /eqP ->.
      by exists mc0 ,nil; rewrite cats0.
    move/In_flatten_codom => [] t' /Aux_seq_ad_lt_m_prefix.
    rewrite prefixs0 => /eqP /nilP.
    by rewrite /nilp size_rcons.
  case Hmc0:(mc0<mc).
    rewrite in_cons => /orP.
    elim => //=.
      by move/eqP => ->; exists mc0, nil; rewrite cats0.
  move/In_flatten_codom => [] t' /Hind [] mc' [] ad' [] Hmc' [] Had' <-.
  by exists mc', (t'::ad') => //=; rewrite ffunE cat_rcons.
Qed.

Lemma Aux_seq_ad_lt_m_inversion (T:KMTE) mc mc' s ad:
  Some mc' = m_from_add T ad  ->
  mc' < mc ->
  (Aux_seq_ad_lt_m T mc s) != nil.
Proof.
  elim: ad T mc mc' s => [|t ad Hind] [//=|mc0 acc0 b0 f0] mc mc' s.
    by move => //= [] -> ->.
  rewrite m_from_add_Br -/Anonymisation ffunE  => Hmc' Hlt //=.
  case : (mc0 < mc) => //=.
  move/(_ (f0 t) mc mc' (rcons s t) Hmc' Hlt) in Hind.
  apply/negP; rewrite eq_sym => /eqP /flatten0 H.
  move/negP in Hind.
  apply:Hind; apply/eqP.
  apply:H; rewrite -codomE.
  exact: codom_f.
Qed.


Lemma Aux_seq_ad_lt_m_abs mc0 mc acc0 b0 f0 s ad:
  (s++ad) \in (Aux_seq_ad_lt_m (Br_E mc0 acc0 b0 f0) mc s) ->
  ad!=nil ->
  (mc0 < mc) = false.
Proof.
  move => //=.
  case (mc0 < mc) => //=.
  rewrite in_cons => /orP [] //=.
  by move/eqP; rewrite -{2}(cats0 s) => /catL ->.
Qed.

Lemma Aux_seq_ad_lt_m_remove ad s T mc:
  Aux_seq_ad_lt_m T mc s = [::(s++ad)] ->
  Aux_seq_ad_lt_m (removeE_add T ad) mc s = nil.
Proof.
  elim: ad T s mc => [//=|t ad Hind] [//=| mc0 acc0 b0 f0] s mc //=.
  case: (mc0 < mc) => //=.
    inversion 1 as [Hs].
    by move: Hs; rewrite -{1}(cats0 s)=> /catL.
  move => H.
  apply: flatten_codom_nil => t0.
  move/codom_flatten1: H => [] t1 [] Ht1 Haux.
  have: (s++t::ad) \in Aux_seq_ad_lt_m (f0 t1) mc (rcons s t1).
    by rewrite Ht1 in_cons eqxx.
  move/Aux_seq_ad_lt_m_prefix => /prefixP [] s'.
  rewrite cat_rcons => /catL.
  inversion 1; subst.
  rewrite ffunE.
  case Heq: (t0==t1).
    by move/eqP: Heq => ->;apply:Hind; rewrite cat_rcons.
  apply:Haux.
    exact:mem_enum.
  by apply/negbT; rewrite eq_sym.
Qed.

Lemma Aux_seq_ad_lt_m_removeE ad s s' T mc:
  (s'++ad)::s = Aux_seq_ad_lt_m T mc s' ->
  s= Aux_seq_ad_lt_m (removeE_add T ad) mc s' .
Proof.
  move =>H.
  have: (s' ++ ad \in Aux_seq_ad_lt_m T mc s').
    by rewrite -H in_cons eqxx.
  move/Aux_seq_ad_lt_m_consistency => [] mc' [] ad' [] Hmc' [] Hlt /catL Had;subst.
  move: Hmc' H.
  elim: ad T s s' mc mc' Hlt => [|t ad Hind] [//=|mc0 acc0 b0 f0] // s s' mc mc' Hlt.
    by move => //= [] <-; rewrite Hlt; inversion 1.
  rewrite m_from_add_Br -/Anonymisation ffunE=> Hmc' H.
  have Hlt2: (mc0< mc)= false.
    apply: (@Aux_seq_ad_lt_m_abs mc0 mc acc0 b0 f0 s' (t::ad)) => //.
    by rewrite -H in_cons eqxx.
  move: H => //=; move: Hlt2 => ->.
  move: (Dec_enum t) => [] l1 [] l2 [] Hl1 [] Hl2 ->.
  rewrite !map_cat !map_cons ffunE eqxx !flatten_cat.
  have: ( [seq Aux_seq_ad_lt_m  ([ffun t1 => if t1 == t then removeE_add (f0 t1) ad else f0 t1] t0)
  mc (rcons s' t0) | t0 <- l1]) = [seq Aux_seq_ad_lt_m (f0 t0) mc (rcons s' t0) | t0 <- l1 ].
    by apply eq_in_map => t1 /Hl1 /negbTE; rewrite ffunE => ->.
  move => ->.
  have: ( [seq Aux_seq_ad_lt_m  ([ffun t1 => if t1 == t then removeE_add (f0 t1) ad else f0 t1] t0)
  mc (rcons s' t0) | t0 <- l2]) = [seq Aux_seq_ad_lt_m (f0 t0) mc (rcons s' t0) | t0 <- l2 ].
    by apply eq_in_map => t1 /Hl2 /negbTE; rewrite ffunE => ->.
  move => ->.
  case Hl: (flatten [seq Aux_seq_ad_lt_m (f0 t0) mc (rcons s' t0) | t0 <- l1]) => [|a l]; last first.
    rewrite cat_cons; inversion 1 as [ [Ha Hs]].
    have: (a \in flatten [seq Aux_seq_ad_lt_m (f0 t0) mc (rcons s' t0) | t0 <- l1]).
      by rewrite Hl in_cons eqxx.
    move/flattenP => [] s0.
    move/mapP => [] t0 Hint0 -> /Aux_seq_ad_lt_m_prefix => /prefixP [] s1.
    move: Ha => <-; rewrite cat_rcons => /catL.
    inversion 1.
    by move/Hl1: Hint0; subst; rewrite eqxx.
  rewrite !cat0s //= -cat1s => H.
  move: (cat_prefix H) => [] s0.
  elim => Hs0.
    move/(_ (f0 t) s0 (rcons s' t) mc mc' Hlt Hmc'): Hind.
    rewrite cat_rcons -cat1s Hs0; move/(_ Logic.eq_refl) => <-.
    by move: Hs0 H => <-; rewrite -catA => /catL.
  elim : (cat1 Hs0); last first.
    move => [] {}H.
    by move:H (Aux_seq_ad_lt_m_inversion (rcons s' t) Hmc' Hlt) => ->.
  move => [] H1 {}Hs0.
  move: H; rewrite H1 => /catL ->.
  by rewrite Aux_seq_ad_lt_m_remove ?cat0s //= cat_rcons.
Qed.


Lemma nil_aux_seq_ad_lt_m T mc s:
  nil = Aux_seq_ad_lt_m T mc s ->
  removeE_strict_covered T mc = T.
Proof.
  elim : T mc s => // mc0 acc0 b0 f0 Hind mc s.
  rewrite Aux_seq_ad_lt_m_Br /removeE_strict_covered -/removeE_strict_covered  -codomE.
  case: (mc0 < mc).
    by inversion 1.
  move/flatten0 => Hind'.
  apply f_equal; apply/ffunP => t; rewrite ffunE.
  apply:Hind.
  symmetry.
  apply:Hind'.
  exact:codom_f.
Qed.

Lemma removeE_strict_removeE (T:KMTE) ad mc mc':
  Some mc' = m_from_add T ad ->
  mc' < mc ->
  removeE_strict_covered T mc = (removeE_strict_covered  (removeE_add T ad) mc).
Proof.
  elim: ad T mc mc' => [|//= t ad Hind] [//=| mc0 acc0 b0 f0] mc mc'.
    by move => [] -> //= ->.
  rewrite  //= ffunE.
  case: (mc0 < mc) => //= Hmc' Hlt.
  apply: f_equal; apply/ffunP => t0; rewrite !ffunE.
  case Heq: (t0==t) => //=; move/eqP: Heq => ->.
  exact:(Hind _ _ mc').
Qed.


Lemma remove_strict_cover_Rel_small_step (T:KMTE) A ad mc:
  Is_Front T ad ->
  No_Possible_acc T ad ->
  some mc = m_from_add T ad ->
  (clos_refl_trans _ Rel_small_step) (A>removeE_strict_covered T mc ,A) (A>T,A).
Proof.
  move =>HFront HnPoss Hmc.
  apply: (Rel_clos_trans_inc Inc_clean_small_step).
  remember (Seq_ad_lt_m T mc) as l.
  move: l T A ad mc HFront HnPoss Hmc Heql.
  elim => [|s l Hind] [//=| mc0 acc0 b0 f0] A ad mc HFront HnPoss Hmc .
    move/nil_aux_seq_ad_lt_m => ->.
    exact: rt_refl.
  move => H.
  have: (s \in Seq_ad_lt_m (Br_E mc0 acc0 b0 f0) mc).
    by rewrite -H in_cons eqxx.
  move/Aux_seq_ad_lt_m_consistency => [] mc' [] ad' [] Hmc' []Hlt Hs.
  move: (removeE_strict_removeE Hmc' Hlt) => ->.
  have Hneq: ~~(ad ==ad').
    apply/negP; move/eqP => Heq; subst.
    move: Hmc Hmc' Hlt => <- [] -> .
    by rewrite /Order.lt //= eqxx.
  have Hnprefix:  ~~ prefix ad' ad.
    apply/negP => Hprefix.
    exact: (@HnPoss ad' mc' mc).
  apply:(@rt_trans _ _ _  (A> removeE_add (Br_E mc0 acc0 b0 f0) ad',A) ).
    apply:( Hind _ _ ad).
    - by rewrite /KMTE_to_Kmtree removeEC -remove_keeps_front.
    - by rewrite /KMTE_to_Kmtree removeEC; apply No_poss_remove.
    - by rewrite /KMTE_to_Kmtree removeEC -remove_keeps_m_from_ad.
    - by apply Aux_seq_ad_lt_m_removeE; rewrite Hs.
  rewrite removeEC.
  apply: rt_step.
  apply: (rel_small_clean A nil HFront Hmc Hmc') => //=.
  by move/andP: Hlt => [] _.
Qed.

(*
  Now we show that the last case of Rel implies that, if you have a tree
  satisfying Not_Front_Antichain, then the markingc you will remove from
  front will form an antichain with the other non-front markingcs
*)

Lemma Rel_ext_nfa (T:KMTE) ad mc:
  Is_Front T ad ->
  Front_leaves T->
  ~~ ad_covered_not_front T ad ->
  No_Possible_acc T ad ->
  Some mc = m_from_add T ad ->
  Not_Front_Antichain T ->
  Not_Front_Antichain (remove_Front (removeE_strict_covered T mc) ad ).
Proof.
  case: ad T mc => [//=| t ad ] [//=| mc0 acc0 b0 f0] mc.
    move => //= /eqP -> /Front_leaves_invariant Hfl _ _ [] -> _.
    rewrite /Order.lt //= eqxx //= /Not_Front_Antichain Markings_not_Front_Br flatten_codom_nil//=.
    move => t; move/(_ mc t) : Hfl; rewrite !ffunE.
    by case: (f0 t).
  move => Hfront Hfl Hncov Hnpos Hmc.
  apply: contraLR => //=.
  case Hlt: (mc0 < mc) => // Hnfa.
  have Hncov': ( No_Possible_acc (A>(f0 t)) ad).
    move => ad' mc1 mc2 Hmc1 Hmc2 Hprefix.
    by apply: (Hnpos (t::ad')  mc1 mc2) => //=; rewrite ?ffunE ?Hprefix ?eqxx .
  move: (@Nfa_remove_Front_inv  _
  (A> Br_E mc0 acc0 b0 [ffun t => removeE_strict_covered (f0 t) mc]) (t::ad) mc) => [] //.
  - move: Hfront Hmc; rewrite m_from_add_Br !Is_front_Br -/Anonymisation !ffunE => Hfront Hmc.
    exact: removeE_strict_covered_is_front.
  - move: Hfl => //=.
    case: (b0).
      move => /Front_leaves_invariant => Hfl.
      apply/forallP => t0; move: (Hfl mc t0); rewrite !ffunE.
      by case (f0 t0).
    move/forallP => Hfl; apply/forallP => t0.
    move/(_ t0): Hfl; rewrite !ffunE.
    exact: removeE_strict_covered_fl.
  - move: Hmc; rewrite !m_from_add_Br -/Anonymisation !ffunE => Hmc.
    exact: removeE_strict_covered_mfad.
  - apply contra, antichain_subseq.
    case b0 => //=.
    rewrite eqxx.
    apply: subseq_codom.
      by rewrite !size_codom.
    move => i.
    case Hsize: (size (enum transition) <= i).
    rewrite (nth_default) => //=.
      by rewrite sub0seq.
    by rewrite size_map.
    have Hlt':( i < size (enum transition))%N.
      rewrite ltnNge.
      by apply/negPf.
    move : Hsize Hlt'; rewrite -cardE  => _ Hsize.
    rewrite !(nth_codom nil _ (Ordinal Hsize)) //= !ffunE.
    exact: removeE_strict_covered_M_n_front.
  - have: (A> Br_E mc0 acc0 b0 [ffun t0 => removeE_strict_covered (f0 t0) mc]) =
    (A> (removeE_strict_covered (Br_E mc0 acc0 b0 f0) mc)).
      move => //=.
      by rewrite Hlt.
    move => ->.
    move/hasP => [] mc'/inv_Markings_not_front [] ad' [] Hmc' Had' /orP.
    elim.
    2:case Heq: (mc'==mc).
    1,2:move => Hleq.
    1,2:move: (Is_front_removeE_strict_covered Had' Hmc') => {}Had'.
    1,2:move/mfad_removeE_strict_covered/eqP:Hmc' => Hmc'.
    1,2:exfalso; move/negP: Hncov; apply.
    1,2:rewrite /ad_covered_not_front -Hmc; apply/hasP.
    1,2:exists mc' => //.
    3: by move/eqP: Heq Hleq => ->.
    1,2: exact:(inv2_Markings_not_front Hfl Hmc').
    + move => Hleq.
      exfalso.
      move: (removeE_strict_coveredF Hmc'); apply.
      rewrite /Order.lt //=; apply /andP; split => //=.
      by apply/negbT; rewrite eq_sym.
Qed.

Lemma Rel_small_step_rel (T1 T2: KMTE) A1 A2:
  consistentE_tree T2 ->
  Front_leaves T2 ->
  Not_Front_Antichain T2 ->
  Rel (T1,A1) (T2,A2) ->
  clos_trans _ Rel_small_step (A>T1,A1) (A>T2,A2).
Proof.
  move => Hcon Hfl Hnfa.
  inversion 1 as [
  T ACC ad T' Hfront HT' Had_cov [HT1 HA1] [HT2 HA2]  |
  T A ad T' ad' a Hfront HT' Hnot_cov Hposs Ha  [HT1 HA1] [HT2 HA2] |
  T A ad T' mc Hfront HT' Hnot_cov Hnposs Hmc [HT1 HA1] [HT2 HA2]]; subst.
  all: apply (@clos_t_rt _ _ _ (A>(saturate_KMTree A2 T2 ad),A2) ).
  2,4,6: exact: Saturate_Rel_small_step.
  1,2:apply: t_step.
  - rewrite removeEC -{2}(cat0s A2).
    apply: Rel_small_step_cln => //=.
      by rewrite -saturate_KMTree_Is_Front.
    exact: saturated_node_saturate.
  - rewrite -cat1s to_frontEC.
    move: (Poss_acc_inv Hposs) => [] mc [] mc' [] Hmc Hmc'.
    apply: (Rel_small_step_acc ).
      inversion_clear Hposs as [mc0 mc0' Hmc0 Hmc0' Hlt Hprefix].
      move/prefixP: Hprefix => [] s Had.
      subst ad.
      apply: (@Intern_not_front _ _ _ s mc0') => //=.
        - exact: saturate_KMTree_Fl.
        - by move:Hmc0 Hmc0' Hlt; case s => //=; rewrite !cats0 => <- [] ->; rewrite /Order.lt //= eqxx.
        - by rewrite -Hmc0' eqxx.
    apply: (not_saturate_node_from_add Hmc).
    rewrite /saturated_markingc cat1s.
    apply/negP.
    move/allP'/(_ (computingE_acceleration (saturate_KMTree A2 T2 ad) ad' ad)).
    move/(_ (List.in_eq(computingE_acceleration (saturate_KMTree A2 T2 ad) ad' ad) A2)).
    move: (@cptE_acc_accelerate _ (saturate_KMTree A2 T2 ad) ad' ad mc (computingE_acceleration (saturate_KMTree A2 T2 ad) ad' ad)) => [] //.
      exact: saturate_KMTree_consistentE.
    move => mc0 [] <- Hlt /orP.
    by elim => //= /eqP [] Heq; subst; move: Hlt; rewrite /Order.lt //= eqxx.
  - apply (@clos_t_rt _ _ _ (A>(removeE_strict_covered (saturate_KMTree A2 T2 ad) mc),A2) ); last first.
      apply: (@remove_strict_cover_Rel_small_step _ _ ad) => //=.
      by rewrite -saturate_KMTree_Is_Front.
    apply: t_step.
    rewrite Front_extensionEC -{2}(cat0s A2).
    apply: Rel_small_step_exp.
    + by apply: removeE_strict_covered_is_front => //=; rewrite -saturate_KMTree_Is_Front.
    + apply:(@saturate_node_from_add _ mc).
        exact: removeE_strict_covered_mfad.
      apply: (saturate_node_inv Hmc).
      exact: saturated_node_saturate.
    + apply: Rel_ext_nfa => //=.
      * by rewrite -saturate_KMTree_Is_Front.
      * exact: saturate_KMTree_Fl.
      * exact: saturate_KMTree_nfa.
Qed.



Lemma term_rel_No_Front (T:KMTE) A:
  Front_leaves T ->
  Not_Front_Antichain T ->
  consistentE_tree  T ->
  (forall T' A', ~ Rel (T',A') (T,A)) ->
  No_Front T.
Proof.
  move => Hfl Hac Hcon HnR.
  case Hnf: (No_Front (A>T)) => //=.
  move/negP/No_No_Front: Hnf => [] ad HFront.
  move: (Is_Front_m_f_ad HFront) => [] mc Hmc.
  remember (saturate_KMTree A T ad) as T'.
  exfalso.
  case Hcov: (ad_covered_not_front (A>T') ad).
    apply:( HnR (removeE_add T' ad) A).
    exact: Rel_clean.
  move: (saturate_KMTree_mfa A Hmc) => [] mc' Hmc'.
  move: (Poss_acc_Dec Hmc').
  elim =>[ [] ad' Hposs| Hnposs].
    apply:( HnR (to_FrontE T' ad') ((computingE_acceleration T' ad' ad)::A)).
    apply: (@Rel_accel _ _ ad _ ad') => //=.
      by rewrite Hcov.
    by rewrite HeqT'.
  apply:( HnR (Front_extensionE (removeE_strict_covered T' mc') ad) A).
  apply: Rel_explo => //=.
  all: by rewrite ?Hcov ?HeqT'.
Qed.


End Rel.