(**************************************************************)
(*   Copyright (c) 2024 Thibault Hilaire                      *)
(*                      David Ilcinkas                        *)
(*                      Jerome Leroux                         *)                           
(*   Authors : Thibault Hilaire [*]                           *)
(*             David Ilcinkas [+,*]                           *)
(*             Jerome Leroux  [+,*]                           *)
(*                                                            *)
(*                             [*] Affiliation Univ. Bordeaux *)
(*                             [+] Affiliation CNRS           *)
(**************************************************************)
(*      This file is distributed under the terms of the       *)
(*                      MIT License                           *)
(**************************************************************)


From mathcomp Require Import all_ssreflect zify.
From Coq Require Import Relations.
From AlmostFull.PropBounded Require Import AlmostFull AFConstructions.
Import Order.TTheory.
From Petri_Nets_in_Coq.Karp_Miller Require Import orderext petrinet pnkmaccel.
From Petri_Nets_in_Coq.Tools Require Import  Utils.
From Petri_Nets_in_Coq.MinCov_AbstractMinCov Require Import New_transitions  KMTrees AbstractMinCov.
From Coq Require Import Arith Lia Wellfounded FunInd Recdef.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope order_scope.
Local Open Scope petri_net_scope.

Section Completion.


Variable pn : petri_net.


Notation place := (place pn).
Notation marking := (marking pn).
Notation markingc := (markingc pn).
Notation transition := (transition pn).
Notation transitionc:= (transitionc pn).
Notation omega_transition:= (omega_transition pn).
Notation abstraction := (abstraction pn).
Notation acceleration := (acceleration pn).
Notation apply_transitionc := (@apply_transitionc pn).
Notation t_to_abstraction := (@t_to_abstraction pn).
Notation good_compoc := (@good_compoc pn).
Notation nil_transitionc :=(nil_transitionc pn).
Notation nil_abstraction :=(nil_abstraction pn).
Notation address := (address pn).
Notation KMTree := (KMTree pn).
Notation Empty := (Empty pn).
Notation Rel_small_step:=(@Rel_small_step pn).


Notation vsv := ((@val _ _ _) \o sval ).
Notation vsa := (vsv \o val) .


(*
  We define the notion of exploring sequence that allows our KMTree to
  not directly cover a marking but to say that in a further step it will cover it
*)
Section Exploring_sequence.

Definition post_exp_seq := seq (transition * (seq acceleration)).

Definition sub_exp_seq := ((seq acceleration) * post_exp_seq )%type.

Definition prefix_sub_exp_seq (s1 s2 : sub_exp_seq) := s1.1 = s2.1 /\ (exists s, s1.2 ++ s = s2.2).

Lemma  prefix_sub_exp_seq0 s:
  prefix_sub_exp_seq s (nil,nil) ->
  s = (nil,nil).
Proof.
  move:s => [s1 s2] [ //= Hs1 [s Hs]].
  rewrite Hs1.
  by move/nilP:Hs; rewrite cat_nilp; move/andP => [/nilP -> _].
Qed.


Definition Post_abs_from_exp_seq(s : post_exp_seq) : abstraction :=
  abstraction_compo_seq (comp_acc_aux s).

Lemma Post_abs_from_exp_seq_cat (s s' : post_exp_seq) mc mc' :
  Some mc' = apply_transitionc mc (abstraction_compo (Post_abs_from_exp_seq s) (Post_abs_from_exp_seq s')) ->
  Some mc' =apply_transitionc mc (Post_abs_from_exp_seq (s ++ s')).
Proof.
  move/eqP;rewrite /Post_abs_from_exp_seq /comp_acc_aux map_cat.
  by move/abstraction_compo_seq_cat/eqP => ->.
Qed.

Lemma cat_Post_abs_from_exp_seq (s s' : post_exp_seq) mc mc' :
  Some mc' =apply_transitionc mc (Post_abs_from_exp_seq (s ++ s')) ->
  Some mc' = apply_transitionc mc (abstraction_compo (Post_abs_from_exp_seq s) (Post_abs_from_exp_seq s')).
Proof.
  move/eqP; rewrite /Post_abs_from_exp_seq /comp_acc_aux map_cat.
  by move/abstraction_cat_compo_seq/eqP => ->.
Qed.



Lemma Post_abs_from_exp_seq_extended s t acc acc' mc mc':
  Some mc' = apply_transitionc mc (abstraction_compo (Post_abs_from_exp_seq (s ++ [::(t,acc)]))
  (abstraction_compo_seq (map val acc'))) ->
  Some mc' = apply_transitionc mc (Post_abs_from_exp_seq (s ++ [::(t,acc++acc')])).
Proof.
  move => H.
  apply Post_abs_from_exp_seq_cat.
  rewrite -good_compo_transitionc.
  case Hmc1: apply_transitionc => [mc1|]; last first.
    move: H; rewrite -good_compo_transitionc.
    case Hmc': (apply_transitionc mc) => [mc2|].
    2: by [].
    move/eqP : Hmc'; rewrite eq_sym.
    move/eqP/cat_Post_abs_from_exp_seq.
    by rewrite -good_compo_transitionc Hmc1.
  rewrite /obind /oapp.
  move: H; rewrite -good_compo_transitionc.
  case Hmc2: (apply_transitionc mc) => [mc2|//=].
  symmetry in Hmc2.
  move/cat_Post_abs_from_exp_seq: Hmc2.
  rewrite -good_compo_transitionc Hmc1 /obind /oapp => Hmc2 Hmc'.
  rewrite /Post_abs_from_exp_seq /comp_acc_aux map_cons /(map _ nil) abstraction_compo_seq1.
  rewrite -good_compo_transitionc.
  suff: (Some mc'= apply_transitionc mc1 (abstraction_compo (t_to_abstraction (t, acc ++ acc').1)
     (abstraction_compo_seq [seq val i | i <- (t, acc ++ acc').2] ))).
    move => <- //=.
    by move/eqP: (nil_transitionc_id mc') => ->.
  move: Hmc2.
  rewrite -!good_compo_transitionc.
  case: (apply_transitionc mc1) => [mc3 |//=].
  rewrite /obind /oapp map_cat => H.
  apply/eqP; apply abstraction_compo_seq_cat; apply/eqP.
  move: H.
  rewrite -good_compo_transitionc.
  case: (apply_transitionc) => [mc4|//=].
  by move/eqP : (nil_transitionc_id mc4) => -> [] <- .
Qed.


Lemma seq_extended_post_abs_from  mc mc' t acc acc':
  Some mc' = apply_transitionc mc (Post_abs_from_exp_seq [:: (t, acc ++ acc')]) ->
  Some mc' = apply_transitionc mc (abstraction_compo (Post_abs_from_exp_seq [::(t,acc)])
   (abstraction_compo_seq (map val acc'))).
Proof.
  rewrite /Post_abs_from_exp_seq /comp_acc_aux abstraction_compo_seq1.
  rewrite -!good_compo_transitionc /fst /snd.
  case: (apply_transitionc mc) => [mc0|//=].
  rewrite !/(obind _ (some mc0)) /oapp.
  case Hmc0: (apply_transitionc mc0) => [mc1|//=].
  rewrite /obind /oapp.
  move/eqP:(nil_transitionc_id mc1) => -> [] ->.
  move: Hmc0; rewrite map_cat.
  move/eqP; rewrite eq_sym.
  move/abstraction_cat_compo_seq/eqP; rewrite -good_compo_transitionc.
  case: (apply_transitionc) =>  [mc2|//=].
  rewrite /obind /oapp.
  by move/eqP:(nil_transitionc_id mc2) => ->.
Qed.



Lemma  Post_abs_nil :
  Post_abs_from_exp_seq nil = nil_abstraction .
Proof.
  by [].
Qed.


Definition abs_from_exp_seq (s : sub_exp_seq) := abstraction_compo (abstraction_compo_seq (map val s.1))
  (Post_abs_from_exp_seq s.2).

Lemma Fire_prefix (mc mc' : markingc) (s s' : sub_exp_seq):
  prefix_sub_exp_seq s' s ->
  Some mc' = apply_transitionc mc (abs_from_exp_seq s) ->
  exists mc'', Some mc'' = apply_transitionc mc (abs_from_exp_seq s').
Proof.
  move => [] Heq [s0 Hs0].
  rewrite /abs_from_exp_seq -good_compo_transitionc.
  case Hmc: (apply_transitionc mc) => [m| //=] .
  rewrite -good_compo_transitionc Heq Hmc /= -Hs0.
  move/eqP ;rewrite /Post_abs_from_exp_seq /comp_acc_aux map_cat.
  move/abstraction_cat_compo_seq.
  rewrite -good_compo_transitionc.
  case : (apply_transitionc m) => [m'|//=] _.
  by exists m'.
Qed.

Lemma Covered_prefix_exp_seq mc0 mc1 mc2  (s : post_exp_seq) :
  Some mc2 = apply_transitionc mc1 (Post_abs_from_exp_seq s) ->
  (exists s1 s2 mc, s = s1++s2 /\
  Some mc = apply_transitionc mc1 (Post_abs_from_exp_seq s1) /\
  mc <= mc0)
  \/
  (forall s1 s2 mc, s = s1++s2 ->
  Some mc = apply_transitionc mc1 (Post_abs_from_exp_seq s1) ->
  ~~ (mc <= mc0)) .
Proof.
  elim : s mc1 mc2 mc0 => [| [t acc] s Hind] mc1 mc2 mc0 Hmc2.
    cbn in Hmc2.
    move/eqP : (nil_transitionc_id mc1) Hmc2 => -> [] <-.
    case Hleq: (mc2 <= mc0).
      left; exists nil, nil, mc2 => //=.
      split => //=; split => //=.
      by move/eqP : (nil_transitionc_id mc2) => ->.
    right; case => [|] .
    2: by inversion 1.
    case => [ mc _|].
    2: by inversion 1.
    simpl; move/eqP : (nil_transitionc_id mc2) => -> [] ->.
    by rewrite Hleq.
  move: Hmc2; rewrite /Post_abs_from_exp_seq /comp_acc_aux map_cons abstraction_compo_seq1 -!good_compo_transitionc.
  case Hmc: apply_transitionc => [mc|//=].
  rewrite /obind /oapp.
  case Hmc': apply_transitionc => [mc'|//=].
  move/(@Hind mc' mc2 mc0) => {}Hind.
  case Hleq: (mc1 <= mc0).
    left; exists nil, ((t,acc)::s), mc1; split => //=; split => //=.
    by move/eqP: (nil_transitionc_id mc1) => ->.
  case Hleq': (mc' <= mc0).
    left; exists [::(t,acc)], s, mc'; split => //; split => //.
    rewrite -!good_compo_transitionc /fst /snd Hmc //= Hmc' //=.
    by move/eqP : (nil_transitionc_id mc') => ->.
  case :Hind.
    move => [] s1 [] s2 [] mc3 [Hs[ Hmc3 Hleq'']].
    left; exists ((t,acc)::s1), s2, mc3; split => //.
      by rewrite cat_cons Hs.
    split => //.
    by rewrite map_cons abstraction_compo_seq1 -!good_compo_transitionc Hmc /= Hmc'/= Hmc3.
  move => Hind; right.
  case => [|a s1] s2 mc3.
    move => _ /=.
    by move/eqP: (nil_transitionc_id mc1) => -> [] ->; rewrite Hleq.
  rewrite cat_cons; inversion 1.
  rewrite map_cons -!good_compo_transitionc Hmc /= Hmc' /= => Hmc3.
  exact:(Hind s1 s2).
Qed.

Lemma Max_cov_prefix mc0 mc1 mc2  (s : post_exp_seq) :
  Some mc2 = apply_transitionc mc1 (Post_abs_from_exp_seq s) ->
  (exists s1 s2 mc, s = s1++s2 /\
  Some mc = apply_transitionc mc1 (Post_abs_from_exp_seq s1) /\
  mc <= mc0) ->
  exists s1 s2 mc, s = s1++s2 /\
  Some mc = apply_transitionc mc1 (Post_abs_from_exp_seq s1) /\
  mc <= mc0
  /\
  ((s2 = nil) \/ (~s2 = nil) /\
  forall s3 s4 mc, s2 = s3++s4 ->
  ~ s3 = nil ->
  Some mc = apply_transitionc mc1 (Post_abs_from_exp_seq (s1 ++ s3)) ->
  ~~(mc <= mc0))  .
Proof.
  elim : s mc1 mc2 mc0 => [| [t acc] s Hind] mc1 mc2 mc0 Hmc2.
    move => [] s1 [] s2 [] mc3 [Hs [Hmc3 Hleq]].
    exists s1, s2, mc3 ; split => //=; split => //= ; split => //=.
    left.
    symmetry in Hs.
    by move/nilP: Hs; rewrite cat_nilp => /andP [/nilP Hs1 /nilP Hs2]; subst.
  move: Hmc2; rewrite /Post_abs_from_exp_seq /comp_acc_aux map_cons abstraction_compo_seq1 -!good_compo_transitionc.
  case Hmc: apply_transitionc => [mc|//=].
  rewrite /obind /oapp.
  case Hmc': apply_transitionc => [mc'|//=] Hmc2.
  move/(_ mc' mc2 mc0 Hmc2) in Hind.
  case : (Covered_prefix_exp_seq mc0 Hmc2) => Hinds.
    move => _.
    move: (Hind Hinds) => [] s1 [] s2 [] mc3 [{}Hinds [Hsmc3 [Hindleq {}Hind]]].
    exists ((t,acc)::s1), s2, mc3.
    rewrite cat_cons Hinds; split => //.
    rewrite map_cons abstraction_compo_seq1 -!good_compo_transitionc Hmc /obind /oapp Hmc'.
    split => //; split => //.
    case: Hind.
      move => ->; by left.
    move => [Hnil Hs2].
    right;split => //s3 s4 mc4.
    rewrite cat_cons map_cons -!good_compo_transitionc Hmc /obind /oapp Hmc'.
    exact:Hs2.
  move  => [] [|a s1] [] s2 [] mc3 [Hs [Hmc3 Hleq]]; last first.
    inversion Hs; subst.
    exists ((t,acc)::s1), s2, mc3.
    split => //; split => //; split => //.
    generalize dependent s2.
    case => [_ _ _ _ |a2 s2].
      by left.
    move => Hinds Hind Hmc2 Hs.
    right; split => // s3 s4 mc4.
    move/(_ (s1++s3) s4 mc4) in Hinds.
    move => Heq; subst.
    rewrite cat_cons map_cons -!good_compo_transitionc Hmc //= Hmc' //= => _.
    apply:Hinds.
    by rewrite Heq catA.
  exists nil, ((t,acc)::s), mc3.
  split => //; split => //; split => //.
  right;split => //.
  move => [//=|a s3] s4 mc4.
  rewrite cat_cons; inversion 1 => _.
  rewrite cat0s map_cons -!good_compo_transitionc Hmc //= Hmc' //=.
  exact: (Hinds s3 s4).
Qed.


(* The different properties of the exploring sequences *)

Definition inv_exp_seq1 (s : sub_exp_seq) (mc1 mc2 : markingc) :=
  Some mc2 = apply_transitionc mc1 (abs_from_exp_seq s).

Definition inv_exp_seq2 (s : sub_exp_seq) (mc1 : markingc) (T: KMTree) :=
  exists ad, Some mc1 = m_from_add T ad /\ Is_Front T ad.

Definition inv_exp_seq3 (s : sub_exp_seq) (mc1 : markingc) (T:KMTree):= forall s' mc3,
  prefix_sub_exp_seq s' s ->
  Some mc3 = apply_transitionc mc1 (abs_from_exp_seq s') ->
  ~~ covered_not_front T mc3 .

Definition inv_exp_seq4 (s : sub_exp_seq) (A: seq acceleration) :=
  All_Acc A s.1 /\ List.Forall (fun x => All_Acc A x.2 ) s.2.

Lemma  inv_exp_seq4_middle s1 s2 s3 t acc A:
  inv_exp_seq4 (s1,s2++(t,acc)::s3) A ->
  inv_exp_seq4 (acc,s3) A.
Proof.
  move => [] _.
  rewrite List.Forall_app.
  move => [] _.
  by inversion 1.
Qed.

(* The definition of an exploring sequence *)

Record Exp_seq (T: KMTree) (A : seq acceleration) := Mixin {
  S : sub_exp_seq;
  mc1 : markingc;
  mc2 : markingc;
  Coherent_mc : inv_exp_seq1 S mc1 mc2;
  In_T : inv_exp_seq2 S mc1 T;
  Not_covered: inv_exp_seq3 S mc1 T;
  In_Acc : inv_exp_seq4 S A;
}.

Definition prefix_exp_seq T A (s1 s2 : Exp_seq T A) :=
prefix_sub_exp_seq (S s1) (S s2) /\
mc1 s1 = mc1 s2.

(*
  We define the notion of quasi-covered that will be preserved by Rel_small_step
*)
Section quasi_covered.

Inductive quasi_covered (mc : markingc) (T: KMTree) (A: seq acceleration): Prop :=
|Front_cov mc' ad: ~~ Is_Front T ad ->
  Some mc' = m_from_add T ad ->
  mc <= mc' -> quasi_covered mc T A
|Exp_seq_cov (s : Exp_seq T A) : mc <= mc2 s -> quasi_covered mc T A
.

Definition All_quasi_covered m T A := forall mc, coverable m mc ->
quasi_covered (embedm mc) T A .

Lemma qc_suffix mc T A A':
  quasi_covered mc T A ->
  quasi_covered mc T (A'++A).
Proof.
  case => [mc' ad| [s mc1 mc2 Hco Hin_T Hnot_cov Hin_A Hleq]].
    by apply (@Front_cov  _ _ _ mc' ad).
  unshelve apply: (Exp_seq_cov).
  1:apply (@Mixin _ (A'++A) s mc1 mc2) => //=.
  2: by [].
  clear Hleq; move: Hin_A => [Hs1 Hs2].
  split.
    exact: All_Acc_suffix.
  move: Hs2.
  rewrite !List.Forall_forall => Hs2 x Hin.
  by apply All_Acc_suffix, Hs2.
Qed.

Lemma All_qc_suffix m T A A':
  All_quasi_covered m T A ->
  All_quasi_covered m T (A'++A).
Proof.
  move => Hall mc.
  move/Hall.
  exact: qc_suffix.
Qed.

(* We show that our operators preserve the quasi-cover property *)

Lemma init_quasi_covered A (m0: marking):
  All_quasi_covered (m0) (Br (embedm m0) true [ffun=> Empty]) A .
Proof.
  move => mc [ mc' [[s Hmc'] Hleq]].
  move/seqc_inv : Hmc' => Hmc'.
  set T:=(Br (embedm m0) true [ffun=> Empty]).
  set s':= (nil, [seq (t,nil)| t <- s]) : sub_exp_seq.
  unshelve apply: (Exp_seq_cov).
  1: apply: (@Mixin T A s' (embedm m0) (embedm mc')).
  5: by rewrite lemc_embed.
  - apply/eqP; apply:(@good_compoc _ _ _ (embedm m0)).
    cbn; move/eqP: (nil_transitionc_id (embedm m0)) => -> //=.
    apply useful_compoc.
    rewrite -{}Hmc'.
    apply f_equal.
    rewrite -!map_comp .
    apply eq_map =>  t //=.
    cbn; apply/eqP.
    by move/eqP: (compo_neutralR (t_to_transitionc t)) => ->.
  - by exists nil.
  - by move => s2 mc3 [Heq [s3 Hs3]] Hmc3 .
  - split => //=.
    elim  s => //= a l Hind.
    rewrite List.Forall_cons_iff; split => //=.
Qed.


Lemma RemoveFront_keep_qc m0 T A ad:
  Is_Front T ad ->
  Front_leaves T ->
  saturated_node A T ad ->
  ad_covered_not_front T ad ->
  All_quasi_covered m0 T A  ->
  All_quasi_covered m0 (remove_add T ad) A .
Proof.
  move => HFront Hfl Hsat Hadcov Hqc m.
  move/Hqc.
  (* First, we deal with the case when the markingc is covered by a non-front markingc *)
  case => [mc' ad' Had' Hmc' Hleq|].
    have Hneq:( ad' != ad).
      by apply/eqP => H; subst; move: HFront Had' => -> .
    have Hnprefix:(~~ prefix ad ad').
      apply/prefixP.
      move => [s Hs]; subst; generalize dependent s.
      move => [//= | t s].
        by rewrite cats0 HFront.
      by rewrite -(@Front_leaves_Is_Front _ _ _ HFront Hfl (t::s)).
    apply:(@Front_cov _ _ _ mc' ad' ) => //=.
      rewrite -remove_keeps_front //=.
    rewrite -remove_keeps_m_from_ad //=.
  (* Then we deal with the exploring sequence case *)
  move => [s mc1 mc2 Hco Hin_T Hnot_cov Hin_A] //= Hm.
  move: Hin_T => [ ad' [Hmc1 HFront_mc1] ].
  (* If the exploring sequence starts from another location it doesn't change *)
  case Heq: (ad' == ad); last first.
    have Hnprefix:(~~ prefix ad ad').
      apply/prefixP.
      move => [s0 Hs]; subst;generalize dependent s0.
      move => [//= | t s0].
        by rewrite cats0 eqxx.
      by rewrite -(@Front_leaves_Is_Front _ _ _ HFront Hfl (t::s0)).
    unshelve apply: (Exp_seq_cov).
    apply (@Mixin _ A s mc1 mc2) => //=.
    3: exact: Hm.
    - exists ad'; rewrite -remove_keeps_m_from_ad //=.
      split => //=; rewrite -remove_keeps_front //=.
      1,2:by apply/negPf.
    - move => s' mc3 Hprefix Hmc3.
      move: (Hnot_cov s' mc3 Hprefix Hmc3).
      by rewrite /covered_not_front (remove_marking_not_front HFront).
  (* We show that no exploring sequence can start from the place we remove *)
  move/eqP in Heq; subst.
  suff H:~~ covered_not_front T mc1.
    by move: Hadcov H; rewrite /ad_covered_not_front -Hmc1 /covered_not_front  => ->.
  move/(_ (s.1,nil) mc1): Hnot_cov => -> //.
    rewrite /prefix_sub_exp_seq //=; split => //=.
    by exists s.2.
  rewrite /abs_from_exp_seq.
  apply/eqP.
  apply (@good_compoc _ _ mc1 mc1 mc1); last first.
    by move/eqP: (nil_transitionc_id mc1) => ->.
  case Happ: (apply_transitionc) => [mc3|]; last first.
    by move: Hco; rewrite /inv_exp_seq1 -good_compo_transitionc Happ.
  rewrite (@saturated_marking_all_Acc _ mc1 mc3 s.1 A) //=.
    by move: Hin_A => [H _].
  exact: (saturate_node_inv Hmc1 ).
Qed.


Lemma RemoveFront2_keep_qc m0 T A ad ad' mc mc':
ad' != ad ->
Is_Front T ad ->
Is_Front T ad' ->
Front_leaves T ->
Some mc = m_from_add T ad ->
Some mc' = m_from_add T ad' ->
mc <= mc' ->
All_quasi_covered m0 T A  ->
All_quasi_covered m0 (remove_add T ad) A .
Proof.
  move => Hneq Had Had' Hfl Hmc Hmc' Hleq Hqc m.
  move/Hqc.
  (* First, we deal with the case when the markingc is covered by a non-front markingc *)
  case => [mc0 ad0 Had0 Hmc0 Hleq0|].
    have Hneq0:( ad0 != ad).
      by apply/eqP => H; subst; move: Had Had0 => -> .
    have Hnprefix0:(~~ prefix ad ad0).
      apply/prefixP.
      move => [s Hs]; subst;generalize dependent s.
      move => [//= | t s].
        by rewrite cats0 Had.
      by rewrite -(@Front_leaves_Is_Front _ _ _ Had Hfl (t::s)).
    apply:(@Front_cov _ _ _ mc0 ad0 ) => //=.
      rewrite -remove_keeps_front //=.
    rewrite -remove_keeps_m_from_ad //=.
  (* Then we deal with the exploring sequence case *)
  move => [[s1 s2] mc0 mc1 Hco Hin_T Hnot_cov Hin_A] //= Hm.
  move: Hin_T => [ ad0 [Hmc0 Had0] ].
  (* If the exploring sequence starts from another location it doesn't change *)
  case Heq:(ad0 == ad); last first.
    have Hnprefix0:(~~ prefix ad ad0).
      apply/prefixP.
      move => [s0 Hs]; subst;generalize dependent s0.
      move => [//= | t s0].
        by rewrite cats0 eqxx.
      by rewrite -(@Front_leaves_Is_Front _ _ _ Had Hfl (t::s0)).
    unshelve apply: (Exp_seq_cov).
    apply (@Mixin _ A (s1,s2) mc0 mc1) => //=.
    3: exact: Hm.
    - exists ad0; rewrite -remove_keeps_m_from_ad //=.
      split => //=; rewrite -remove_keeps_front //=.
      1,2:by apply/negPf.
    - move => s' mc3 Hprefix Hmc3.
      move: (Hnot_cov s' mc3 Hprefix Hmc3).
      by rewrite /covered_not_front (remove_marking_not_front Had).
  (* If the exploring sequence starts from where we remove, we can simulate it at another front node *)
  have Hnprefix0:(~~ prefix ad ad').
      apply/prefixP.
      move => [s0 Hs]; subst;generalize dependent s0.
      move => [//= | t s0].
        by rewrite cats0 eqxx.
      by rewrite -(@Front_leaves_Is_Front _ _ _ Had Hfl (t::s0)).
  move/eqP in Heq; subst.
  move: Had0 Hmc0 => _; rewrite -Hmc; move => [] Heq; subst.
  move: Hco; rewrite /inv_exp_seq1 => Hs; symmetry in Hs.
  move: (apply_transitioncM Hs Hleq) => [] mc2 [] Hs' Hleq'.
  unshelve apply: (Exp_seq_cov).
  apply (@Mixin _ A (s1,s2) mc' mc2) => //=.
  3: exact : (Order.POrderTheory.le_trans Hm Hleq').
  - exists ad'.
    by rewrite -remove_keeps_m_from_ad //= -remove_keeps_front.
  - move => [s1' s2'] mc3 []; rewrite /fst /snd => Heq; subst.
    move => [] s Hprefix Hmc3.
    apply/negP; move/hasP => [] x.
    rewrite -(remove_marking_not_front Had) => Hin Hleqx.
    have :(exists mc4, Some mc4 = apply_transitionc mc (abs_from_exp_seq (s1,s2')) ).
      move: Hs; rewrite -Hprefix /abs_from_exp_seq /fst /snd -!good_compo_transitionc.
      case: apply_transitionc => [a //=|//=].
      rewrite /Post_abs_from_exp_seq /comp_acc_aux map_cat; move/eqP.
      rewrite eq_sym; move/abstraction_cat_compo_seq/eqP.
      rewrite -good_compo_transitionc.
      case: apply_transitionc => [b //= _|//=].
      by exists b.
    move => [] mc4 Hmc4.
    suff: ~~ covered_not_front T mc4.
      move/negP; apply.
      apply/hasP; exists x => //=.
      apply :(Order.POrderTheory.le_trans _ Hleqx).
      symmetry in Hmc4.
      move: (apply_transitioncM Hmc4 Hleq) => [] mc5.
      by rewrite -Hmc3 => [[]] [] => ->.
    apply: (Hnot_cov (s1,s2') mc4) => //=.
    by rewrite /prefix_sub_exp_seq; split => //=; exists s.
Qed.



Lemma Saturate_keep_qc m0 T a A ad:
  Is_Front T ad ->
  All_quasi_covered m0 T A ->
  All_quasi_covered m0 (saturate_a_little a T ad) A .
Proof.
  move => HFront Hqc m.
  move/Hqc.
  (* First, we deal with the case when the markingc is covered by a non-front markingc *)
  case => [mc ad' Had' Hmc Hleq|].
    apply:(@Front_cov _ _ _ mc ad' ) => //=.
      by rewrite -saturate_keeps_front.
    rewrite Hmc.
    rewrite saturate_m_from_2ad //=.
    apply/eqP => Heq; subst.
    by move: HFront Had' => ->.
  (* Then we deal with the exploring sequence case *)
  move => [s mc1 mc2 Hco Hin_T Hnot_cov Hin_A] /= Hleq.
  move: Hin_T => [ ad' [Hmc1 HFront_mc1] ].
  (* If the exploring sequence starts from another location it doesn't change *)
  case Heq: (ad' == ad); last first.
    unshelve apply: (Exp_seq_cov).
    apply (@Mixin _ A s mc1 mc2) => //=.
    3: exact: Hleq.
    - exists ad'; rewrite -saturate_keeps_front Hmc1 saturate_m_from_2ad //=.
      by apply/negPf.
    - move => s' mc3 Hprefix Hmc3.
      move: (Hnot_cov s' mc3 Hprefix Hmc3).
      by rewrite /covered_not_front (saturate_markingc_not_front a HFront ).
  (* The case when the exploring sequence starts where we accelerate *)
  move/eqP in Heq; subst.
  (* Does the acceleration do something, first if it doesn't *)
  case Ha: (apply_transitionc mc1 a) => [mca|]; last first.
    unshelve apply: (Exp_seq_cov).
    apply (@Mixin _ A s mc1 mc2) => //=.
    3: exact: Hleq.
    - exists ad; rewrite -saturate_keeps_front; split => //=.
      by apply saturate_m_from_ad_None.
    - move => s' mc3 Hprefix Hmc3.
      move: (Hnot_cov s' mc3 Hprefix Hmc3).
      by rewrite /covered_not_front (saturate_markingc_not_front a HFront ).
  (* Case when it does something *)
  have Hleq2:(mc1 <= mca).
    apply (@acc_no_decrease _ _ _ a).
    by rewrite Ha.
  rewrite/inv_exp_seq1 in Hco; symmetry in Hco.
  move: (@apply_transitioncM _ mc1 mc2 mca (abs_from_exp_seq s) Hco Hleq2) => [mca' [Hmca Hleq3]].
  unshelve apply: (Exp_seq_cov).
  apply (@Mixin _ A s mca mca') => //=.
  3: by apply:(Order.POrderTheory.le_trans Hleq).
  - exists ad; rewrite -saturate_keeps_front; split => //=.
    exact:(saturate_m_from_ad_Some Hmc1).
  - move => s' mc3 Hprefix Hmc3.
    rewrite /covered_not_front -(@saturate_markingc_not_front pn a T ad) => //=.
    apply/negP.
    move/hasP => [mc Hin Hleq4].
    symmetry in Hco.
    move: (Fire_prefix Hprefix Hco) => [mc'' Hmc''].
    move/(_ s' mc'' Hprefix Hmc'' ): Hnot_cov.
    move/negP => Hcov.
    apply Hcov.
    apply/hasP.
    exists mc => //=.
    apply (@Order.POrderTheory.le_trans _ _ mc3) => //=.
    symmetry in Hmc''.
    move: (apply_transitioncM Hmc'' Hleq2 ) => [m' [Hm' Hleqm']].
    move: Hm'; rewrite -Hmc3.
    by move => [] ->.
Qed.


Lemma To_Front_keep_qc m0 mc1 T A ad:
  ~~Is_Front T ad ->
  Some mc1 = m_from_add T ad ->
  Front_leaves T ->
  Not_Front_Antichain T ->
  consistent_tree A T ->
  All_quasi_covered m0 T A  ->
  All_quasi_covered m0 (to_Front T ad) A .
Proof.
  move => HFront Hmc1 Hfl Hnfa Hcon Hqc m.
  move/Hqc.
  (* First, we deal with the case when the markingc is covered by a non-front markingc *)
  case => [mc ad' Had' Hmc Hleq|].
    (* We need to know where we do the change inside the KMTree *)
    case Heq: (ad == ad').
    (* If we are at the place of the change *)
      move/eqP in Heq; subst ad'.
      move: Hmc1; rewrite -Hmc; move => [] Heq.
      unshelve apply: (Exp_seq_cov).
      apply (@Mixin _ A (nil,nil) mc mc ) => //=.
      5: by [].
      - rewrite /inv_exp_seq1 -good_compo_transitionc;cbn.
        by move/eqP: (nil_transitionc_id mc)=> H; rewrite H .
      - exists ad; split; last first.
          exact:(@Front_to_Front _ _ _ mc).
        exact:from_ad_to_Front.
      2: rewrite /inv_exp_seq4 /All_Acc //=.
      - move => s' mc3 /prefix_sub_exp_seq0 ->.
        rewrite -good_compo_transitionc;cbn.
        move/eqP: (nil_transitionc_id mc)=> H; rewrite H //= {}H; move => [] ->.
      - apply : (@not_Cnf_to_front _ _ _ _ nil) => //=.
        1,2: by rewrite cats0.
    case Hprefix: (prefix ad ad'); last first.
      (* If we are not on the same branch as the change, nothing changes *)
      apply:(@Front_cov _ _ _ mc ad' ) => //=.
      1: rewrite -to_Front_keeps_front => //=.
      3: rewrite -to_front_keeps_m_from_ad => //=.
      1,3: by move: Heq; rewrite eq_sym => /eqP/eqP.
      1,2: by rewrite Hprefix.
    (* If we are on the same branch *)
    move/prefixP: Hprefix => [s Hs]; subst ad'.
    move/negP/negP/neq_nil: Heq => Hs.
    move:(consistent_cpt_acc Hcon Hmc1 Hmc) => [] b []Hall [Heqb Hprefix].
    unshelve apply: (Exp_seq_cov).
      apply (@Mixin _ A (nil,b) mc1 mc ) => //=.
      5: by [].
      - rewrite /inv_exp_seq1 /abs_from_exp_seq -good_compo_transitionc /fst //=.
        move/eqP: (nil_transitionc_id mc1 ) => -> //=.
      - rewrite /inv_exp_seq2; exists ad; split.
          exact: from_ad_to_Front.
        exact: (Front_to_Front Hmc1).
      - move => s' mc3 [] Hs1 [] x Hs2.
        simpl in Hs1, Hs2.
        rewrite /abs_from_exp_seq Hs1 -good_compo_transitionc /map /abstraction_compo_seq /foldr.
        move/eqP: (nil_transitionc_id mc1) => -> //=.
        case Hx: (x) =>[|a l].
          move: Hx Hs2 => ->; rewrite cats0 => ->.
          rewrite -Heqb => [] [] ->.
          exact:(not_Cnf_to_front _ Had').
        move/(_ s'.2 x Hs2): Hprefix => [].
          by rewrite Hx.
        move => [] mc'' [] s3 [] s4 [] Hs'2 [] Hs3s4 [] Hs4 Hmc'' .
        rewrite -Hs'2 => [] [] ->.
        suff HnFront:(~~ Is_Front T (ad++s3)).
          exact:(@not_Cnf_to_front _ _ _ _ s3).
        move:Hfl Hmc Had'; rewrite -Hs3s4 catA.
        exact: (Not_front_prefix).
      - split => //=.
  (* Now we attack the case when we are quasi-covered by an exploring sequence *)
  move => [[s1 s2] mc mc' Hco Hin_T Hnot_cov Hin_A] /= Hleq.
  move: Hin_T => [ ad' [Hmc HFront_mc] ].
  have Hneq:(ad' != ad).
    apply/eqP => H; subst.
    by move: HFront_mc HFront => ->.
  (* If the exploring sequence starts from a Front not affected by to_Front then nothing changes *)
  case Hprefix: (prefix ad ad'); last first.
    unshelve apply: (Exp_seq_cov).
    - apply (@Mixin _ A (s1,s2) mc mc' ) => //=.
    3: by [].
    - exists ad'; split.
      1:rewrite -(to_front_keeps_m_from_ad _ Hneq ) => //=.
      2:rewrite -(to_Front_keeps_front _ Hneq ) => //=.
      1,2: by rewrite Hprefix.
    - move => [s1' s2'] mc3 Hpre Hmc3.
      move/(_ (s1',s2') mc3 Hpre Hmc3): Hnot_cov.
      apply: contra.
      move/hasP => [] x Hin Hleq3x.
      apply/hasP; exists x => //=.
      by move/mem_subseq/(_  x Hin): (to_front_markingc_not_front T ad).
  (* If it starts from a Front affected *)
  move/prefixP: Hprefix => [] s Had'; subst ad'.
  rewrite eq_sym in Hneq.
  move/negP/negP/neq_nil: Hneq => Hs'.
  move: (consistent_cpt_acc Hcon Hmc1 Hmc) => [] b []Hall [Heqb Hprefix].
  elim: (nil_or_rcons b).
    move => Hb; subst.
    move: Heqb; rewrite /comp_acc_aux //=.
    move/eqP: (nil_transitionc_id mc1) => -> [] Heq; subst.
    unshelve apply: (Exp_seq_cov).
    - apply (@Mixin _ A (s1,s2) mc1 mc' ) => //=.
    3: by [].
    - exists ad.
      by rewrite (Front_to_Front Hmc1) -(from_ad_to_Front Hmc1).
    - move => s' mc3 Hprefix_sub Hmc3.
      move:(Hnot_cov s' mc3 Hprefix_sub Hmc3).
      apply:contra => /hasP [] mc Hin Hleq_mc.
      apply/hasP; exists mc => //=.
      move/mem_subseq:(to_front_markingc_not_front T ad) => H.
      exact:H.
  move => [] [t acc] [] b' => Heqb'.
  unshelve apply: (Exp_seq_cov).
  - apply (@Mixin _ A (nil,b'++[::(t,acc++s1)]++s2) mc1 mc' ) => //=.
  5: by [].
  - rewrite /inv_exp_seq1 /abs_from_exp_seq -good_compo_transitionc /=.
    move/eqP : (nil_transitionc_id mc1) => -> /=.
    rewrite -cat1s catA.
    apply Post_abs_from_exp_seq_cat.
    suff: (apply_transitionc mc (abstraction_compo_seq [seq val i | i <- (s1, s2).1])
    = apply_transitionc mc1 (Post_abs_from_exp_seq (b' ++ [:: (t, acc ++ s1)]))).
      move: Hco; rewrite/inv_exp_seq1 /abs_from_exp_seq.
      rewrite -!@good_compo_transitionc.
      by move => -> ->.
    rewrite /fst.
    case Hs1: (apply_transitionc mc) => [mc''|].
      apply: Post_abs_from_exp_seq_extended.
      rewrite -good_compo_transitionc -Heqb' -Heqb //=.
    by move: Hco; rewrite /inv_exp_seq1 /abs_from_exp_seq -good_compo_transitionc Hs1.
  - exists (ad); split.
    2:exact:(Front_to_Front Hmc1).
    exact: (from_ad_to_Front Hmc1).
  2:{ split => //=;apply List.Forall_app.
      move: Heqb' Hall => ->; rewrite List.Forall_app => [] [] Hb' H_t_acc.
      split => //=.
      apply List.Forall_cons => //=.
        apply List.Forall_app; split.
          by apply List.Forall_inv in H_t_acc.
      all: by move: Hin_A => [H H'].
    }
  - move => [s1' s2'] mc3 [] H.
    simpl in H; move: H => -> [] x Hseq.
    simpl in Hseq;clear s1'; symmetry in Heqb'.
    rewrite /abs_from_exp_seq -good_compo_transitionc //=.
    move/eqP: (nil_transitionc_id mc1) => -> //=.
    (* Is the sequence a prefix of b'? *)
    move: (cat_prefix Hseq) => [] s'.
    case.
      move=> Hs2'b' Hmc3.
      move/(_ s2' (s'++[::(t,acc)])): Hprefix => [].
        - by rewrite -Heqb' -Hs2'b' catA.
        - by move/nilP; rewrite cat_nilp => /andP [].
        - move => mc0 [] s3 [] s4 [].
          rewrite -Hmc3 => [] [] -> [] Hs [] Hs4 {}Hmc3.
          suff HnFront:(~~ Is_Front T (ad++s3)).
            exact:(@not_Cnf_to_front _ _ _ _ s3).
          apply/negP => Hs3.
          move/(_ s4 Hs4): (Front_leaves_Is_Front Hs3 Hfl) Hmc.
          by rewrite -catA Hs => <-.
    move => H; move: H Hseq => <-.
    rewrite -catA.
    move/catL => Hs'_x.
    move/cat_Post_abs_from_exp_seq.
    move:(Hprefix b' [::(t,acc)] Heqb') => [].
      by [].
    move => mc2 [] s3 [] s4 [] Hmc2 [] Hs [] Hs4 Hs3.
    rewrite -good_compo_transitionc -Hmc2 //=.
    rewrite -cat1s in Hs'_x.
    move:(cat_prefix Hs'_x) => [] s0.
    elim; last first.
      move => Hs0 Hmc3.
      suff :(~~ covered_not_front T mc3).
        apply/contra => /hasP [] mc4 Hin Hleq34.
        move/mem_subseq: (to_front_markingc_not_front T ad) => Hmem.
        apply/hasP; exists mc4 => //=.
        exact: Hmem.
      apply: (Hnot_cov (s1,s0)).
        split => //=; exists x.
        move: Hs0 Hs'_x => <-; rewrite -catA.
        exact: catL.
      move: Hmc3; rewrite -Hs0.
      move/cat_Post_abs_from_exp_seq.
      rewrite -good_compo_transitionc.
      case Hmc0: (apply_transitionc mc2) => [mc0|//=].
      symmetry in Hmc0.
      move/(seq_extended_post_abs_from): Hmc0.
      rewrite -good_compo_transitionc.
      move: Heqb; rewrite -Heqb'; move/cat_Post_abs_from_exp_seq.
      rewrite -good_compo_transitionc -Hmc2 /obind /oapp => <-.
      by rewrite /abs_from_exp_seq -good_compo_transitionc => <-.
    move =>H.
    move:(cat1 H) => {H}; elim; last first.
      move => [-> _] //=.
      move/eqP: (nil_transitionc_id mc2 ) => -> [] ->.
      suff HnFront:(~~ Is_Front T (ad++s3)).
        exact:(@not_Cnf_to_front _ _ _ _ s3).
      apply/negP => {}Hs3.
      move/(_ s4 Hs4): (Front_leaves_Is_Front Hs3 Hfl) Hmc.
      by rewrite -catA Hs => <-.
    move => [-> _].
    move/seq_extended_post_abs_from; rewrite -good_compo_transitionc.
    move: Heqb; rewrite -Heqb'; move/cat_Post_abs_from_exp_seq.
    rewrite -good_compo_transitionc -Hmc2 /obind /oapp => <- Hmc3.
    suff :(~~ covered_not_front T mc3).
        apply/contra => /hasP [] mc4 Hin Hleq34.
        move/mem_subseq: (to_front_markingc_not_front T ad) => Hmem.
        apply/hasP; exists mc4 => //=.
        exact: Hmem.
    apply: (Hnot_cov (s1,nil)) .
      split => //=; by exists s2.
    rewrite /abs_from_exp_seq /snd /fst -good_compo_transitionc -Hmc3 //=.
    by move/eqP: (nil_transitionc_id mc3) => ->.
Qed.




Lemma RemoveNFront_keep_qc m0 mc mc' T A ad ad':
  Is_Front T ad ->
  Front_leaves T->
  Not_Front_Antichain T ->
  consistent_tree A T ->
  Some mc = m_from_add T ad ->
  Some mc' = m_from_add T ad' ->
  mc' <= mc ->
  ~~ prefix ad' ad ->
  All_quasi_covered m0 T A  ->
  All_quasi_covered m0 (remove_add T ad') A .
Proof.
  move => Had Hfl Hnfa Hcon Hmc Hmc' Hleq Hnotprefix Hacc Hqc.
  have Hneq: (ad != ad').
    apply/eqP => H; subst.
    by rewrite prefix_refl in Hnotprefix.
  case Had': (Is_Front T ad').
    exact: (@RemoveFront2_keep_qc _ _ _ _ ad mc' mc).
  rewrite Remove_absorb_to_front.
  apply: (@RemoveFront2_keep_qc _ _ _ _ ad mc' mc) => //=.
  - exact: (Front_to_Front Hmc').
  - by rewrite -to_Front_keeps_front.
  - exact:To_front_keeps_fl.
  - exact : from_ad_to_Front.
  - by rewrite -to_front_keeps_m_from_ad.
  - apply:(@To_Front_keep_qc _ mc') => //=.
    by rewrite Had'.
Qed.


Lemma Front_extension_keep_qc m0 T A ad:
  Is_Front T ad ->
  Front_leaves T ->
  saturated_node A T ad ->
  All_quasi_covered m0 T A ->
  All_quasi_covered m0 (Front_extension T ad) A .
Proof.
  move => HFront Hfl  Hsat Hqc.
  move: (Front_extension_or T ad).
  case => [ -> //=| [] mc Hmc] m.
  move/Hqc.
  (* First, we deal with the case when the markingc is covered by a non-front markingc *)
  case => [mc' ad' Had' Hmc' Hleq|].
    case Heq: (ad == ad').
      by move/eqP : Heq HFront Had' => -> ->.
    have Hprefix:(~~ prefix ad ad').
      apply/prefixP.
      move => [s  Hs]; subst.
      generalize dependent s.
      move => [//= | t s].
        by rewrite cats0 eqxx.
      by rewrite -(@Front_leaves_Is_Front _ _ _ HFront Hfl (t::s)).
    apply:(@Front_cov _ _ _ mc' ad' ) => //=.
    1:rewrite -Front_ext_keeps_front => //=.
    2: rewrite -Front_ext_keeps_m_from_ad => //=.
    1,2: by rewrite eq_sym; apply/negPf.
  (* Now we attack the case when we are quasi-covered by an exploring sequence *)
  move => [[s1 s2] mc1 mc2 Hco Hin_T Hnot_cov Hin_A] /= Hleq.
  move: Hin_T => [ ad' [Hmc1 HFront_mc1] ].
  move: Hco; rewrite /inv_exp_seq1 /abs_from_exp_seq /snd /fst.
  rewrite -good_compo_transitionc.
  case Hs1: apply_transitionc => [mc'| //].
  rewrite /= => Hmc2.
  (* Does the new non-front node cover a part of the exploring sequence *)
  case:(Covered_prefix_exp_seq mc Hmc2).
    (* case if it does *)
    move/(Max_cov_prefix Hmc2) => [] s3 [] s4 [] mc0 [] Hs2 [] Hmc0 [] Hleq0.
    case => [ Hs4|].
      move: Hs4 Hs2 => ->; rewrite cats0 =>Hs2; subst s3.
      apply:(@Front_cov _ _ _ mc ad).
      - exact:Front_ext_not_front.
      - by rewrite -Front_ext_keeps_m_from_same_ad.
      - move: Hmc2 Hmc0 Hleq Hleq0 => <- [] ->.
        exact: (Order.POrderTheory.le_trans).
    move: Hs2.
    case: s4 => [_ [] Habs //=| []t acc s4 Hs2 [] _ Hs4].
    subst s2.
    move:Hmc2 ; rewrite /Post_abs_from_exp_seq /comp_acc_aux map_cat.
    move/eqP/abstraction_cat_compo_seq/eqP.
    rewrite -good_compo_transitionc -Hmc0 /obind /oapp map_cons.
    rewrite abstraction_compo_seq1 -!good_compo_transitionc.
    case Hmc1': (apply_transitionc mc0) => [mc1'|//=].
    rewrite /obind /oapp.
    case Hmc2': (apply_transitionc mc1') => [mc2'|//=] Hmc2.
    symmetry in Hmc2.
    move: (apply_transitioncM Hmc1' Hleq0) => [] mc1'' [Hmc1'' Hleq1].
    move: (apply_transitioncM Hmc2' Hleq1) => [] mc2'' [Hmc2'' Hleq2].
    move: (apply_transitioncM Hmc2 Hleq2) => [] mc3'' [Hmc3'' Hleq3].
    unshelve apply: (Exp_seq_cov).
    - apply (@Mixin _ A (acc,s4) mc1'' mc3'' ) => //=.
    5: exact: (Order.POrderTheory.le_trans Hleq).
    - by rewrite /inv_exp_seq1 /abs_from_exp_seq -good_compo_transitionc Hmc2''.
    - exists (rcons ad t).
      exact:(Front_extension_t Hmc).
    2:exact: (inv_exp_seq4_middle Hin_A).
    - move => [s'1 s'2] mc3 [].
      rewrite /fst /snd => -> [] s Heq.
      rewrite /abs_from_exp_seq -good_compo_transitionc Hmc2'' /= => Hmc3.
      symmetry in Hmc2.
      move: Hmc2.
      rewrite -Heq map_cat => /eqP.
      move/abstraction_cat_compo_seq/eqP; rewrite -good_compo_transitionc.
      case Hmc3':(apply_transitionc) => [mc3'|//=].
      rewrite /obind /oapp => Hmc2.
      have :(~~(mc3' <= mc )).
        apply:(Hs4 ((t,acc)::s'2) s) => //=.
          by rewrite Heq.
        rewrite /Post_abs_from_exp_seq /comp_acc_aux map_cat.
        apply/eqP; apply abstraction_compo_seq_cat; apply/eqP.
        rewrite -good_compo_transitionc -Hmc0 /obind /oapp map_cons abstraction_compo_seq1.
        by rewrite -!good_compo_transitionc Hmc1' /obind /oapp Hmc2' Hmc3'.
      have:(~~ (covered_not_front T mc3')).
        apply: (Hnot_cov (s1,s3++((t,acc)::s'2)) mc3').
        rewrite /prefix_sub_exp_seq; split => //=.
          by exists s; rewrite -Heq -catA.
        rewrite /abs_from_exp_seq -good_compo_transitionc Hs1 //=.
        rewrite /Post_abs_from_exp_seq /comp_acc_aux map_cat.
        apply/eqP; apply abstraction_compo_seq_cat; apply/eqP.
        rewrite -good_compo_transitionc -Hmc0 /obind /oapp map_cons abstraction_compo_seq1.
        by rewrite -!good_compo_transitionc Hmc1' /obind /oapp Hmc2' Hmc3'.
      move => Hnfmc3' Hleq3c.
      move:(Not_covered_not_front_Front_extension Hmc Hnfmc3' Hleq3c).
      have: (mc3' <= mc3).
        move:(apply_transitioncM Hmc3' Hleq2) => [] MC [].
        by rewrite -Hmc3; move => [] ->.
      move =>H1 /negP H2.
      apply/negP; move/hasP => [] x Hinx H3x.
      apply:H2; apply/hasP; exists x => //=.
      apply:(Order.POrderTheory.le_trans H1 H3x).
  (* case if it doesn't *)
  move => Hmc_not_cov.
  case Heq: (ad==ad'); last first.
    (* If the exploring sequence is in another branch, then nothing changes *)
    case Hprefix: (prefix ad ad').
      move/prefixP: Hprefix => [] [|t s].
        rewrite cats0 => /eqP Heq'.
        by move/eqP: Heq' Heq => ->; rewrite eqxx.
      move => Heq'.
      move: (Front_leaves_Is_Front HFront Hfl).
      by move/(_ (t::s) isT); move : Heq' Hmc1 => -> <-.
    unshelve apply: (Exp_seq_cov).
    - apply (@Mixin _ A (s1,s2) mc1 mc2 ) => //=.
    4: by [].
    - by rewrite /inv_exp_seq1 /abs_from_exp_seq -good_compo_transitionc Hs1.
    - exists ad'; split.
      1:rewrite -Front_ext_keeps_m_from_ad //=.
      3: rewrite -Front_ext_keeps_front //=.
      1,3 :apply/eqP => Heq'; subst;by move: Heq; rewrite eqxx.
      1,2: by rewrite Hprefix.
    - move => [s1' s2'] mc3 [] .
      rewrite /fst /snd => Heqs; subst s1'.
      move => [] s Heqs.
      rewrite /abs_from_exp_seq -good_compo_transitionc Hs1 //= => Hmc3.
      have Hnotleq:(~~ (mc3 <= mc)).
        exact: (Hmc_not_cov s2' s).
      have Hnot_cov_mc3:(~~ covered_not_front T mc3).
        apply: (Hnot_cov (s1,s2') mc3).
          rewrite /prefix_sub_exp_seq; split => //=.
          by exists s.
        by rewrite /abs_from_exp_seq -good_compo_transitionc Hs1.
      exact: (Not_covered_not_front_Front_extension Hmc).
  (* If the exploring sequence is in the same branch *)
  move/eqP in Heq; subst.
  move: HFront Hmc => _; rewrite -Hmc1; move => [] Heq; subst mc.
  move: (saturate_node_inv Hmc1 Hsat) => {}Hsat.
  symmetry in Hs1.
  move: Hin_A => [Hin_s1 Hin_s2].
  move: (saturated_marking_all_Acc  Hin_s1 Hsat Hs1) => Heq; subst mc'.
  generalize dependent s2.
  case => [| [t acc] s2 ] Hprefix Hnot_cov Hmc2 Hin_s1 Hin_s2.
    apply:(@Front_cov _ _ _ mc1 ad').
    - exact:Front_ext_not_front.
    - by rewrite -Front_ext_keeps_m_from_same_ad.
    - move: Hmc2; rewrite /Post_abs_from_exp_seq //=.
      by move/eqP: (nil_transitionc_id mc1) => -> [] <-.
  move: Hmc2; rewrite /Post_abs_from_exp_seq /comp_acc_aux map_cons abstraction_compo_seq1 -!good_compo_transitionc.
  case Hmc1': (apply_transitionc) => [mc1'|//=].
  rewrite /(obind _ (Some mc1')) /oapp.
  case Hmc2': (apply_transitionc) => [mc2'|//=].
  rewrite /obind /oapp => Hmc2.
  unshelve apply: (Exp_seq_cov).
  - apply (@Mixin _ A (acc,s2) mc1' mc2 ) => //=.
  5: by [].
  - by rewrite /inv_exp_seq1 /abs_from_exp_seq -good_compo_transitionc Hmc2'.
  - exists (rcons ad' t).
    exact:(Front_extension_t Hmc1).
  2:by inversion Hin_s2.
  - move => [s1' s2'] mc3 []; rewrite /fst /snd => Heq; subst s1'.
    move => [] s Heq Hmc3.
    have Hnotleq:(~~ (mc3 <= mc1)).
      apply: (Hnot_cov ((t,acc)::s2') s).
        by rewrite -Heq cat_cons.
      rewrite /Post_abs_from_exp_seq /comp_acc_aux map_cons abstraction_compo_seq1 -!good_compo_transitionc.
      by rewrite Hmc1' Hmc3 /abs_from_exp_seq -good_compo_transitionc.
    have Hnot_cov_mc3:(~~ covered_not_front T mc3).
      apply: (Hprefix (s1,(t,acc)::s2') mc3).
        rewrite /prefix_sub_exp_seq; split => //=.
        by exists s; rewrite Heq.
      rewrite /abs_from_exp_seq -good_compo_transitionc -Hs1 /obind /oapp /Post_abs_from_exp_seq.
      rewrite /comp_acc_aux map_cons abstraction_compo_seq1 -!good_compo_transitionc.
      by rewrite Hmc1' Hmc3 /abs_from_exp_seq -good_compo_transitionc.
    exact: (Not_covered_not_front_Front_extension Hmc1).
Qed.


(* The finals lemmas for the correction *)

Lemma Rel_small_step_quasi_covered T1 T2 A1 A2 (m0: marking):
  consistent_head A2 m0 T2 ->
  consistent_tree A2 T2 ->
  Not_Front_Antichain T2 ->
  Front_leaves T2 ->
  All_quasi_covered m0 T2 A2 ->
  Rel_small_step (T1,A1) (T2,A2) ->
  All_quasi_covered m0 T1 A1.
Proof.
  move => Hcon_H Hcon Hnfa Hfl HA.
  inversion 1 as
  [ T A A' ad mc acc mc' Hfront Hacc Hmc Hmc' Hneq |
    T A A' ad Hfront Hsat Hcover |
    T A A' ad HnFront HnSat  |
    T A A' ad mc ad' mc' Hfront Hmc Hmc' Hleq Hprefix |
    T A A' ad Hfront Hsat Hac ] .
  all: apply:All_qc_suffix.
  - exact: Saturate_keep_qc.
  - exact: (RemoveFront_keep_qc).
  - move/not_saturated_node_inv: HnSat => [] mc [] Hmc _.
    apply:(To_Front_keep_qc) => //=.
    apply: Hmc.
  - apply: (RemoveNFront_keep_qc Hfront Hfl Hnfa Hcon Hmc Hmc') => //=.
  - exact:Front_extension_keep_qc.
Qed.


Lemma cls_Rel_small_step_quasi_covered T1 T2 A1 A2 (m0: marking):
  consistent_head A2 m0 T2 ->
  consistent_tree A2 T2 ->
  Not_Front_Antichain T2 ->
  Front_leaves T2 ->
  All_quasi_covered m0 T2 A2 ->
  clos_refl_trans_1n _ Rel_small_step (T1,A1) (T2,A2) ->
  All_quasi_covered m0 T1 A1.
Proof.
  remember (T1,A1) as T.
  remember (T2,A2) as T'.
  have : (T2 = T'.1) /\ (A2 = T'.2) /\ (T1 = T.1) /\ (A1 = T.2).
    by rewrite HeqT HeqT'.
  move => [] -> [] -> [] -> -> { HeqT HeqT' T1 T2 A1 A2} Hcons_H Hcons Hnfa Hfl Hall Hclos.
  elim: Hclos Hcons_H Hcons Hnfa Hfl Hall => //=.
  move =>  [] T1 A1 [] T2 A2 [] T3 A3 //= Hrel Hclos Hind Hcons_H Hcons Hnfa Hfl.
  move/(Hind Hcons_H Hcons Hnfa Hfl) => {}Hind.
  move: Hind Hrel.
  apply: Rel_small_step_quasi_covered.
  - exact: (cls_Rel_small_step_keep_consistent_head Hclos).
  - exact: (cls_Rel_small_step_keep_cons Hclos).
  - exact: (cls_Rel_small_step_keep_antichain Hclos).
  - exact: (cls_Rel_small_step_keep_fl Hclos).
Qed.


Theorem Rel_small_step_all_covered T A (m0: marking):
  clos_refl_trans_1n _ Rel_small_step (T,A) (KMTree_init m0) ->
  No_Front T ->
  forall m, coverable m0 m -> exists (mc: markingc),
  mc \in Markings_of_T T /\
  m \in mc  .
Proof.
  inversion_clear 1 as [[HT HA] | [T1 A1] [T2 A2] Hrel Hclos ].
  (* the case when we don't do any step, and it is impossible *)
  - move => Hnf m.
    by move/No_Front_abs/(_ nil): Hnf.
  (* the case when we do at least one step *)
  - move => Hnf m.
    move: (Cons_head_init nil m0) => HconsH0.
    move: (Cons_init nil m0) => Hcons0.
    move: (Front_leaves_init m0) => Hfl0.
    move: (@init_quasi_covered nil m0) => Hall0.
    have Hall': (All_quasi_covered m0 T1 A1).
      exact: (@cls_Rel_small_step_quasi_covered _ (Br (embedm m0) true [ffun=> Empty]) _ nil).
    have Hall: (All_quasi_covered m0 T A).
      apply: (@Rel_small_step_quasi_covered _ T1 _ A1) => //.
      + exact: (cls_Rel_small_step_keep_consistent_head Hclos).
      + exact: (cls_Rel_small_step_keep_cons Hclos).
      + exact: (cls_Rel_small_step_keep_antichain Hclos).
      + exact: (cls_Rel_small_step_keep_fl Hclos).
    move/Hall.
    inversion 1 as [mc ad Had Hmc Hleq| s Hleq].
      exists  mc; split => //= .
        exact:(inv2_Markings_of Hmc).
      by rewrite mem_mcP.
    move: s Hleq => [] s mc1 mc2 Hco Hin Hnco HinA //= Hleq.
    move: Hin => [] ad [] _ Habs.
    move: (No_Front_abs Hnf ad).
    by rewrite Habs.
Qed.



End quasi_covered.
End Exploring_sequence.
End Completion.