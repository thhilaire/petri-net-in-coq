(**************************************************************)
(*   Copyright (c) 2024 Thibault Hilaire                      *)
(*                      David Ilcinkas                        *)
(*                      Jerome Leroux                         *)                           
(*   Authors : Thibault Hilaire [*]                           *)
(*             David Ilcinkas [+,*]                           *)
(*             Jerome Leroux  [+,*]                           *)
(*                                                            *)
(*                             [*] Affiliation Univ. Bordeaux *)
(*                             [+] Affiliation CNRS           *)
(**************************************************************)
(*      This file is distributed under the terms of the       *)
(*                      MIT License                           *)
(**************************************************************)

From mathcomp Require Import all_ssreflect zify.
From Coq Require Import Relations.
Import Order.TTheory.
From Petri_Nets_in_Coq.Karp_Miller Require Import orderext petrinet pnkmaccel.
From Petri_Nets_in_Coq.Tools Require Import Utils.
From Petri_Nets_in_Coq.MinCov_AbstractMinCov Require Import New_transitions KMTrees.
From Coq Require Import Arith Lia Wellfounded.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope order_scope.
Local Open Scope petri_net_scope.


Section AbstractMinCov.

Variable pn : petri_net.


Notation place := (place pn).
Notation marking := (marking pn).
Notation markingc := (markingc pn).
Notation transition := (transition pn).
Notation transitionc:= (transitionc pn).
Notation omega_transition:= (omega_transition pn).
Notation abstraction := (abstraction pn).
Notation acceleration := (acceleration pn).
Notation nil_abstraction :=(nil_abstraction pn).
Notation apply_transitionc :=(@apply_transitionc pn).
Notation t_to_abstraction := (@t_to_abstraction pn).
Notation good_compoc := (@good_compoc pn).
Notation KMTree := (KMTree pn).
Notation Empty := (Empty pn).



Section Rel_small_step.

(* The relation corresponding to one step of our algorithm that computes the clover of a petri net *)

Variant Rel_small_step : (KMTree * seq acceleration) -> ( KMTree * seq acceleration) -> Prop :=
| Rel_small_step_sat T A A' ad mc (a:acceleration) mc': Is_Front T ad
    -> List.In a A
    -> Some mc = m_from_add T ad
    -> Some mc' = apply_transitionc mc a
    -> mc' != mc
    -> Rel_small_step (saturate_a_little a T ad, A'++A) (T,A)
| Rel_small_step_cln T A A' ad: Is_Front T ad
    -> saturated_node A T ad
    -> ad_covered_not_front T ad
    -> Rel_small_step (remove_add T ad, A'++A) (T,A)
| Rel_small_step_acc T A A' ad : ~~ Is_Front T ad 
    -> ~~ (saturated_node (A'++A) T ad)
    -> Rel_small_step (to_Front T ad,A'++A) (T,A)
| Rel_small_step_cov T A A' ad mc ad' (mc': markingc): Is_Front T ad
    -> Some mc = m_from_add T ad
    -> Some mc' = m_from_add T ad'
    -> mc' <= mc
    -> ~~ prefix ad' ad
    -> Rel_small_step (remove_add T ad', A'++A) (T,A)
| Rel_small_step_exp T A A' ad: Is_Front T ad
    -> saturated_node A T ad
    -> Not_Front_Antichain (remove_Front T ad)
    -> Rel_small_step (Front_extension T ad, A'++A) (T,A)
.


Lemma Rel_small_step_nEmpty mc :
  Rel_small_step (Front_extension (Br mc true [ffun=> Empty]) nil, nil++nil) ((Br mc true [ffun=> Empty]),nil).
Proof.
  apply: Rel_small_step_exp => //=.
  rewrite /Not_Front_Antichain Markings_not_Front_Br flatten_codom_nil //=.
  by move => t; rewrite ffunE.
Qed.


Lemma Rel_small_step_keep_fl T1 T2 A1 A2:
  Rel_small_step (T1,A1) (T2,A2) -> Front_leaves T2 -> Front_leaves T1.
Proof.
  inversion 1;subst => //= Hfl.
  - exact: saturate_keeps_fl.
  - 1,3: exact: remove_keeps_fl.
  - exact: To_front_keeps_fl.
  - exact: Front_extension_keeps_fl.
Qed.

Lemma cls_Rel_small_step_keep_fl T1 T2 A1 A2:
  clos_refl_trans_1n _ Rel_small_step (T1,A1) (T2,A2) -> Front_leaves T2 -> Front_leaves T1.
Proof.
  remember (T1,A1) as T.
  remember (T2,A2) as T'.
  have : (T2 = T'.1) /\ (T1 = T.1).
    by rewrite HeqT HeqT'.
  move => [] -> -> { HeqT HeqT' T1 T2 A1 A2}.
  elim => //=.
  move =>  [] T1 A1 [] T2 A2 [] T3 A3 Hrel _ Hind.
  move/Hind.
  exact: (Rel_small_step_keep_fl Hrel).
Qed.

Lemma Rel_small_step_keep_consistent T1 T2 A1 A2:
  Rel_small_step (T1,A1) (T2,A2) -> Front_leaves T2 -> consistent_tree A2 T2 -> consistent_tree A1 T1.
Proof.
  inversion_clear 1 as
  [ T A' S ad mc acc mc' HFront Hmc Hmc' Hneq |
    T A' S ad HFront Hsat Hcover |
    T A' S ad  HFront Hacc |
    T A' S ad mc ad' mc' HFront Hmc Hmc' Hleq Hprefix |
    T A' S ad HFront Hac ] => Hcons Hfl.
  all: apply consistent_tree_suffix.
  - exact: saturate_keeps_consistent.
  - 1,3: exact: remove_keeps_consistent.
  - exact: To_Front_keeps_consistent.
  - exact: Front_extension_keeps_consistent.
Qed.

Lemma cls_Rel_small_step_keep_cons T1 T2 A1 A2:
  clos_refl_trans_1n _ Rel_small_step (T1,A1) (T2,A2) ->
  consistent_tree A2 T2 ->
  Front_leaves T2 ->
  consistent_tree (A1) T1.
Proof.
  remember (T1,A1) as T.
  remember (T2,A2) as T'.
  have : (T2 = T'.1) /\ (A2 = T'.2) /\ (T1 = T.1) /\ (A1 = T.2).
    by rewrite HeqT HeqT'.
  move => [] -> [] -> [] -> -> { HeqT HeqT' T1 T2 A1 A2}.
  elim => //=.
  move =>  [] T1 A1 [] T2 A2 [] T3 A3 Hrel Hclos Hind.
  move/Hind => {}Hind Hfl.
  have Hfl2: (Front_leaves T2).
    exact: (cls_Rel_small_step_keep_fl Hclos Hfl).
  move: Hfl2 (Hind Hfl).
  exact: (Rel_small_step_keep_consistent Hrel).
Qed.


Lemma Rel_small_step_keep_antichain T1 T2 A1 A2:
  Rel_small_step (T1,A1) (T2,A2) ->
  Front_leaves T2 ->
  Not_Front_Antichain T2 ->
  Not_Front_Antichain T1.
Proof.
  inversion_clear 1 => //= Hgood HNFA.
  - by rewrite /Not_Front_Antichain Saturate_F_keep_NF.
  - 1,3: exact: Remove_keep_antichain.
  - exact: To_Front_keep_antichain.
  - by rewrite /Not_Front_Antichain -Ext_keep_antichain.
Qed.

Lemma cls_Rel_small_step_keep_antichain T1 T2 A1 A2:
  clos_refl_trans_1n _ Rel_small_step (T1,A1) (T2,A2) ->
  Not_Front_Antichain T2 ->
  Front_leaves T2 ->
  Not_Front_Antichain T1.
Proof.
  remember (T1,A1) as T.
  remember (T2,A2) as T'.
  have : (T2 = T'.1) /\ (T1 = T.1).
    by rewrite HeqT HeqT'.
  move => [] ->  -> { HeqT HeqT' T1 T2 A1 A2}.
  elim => //=.
  move =>  [] T1 A1 [] T2 A2 [] T3 A3 Hrel Hclos Hind.
  move/Hind => {}Hind Hfl.
  have Hfl2: (Front_leaves T2).
    exact: (cls_Rel_small_step_keep_fl Hclos Hfl).
  move: Hfl2 (Hind Hfl).
  exact: (Rel_small_step_keep_antichain Hrel).
Qed.


Lemma  Rel_small_step_keep_consistent_head T1 T2 A1 A2 mc0:
  Rel_small_step (T1,A1) (T2,A2) ->
  consistent_head A2 mc0 T2 ->
  consistent_head A1 mc0 T1.
Proof.
  move => HR Hcon.
  have: exists A, A1= A++A2.
    by inversion_clear HR; eexists.
  move => [] A HA; subst.
  apply: consistent_head_suffix.
  move: HR Hcon.
  inversion_clear 1 as
  [ T A1 S ad mc acc mc' _ Hin Hmc Hmc' Hneq |
    T A1 S ad HFront _ Hcover |
    T A1 S ad  _ _|
    T A1 S ad _ ad' mc' _ _ Hmc' _ Hprefix |
    T A1 S ad _ _ _ ].
  - move : ad T2 mc acc mc' Hmc Hmc' Hneq Hin.
    elim => [| a l _ ] [ //=|]; last first.
      by [].
    move => mc b f mc1 a mc2 [] -> Hmc2 Hneq Hin [] acc [] Hall Hmc.
    rewrite /saturate_a_little -Hmc2.
    move/negbTE:Hneq => ->.
    exists (acc++[::a]); split.
      apply List.Forall_app; split => //=.
      exact: List.Forall_cons.
    rewrite map_cat map_cons.
    apply:abstraction_compo_seq_cat.
    apply: (@good_compoc _ _ _ mc).
      by rewrite Hmc.
    by rewrite Hmc2 /abstraction_compo_seq /foldr abst_compo_neutralR.
  - have Had:(ad <> nil).
      move => Had; subst; move : Hcover; rewrite /ad_covered_not_front.
      move: HFront.
      by case: T2 => // mc b f /eqP ->.
    generalize dependent ad => ad.
    by elim : ad T2 => [ //= | a l _ ] [|].
  - elim: ad T2 => [| t l _ ] [ //=|]; last first.
      by [].
    move => mc b f.
    by rewrite /to_Front /consistent_head.
 - have Had':(ad' <> nil).
      by move => Had'; rewrite Had' prefix0s in Hprefix.
    generalize dependent ad' => ad'.
    by case : ad' T2 => [ //= | a l ] [|].
  - generalize dependent ad => ad.
    by case : ad T2 => [ //= | a l ] [|].
Qed.

Lemma cls_Rel_small_step_keep_consistent_head T1 T2 A1 A2 mc0:
  clos_refl_trans_1n _ Rel_small_step (T1,A1) (T2,A2) ->
  consistent_head A2 mc0 T2 ->
  consistent_head (A1) mc0 T1.
Proof.
  remember (T1,A1) as T.
  remember (T2,A2) as T'.
  have : (T2 = T'.1) /\ (A2 = T'.2) /\ (T1 = T.1) /\ (A1 = T.2).
    by rewrite HeqT HeqT'.
  move => [] -> [] -> [] -> -> { HeqT HeqT' T1 T2 A1 A2}.
  elim => //=.
  move =>  [] T1 A1 [] T2 A2 [] T3 A3 Hrel _ Hind.
  move/Hind.
  exact: (Rel_small_step_keep_consistent_head Hrel).
Qed.

End Rel_small_step.

Section Terminal_Rel_small_step.


Lemma term_Rel_small_step_No_Front T A:
  Front_leaves T ->
  Not_Front_Antichain T ->
  consistent_tree A T ->
  (forall T' A', ~ Rel_small_step (T',A') (T,A)) ->
  No_Front T.
Proof.
  move => Hfl Hac Hcon HnR.
  case Hnf: (No_Front T) => //=.
  move/negP/No_No_Front: Hnf => [] ad HFront.
  move: (Is_Front_m_f_ad HFront) => [] mc Hmc.
  case Hsat: (saturated_markingc mc A); last first.
    move/negbT/not_saturated: Hsat => [] a [] mc' [] Hin [] Hmc' Hneq.
    exfalso; apply (HnR (saturate_a_little a T ad) ([::]++A)).
    by apply (@Rel_small_step_sat T A nil ad mc a mc').
  exfalso.
  have: (saturated_node A T ad).
    by apply: (saturate_node_from_add Hmc).
  move => {}Hsat.
  case Hnfa: (Not_Front_Antichain (remove_Front T ad)).
    apply (HnR (Front_extension T ad) ([::]++A)).
    exact: Rel_small_step_exp.
  move/negbT in Hnfa.
  move: (Nfa_remove_Front_inv HFront Hfl Hmc Hnfa).
  case.
    by rewrite Hac.
  move/hasP => [] mc' /inv_Markings_not_front [] ad' [Hmc' Had'] Hor.
  elim : (orP Hor) => {}Hor.
    apply: (HnR (remove_add T ad) (nil++A)).
    apply: Rel_small_step_cln => //=.
    rewrite /ad_covered_not_front -Hmc.
    apply/hasP; exists mc' => //=.
    apply/inv2_Markings_not_front => //=.
      by rewrite Hmc'.
    by [].
  case Hprefix : (prefix ad' ad); last first.
    apply: (HnR (remove_add T ad') (nil++A)).
    apply: (Rel_small_step_cov A nil HFront Hmc Hmc') => //=.
    by apply/negPf.
  case Heq: (mc' == mc).
    apply: (HnR (remove_add T ad) (nil++A)).
    apply: Rel_small_step_cln => //=.
    rewrite /ad_covered_not_front -Hmc.
    apply/hasP; exists mc' => //=.
      apply/inv2_Markings_not_front => //=.
        by rewrite Hmc'.
      by [].
    by move/eqP : Heq => ->.
  have Hposs:Possible_acceleration T ad' ad.
    apply: (Unique_acc Hmc' Hmc) => //.
    rewrite /Order.lt //=.
    apply/andP; split => //=.
    by apply/negbT; rewrite eq_sym.
  move: (@cpt_acc_accelerate _ A T ad' ad mc' Hcon Hposs Hmc') => [] a [] mc'' [] Hmc'' Hlt.
  apply: (HnR (to_Front T ad') ([::a]++A)).
  apply: (Rel_small_step_acc Had' ) => //=.
  apply: (not_saturate_node_from_add Hmc').
  apply/nandP; left.
  rewrite -Hmc''.
  apply/norP; split => //=.
  apply/eqP ; move => []; apply/eqP.
  by move: Hlt; rewrite /Order.lt //= eq_sym; move/andP => [] ->.
Qed.


End Terminal_Rel_small_step.

End AbstractMinCov.