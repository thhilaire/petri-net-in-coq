(**************************************************************)
(*   Copyright (c) 2024 Thibault Hilaire                      *)
(*                      David Ilcinkas                        *)
(*                      Jerome Leroux                         *)                           
(*   Authors : Thibault Hilaire [*]                           *)
(*             David Ilcinkas [+,*]                           *)
(*             Jerome Leroux  [+,*]                           *)
(*                                                            *)
(*                             [*] Affiliation Univ. Bordeaux *)
(*                             [+] Affiliation CNRS           *)
(**************************************************************)
(*      This file is distributed under the terms of the       *)
(*                      MIT License                           *)
(**************************************************************)


From mathcomp Require Import all_ssreflect zify.
From Coq Require Import Relations.
Import Order.TTheory.
From Petri_Nets_in_Coq.Karp_Miller Require Import orderext petrinet pnkmaccel.
From Petri_Nets_in_Coq.Tools Require Import  Utils.
From Petri_Nets_in_Coq.MinCov_AbstractMinCov Require Import New_transitions.
From Coq Require Import Arith Lia Wellfounded .


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope order_scope.
Local Open Scope petri_net_scope.


Section KMTree.

Variable pn : petri_net.


Notation place := (place pn).
Notation marking := (marking pn).
Notation markingc := (markingc pn).
Notation transition := (transition pn).
Notation transitionc:= (transitionc pn).
Notation omega_transition:= (omega_transition pn).
Notation abstraction := (abstraction pn).
Notation acceleration := (acceleration pn).
Notation nil_abstraction :=(nil_abstraction pn).
Notation apply_transitionc :=(@apply_transitionc pn).
Notation t_to_abstraction := (@t_to_abstraction pn).
Notation good_compoc := (@good_compoc pn).



(*
  We define the type KMTree that will be our cover tree for our petri nets:
  - the markingc is the configuration of the node
  - the boolean is here to denote specific nodes, we call them Front nodes and they are
    the nodes from wich we can extend the tree
  - the function f is here to define the children of the current node
*)
Unset Elimination Schemes.

Inductive KMTree :=
| Empty
| Br of markingc &  bool & {ffun transition -> KMTree}.



Lemma KMTree_ind P (PDLf : P Empty)
      (PBr: forall mc b (f : {ffun transition -> KMTree}),
          (forall t, P (f t)) -> P (Br mc b f)) : forall t, P t.
Proof.
  fix IHf 1 => -[|mc b f]; [exact: PDLf|].
  by apply:PBr => t; apply: IHf.
Qed.

Set Elimination Schemes.

Definition KMTree_init (m0: marking): (KMTree *seq acceleration) :=
  ((Br (embedm m0) true [ffun=> Empty]), nil).

Notation vsv := ((@val _ _ _) \o sval ).
Notation vsa := (vsv \o val) .


Definition EmptyTree (T: KMTree) : bool := match T with
| Empty => true
| _ => false
end.

Lemma  EmptyTreeT T:
   (EmptyTree T) -> T= Empty .
Proof.
  by elim : T.
Qed.


(*
  We define the address of a node in the KMTree, it will help us
  access specific nodes in a KMTree from the root.
  An address will be called consistent with a KMTree if it ends
  on a none-empty node.
*)

Definition address:= seq transition.


Section m_from_add.

(*
  We define the fonction that gives us a markingc from an address and a KMTree when
  the address is consistent, and returns None otherwise
*)

Fixpoint m_from_add T a : option markingc := match T,a with
| Empty, _ => None
| Br mc b f, nil => Some mc
| Br mc b f, t::l => m_from_add (f t) l
end.

Lemma m_from_add_Br mc b f t ad:
  m_from_add (Br mc b f) (t::ad) = m_from_add (f t) ad  .
Proof.
  by [].
Qed.

Lemma m_from_add_prefix mc T ad s:
  Some mc = m_from_add T (ad++s) ->
  exists mc', Some mc' = m_from_add T ad.
Proof.
  elim: ad T s => [//=| t ad Hind] [//=| mc0  b f] s.
    by move => _; exists mc0.
  rewrite cat_cons !m_from_add_Br.
  exact:Hind.
Qed.

End m_from_add.

Section Is_Front.

(*
  We define a predicate over a KMTree and an address computing whether the address
  corresponds to a front node
*)

Fixpoint Is_Front T ad : bool := match T,ad with
| Br mc b f, nil => b == true
| Br mc b f, a::l => Is_Front (f a) l
|_,_ => false
end.

Lemma Is_front_Br mc b f t ad:
  Is_Front (Br mc b f) (t::ad) = Is_Front (f t) ad  .
Proof.
  by [].
Qed.

Lemma Is_Front_m_f_ad T ad:
  Is_Front T ad ->
  exists mc, Some mc = m_from_add T ad.
Proof.
  elim: ad T => [//=| t ad Hind] [//=| mc b f].
    move => _; by exists mc.
  rewrite Is_front_Br m_from_add_Br.
  exact: Hind.
Qed.

End Is_Front.

Section remove.


(*
  remove_add is a function that removes the subtree at an address in T
  but does nothing if the address is not consistent with the KMTree
*)

Fixpoint remove_add T a : KMTree := match T,a with
| _, nil => Empty
| Br mc b f, t'::a' =>
    Br mc b [ffun t => if t==t' then remove_add (f t) a' else f t]
| _,_ => T
end.

Lemma remove_add_Br mc b f t ad:
  remove_add (Br mc b f) (t::ad) =
  Br mc b [ffun t0 => if t0 == t then remove_add (f t0) ad else f t0]  .
Proof.
  by [].
Qed.

Lemma remove_Empty T t ad:
  remove_add T (t::ad) = Empty -> T = Empty.
Proof.
   by elim: T t ad.
Qed.

Lemma remove_nil T :
  remove_add T nil = Empty.
Proof.
  by elim : T.
Qed.

Lemma remove_m_from_ad T ad:
  m_from_add (remove_add T ad) ad = None.
Proof.
   elim: ad T => [//=|t ad Hind] [//=| mc b f] //=.
   by rewrite ffunE eqxx.
Qed.

Lemma remove_m_from_prefix T ad ad':
  prefix ad' ad ->
  m_from_add (remove_add T ad') ad = None.
Proof.
  move => /prefixP-[] s ->.
   elim: ad' s T => [//=|t {}ad Hind] s [//=| mc b f] //=.
   by rewrite ffunE eqxx.
Qed.

Lemma remove_keeps_front T ad ad' :
  ~~(ad ==ad') ->
  ~~ prefix ad' ad ->
  Is_Front T ad = Is_Front (remove_add T ad') ad.
Proof.
  move => Heq Hprefix.
  elim : ad T ad' Heq Hprefix => //= [ | t ad Hind ] [|mc b f] [|t' ad'] //.
  case Heq:(t==t'); last first.
    by rewrite remove_add_Br !Is_front_Br ffunE Heq.
  move/eqP: Heq => <-.
  move /eqP /eqseqP => /=.
  rewrite //= eqxx andTb ffunE eqxx => Heq Hprefix.
  exact: Hind.
Qed.

Lemma remove_keeps_m_from_ad T ad ad' :
  ~~(ad ==ad') ->
  ~~ prefix ad' ad ->
  m_from_add T ad = m_from_add ( remove_add T ad') ad.
Proof.
  move => Heq Hprefix.
  elim : ad T ad' Heq Hprefix => //= [ | t ad Hind ] [|mc b f] [|t' ad'] //.
  case Heq:(t==t'); last first.
    by rewrite remove_add_Br !m_from_add_Br ffunE Heq.
  move/eqP: Heq => <-.
  move /eqP /eqseqP => /=.
  rewrite //= eqxx andTb ffunE eqxx => Heq Hprefix.
  exact: Hind.
Qed.

End remove.

Section to_Front.


(*
  to_Front is a function that sets the node at the address as a Front node
  and clears the subtrees below it.
  It does nothing if the address is not consistent with the KMTree.
*)

Fixpoint to_Front T ad : KMTree := match T,ad with
| Empty,_ => Empty
| Br mc b f, nil   => Br mc true [ffun t => Empty]
| Br mc b f, t0::l => Br mc b    [ffun t => if t==t0 then (to_Front (f t) l) else (f t)]
end.


Lemma to_Front_Br mc b f t ad:
  to_Front (Br mc b f) (t::ad) =
  Br mc b [ffun t0 => if t0 == t then to_Front (f t0) ad else f t0]  .
Proof.
  by [].
Qed.

Lemma Remove_absorb_to_front T ad:
  remove_add T ad = remove_add (to_Front T ad) ad.
Proof.
  elim : ad T => [//=|t ad Hind] [//=|mc b f] //.
  rewrite to_Front_Br !remove_add_Br.
  apply f_equal; apply/ffunP => t0.
  rewrite !ffunE.
  by case :(t0==t).
Qed.

Lemma to_front_suffix T ad s:
  s != nil ->
  None = m_from_add (to_Front T ad) (ad++s) .
Proof.
  elim: ad T s => [//=|t ad Hind] [//=| mc b f] [//=|t' s] _.
    by rewrite /to_Front m_from_add_Br ffunE.
  rewrite to_Front_Br cat_cons m_from_add_Br ffunE eqxx.
  exact:Hind.
Qed.


Lemma from_ad_to_Front:
  forall T ad mc , Some mc = m_from_add T ad -> Some mc = m_from_add ( to_Front T ad) ad .
Proof.
  elim => [//=| mc b f Hind] [| t ad] mc'.
    by [].
  rewrite to_Front_Br !m_from_add_Br ffunE eqxx.
  exact:Hind.
Qed.


Lemma Front_to_Front:
  forall T ad mc,  Some mc = m_from_add T ad -> Is_Front (to_Front T ad) ad .
Proof.
  move => /[swap].
  elim => //= [| t ad Hind].
    by case.
  case => // mc b f mc' /= Hmc'.
  rewrite ffunE eqxx.
  exact: (Hind _ _ Hmc').
Qed.

Lemma to_Front_keeps_front T ad ad' :
  ~~(ad ==ad') ->
  ~~ prefix ad' ad ->
  Is_Front T ad = Is_Front (to_Front T ad') ad.
Proof.
  move => Heq Hprefix.
  elim : ad T ad' Heq Hprefix => //= [ | t ad Hind ] [|mc b f] [|t' ad'] //.
  case Heq:(t==t'); last first.
    by rewrite to_Front_Br !Is_front_Br ffunE Heq.
  move/eqP: Heq => <-.
  move /eqP /eqseqP => /=.
  rewrite //= eqxx andTb ffunE eqxx => Heq Hprefix.
  exact: Hind.
Qed.

Lemma to_front_keeps_m_from_ad T ad ad' :
  ~~(ad ==ad') ->
  ~~ prefix  ad' ad ->
  m_from_add T ad = m_from_add ( to_Front T ad') ad.
Proof.
  move => Heq Hprefix.
  elim : ad T ad' Heq Hprefix => //= [ | t ad Hind ] [|mc b f] [|t' ad'] //.
  case Heq:(t==t'); last first.
    by rewrite to_Front_Br !m_from_add_Br ffunE Heq.
  move/eqP: Heq => <-.
  move /eqP /eqseqP => /=.
  rewrite //= eqxx andTb ffunE eqxx => Heq Hprefix.
  exact: Hind.
Qed.



End to_Front.

Section remove_Front.

(*
  Remove_Front is the function that takes a KMTree and an address and, if the address
  is consistent, sets the node as not front
*)

Fixpoint remove_Front T ad : KMTree := match T,ad with
| Empty,_ => Empty
| Br mc b f, nil => Br mc false f
| Br mc b f, t0::l => Br mc b [ffun t => if t==t0 then (remove_Front (f t) l) else (f t)]
end.

Lemma remove_Front_Br mc b f t ad:
  remove_Front (Br mc b f) (t::ad) =
  Br mc b [ffun t0 => if t0 == t then remove_Front (f t0) ad else f t0]  .
Proof.
  by [].
Qed.


End remove_Front.

Section Front_extension.

(*
  Front_extension takes a MKTree and an address and extends the node at the address by firing
  any possible transition, adding the new nodes in Front.
  If the address is not consistent, then it does nothing.
*)

Fixpoint Front_extension T ad: KMTree := match T,ad with
| Empty,_ => Empty
| Br mc b f,nil => Br mc false [ffun t => match nextmc mc t with
                                   | None => Empty
                                   | Some mc' => Br mc' true [ffun => Empty]
                                   end]
| Br mc b f,t0::l => Br mc b [ffun t => if t==t0 then (Front_extension (f t) l) else (f t)]
end.



Lemma Front_extension_Br mc b f t ad:
  Front_extension (Br mc b f) (t::ad) =
  Br mc b [ffun t0 => if t0 == t then Front_extension (f t0) ad else f t0]  .
Proof.
  by [].
Qed.

Lemma Front_extension_t T ad (t: transition) mc1 mc2:
  Some mc1 = m_from_add T ad ->
  Some mc2 = apply_transitionc mc1 (t_to_abstraction t) ->
  Some mc2 = m_from_add (Front_extension T ad) (rcons ad t) /\
  Is_Front ( Front_extension T ad) (rcons ad t).
Proof.
  elim : ad T t mc1 mc2 => [|t ad Hind] [//=|mc b f] t0 mc1 mc2.
    move => [] -> Hmc2; rewrite /rcons /Front_extension //= ffunE.
    symmetry in Hmc2.
    by move/eqP/apply_eq_next/eqP: Hmc2 => ->.
  rewrite rcons_cons Front_extension_Br Is_front_Br !m_from_add_Br ffunE eqxx.
  exact:Hind.
Qed.


Lemma Front_extension_none_empty T ad:
  Front_extension T ad = Empty -> T = Empty.
Proof.
  by elim: T ad => //= mc b f Hind [].
Qed.

Lemma Front_extension_or T ad:
  (Front_extension T ad) = T \/ (exists mc, Some mc= m_from_add T ad) .
Proof.
  elim : ad T => [//=|t ad Hind] [//=|mc b f].
  1,3: by left.
  - by right; exists mc.
  -rewrite Front_extension_Br.
    move: (Hind (f t)).
    case => {}Hind.
    left.
      apply f_equal2 => //=.
      apply/ffunP => t'.
      rewrite ffunE.
      case Heq:(t'==t) => //=.
      by move/eqP :Heq => ->.
    by right; rewrite m_from_add_Br.
Qed.


Lemma Front_ext_keeps_front T ad ad' :
  ~~(ad ==ad') ->
  ~~ prefix ad' ad ->
  Is_Front T ad = Is_Front (Front_extension T ad') ad.
Proof.
  move => Heq Hprefix.
  elim : ad T ad' Heq Hprefix => //= [ | t ad Hind ] [|mc b f] [|t' ad'] //.
  case Heq:(t==t'); last first.
    by rewrite Front_extension_Br !Is_front_Br ffunE Heq.
  move/eqP: Heq => <-.
  move /eqP /eqseqP => /=.
  rewrite //= eqxx andTb ffunE eqxx => Heq Hprefix.
  exact: Hind.
Qed.

Lemma Front_ext_not_front T ad:
  ~~ Is_Front (Front_extension T ad) ad .
Proof.
  elim : ad T => [//=| t ad Hind] [//=| mc b f].
    by rewrite /Front_extension.
  by rewrite Front_extension_Br Is_front_Br ffunE eqxx.
Qed.


Lemma Front_ext_keeps_m_from_ad T ad ad' :
  ~~(ad ==ad') ->
  ~~ prefix ad' ad ->
  m_from_add T ad = m_from_add (Front_extension T ad') ad.
Proof.
  move => Heq Hprefix.
  elim : ad T ad' Heq Hprefix => //= [ | t ad Hind ] [|mc b f] [|t' ad'] //.
  case Heq:(t==t'); last first.
    by rewrite Front_extension_Br !m_from_add_Br ffunE Heq.
  move/eqP: Heq => <-.
  move /eqP /eqseqP => /=.
  rewrite //= eqxx andTb ffunE eqxx => Heq Hprefix.
  exact: Hind.
Qed.

Lemma Front_ext_keeps_m_from_same_ad T ad :
  m_from_add T ad = m_from_add (Front_extension T ad) ad.
Proof.
  elim : ad T => //= [ | t ad Hind ] [|mc b f] //.
  by rewrite  Front_extension_Br !m_from_add_Br ffunE eqxx.
Qed.

End Front_extension.

Section Front_leaves.
(*
  Front_leaves is a property over a KMTree T that is true if and only if
  the nodes of T that are front are leaves meaning they don't have any children
*)

Fixpoint Front_leaves T :bool := match T with
| Br mc true f => [ forall t: transition, match f t with
                    | Empty => true
                    | _ => false
                    end ]
| Br mc false  f =>[forall t: transition, Front_leaves (f t)]
| _ => true
end.

Lemma Front_leaves_invariant mc f :
  Front_leaves (Br mc true f) -> (forall t, (f t)= Empty).
Proof.
  rewrite /Front_leaves => /[swap] t /forallP-/(_ t).
  by case: (f t).
Qed.

Lemma Front_leaves_invariant2 mc b f :
  Front_leaves (Br mc b f) -> (forall t, Front_leaves (f t)).
Proof.
  case b.
    by move/Front_leaves_invariant => Hfl t; rewrite Hfl.
  by move/forallP.
Qed.

Lemma Intern_not_front T ad s mc:
  Front_leaves T -> s!=nil -> Some mc == m_from_add T (ad++s) -> ~~ Is_Front T ad.
Proof.
  elim : ad T s mc => /= [| t ad Hind].
    case => // mc  [] // f s m /Front_leaves_invariant.
    by case : s => //= t s ->.
  case => // mc [] f s m Hfl /= Hs Hvalid.
    by rewrite (Front_leaves_invariant Hfl).
  move/Front_leaves_invariant2 in Hfl.
  exact: (Hind _ _ _ (Hfl t) Hs Hvalid).
Qed.

Lemma Not_front_prefix mc T ad s:
  Front_leaves T ->
  Some mc = m_from_add T (ad++s) ->
  ~~ Is_Front T (ad++s) ->
  ~~ Is_Front T ad.
Proof.
  elim : ad T s mc => [//=|t ad Hind] [//=| mc0 b f] s mc; last first.
    rewrite cat_cons !Is_front_Br.
    move/Front_leaves_invariant2/(_ t).
    apply : Hind.
  case: s => // t s.
  case Hb: (b) => //.
  move/Front_leaves_invariant/(_ t).
  by rewrite Is_front_Br m_from_add_Br => ->.
Qed.


Lemma Not_Front_root mc b f t ad:
  Front_leaves (Br mc b f) ->
  Is_Front (Br mc b f) (t::ad) ->
  b = false .
Proof.
  case Hb: b; subst => // Hfl HFront.
  move/Front_leaves_invariant : Hfl => Hfl.
  by move : HFront; rewrite Is_front_Br Hfl.
Qed.

Lemma Not_Front_root2 mc mc' b f t ad:
  Front_leaves (Br mc b f) ->
  Some mc' = m_from_add (Br mc b f) (t::ad) ->
  b=false.
Proof.
  case Hb: b; subst => //.
  by move/Front_leaves_invariant/(_ t); rewrite m_from_add_Br => ->.
Qed.

Lemma Front_leaves_Is_Front T ad:
  Is_Front T ad ->
  Front_leaves T ->
  forall s, s != nil ->
  None = m_from_add T (ad++s) .
Proof.
  elim: ad T => [|t ad Hind] [|mc b f] //.
    move/eqP => -> /Front_leaves_invariant HFront.
    case => [//=| t s _].
    by rewrite m_from_add_Br HFront.
  rewrite Is_front_Br => HFront /Front_leaves_invariant2/(_ t) Hfl //=.
  exact:Hind.
Qed.


Lemma Front_leaves_init m0:
  Front_leaves (KMTree_init m0).1.
Proof.
  by apply/forallP => p; rewrite ffunE.
Qed.

End Front_leaves.

Section Markings_from_tree.


(*
  We define a function that gives the non-front markingcs of a KMTree.
  Intuitively it is only used if the Tree respects Front_leaves, i.e.,
  when the nodes in Front are leaves, which explains why we define it that way.
*)

Fixpoint Markings_of_T T : seq markingc := match T with
| Br mc _ f => mc :: (flatten ( codom (Markings_of_T \o f) ))
| _ => nil
end.

Lemma Markings_of_Br mc b f :
  Markings_of_T (Br mc b f) =
  mc :: (flatten ( codom (Markings_of_T \o f) )) .
Proof.
  by [].
Qed.

Lemma inv_Markings_of (mc : markingc) T:
  mc \in Markings_of_T T ->
  exists ad, Some mc = m_from_add T ad.
Proof.
  elim : T mc => [//=| mc0 b f0 Hind] mc.
  rewrite Markings_of_Br in_cons => Hor.
  elim: (orP Hor).
    by move/eqP => ->; exists nil.
  move/In_flatten_codom => [] t.
  move/Hind => [] ad Hmc.
  by exists (t::ad).
Qed.

Lemma inv2_Markings_of (mc: markingc) T ad :
  Some mc = m_from_add T ad ->
   mc \in (Markings_of_T T).
Proof.
  elim: ad T mc => [|t ad Hind] [|mc0 b f] mc //=.
    by move => [] ->; apply: mem_head.
  move => Hmc.
  rewrite in_cons; apply/orP; right.
  apply /flattenP.
  exists (Markings_of_T (f t)) => //.
    exact: codom_f.
  exact: Hind.
Qed.

Fixpoint Markings_not_front_of_T T : seq markingc := match T with
| Br mc false f => mc :: (flatten ( codom (Markings_not_front_of_T \o f) ))
| _ => nil
end.


Definition Not_Front_Antichain (T : KMTree) : bool := antichain (Markings_not_front_of_T T).


Lemma Markings_not_Front_Br mc f :
  Markings_not_front_of_T (Br mc false f) =
  mc :: (flatten ( codom (Markings_not_front_of_T \o f) )) .
Proof.
  by [].
Qed.

Lemma NFA_invariant mc b f:
  Front_leaves (Br mc b f) ->
  Not_Front_Antichain (Br mc b f) ->
  forall t, Not_Front_Antichain (f t) .
Proof.
  case : b.
    move/Front_leaves_invariant => Hfl Hnfa t.
    by rewrite Hfl.
  move => _ Hnfa t; move : Hnfa.
  apply antichain_subseq.
  rewrite Markings_not_Front_Br.
  apply (@cat_subseq _ nil (Markings_not_front_of_T (f t)) [::mc] _) => //=.
  apply subseq_flatten.
  by rewrite codom_f.
Qed.


Lemma inv_Markings_not_front (mc : markingc) T:
  mc \in Markings_not_front_of_T T ->
  exists ad, Some mc = m_from_add T ad  /\ ~~ Is_Front T ad.
Proof.
  elim : T mc => [//=| mc0 [//=|] f0 Hind] mc.
  rewrite Markings_not_Front_Br in_cons => Hor.
  elim: (orP Hor).
    by move/eqP => ->; exists nil.
  move/In_flatten_codom => [] t.
  move/Hind => [] ad Hmc.
  by exists (t::ad).
Qed.

Lemma inv2_Markings_not_front (mc: markingc) T ad :
  Front_leaves T ->
  Some mc == m_from_add T ad ->
   ~~ Is_Front T ad ->
   mc \in (Markings_not_front_of_T T).
Proof.
  elim: ad T mc => [|t ad Hind] [|mc [] f] mc0 //.
  - move => _ /= /eqP/Some_inj--> _.
    exact: mem_head.
  - by move => /Front_leaves_invariant/(_ t)/=->.
  - move => /= /forallP/(_ t)-Hfl Hmc Hfront.
    move => /(_ _ _ Hfl Hmc Hfront) in Hind.
    rewrite in_cons; apply/orP; right.
    apply /flattenP.
    exists (Markings_not_front_of_T (f t)) => //.
    exact: codom_f.
Qed.

Lemma antichain_Br (mc0 mc1 mc2 : markingc)  f t t' :
  Not_Front_Antichain (Br mc0  false f) ->
  mc1 \in Markings_not_front_of_T ( f t) ->
  mc2 \in Markings_not_front_of_T ( f t') ->
  t' != t ->
  ~~( (mc1 <= mc2) || (mc2 <= mc1)) .
Proof.
  rewrite /Not_Front_Antichain Markings_not_Front_Br /= codomE => /andP[_ H].
  move: H.
  have Hin:( t \in enum transition).
    by apply/mem_enum.
  have Hin':( t' \in enum transition).
    by apply/mem_enum.
  move: Hin Hin'.
  elim: (enum transition) => // a l Hind.
  rewrite in_cons => /orP Hort.
  rewrite in_cons => /orP Hort'.
  case: Hort; case: Hort' => /=.
  - move/eqP => -> /eqP ->.
    by rewrite eqxx.
  3: move => Hin' Hin Hac Hmc1 Hmc2 Hneq.
  3: apply: Hind => //=.
  3: move: Hac; apply antichain_subseq; exact: suffix_subseq.
  - move => Hin' /eqP -> Hac Hmc1 Hmc2 Hneq.
    apply:(In_antichain Hmc1 Hmc2).
    move: Hac; apply antichain_subseq.
    apply: cat_subseq => //=.
    apply: subseq_flatten.
    exact:map_f.
  - move/eqP => -> Hin Hac Hmc1 Hmc2 _.
    rewrite orbC.
    apply : (In_antichain Hmc2 Hmc1).
    move: Hac; apply antichain_subseq.
    apply: cat_subseq => //=.
    apply: subseq_flatten.
    exact:map_f.
Qed.



Lemma NFA_contradiction T ad ad' mc:
  Front_leaves T ->
  Some mc = m_from_add T ad ->
  Some mc = m_from_add T ad' ->
  ~~ Is_Front T ad ->
  ~~ Is_Front T ad' ->
  ad != ad' ->
  Not_Front_Antichain T ->
  False.
Proof.
  elim: ad ad' T mc => [//=|t ad Hind] [//=|t' ad'] [//=|mc0 [//=|] f] mc Hfl.
    - rewrite m_from_add_Br Is_front_Br //=.
      move => [] -> Hmc0 _ Hnf _.
      rewrite /Not_Front_Antichain  Markings_not_Front_Br.
      have Hin:(mc0 \in flatten (codom (Markings_not_front_of_T \o f))).
        apply: (@In_codom_flatten _ _ _ t').
        apply: (@inv2_Markings_not_front _ _ ad') => //=.
          by move/Front_leaves_invariant2 : Hfl.
        by rewrite Hmc0.
      rewrite -cat1s => Hac.
      have Hin': (mc0 \in [::mc0]).
        by rewrite in_cons eqxx.
      move: (In_antichain Hin' Hin Hac).
      by rewrite Order.POrderTheory.le_refl.
    - rewrite m_from_add_Br Is_front_Br //=.
      move => Hmc []  <- Hnf _ _.
      rewrite /Not_Front_Antichain  Markings_not_Front_Br.
      have Hin:(mc \in flatten (codom (Markings_not_front_of_T \o f))).
        apply: (@In_codom_flatten _ _ _ t).
        apply: (@inv2_Markings_not_front _ _ ad) => //=.
          by move/Front_leaves_invariant2 : Hfl.
        by rewrite Hmc.
      rewrite -cat1s => Hac.
      have Hin': (mc \in [::mc]).
        by rewrite in_cons eqxx.
      move: (In_antichain Hin' Hin Hac).
      by rewrite Order.POrderTheory.le_refl.
    - move/Front_leaves_invariant in Hfl.
      by rewrite (Hfl mc t) .
    - rewrite !m_from_add_Br !Is_front_Br => Hmc Hmc' Hnf Hnf'.
      case Heq:(t==t').
        case Heq':(ad==ad').
          move/eqP in Heq; move/eqP in Heq'; subst.
          by rewrite eqxx.
        move/eqP in Heq; move/eqP/eqP in Heq'; subst.
        rewrite /Not_Front_Antichain Markings_not_Front_Br /antichain /pairwise => _ /andP [_ H].
        apply:(Hind ad' (f t') mc) => //=.
          by move/Front_leaves_invariant2: Hfl.
        move: H; apply antichain_subseq.
        apply: subseq_flatten.
        exact: codom_f.
      move =>_ Hac.
      rewrite eq_sym in Heq.
      move/eqP/eqP in Heq.
      have Hin:(mc \in (Markings_not_front_of_T (f t))).
        apply: (@inv2_Markings_not_front _ _ ad) => //=.
          by move/Front_leaves_invariant2 : Hfl.
        by rewrite Hmc.
      have Hin':(mc \in (Markings_not_front_of_T (f t'))).
        apply: (@inv2_Markings_not_front _ _ ad') => //=.
          by move/Front_leaves_invariant2 : Hfl.
        by rewrite Hmc'.
      move: (antichain_Br Hac Hin Hin' Heq ).
      by rewrite Order.POrderTheory.le_refl.
Qed.

Lemma Remove_front_Mnf T ad mc :
  Is_Front T ad ->
  Front_leaves T ->
  Some mc = m_from_add T ad ->
  exists s s', s++s' = Markings_not_front_of_T T /\
  s++(mc::s')= Markings_not_front_of_T (remove_Front T ad).
Proof.
  elim: ad T mc => [|t ad Hind] [//=| mc0 b0 f0 ] mc .
    move => //= /eqP -> /Front_leaves_invariant Hfl [] ->; exists nil, nil; rewrite !cat0s.
    split => //=; apply f_equal.
    by rewrite flatten_codom_nil //= => t; rewrite Hfl.
  rewrite Is_front_Br m_from_add_Br => HFront.
  case: b0.
    by move/Front_leaves_invariant => ->.
  move/Front_leaves_invariant2 => Hfl Hmc.
  rewrite remove_Front_Br !Markings_not_Front_Br !codomE.
  move: (@Dec_enum transition t) => [] s1 [] s2 [] Hs1 [] Hs2 ->.
  rewrite !map_cat !map_cons !flatten_cat //= ffunE eqxx.
  move/(_ _ _ HFront (Hfl t) Hmc): Hind => [] s3 [] s4 [] <- <-.
  exists (mc0 :: flatten [seq (Markings_not_front_of_T \o f0) i | i <- s1] ++ s3),
  (s4 ++ flatten [seq (Markings_not_front_of_T \o f0) i | i <- s2]).
  rewrite !cat_cons !catA //=; split => //=.
  apply f_equal => //=.
  rewrite -!catA //=; apply f_equal2.
  2: apply f_equal, f_equal, f_equal.
  all: apply f_equal, eq_in_map => t0.
  1:move/Hs1.
  2: move/Hs2.
  all: by move/negbTE => //=; rewrite ffunE => ->.
Qed.

Lemma Nfa_remove_Front_inv T ad mc:
  Is_Front T ad ->
  Front_leaves T ->
  Some mc = m_from_add T ad ->
  ~~Not_Front_Antichain (remove_Front T ad) ->
  ~~Not_Front_Antichain T \/
  has ((fun m => (mc <= m )||(m <= mc )) ) (Markings_not_front_of_T T).
Proof.
  move => Hfront Hfl Hmc.
  move: (Remove_front_Mnf Hfront Hfl Hmc) => [] s [] s' [].
  rewrite /Not_Front_Antichain => <- <-.
  move/contra: (@in_middle_antichain _ s s' mc) => Hac.
  move/Hac; rewrite cat_cons //=; move/nandP.
  case => {}Hac.
    right; move/allPn: Hac => [] mc' Hin /negbNE Hor.
    by apply/hasP; exists mc'.
  by left.
Qed.


Lemma remove_marking_not_front T ad:
  Is_Front T ad ->
  Markings_not_front_of_T T = Markings_not_front_of_T (remove_add T ad) .
Proof.
  elim : ad T  => [| t ad Hind] [//=| mc b f] .
    by move/eqP => ->.
  case: b => //= Hfront.
  apply f_equal, f_equal.
  apply eq_codom => t0.
  rewrite /comp ffunE.
  case H: (t0==t) => //=.
  by apply:Hind; move/eqP : H => ->.
Qed.

Lemma to_front_markingc_not_front T ad:
  subseq (Markings_not_front_of_T (to_Front T ad)) (Markings_not_front_of_T T) .
Proof.
  elim : ad T => [//=|t ad Hind] [//=| mc [//=|] f].
    by rewrite /to_Front.
  rewrite to_Front_Br !Markings_not_Front_Br subseqE !codomE.
  move: (Dec_enum t) => [] l1 [] l2 [] Hl1 [] Hl2 ->.
  rewrite !map_cat !map_cons //= !flatten_cat ffunE eqxx //=.
  have H:(forall l , (forall t' : transition, t' \in l -> t' != t) ->
  [seq (Markings_not_front_of_T \o [ffun t0 => if t0 == t then to_Front (f t0) ad else f t0]) i| i <- l]
  =
  [seq (Markings_not_front_of_T \o f) i | i <- l]
  ).
    move => l Hl //=.
    apply eq_in_map => t'.
    rewrite !ffunE.
    by move/Hl/negPf => ->.
  apply cat_subseq.
    by rewrite (H l1 Hl1).
  apply cat_subseq.
    exact:Hind.
  by rewrite (H l2 Hl2).
Qed.


Lemma Front_extension_m_not_front T ad mc mc':
  Some mc = m_from_add T ad ->
  mc' \in Markings_not_front_of_T (Front_extension T ad) ->
  mc' = mc \/ mc' \in Markings_not_front_of_T T .
Proof.
  elim: ad T mc mc' => [|t ad Hind] [//=|mc b f] mc1 mc2.
    move => [] ->.
    rewrite in_cons => /orP.
    case.
      by move/eqP => -> ; left.
    move/In_flatten_codom => [] t //=.
    rewrite ffunE.
    by case : nextmc.
  rewrite m_from_add_Br Front_extension_Br.
  case : b => //=.
  move/Hind => {}Hind.
  rewrite in_cons => /orP.
  case.
    by move/eqP => -> ; right; rewrite in_cons eqxx.
  move/In_flatten_codom => [] t' //=.
  rewrite ffunE.
  case Heq:(t'==t).
    move/eqP: Heq => ->.
    move/Hind.
    case => [->| Hin].
      by left.
    right.
    rewrite in_cons; apply/orP; right.
    exact: (In_codom_flatten Hin).
  move => Hin.
  right; rewrite in_cons.
  apply/orP; right.
  exact: (In_codom_flatten Hin).
Qed.



Lemma in_mnf_Front_extension T ad mc mc':
  mc \in Markings_not_front_of_T (Front_extension T ad) ->
  Some mc'= m_from_add T ad ->
  mc = mc' \/ mc \in Markings_not_front_of_T T .
Proof.
  elim : ad T mc mc' => [|t ad Hind] [//=|mc b f] mc1 mc2.
    have :(Markings_not_front_of_T (Front_extension (Br mc b f) [::]) = [::mc]).
      rewrite /Front_extension Markings_not_Front_Br.
      apply f_equal.
      apply:flatten_codom_nil => t.
      rewrite //= ffunE.
      by case: nextmc.
    move => ->.
    rewrite in_cons //=.
    by move/orP; case => //= /eqP => -> [] ->; left.
  rewrite Front_extension_Br m_from_add_Br //=.
  case: b => //=.
  rewrite in_cons => /orP.
  case.
    move/eqP => -> _; right; by rewrite in_cons eqxx.
  move/In_flatten_codom => [] t0 //=.
  rewrite ffunE.
  case  Heq: (t0==t); last first.
    move => Hin _; right.
    rewrite in_cons; apply/orP; right.
    exact: (In_codom_flatten Hin).
  move/eqP: Heq => ->.
  move => H1 H2; move: (Hind _ _ _ H1 H2).
  case.
    by move => ->; left.
  move => Hin; right;rewrite in_cons; apply/orP; right.
  exact: (In_codom_flatten Hin).
Qed.

Lemma Nfa_init m0 :
  Not_Front_Antichain (KMTree_init m0).1.
Proof.
  by [].
Qed.


End Markings_from_tree.

Section Covered_in_T.

(*
  We define a property over a KMTree computing whether a configuration of a KMTree
  is covered by a non-front configuration of the same KMTree
*)


Definition covered_not_front T mc : bool :=
  has (fun m => mc <= m) (Markings_not_front_of_T T).

Definition ad_covered_not_front  T  ad : bool := match m_from_add T ad with
| None => false
| Some mc => has (fun m => mc <= m) (Markings_not_front_of_T T)
end.

Lemma inv_ad_covered_not_front T ad:
  ad_covered_not_front T ad ->
  exists mc mc' ad', Some mc = m_from_add T ad /\
  Some mc' = m_from_add T ad' /\
  mc <= mc' /\ ~~ Is_Front T ad'.
Proof.
  rewrite /ad_covered_not_front.
  case : (m_from_add) => [mc| //=].
  move/hasP => [] mc' /inv_Markings_not_front [] ad' [Hmc' HFront] Hleq.
  by exists mc, mc', ad'.
Qed.


Lemma not_Cnf_to_front mc T ad s:
  Front_leaves T ->
  ~~ Is_Front T (ad++s) ->
  Some mc = m_from_add T (ad++s) ->
  Not_Front_Antichain T ->
  ~~ covered_not_front (to_Front T ad) mc .
Proof.
  move => Hfl Hnf Hmc Hnfa.
  apply/negP; move/hasP => [] mc'.
  case Heq:(mc == mc'); last first.
    have Hin: (mc \in Markings_not_front_of_T T).
      apply: (@inv2_Markings_not_front _ _ (ad++s)) => //=.
      by rewrite Hmc.
    move/mem_subseq: (to_front_markingc_not_front T ad) => Hsubset.
    move/Hsubset => Hin' Hleq.
    move/negP/negP in Heq.
    move: (In_same_antichain Hin Hin' Heq Hnfa).
    by rewrite Hleq.
  move/eqP : Heq => <-.
  move/inv_Markings_not_front => [] ad' [Had' Hfront].
  case Hprefix : (prefix ad ad').
    move: Hprefix => /prefixP [] s' Heq _; subst.
    case: s' Hfront Had' => [|t s' Hind].
      move : (m_from_add_prefix Hmc ) => [] mc'' Hmc''.
      by rewrite cats0 (Front_to_Front Hmc'').
    by rewrite -to_front_suffix.
  move => _; move: Hprefix Hfront Had' => /idPn Hnotprefix.
  rewrite -(@to_Front_keeps_front T ad' ad ) //=.
  rewrite -(@to_front_keeps_m_from_ad T ad' ad) //=.
  2,3: apply/eqP => H; subst.
  2,3: move/negP in Hnotprefix; apply: Hnotprefix.
  2,3: by rewrite prefix_refl.
  move => Hnf' Hmc'.
  apply:(NFA_contradiction Hfl Hmc' Hmc) => //.
  apply/negP.
  move: Hnotprefix => /[swap] /eqP->.
  by rewrite prefix_prefix.
Qed.

Lemma not_Cnf_to_frontM T ad s mc1 mc2:
  Front_leaves T ->
  Not_Front_Antichain T ->
  ~~ Is_Front T (ad++s) ->
  Some mc1 = m_from_add T (ad++s) ->
  mc1 <= mc2 ->
  ~~ covered_not_front (to_Front T ad) mc2.
Proof.
  move => Hfl Hnfa Hnf Hmc1 Hleq.
  apply/negP; move/hasP => [] mc3.
  case Heq:(mc1 == mc3); last first.
    have Hin: (mc1 \in Markings_not_front_of_T T).
      apply: (@inv2_Markings_not_front _ _ (ad++s)) => //=.
      by rewrite Hmc1.
    move/mem_subseq: (to_front_markingc_not_front T ad) => Hsubset.
    move/Hsubset => Hin' Hleq'.
    move/negP/negP in Heq.
    move: (In_same_antichain Hin Hin' Heq Hnfa).
    by rewrite (Order.POrderTheory.le_trans Hleq Hleq') orTb.
  move/eqP : Heq => <-.
  move/inv_Markings_not_front => [] ad' [Had' Hfront].
  case Hprefix : (prefix ad ad').
    move: Hprefix => /prefixP [] s' Heq _; subst.
    case: s' Hfront Had' => [|t s' Hind].
      move : (m_from_add_prefix Hmc1 ) => [] mc' Hmc'.
      by rewrite cats0 (Front_to_Front Hmc').
    by rewrite -to_front_suffix.
  move => Hle; move: Hprefix Hfront Had' => /idPn Hnotprefix.
  rewrite -(@to_Front_keeps_front T ad' ad ) //=.
  rewrite -(@to_front_keeps_m_from_ad T ad' ad) //=.
  2,3: apply/eqP => H; subst.
  2,3: by rewrite prefix_refl in Hnotprefix.
  move => Hnf' Hmc1'.
  apply:(NFA_contradiction Hfl Hmc1' Hmc1) => //.
  apply/negP.
  move: Hnotprefix => /[swap] /eqP->.
  by rewrite prefix_prefix.
Qed.


Lemma Not_covered_not_front_Front_extension T ad mc mc':
  Some mc = m_from_add T ad ->
  ~~ covered_not_front T mc' ->
  ~~ (mc' <= mc) ->
  ~~ covered_not_front (Front_extension T ad) mc'.
Proof.
  move => Hmc Hnotcov Hleq.
  apply/negP.
  rewrite /covered_not_front => [].
  move/hasP => [] mc0 Hin Hleq'.
  move: (in_mnf_Front_extension Hin Hmc).
  case => Habs.
    by move:  Habs Hleq' Hleq => -> ->.
  move/negP : Hnotcov; apply; apply/hasP.
  by exists mc0.
Qed.

End Covered_in_T.

Section Possible_acc.

(*
  We define a predicate on KMTree expressing whether an accelaration
  may be computable from the given addresses
*)

Inductive Possible_acceleration T ad ad' : Prop :=
|Unique_acc mc mc': (Some mc = m_from_add T ad) -> (Some mc' = m_from_add T ad')
-> (mc < mc')  -> prefix ad ad' -> Possible_acceleration T ad ad'.

Lemma Poss_acc_inv T ad ad':
  Possible_acceleration T ad ad' ->
  (exists mc mc', Some mc = m_from_add T ad /\ Some mc' = m_from_add T ad').
Proof.
  inversion 1 as [mc mc' Hmc Hmc' Hneq Hs].
  by exists mc, mc'.
Qed.


Definition No_Possible_acc T ad' : Prop :=
forall ad mc mc', (Some mc = m_from_add T ad) -> (Some mc' = m_from_add T ad') ->
prefix ad ad' -> ~(mc < mc').

Lemma No_poss_acc_inv T ad':
  No_Possible_acc T ad' ->
  forall ad, ~ Possible_acceleration T ad ad'.
Proof.
  move => Hnpa ad.
  inversion_clear 1 as [mc mc' Hmc Hmc' Hlt Hs].
  by move: Hlt (Hnpa ad mc mc' Hmc Hmc' Hs).
Qed.

Lemma Poss_acc_Dec T ad mc:
  Some mc = m_from_add T ad ->
  (exists ad', Possible_acceleration T ad' ad) \/ (No_Possible_acc T ad).
Proof.
  elim: ad T mc => [|t ad Hind] [|mc0 b f] mc //=.
  - move => [] <-.
    right => ad mc1 mc2 Hmc [] ->.
    rewrite prefixs0.
    move: Hmc => /[swap] /eqP -> /= [] ->.
    by rewrite /Order.lt //= eqxx.
  - move => Hmc.
    move: (Hind (f t) mc Hmc).
    elim.
      move => [] ad' [] mc1 mc2 Hmc1 Hmc2 Hlt Hprefix.
      left; exists (t::ad').
      apply: (@Unique_acc _ _ _ mc1 mc2 ) => //=.
      by rewrite eqxx Hprefix.
    move => Hnposs.
    case Hlt: (mc0 < mc).
      left; exists nil.
      exact: (@Unique_acc _ _ _ mc0 mc ) .
    right => [] [| t' ad'] mc1 mc2.
      move => [] -> //=.
      rewrite -Hmc => [] [] ->.
      by move: Hlt => ->.
    move => //= Hmc1 Hmc2.
    inversion 1. move: H {H1} => /andP [/eqP-Heq Hprefix].
    subst => Hlt'.
    move/No_poss_acc_inv: Hnposs.
    move/(_ ad'); apply.
    apply: (@Unique_acc _ _ _ mc1 mc2 ) => //.
Qed.

Lemma No_poss_remove T ad ad':
  No_Possible_acc T ad ->
  No_Possible_acc (remove_add T ad') ad .
Proof.
  move => Hnp s1 mc mc'.
  move/(_ s1 mc mc') in Hnp.
  case Had's1: (s1==ad').
    move/eqP: Had's1 => ->.
    by rewrite remove_m_from_ad.
  case Hprefix: (prefix ad' s1).
    move: Hprefix => /idP.
    by move/remove_m_from_prefix => ->.
  move/negbT in Had's1; move/idPn : Hprefix => Hnprefix.
  rewrite -remove_keeps_m_from_ad //=.
  case Had'ad: (ad==ad').
    move/eqP: Had'ad => ->.
    by rewrite remove_m_from_ad.
  case Hprefix: (prefix ad' ad).
    move: Hprefix => /idP.
    by move/remove_m_from_prefix => ->.
  move/negbT in Had'ad; move/idPn : Hprefix => Hnprefix2.
  by rewrite -remove_keeps_m_from_ad.
Qed.


End Possible_acc.


Section All_Acc.

(* We define a predicate over sequences of accelerations *)

Definition All_Acc (A: seq acceleration) : seq acceleration -> Prop :=
  fun l => List.Forall (fun x => List.In x A) l.

Lemma All_Acc_suffix s1 s2 s3:
  All_Acc s3 s1 ->
  All_Acc (s2++s3) s1.
Proof.
  elim: s1 s2 s3 => //= [|a s Hind] s1 s2.
    by rewrite /All_Acc.
  inversion_clear 1.
  apply List.Forall_cons; last first.
    exact: Hind.
  by apply List.in_or_app; right.
Qed.


Lemma saturated_marking_all_Acc (mc mc': markingc) (s : seq acceleration) (A : seq acceleration):
  All_Acc A s ->
  saturated_markingc mc A ->
  Some mc' = apply_transitionc mc (abstraction_compo_seq (map val s)) ->
  mc = mc'.
Proof.
  elim : s A mc mc' => [//| a s Hind] A mc mc'.
    move => _ _ ; rewrite /map /abstraction_compo_seq /foldr.
    by cbn; move/eqP: (nil_transitionc_id mc) => -> [].
  inversion_clear 1 as [| a' s' Ha Hs] => Hsat.
  rewrite map_cons abstraction_compo_seq1 => Hmc'.
  apply:(Hind A) => //.
  suff H:(Some mc =apply_transitionc mc (val a)).
    move: Hmc' => ->.
    by rewrite -good_compo_transitionc -H.
  move/allP'/(_ a Ha)/orP : Hsat.
  case.
    by move/eqP => ->.
  move/eqP => Hnone.
  by rewrite -good_compo_transitionc -Hnone in Hmc'.
Qed.

Definition Eaccelerate A mc mc':=
  exists (acc : seq acceleration), All_Acc A acc /\
  Some mc == apply_transitionc  mc'  (abstraction_compo_seq (map val acc)).

End All_Acc.


Section consistent_tree.

(*
  Now we define a property allowing us to restrict ourself to KMTrees
  that correspond to cover trees in the petri net. By this we mean
  KMTrees where the markingc of a child of a node is obtained by firing
  the transition correponding to the edge and the accelerations from
  the markingc of the parent node.
*)


Definition consistent_head A (mc0 : marking) (T : KMTree) : Prop := match T with
| Empty => true
| Br mc b f => Eaccelerate A mc (embedm mc0)
end.

Lemma consistent_head_suffix A A' mc T :
  consistent_head A mc T ->
  consistent_head (A'++A) mc T.
Proof.
  elim: T A A' => [//=| mc' b f Hind] A A' //=.
  move => [] acc [] Hall Hmc'.
  exists acc; split => //=.
  exact: All_Acc_suffix.
Qed.

Lemma Cons_head_init  A m0 :
  consistent_head A m0 (KMTree_init m0).1.
Proof.
  exists nil; split => //=.
  by move/eqP: (nil_transitionc_id (embedm m0)) => ->.
Qed.


Definition consistent_child A (mc : markingc) (f : {ffun transition -> KMTree}) (t : transition) : Prop :=
match nextmc mc t with
| None =>  EmptyTree ( f t)
| Some mc1 => match (f t) with
              | Empty => True
              | Br mc2 b f' => Eaccelerate A mc2 mc1
              end
end.

Lemma consistent_child_suffix A A' mc f t :
  consistent_child A mc f t ->
  consistent_child (A'++A) mc f t.
Proof.
  rewrite /consistent_child.
  case: nextmc => //= mc'.
  case: (f t) => //= mc'' _ _ [] acc [] /(All_Acc_suffix A') H1 H2 .
  by exists acc.
Qed.

Fixpoint consistent_tree A (T: KMTree) : Prop := match T with
| Empty => True
| Br mc b f => forall t, (consistent_child A mc f t)/\( consistent_tree A (f t))
end.

Lemma consistent_tree_Br A mc b f:
  consistent_tree A (Br mc b f) =(forall t, (consistent_child A mc f t) /\ consistent_tree A (f t)).
Proof.
  by [].
Qed.

Lemma consistent_tree_suffix A A' T :
  consistent_tree A T ->
  consistent_tree (A'++A) T.
Proof.
  elim: T A A' => [//=| mc b f Hind] A A'.
  rewrite consistent_tree_Br => Hcon t.
  split.
  1: apply: consistent_child_suffix.
  2:apply:Hind.
  all: by move:(Hcon t) => [Hchild Hcon'].
Qed.

Lemma Cons_init A m0:
  consistent_tree A (KMTree_init m0).1.
Proof.
  move => t; rewrite /consistent_child ffunE.
  by split => //=;case: nextmc.
Qed.


End consistent_tree.

Section Compute_acceleration.

(*
  We define a way to compute acceleration and abstraction of the petri net
  by looking at the KMTree if it is consistent
*)
Definition comp_acc_aux (s: seq (transition * seq acceleration)) :=
  [seq abstraction_compo (t_to_abstraction i.1) (abstraction_compo_seq (map val i.2))| i <- s].

Definition computing_acceleration (s: seq (transition * seq acceleration)): acceleration :=
  abs_to_acc (abstraction_compo_seq (comp_acc_aux s)).


Lemma consistent_cpt_acc A T ad s mc mc':
  consistent_tree A T ->
  Some mc = m_from_add T ad ->
  Some mc' = m_from_add T (ad ++ s) ->
  exists (acc: seq (transition * seq acceleration)),
  List.Forall (fun x => All_Acc A x.2 ) acc /\
  Some mc' = apply_transitionc mc (abstraction_compo_seq (comp_acc_aux acc)) /\
  (forall s1 s2, s1++s2= acc -> s2 <> nil -> exists mc'' s3 s4, Some mc'' = apply_transitionc mc (abstraction_compo_seq (comp_acc_aux s1))
  /\ s3++s4 = s
  /\ s4 != nil
  /\ Some mc'' = m_from_add T (ad++s3) ) .
Proof.
  (* we want to know where to start computing *)
  elim : ad s T mc mc' => [| t ad Hind]; last first.
    move => s [//=|mc0 b0 f0] mc mc'.
    rewrite consistent_tree_Br =>/(_ t) [_ Hcon].
    rewrite cat_cons !m_from_add_Br /comp_acc_aux -/comp_acc_aux.
    by apply Hind.
  case  => [| t s /=] [//=|mc0 b0 f0].
    move => mc mc' _ [] -> [] -> /=.
    exists nil; split => //=; split.
      cbn; symmetry; apply /eqP.
      exact : nil_transitionc_id.
    move => s1 s2 /nilP.
    by rewrite cat_nilp => /andP [] /nilP -> /nilP ->.
  elim: s t mc0 b0 f0 => [t mc0 b0 f0 | t' s Hind t mc0 b0 f0 ] mc mc'.
  all: rewrite consistent_tree_Br => /(_ t) /=.
  all: case Hf0: (f0 t) => [//=| mc1 b1 f1] [Hchild Hcon].
  all: move: Hchild; rewrite /consistent_child; case Hnext: (nextmc) => [mc2|]; last first.
  1,3:by rewrite Hf0.
  all: rewrite Hf0; move/eqP : Hnext; move/next_eq_apply => /eqP Hmc2.
  all: rewrite /Eaccelerate => [] [] acc [] Hall /eqP Hmc1.
    move => [] -> [] ->.
    exists [::(t,acc)].
    rewrite /comp_acc_aux map_cons /fst /snd /(map _ nil) abstraction_compo_seq1.
    split.
      exact:List.Forall_cons.
    split.
      apply/eqP; apply: (@good_compoc _ _ mc0 mc1); last first .
        by move/eqP: (nil_transitionc_id mc1) => ->.
      by rewrite -good_compo_transitionc Hmc2 //= Hmc1.
    move => s1 s2 Hs.
    move: (cat1 Hs) => {Hs}.
    case => [] [] -> ->; last first.
      exists mc0, nil, [::t]; split => //=.
      cbn; symmetry; apply /eqP.
      exact : nil_transitionc_id.
    by [].
  move => [] H; subst; rewrite m_from_add_Br => Hmc'.
  move: (Hind t' mc1 b1 f1 mc1 mc' Hcon) => [].
  - by [].
  - by rewrite Hmc'.
  - move => s' [] Halls' [Hs1' Hs2'].
    exists ((t,acc)::s').
    split.
      exact: List.Forall_cons.
    rewrite /comp_acc_aux map_cons abstraction_compo_seq1.
    split.
      rewrite -!good_compo_transitionc Hmc2 //= -Hmc1 //=.
    move => s1 s2.
    case: s1 => [ _|a s1].
      exists mc0, nil, [::t,t' & s]; split => //=.
      cbn; symmetry; apply /eqP.
      exact : nil_transitionc_id.
    rewrite cat_cons; inversion 1 as [[Ha Hs']] => Hs2.
    move: (Hs2' s1 s2 Hs' Hs2) => [] mc3 [] s3 [] s4 [] Hms1 [] Hs3s4 Hmc3.
    exists mc3, (t::s3), s4; split; last first.
      split.
        by rewrite cat_cons Hs3s4.
      by rewrite Hf0.
    rewrite map_cons abstraction_compo_seq1 -!good_compo_transitionc Hmc2 //= -Hmc1 //=.
Qed.

Lemma cpt_acc_accelerate A T ad ad' mc:
  consistent_tree A T ->
  Possible_acceleration T ad (ad') ->
  Some mc = m_from_add T ad ->
  exists (a: acceleration) mc', Some mc' = apply_transitionc mc a /\ mc < mc'.
Proof.
  move => Hcons.
  inversion 1 as [mc1 mc2 Hmc1 Hmc2 Hlt Hs].
  move: Hs => /prefixP [s Hprefix]. subst; clear H.
  move: (consistent_cpt_acc Hcons Hmc1 Hmc2) => [] s' [] Hall [Hs1' Hs2'].
  rewrite -Hmc1 => [] [] ->.
  exists (computing_acceleration s').
  rewrite /computing_acceleration.
  remember (abstraction_compo_seq (comp_acc_aux s' )) as b.
  move: (abs_to_acc_inv Hs1' Hlt) => [mc' Hmc'].
  exists mc'; split => //=.
  apply: (Order.POrderTheory.lt_le_trans Hlt).
  by apply: (@abs_to_acc_increase pn mc1 mc2 mc' b).
Qed.

Theorem cover_consistent_KMTree A m0 T:
  consistent_tree A T ->
  consistent_head A m0 T ->
  forall (mc:markingc) m,
  mc \in Markings_of_T T ->
  m \in mc ->
  coverable m0 m.
Proof.
  case : T => [//=| mc0 b f] Hcon.
  rewrite /consistent_head => [] [] acc [] Hall /eqP Hmc0 mc m.
  move/inv_Markings_of => [] ad Hmc.
  move: (@consistent_cpt_acc A (Br mc0 b f) nil ad mc0 mc Hcon Logic.eq_refl Hmc) => [] s [] Halls [Hmc' Hprefix].
  symmetry in Hmc', Hmc0; move: m.
  apply: coverability_preserved_by_abstraction _ Hmc'.
  apply: coverability_preserved_by_abstraction _ Hmc0.
  move => m Hm; exists m0; split.
    by exists nil.
  by move: Hm; rewrite in_embedm.
Qed.


End Compute_acceleration.



Section Saturate_a_little.

(*
  We define a function that applies an acceleration on a node inside a KMTree
  only if this acceleration is useful
*)

Fixpoint saturate_a_little (a : acceleration) (T : KMTree) (ad : address): KMTree := match T,ad with
| Empty,_ => Empty
| Br mc b f, t'::ad' => Br mc b [ffun t => if t==t' then saturate_a_little a (f t) ad' else f t]
| Br mc b f, nil => match apply_transitionc mc a with
                      | None => Br mc b f
                      | Some mc' => if (mc' == mc) then Br mc b f else Br mc' b f
                      end
end.

Lemma saturate0 a T ad : saturate_a_little a T ad = Empty -> T = Empty.
Proof.
  case : T => //= mc b f.
  case : ad => //=.
  case : apply_transitionc => //= mc'.
  by case (mc'== mc).
Qed.

Lemma saturate_a_little_Br a mc b f t ad:
  saturate_a_little a (Br mc b f) (t::ad) =
  Br mc b [ffun t0 => if t0 == t then saturate_a_little a (f t0) ad else f t0]  .
Proof.
  by [].
Qed.

Lemma saturate_none_empty a T ad mc :
  Some mc = m_from_add T ad -> saturate_a_little a T ad <> Empty.
Proof.
  elim : ad T a mc => [|t ad Hind].
    case => //= mc b f a m.
    inversion_clear 1.
    case : apply_transitionc => //= m'.
    by case : (m'==mc).
  by case.
Qed.

Lemma saturate_to_FrontC:
  forall ad T a, (to_Front (saturate_a_little a T ad) ad) = (saturate_a_little a (to_Front T ad) ad).
Proof.
  elim => [|t ad Hind] [//|mc b f] a /=; last first.
    apply: congr1; apply/ffunP => t0.
    rewrite !ffunE.
    by case: (t0==t).
  case: apply_transitionc => // mc0.
  by case: (mc0==mc).
Qed.


Lemma saturate_markingc_not_front a T ad:
  Is_Front T ad ->
  Markings_not_front_of_T T = Markings_not_front_of_T (saturate_a_little a T ad) .
Proof.
  elim : ad T a => [| t ad Hind] [//=| mc b f] a.
    move/eqP => -> //=.
    case: apply_transitionc => //= mc'.
    by case: (mc'==mc).
  rewrite Is_front_Br saturate_a_little_Br => HFront.
  rewrite /Markings_not_front_of_T.
  case b => //=.
  apply f_equal2, f_equal => //.
  rewrite -/Markings_not_front_of_T.
  apply eq_codom => t'.
  rewrite /comp ffunE.
  case Heq:(t'==t) => //=.
  move/eqP : Heq => ->.
  exact: Hind.
Qed.



Lemma saturate_keeps_front a T ad ad' :
  Is_Front T ad = Is_Front (saturate_a_little a T ad') ad.
Proof.
  elim : ad T ad' a => //= [ | t ad Hind ] [|mc b f] [|t' ad'] a //=.
  1,2: case: apply_transitionc => [mc'| //=].
  1,2: by case (mc'==mc).
  rewrite ffunE .
  by case (t==t').
Qed.

Lemma saturate_m_from_ad_None (a: acceleration) T ad mc :
  Some mc = m_from_add T ad ->
  None = apply_transitionc mc a ->
  Some mc = m_from_add (saturate_a_little a T ad) ad.
Proof.
  elim: ad T a mc => [| t ad Hind] [//=|mc0 b f] a mc.
    by move => [] -> /= <-.
  rewrite saturate_a_little_Br !m_from_add_Br ffunE eqxx.
  exact: Hind.
Qed.

Lemma saturate_m_from_ad_Some (a: acceleration) T ad mc mc' :
  Some mc = m_from_add T ad ->
  Some mc' = apply_transitionc mc a ->
  Some mc' = m_from_add (saturate_a_little a T ad) ad.
Proof.
  elim: ad T a mc mc' => [| t ad Hind] [//=|mc0 b f] a mc mc'.
    move => [] -> /= <-.
    case Heq:(mc'==mc0) => //=.
    by move/eqP: Heq => ->.
  rewrite saturate_a_little_Br !m_from_add_Br ffunE eqxx.
  exact: Hind.
Qed.

Lemma saturate_m_from_2ad a T ad ad' :
  ad != ad' -> m_from_add (saturate_a_little a T ad') ad = m_from_add T ad.
Proof.
  elim : ad T ad' a => [|t ad Hind] [|mc b f] [|t' ad'] a //=.
    move => _; case : apply_transitionc => //= mc'.
    by case: (mc'==mc).
  case Ht:(t==t'); last first.
    rewrite ffunE.
    by move/negPf : Ht => ->.
  move/eqP : Ht => ->.
  rewrite ffunE eqxx.
  rewrite /eq_op //= eqxx andTb.
  apply:Hind.
Qed.

End Saturate_a_little.

Section No_Front.

(* We define a predicate over a KMTree that computes
   whether there are still Front nodes inside it *)

Fixpoint No_Front T :bool := match T with
| Br mc true f => false
| Br mc false  f =>[forall t: transition, No_Front (f t)]
| _ => true
end.

Lemma No_Front_Br mc f:
  No_Front (Br mc false f) ->
  forall t, No_Front (f t).
Proof.
  move/forallP => H t.
  by move:(H t).
Qed.

Lemma No_Front_abs T :
  No_Front T ->
  forall ad, ~~ Is_Front T ad .
Proof.
  move => H ad.
  elim: ad T H => [|t ad Hind] [//=|mc [] f] //=.
  by move/forallP/(_ t)/Hind.
Qed.

Lemma  No_No_Front T:
  ~ No_Front T ->
  exists ad, Is_Front T ad.
Proof.
  elim: T => //= mc [] f Hind.
    by move => _; exists nil.
  move/negP; rewrite negb_forall => /existsP [] t.
  move/negP/Hind => [] ad Had.
  by exists (t::ad).
Qed.

Lemma No_front_Markings_of_T T:
  No_Front T ->
  Markings_of_T T = Markings_not_front_of_T T .
Proof.
  elim: T => //= mc [] f Hind // /forallP Hnf.
  apply f_equal, f_equal.
  rewrite !codomE.
  apply eq_in_map => t _ //=.
  exact:Hind.
Qed.


End No_Front.


Section Operations_keeps_properties.
(* Lemmas to show that our operations keep the good property *)

Lemma remove_keeps_fl (T : KMTree) (ad : address) :
  Front_leaves T -> Front_leaves (remove_add T ad).
Proof.
  elim : ad T => [| ad l Hind]//=.
    by elim .
  elim  => [ //=| mc b f _].
  case : b.
    move/Front_leaves_invariant => Hfl; apply/forallP => t.
    case Hlt : (t == ad);rewrite !ffunE {}Hlt //=;rewrite (Hfl t) => //.
    by case l.
  move/Front_leaves_invariant2 => Hfl.
  rewrite remove_add_Br /Front_leaves -/Front_leaves.
  apply/forallP => t; rewrite ffunE.
  case : (t == ad) => //.
  by apply Hind.
Qed.

Lemma saturate_keeps_fl (T : KMTree) (a :acceleration) (ad: address):
  Front_leaves T -> Front_leaves (saturate_a_little a T ad).
Proof.
  elim : ad T => [| ad l Hind] //=.
    elim => //= mc b f _ Hfl.
    case : (apply_transitionc mc a) => //= m.
    by case : (m ==mc) .
  elim => //= mc [|] f Hind' .
    move/Front_leaves_invariant => Hfl.
    apply/forallP => t; rewrite ffunE Hfl => //.
    by case : (t == ad).
  move/forallP => Hfl.
  apply/forallP => t.
  rewrite ffunE.
  case: (t==ad) => //.
  by apply Hind.
Qed.


Lemma To_front_keeps_fl (T : KMTree) (ad : address):
  Front_leaves T -> Front_leaves (to_Front T ad).
Proof.
  elim : ad T => [| ad l Hind]//=.
    case => //= mc b f _.
    apply/forallP => t.
    by rewrite ffunE.
  elim  => //= mc b f HindT.
  case : b.
    move/Front_leaves_invariant => Hfl.
    apply/forallP => t.
    rewrite ffunE !Hfl => //=.
    by case: (t==ad).
  move => /forallP Hfl.
  apply/forallP => t.
  rewrite ffunE;case: (t== ad) => //=.
  by apply Hind.
Qed.



Lemma  Front_extension_keeps_fl (T : KMTree) (ad : address):
  Front_leaves T -> Front_leaves (Front_extension T ad) .
Proof.
  elim : ad T => //= [|t ad  Hind].
    case => //= mc b f _.
    apply/forallP => t.
    rewrite ffunE.
    case : nextmc => //= mc'.
    by apply/forallP => t'; rewrite ffunE.
  case => //= mc b f.
  case : b => //=.
    move/Front_leaves_invariant => Hfl.
    apply/forallP => t0; rewrite ffunE Hfl => //=.
    by case : (t0 == t).
  move/forallP => Hfl.
  apply/forallP => t0; rewrite ffunE.
  case : (t0==t) => //=.
  by apply Hind.
Qed.

(* Lemmas to show that our operations keep the consistent property *)

Lemma remove_keeps_consistent A (T : KMTree) (ad : address) :
  consistent_tree A T -> consistent_tree A (remove_add T ad).
Proof.
  elim : ad T => [| ad l Hind]//=.
    by elim .
  elim  => [ //=| mc b f _ Hcon].
  rewrite  remove_add_Br => t.
  rewrite !ffunE.
  split; last first.
  all :move/( _ (f t)) in Hind.
  all: move/(_ t):Hcon => [H1 H2].
    case (t==ad) => //=.
    by apply Hind.
  move : H1 ;rewrite /consistent_child.
  case : (nextmc mc t) => //=; last first.
    rewrite ffunE.
    by case: (t==ad); case: (f t) ; case l.
  move => m.
  rewrite ffunE;case (t==ad) => //=.
  by elim l ; elim (f t).
Qed.


Lemma saturate_keeps_consistent A (T : KMTree) (ad : address) (a : acceleration):
  consistent_tree A T  ->
  Front_leaves T ->
  Is_Front T ad ->
  List.In a A ->
  consistent_tree A (saturate_a_little a T ad).
Proof.
  elim : ad T => [| ad l Hind]//=.
    elim => //= mc b f Hind Hcon Hfl.
    move/eqP => Hb Hin; subst.
    case (apply_transitionc); last first.
      by rewrite consistent_tree_Br.
    rewrite -/(Front_leaves (Br mc true f)) in Hfl.
    move/Front_leaves_invariant :Hfl => Hfl m.
    case (m == mc).
      by rewrite consistent_tree_Br.
    move=> t; rewrite /consistent_child; move/(_ t): Hfl => ->.
    by case nextmc.
  elim  => [ //=| mc b f Hrec Hcon Hfl Front ] Hin.
  rewrite  saturate_a_little_Br consistent_tree_Br  => t.
  rewrite  !ffunE.
  split; last first;case Htad: (t==ad).
  - apply Hind =>//=.
    + by move/(_ t):Hcon => [] _.
    + by apply (@Front_leaves_invariant2 mc b f ).
    + by move/eqP : Htad => ->.
  - by move/(_ t): Hcon => [_ H].
  all: rewrite /consistent_child ffunE.
  all:move/(_ t):Hcon => [Hcon _]; rewrite Htad.
  all:move: Hcon; rewrite /consistent_child.
  all:case (nextmc mc t) => //=.
  all: case: (f t) => // mc0 b0 f0 mc' Hmc'.
  - rewrite /saturate_a_little.
    elim l => [| //=].
    case Ha :(apply_transitionc mc0 a) => [ m1 | //=].
    case H10 : (m1==mc0) => [//=|].
    move: Hmc' => [] acc [] Hall /eqP Hmc0.
    exists (acc++ [::a]);split.
      apply List.Forall_app; split => //=.
      exact:List.Forall_cons.
    rewrite map_cat.
    apply: abstraction_compo_seq_cat.
    rewrite -good_compo_transitionc -Hmc0 abstraction_compo_seq1 /obind /oapp.
    rewrite -good_compo_transitionc Ha //=.
    move/eqP: (nil_transitionc_id m1) => ->.
    by rewrite eqxx.
Qed.



Lemma To_Front_keeps_consistent A (T : KMTree) (ad : address) :
  consistent_tree A T -> consistent_tree A (to_Front T ad).
Proof.
  elim : ad T => [| ad l Hind]//=.
    case => //= m b f _ => t .
    rewrite /consistent_child ffunE //=.
    by case : (nextmc).
  elim => [//=| mc b f _ Hcon].
  rewrite to_Front_Br consistent_tree_Br => t.
  rewrite ffunE.
  case Ht:(t== ad) => // ; split.
  - 2: apply Hind.
  all : move/(_ t) :Hcon => [Hchild Htree]  //.
  all: move: Hchild; rewrite /consistent_child !ffunE Ht //=.
  - case : nextmc => // [mc0|].
    all: by case: ( f t); case l.
Qed.

Lemma Front_extension_keeps_consistent A (T : KMTree) ( ad : address) :
  consistent_tree A T -> consistent_tree A ( Front_extension T ad).
Proof.
  elim : ad T => [| t ad Hind] [//=| mc b f] .
    move => _.
    rewrite /Front_extension => t.
    rewrite !ffunE; split.
      rewrite /consistent_child ffunE //=.
      case : nextmc => //= mc'.
      exists nil; split => //=.
      move/eqP:(nil_transitionc_id mc') => ->.
      by rewrite eqxx.
    case : nextmc => //= mc' t0.
    rewrite ffunE /consistent_child; split => //=.
    by case : nextmc => // [mc''|]; rewrite ffunE.
  rewrite consistent_tree_Br =>  Hcon.
  rewrite Front_extension_Br consistent_tree_Br => t0.
  move/(_ t0): Hcon => [] Hchild Hcon.
  case Heq: (t0==t).
  all: split; rewrite ?ffunE ?Heq //=.
  2: exact:Hind.
  all: move: Hchild; rewrite /consistent_child ffunE Heq //=.
  case : nextmc => // [mc1|]; last first.
  all:by case: (f t0); case ad.
Qed.



Lemma Saturate_F_keep_NF:
  forall T ad a, Is_Front T ad ->
  Markings_not_front_of_T (saturate_a_little a T ad) = Markings_not_front_of_T T .
Proof.
  elim => //= mc b f Hind ad a .
  case : ad => //= [| t ad HFront].
    move/eqP => ->.
    case : apply_transitionc => //=.
    by move => a0; case : (a0==mc).
  move/(_ t ad a HFront): Hind => Hind.
  case : b => //=.
  apply f_equal2 => //.
  apply f_equal, eq_codom.
  move => t0.
  rewrite /comp ffunE.
  case Ht0: (t0 == t) => //=.
  by move/eqP : Ht0 => ->.
Qed.


(* Lemmas to show that our operations keep the Not_Front_Antichain property *)


Lemma Remove_keep_antichain:
  forall T ad, Not_Front_Antichain T ->
  Not_Front_Antichain (remove_add T ad) .
Proof.
  have Hinc:(forall T ad, subseq (Markings_not_front_of_T  (remove_add T ad))
  (Markings_not_front_of_T T)); last first.
    move => T ad Hac.
    apply (@antichain_subseq pn  _ (Markings_not_front_of_T T)) => //=.
  elim => //=[| mc b f Hind].
    by case.
  case => //= [| t ad]; case: b => //=.
  rewrite eqxx //=.
  have Heq:(forall t', subseq (Markings_not_front_of_T
  (([ffun t0 => if t0 == t then remove_add (f t0) ad else f t0] ) t'))
  (Markings_not_front_of_T (f t'))).
    move => t'; rewrite ffunE.
    by case : (t' == t).
  apply subseq_codom => [|i].
    by rewrite !size_codom.
  case Hsize: (size (enum transition) <= i).
    rewrite (nth_default) => //=.
      by rewrite sub0seq.
    by rewrite size_map.
  have Hlt:( i < size (enum transition))%N.
    rewrite ltnNge.
    by apply/negPf.
  move : Hsize Hlt; rewrite -cardE  => _ Hsize.
  rewrite !(nth_codom nil _ (Ordinal Hsize)).
  apply Heq.
Qed.


Lemma To_Front_keep_antichain:
  forall T ad, Not_Front_Antichain T ->
  Not_Front_Antichain (to_Front T ad) .
Proof.
  suff Hinc:(forall T ad, subseq (Markings_not_front_of_T  (to_Front T ad))
  (Markings_not_front_of_T T)).
    move => T ad Hac.
    apply (@antichain_subseq pn _ (Markings_not_front_of_T T)) => //=.
  elim => //= mc b f Hind.
  case => //= [| t ad].
    by rewrite sub0seq.
  have Heq:(forall t', subseq (Markings_not_front_of_T
  (([ffun t0 => if t0 == t then to_Front (f t0) ad else f t0] ) t'))
  (Markings_not_front_of_T (f t'))).
    move => t'; rewrite ffunE.
    by case : (t' == t).
  case :b => //.
  rewrite subseqE.
  apply subseq_codom => [|i].
    by rewrite !size_codom.
  case Hsize: (size (enum transition) <= i).
    rewrite (nth_default) => //=.
      by rewrite sub0seq.
    by rewrite size_map.
  have Hlt:( i < size (enum transition))%N.
    rewrite ltnNge.
    by apply/negPf.
  move : Hsize Hlt; rewrite -cardE  => _ Hsize.
  rewrite !(nth_codom nil _ (Ordinal Hsize)).
  rewrite /comp.
  apply Heq.
Qed.

Lemma Ext_keep_antichain:
  forall T ad, Is_Front T ad ->
  Front_leaves T ->
  Markings_not_front_of_T (remove_Front T ad) = Markings_not_front_of_T ( Front_extension T ad)  .
Proof.
  move => T ad; move : T.
  elim :ad  => //= [| t ad Hind].
    case => //= m b f.
    move/eqP => -> /Front_leaves_invariant Hfl.
    apply f_equal; apply f_equal; rewrite !codomE /comp.
    apply eq_map => t; rewrite ffunE Hfl => //=.
    by case: nextmc.
  case => // mc b f.
  rewrite Is_front_Br => Hfront Hfl.
  case : b Hfl => //= /forallP Hfl.
  apply f_equal; apply f_equal; rewrite !codomE.
  apply eq_map => t0.
  rewrite /comp !ffunE.
  case Ht: (t0==t) => //=; move/eqP : Ht => ->.
  by rewrite Hind.
Qed.

End Operations_keeps_properties.

Section saturated_node.

(* We define a predicate over KMTree that computes whether a node is saturated *)

Fixpoint saturated_node (A : seq acceleration) (T: KMTree) (ad : address) :=
match T,ad with
| Empty,_ => true
| Br mc b f, t::ad' => saturated_node A (f t) ad'
| Br mc b f, nil => saturated_markingc mc A
end.

Lemma saturate_node_inv mc A T ad:
  Some mc = m_from_add T ad ->
  saturated_node A T ad ->
  saturated_markingc mc A .
Proof.
  elim : ad T mc A => [//=| t ad Hind] [//=|mc0 b0 f0] mc A.
    by move => [] ->.
  exact: Hind.
Qed.


Lemma saturate_node_from_add mc A T ad:
  Some mc = m_from_add T ad ->
  saturated_markingc mc A ->
  saturated_node A T ad.
Proof.
  elim : ad T mc A => [//=| t ad Hind] [//=|mc0  b0 f0] mc A.
    by move => [] ->.
  exact: Hind.
Qed.

Lemma not_saturated_node_inv T A ad:
  ~~ saturated_node A T ad ->
  exists mc, Some mc = m_from_add T ad /\
  ~~ saturated_markingc mc A.
Proof.
  elim : ad  T A => [//=| t ad Hind] [//=| mc b f] A.
    move => //= Hsat.
    by exists mc; split.
  rewrite m_from_add_Br /saturated_node -/saturated_node.
  exact:Hind.
Qed.

Lemma not_saturate_node_from_add mc A T ad:
  Some mc = m_from_add T ad ->
  ~~ saturated_markingc mc A ->
  ~~ saturated_node A T ad.
Proof.
  move/saturate_node_inv => Hyp /negP Hmc.
  by apply/negP => /Hyp.
Qed.

End saturated_node.

End KMTree.