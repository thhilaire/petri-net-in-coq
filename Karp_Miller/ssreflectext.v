(* Some extensions to Ssreflect.  *)

From mathcomp Require Import all_ssreflect.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Section Compatibility.

(* Copy of the ssrbool shim to ensure compatibility with MathComp v1.8.0. *)
Definition PredType : forall T pT, (pT -> pred T) -> predType T.
exact PredType || exact mkPredType.
Defined.

End Compatibility.

Arguments PredType [T pT] toP.

Section SeqExtended.

Lemma take_find (T : Type) (a : pred T) s : all (predC a) (take (find a s) s).
Proof. by elim: s => [|x s IHs] //=; case: ifP => //= ->. Qed.

Lemma take_index (T : eqType) (x : T) s : x \notin take (index x s) s.
Proof.
  apply/negP=> H; move/allP/(_ _ H): (take_find (pred1 x) s).
  by rewrite /= eqxx.
Qed.

End SeqExtended.


Section FinfunExtended.

Section ComponetwiseOperation.

Variable aT : finType.
Variables S T R : Type.

(* ret/pure for {ffun aT -> _} *)
Definition ffret (T : Type) (x : T) : {ffun aT -> T} := [ffun=> x].

(* liftM/fmap for {ffun aT -> _} *)
Definition fflift (S T : Type) (f : S -> T) :=
  fun (x : {ffun aT -> S}) => [ffun a => f (x a)].

(* liftM2/liftA2 for {ffun aT -> _} *)
Definition fflift2 (S T R : Type) (op : S -> T -> R) :=
  fun (x : {ffun aT -> S}) (y : {ffun aT -> T}) => [ffun a => op (x a) (y a)].

Lemma fflift_cancel (f : S -> T) g :
  cancel f g -> cancel (fflift f) (fflift g).
Proof. by move=> ? ?; apply/ffunP => ?; rewrite !ffunE. Qed.

Lemma fflift2_associative (op : S -> S -> S) :
  associative op -> associative (fflift2 op).
Proof. by move=> ? ? ? ?; apply/ffunP => ?; rewrite !ffunE. Qed.

Lemma fflift2_left_id e (op : S -> T -> T) :
  left_id e op -> left_id (ffret e) (fflift2 op).
Proof. by move=> ? ?; apply/ffunP => ?; rewrite !ffunE. Qed.

Lemma fflift2_right_id e (op : T -> S -> T) :
  right_id e op -> right_id (ffret e) (fflift2 op).
Proof. by move=> ? ?; apply/ffunP => ?; rewrite !ffunE. Qed.

Lemma fflift2_commutative (op : S -> S -> S) :
  commutative op -> commutative (fflift2 op).
Proof. by move=> ? ? ?; apply/ffunP => ?; rewrite !ffunE. Qed.

Lemma fflift2_left_distributive (op : S -> T -> S) (add : S -> S -> S) :
  left_distributive op add -> left_distributive (fflift2 op) (fflift2 add).
Proof. by move=> ? ? ? ?; apply/ffunP => ?; rewrite !ffunE. Qed.

Lemma fflift2_left_absorptive (op1 : S -> T -> R) (op2 : S -> R -> S):
  (forall y x, op2 x (op1 x y) = x) ->
  forall y x, (fflift2 op2) x ((fflift2 op1) x y) = x.
Proof. by move=> ? ? ?; apply/ffunP => ?; rewrite !ffunE. Qed.

End ComponetwiseOperation.

Section ComponetwiseRelation.

Variable aT : finType.
Variable rT : Type.
Variable r : rel rT.

Definition relcw : rel {ffun aT -> rT} :=
  fun x y => [forall p : aT, r (x p) (y p)].
(* fun x y => ffall ((fflift2 r) x y).
   where Definition ffall (p : {ffun aT -> bool}) : bool := [forall a, p a]. *)

Lemma relcw_refl : reflexive r -> reflexive relcw.
Proof. by move=> H x; apply/forallP => p; apply: H. Qed.

Lemma relcw_anti : antisymmetric r -> antisymmetric relcw.
Proof.
  move=> H a b /andP [/forallP Ha /forallP Hb]; apply/ffunP => p.
  by apply: H; rewrite Ha Hb.
Qed.

Lemma relcw_trans : transitive r -> transitive relcw.
Proof.
  move=> H b a c /forallP Hab /forallP Hbc; apply/forallP => p.
  exact: H (Hbc _).
Qed.

Variables T : Type.

Lemma relcw_fflift2r_homo (op : rT -> T -> rT) :
  (forall b : T, {homo (op^~ b) : a a' / r a a'}) ->
  forall y : {ffun aT -> T}, {homo (fflift2 op)^~ y : x x' / relcw x x'}.
Proof.
  by move=> H ? ? ? /forallP ?; apply/forallP => ?; rewrite !ffunE; apply: H.
Qed.

Lemma relcw_fflift2r_mono (op : rT -> T -> rT) :
  (forall b : T, {mono (op^~ b) : a a' / r a a'}) ->
  forall y : {ffun aT -> T}, {mono (fflift2 op)^~ y : x x' / relcw x x'}.
Proof. by move=> H ? ? ?; apply: eq_forallb => ?; rewrite !ffunE H. Qed.

End ComponetwiseRelation.

(*
Section Monoid.

Variable aT : finType.
Variables (T : Type) (idm : T).

Section Plain.

Variable op : Monoid.law idm.

Definition fflift2_monoidLaw :=
  Monoid.Law (idm := ffret aT idm)
             (fflift2_associative (Monoid.mulmA op))
             (fflift2_left_id (Monoid.mul1m op))
             (fflift2_right_id (Monoid.mulm1 op)).

End Plain.

Section Commutative.

Variable op : Monoid.com_law idm.

Definition fflift2_commoidLaw :=
  Monoid.ComLaw (com_operator := fflift2_monoidLaw op)
                (fflift2_commutative (Monoid.mulmC op)).

End Commutative.

End Monoid.
*)

End FinfunExtended.

Section FintypeExtended.

Lemma fin_exists_hasP (T : eqType) (fT : finType) (P : fT -> pred T) s :
 reflect (exists x, has (P x) s) (has (fun y => [exists x, P x y]) s).
Proof.
 apply: (iffP hasP).
 - by case=> y y_in_s /existsP [x Pxy]; exists x; apply/hasP; exists y.
 - by case=> x /hasP [y H Pxy]; exists y => //; apply/existsP; exists x.
 Qed.

End FintypeExtended.

Arguments fin_exists_hasP {T fT P s}.
