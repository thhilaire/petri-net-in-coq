(* Basic definitions for Petri nets.
   By Mitsuharu Yamamoto.  *)

From HB Require Import structures.
From AlmostFull.PropBounded Require Import AlmostFull.
From mathcomp Require Import all_ssreflect.
Import Order.TTheory.
Require Import ssreflectext orderext monad afext.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope order_scope.
Declare Scope petri_net_scope.
Delimit Scope petri_net_scope with PN.
Local Open Scope petri_net_scope.

Section PetriNetDef.

Record petri_net :=
  PetriNet {
      place : finType;
      transition : finType;
      _ : transition -> {ffun place -> nat}; (* pre *)
      _ : transition -> {ffun place -> nat}; (* post *)
    }.

Definition marking (pn : petri_net) := {ffun place pn -> nat}.

(* Re-type the 3rd and 4th fields to use the special name "marking".  *)
Definition pre (pn : petri_net) : transition pn -> marking pn :=
  let: PetriNet _ _ p _ := pn in p.
Definition post (pn : petri_net) : transition pn -> marking pn :=
  let: PetriNet _ _ _ p := pn in p.

End PetriNetDef.

Section MarkingDef.

Variable pn : petri_net.

Notation place := (place pn).
Notation transition := (transition pn).
Notation marking := (marking pn).

(* The addition and subtraction operations on markings.  *)
Definition addm : marking -> marking -> marking := fflift2 addn.
Definition subm : marking -> marking -> marking := fflift2 subn.

HB.instance Definition _ := Monoid.isComLaw.Build marking \bot addm 
                              (fflift2_associative addnA)
                              (fflift2_commutative addnC)
                              (fflift2_left_id add0n).

Lemma addmA : associative addm. Proof. exact: Monoid.mulmA. Qed.
Lemma add0m : left_id \bot addm. Proof. exact: Monoid.mul1m. Qed.
Lemma addm0 : right_id \bot addm. Proof. exact: Monoid.mulm1. Qed.
Lemma addmC : commutative addm. Proof. exact: Monoid.mulmC. Qed.
Lemma addmCA : left_commutative addm. Proof. exact: Monoid.mulmCA. Qed.
Lemma addmAC : right_commutative addm. Proof. exact: Monoid.mulmAC. Qed.
Lemma addmACA : interchange addm addm. Proof. exact: Monoid.mulmACA. Qed.

End MarkingDef.


Prenex Implicits addm subm.

Notation "m1 :+: m2" := (addm m1 m2) (at level 50, left associativity)
                        : petri_net_scope.
Notation "m1 :-: m2" := (subm m1 m2) (at level 50, left associativity)
                        : petri_net_scope.

Notation "\summ_ ( i <- r | P ) F" :=
  (\big[addm/ \bot]_(i <- r | P%B) F%PN)
    (at level 41, F at level 41, i, r at level 50,
     format "'[' \summ_ ( i  <-  r  |  P ) '/  '  F ']'") : petri_net_scope.
Notation "\summ_ ( i <- r ) F" :=
  (\big[addm/ \bot]_(i <- r) F%PN)
  (at level 41, F at level 41, i, r at level 50,
   format "'[' \summ_ ( i  <-  r ) '/  '  F ']'") : petri_net_scope.

Section TransitionDef.

Variable pn : petri_net.

Notation marking := (marking pn).

(* One-step transition on a Petri net.  Return a value of "option
   marking" because the transition fails when it is not fireable.  We
   provide a notation later rather than giving a name for the
   multi-step transition so generic rewrite rules such as
   "foldm_rcons_some" can be used directly.  *)
Definition nextm (m : marking) t : option marking :=
  if pre t <= m then Some (m :-: pre t :+: post t) else None.

End TransitionDef.

Prenex Implicits nextm.

(* One-step transition.  *)
Notation "m1 =1{ t }> m2" :=
  (nextm m1 t = Some m2)
    (at level 70, t at next level,
     format "'[hv ' m1 '/' '['  =1{  t  }>  ']' m2 ']'")
  : petri_net_scope.
(* Multi-step transition.  *)
Notation "m1 ={ s }> m2" :=
  (foldm nextm m1 s = Some m2)
    (at level 70, s at next level,
     format "'[hv ' m1 '/' '['  ={  s  }>  ']' m2 ']'")
  : petri_net_scope.

Section PropertyDef.

Variable pn : petri_net.

Notation place := (place pn).
Notation marking := (marking pn).

(* Basic properties of Petri nets.  *)
Definition reachable (m0 m : marking) := exists s, m0 ={ s }> m.
Definition coverable (m0 m : marking) := exists m', reachable m0 m' /\ m <= m'.
Definition bounded (m0 : marking) :=
  forall p, exists n, forall m, reachable m0 m -> m p <= n.
(* Place-boundedness can be expressed as "{for p, bounded m0}" for p : place,
   or "{in ps, bounded m0}" for ps : {set place}. *)
Definition boundedm (m0 : marking) :=
  exists m' : marking, forall m, reachable m0 m -> m <= m'.
Definition boundeds (m0 : marking) :=
  exists s : seq marking, forall m, reachable m0 m -> m \in s.
Definition terminate (m0 : marking) :=
  Acc (fun m2 m1 => exists t, m1 =1{ t }> m2) m0.

End PropertyDef.

Section Theory.

Variable pn : petri_net.

Notation place := (place pn).
Notation transition := (transition pn).
Notation marking := (marking pn).

Lemma lem_addr (m1 m2 : marking) : m1 <= m1 :+: m2.
Proof. by apply/forallP=> p; rewrite ffunE leEnat leq_addr. Qed.

Lemma lem_subLRC (m1 m2 m3 : marking) : (m1 :-: m2 <= m3) = (m1 <= m3 :+: m2).
Proof. by apply/eq_forallb=> p; rewrite !ffunE !leEnat addnC leq_subLR. Qed.

Lemma addmK (m : marking) : cancel (addm^~ m) (subm^~ m).
Proof. by move=> m'; apply/ffunP=> p; rewrite !ffunE addnK. Qed.

Lemma addmBAC (m1 m2 m3 : marking) :
  m1 <= m2 -> m2 :-: m1 :+: m3  = m2 :+: m3 :-: m1.
Proof.
  by move/forallP=> H; apply/ffunP=> p; rewrite !ffunE addnC addnBA // addnC.
Qed.

Lemma submKC (m1 m2 : marking) : m1 <= m2 -> m1 :+: (m2 :-: m1) = m2.
Proof. by move/forallP=> H; apply/ffunP=> p; rewrite !ffunE subnKC. Qed.

Lemma summE (T : Type) s P (F : T -> marking) p :
  (\summ_(t <- s | P t) F t) p = \sum_(t <- s | P t) (F t) p.
Proof.
  elim: s => [| t s IHs]; first by rewrite !big_nil ffunE.
  by rewrite !big_cons; case: ifP; rewrite ?ffunE IHs.
Qed.

Variant nextm_spec (m : marking) t : option marking -> Prop :=
| NextmSome of pre t <= m
  : nextm_spec m t (Some (m :-: pre t :+: post t))
| NextmNone p of ~~ ((pre t) p <= m p)
  : nextm_spec m t None.

Lemma nextmP m t : nextm_spec m t (nextm m t).
Proof.
  rewrite /nextm; case: ifPn; first by move=> H; constructor.
  by rewrite negb_forall; move/existsP=> [p H]; constructor 2 with p.
Qed.

Lemma nextm_add2r (m : marking) t : {homo addm^~ m : m1 m2 / m1 =1{ t }> m2}.
Proof.
  move=> m1 m2; case: nextmP => //= Hpret [<-]; rewrite /nextm ifT.
  - by rewrite addmAC -addmBAC.
  - by rewrite (le_trans Hpret) // lem_addr.
Qed.

(* Strong upward compatibility for one-step transition.  *)
Lemma nextm_ucompat (m1 m2 m1' : marking) t :
  m1 =1{ t }> m2 -> m1 <= m1' ->
  exists m2',
    m1' =1{ t }> m2' /\ m2 <= m2'.
Proof.
  move=> Hm1t /submKC <-.
  exists (m2 :+: (m1' :-: m1)); split; first by exact: nextm_add2r.
  by rewrite lem_addr.
Qed.

(* Strong upward compatibility for multi-step transition.  *)
Lemma nextm_seq_ucompat (m1 m2 m1' : marking) s :
  m1 ={ s }> m2 -> m1 <= m1' ->
  exists m2',
    m1' ={ s }> m2' /\ m2 <= m2'.
Proof.
  move=> H Hle; move: H.
  elim/last_ind: s => [|s t IHs] in m2 *; first by case => <-; exists m1'.
  rewrite foldm_rcons_some => -[m [Hm1s Hmt]].
  case: {IHs} (IHs _ Hm1s) => m' [Hm1's Hlem].
  have [m2' [Hnextm' Hlem2]] := nextm_ucompat Hmt Hlem.
  by exists m2'; split=> //; apply/foldm_rcons_some; exists m'.
Qed.

(* Equivalence between bounded, boundedm, and boundeds *)
Lemma bounded_all_iff (m0 : marking) :
  [<-> bounded m0; boundedm m0; boundeds m0].
Proof.
  do !split.
  - case/fin_all_exists=> f le_f; exists (finfun f) => m /le_f m_le_f.
    by apply/forallP=> p; rewrite ffunE.
  - case=> maxm bounded_by_maxm.
    have [n n_is_max]: exists n, forall p m, reachable m0 m -> m p <= n.
      exists (\max_p maxm p) => p m /bounded_by_maxm /forallP /(_ p).
      exact: joins_min.
    pose coverlist := [seq fflift val f | f : {ffun place -> 'I_n.+1}].
    exists coverlist => m /n_is_max ms_leq_n; apply/imageP=> /=.
    exists (fflift inord m) => //; apply/ffunP=> p.
    by rewrite !ffunE inordK // ltnS.
  - case=> s reachable_in_s p; exists (\max_(m <- s) m p) => m.
    case/reachable_in_s/seq_tnthP=> i ->; rewrite big_tnth.
    (* Work around a mathcomp 1.8.0 incompatibility.  *)
    by rewrite (@joins_sup _ _ _ i).
Qed.

Lemma terminate_ind (P : marking -> Prop) :
  (forall m0, (forall t m1, m0 =1{ t }> m1 -> terminate m1) ->
              (forall t m1, m0 =1{ t }> m1 -> P m1) -> P m0) ->
  forall m0, terminate m0 -> P m0.
Proof.
  move=> hterm m0; elim: m0 / => m0 hAcc IH.
  by apply: hterm => t m1 m0_t_m1; [apply: hAcc | apply: IH]; exists t.
Qed.

Lemma terminate_inf_nextm (m0 : marking) :
  terminate m0 -> forall f, exists n, foldm nextm m0 (mkseq f n) = None.
Proof.
  elim/terminate_ind: m0 / => m0 _ IH f.
  case m0f0: (nextm m0 (f 0)) => [m1|]; last by exists 1%N; rewrite /= m0f0.
  have [n <-] := IH _ _ m0f0 (f \o S); exists n.+1.
  by rewrite /= m0f0 -[1%N]addn0 iotaDl -map_comp /=.
Qed.

Lemma terminate_no_lasso (m0 : marking) :
  terminate m0 <->
  forall s0 m1 t1 s1 m2, m0 ={ s0 }> m1 -> m1 ={ t1 :: s1 }> m2 -> ~ m1 <= m2.
Proof.
  split.
  - elim/terminate_ind: m0 / => m0 _ IH [|t0 s0] m1 t1 s1 m2.
    + case=> <- m0_t1_s1_m2 /(nextm_seq_ucompat m0_t1_s1_m2) [m3 []].
      by case/foldm_cons_some: m0_t1_s1_m2 => m [/IH]; apply.
    + by case/foldm_cons_some=> m [/IH]; apply.
  - pose R (m1 m2 : marking) : Prop := m1 <= m2.
    rewrite -[X in X -> _]/(forall s0 m1 t1 s1 m2, _ -> _ -> ~ R m1 m2).
    move: m0.
    have: almost_full R.
    { apply: (@af_strengthen _ (finfun_lift leq)); last by move=> ? ? /forallP.
      by apply: af_finfun; apply: af_leq. }
    elim: R / => R.
    + move=> ztR m0 no_lasso; constructor=> m1 [t0 m0_t0_m1].
      by case: (no_lasso [::] m0 t0 [::] m1) => //; rewrite foldm1.
    + move=> supR IH m0 no_lasso; constructor=> m1 [t0 m0_t0_m1].
      apply: (IH m0) => s1 m2 t2 s2 m3 m1_s1_m2 m2_t2_s2_m3 [Rm2m3|Rm0m2].
      * apply: (no_lasso (t0 :: s1)) m2_t2_s2_m3 Rm2m3.
        by rewrite foldm_cons_some; exists m1.
      * apply: (no_lasso [::] _ t0 s1) Rm0m2 => //.
        by rewrite foldm_cons_some; exists m1.
Qed.

Lemma terminate_bounded (m0 : marking) :
  terminate m0 -> bounded m0.
Proof.
  elim/terminate_ind: m0 / => m0 _ IH p.
  suff: forall t, exists n, forall s m, m0 ={ t :: s }> m -> m p <= n.
  { case/fin_all_exists=> f fP; exists (m0 p `|` \max_t f t).
    move=> m [[|t s]]; first by case=> <-; rewrite leUl.
    by move/fP/(joins_min _)=> mp_le_ft; rewrite lexU // mp_le_ft // orbT. }
  move=> t; case m0_t_m1: (nextm m0 t) => [m1|].
  - have [n mp_le_n] := IH _ _ m0_t_m1 p.
    exists n => s m /foldm_cons_some [m1' []].
    by rewrite m0_t_m1 => -[<-] m1_s_m; apply: mp_le_n; exists s.
  - by exists 0 => ? ? /foldm_cons_some [? []]; rewrite m0_t_m1.
Qed.

End Theory.
