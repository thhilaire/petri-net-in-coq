# Coq certification of the MinCov algorithm

Extension of the formalization of Petri Nets of
[[Y. Mitsuharu, S. Shogo, and M. Saki]](https://doi.org/10.1145/3018610.3018626)
to prove the correctness and termination of MinCov
[[A. Finkel, S. Haddad, and I. Khmelnitsky]](https://doi.org/10.1007/978-3-030-45231-5_13),
an algorithm that computes a minimal representation of the Coverability set
of a given Petri Net.

Implementation of a variant of MinCov, AbstractMinCov, that is also certified
and used in the proofs about MinCov.


## Used software/libraries
* Coq 
  (coq, version 8.18.0)
  https://coq.inria.fr/
* Mathematical Components
  (coq-mathcomp-ssreflect, version 2.1.0)
  https://math-comp.github.io/
* Almost-full relations in Coq for proving termination
  (coq-almost-full, version 8.14.0)
  https://github.com/coq-community/almost-full
* McZify
  (coq-mathcomp-zify, version 1.5.0+2.0+8.16)
  https://github.com/math-comp/mczify

## Installation
The easiest way to install them all is to use opam:
```
opam pin add coq 8.18.0
opam repo add coq-released https://coq.inria.fr/opam/released
opam install coq-almost-full
opam install coq-mathcomp-zify
make
```

## File structure
Inside the Karp_Miller folder are the files that are used for the formalization
of Petri nets and the Karp-Miller original algorithm. They are taken from
https://bitbucket.org/mituharu/karpmiller/src/master/
from commit bbb0668. 

* ssreflectext.v -- Extension for ssreflect
* orderext.v     -- Extensions of some orders
* monad.v        -- The option monad and the fold monad
* afext.v        -- Extension for almost-full
* petrinet.v	   -- Basic definitions for Petri nets
* pnkmaccel.v	   -- Karp-Miller acceleration on Petri nets
* pnkmtree.v	   -- Karp-Miller tree on Petri nets.
  		              This includes the (terminating) tree construction,
  		              and the soundness and completeness of coverability.



Inside the Tools folder there are:
* wbrD.v and wbrP.v -- Two files to show equivalence (modulo some hypotheses)
                       between the definitions of almost-full and another
                       well-founded relation
* wbr_tree.v        -- Well-founded rewriting relations on trees
* Utils.v           -- Some useful lemmas



Inside the MinCov_AbstractMinCov folder there are:
* New_transitions.v -- New kind of transitions for Petri nets, generalization
                       of the acceleration phenomena of Karp-Miller
* KMTrees.v         -- Implementation of a new coverability tree and functions
                       on it
* AbstractMinCov.v  -- A rewriting small step relation corresponding to
                       AbstractMinCov
* Termination.v     -- The proof that the relation corresponding to
                       AbstractMinCov is well-founded
* Completeness.v    -- The proof that the relation corresponding to
                       AbstractMinCov is complete
* Correctness.v     -- The proof that the relation corresponding to
                       AbstractMinCov is correct
* KMTE.v            -- Implementation of a new explicit coverability tree and
                       functions on it
* MinCov.v          -- Implementation of a relation corresponding to the
                       [MinCov algorithm](https://doi.org/10.1007/978-3-030-45231-5_13),
                       and the lemma showing that it is simulated by the
                       relation in KMTrees.v
