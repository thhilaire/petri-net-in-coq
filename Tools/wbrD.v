(**************************************************************)
(*   Copyright (c) 2024 Thibault Hilaire                      *)
(*                      David Ilcinkas                        *)
(*                      Jerome Leroux                         *)                           
(*   Authors : Thibault Hilaire [*]                           *)
(*             David Ilcinkas [+,*]                           *)
(*             Jerome Leroux  [+,*]                           *)
(*                                                            *)
(*                             [*] Affiliation Univ. Bordeaux *)
(*                             [+] Affiliation CNRS           *)
(**************************************************************)
(*      This file is distributed under the terms of the       *)
(*                      MIT License                           *)
(**************************************************************)

From mathcomp Require Import all_ssreflect .
From AlmostFull.Default Require Import AlmostFull AFConstructions.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import Utf8.


Require Import Wellfounded.
Require Import Relation_Definitions.


Section WBR_prop.


Section Bad_sequence.


Variable (X: Type).
Variable (R: relation X).



(** x is not larger than any element of the list **)
Inductive Not_larger (x:X) : seq X -> Prop :=
     | nlempty :  Not_larger x nil
     | nlextend (e:X) (l:list X) : (R e x -> False)
                              -> Not_larger x l
                              -> Not_larger x (e::l).

Lemma Not_larger_spec  x y l:
  Not_larger y (rcons l x) -> ~R x y.
Proof.
  elim : l => [| a l Hind].
    by inversion 1.
  inversion_clear 1; by apply Hind.
Qed.


(** x is larger than an element of the list **)
Inductive Larger  (x:X) : seq X-> Prop :=
| linit (e:X) (l:list X) : R e x -> Larger x (e::l)
| lextend (e: X) (l:list X): Larger  x l -> Larger x (e::l).


Lemma Larger_and_not_Larger (x : X) (l : seq X):
Not_larger x l -> Larger x l -> False.
Proof.
  move : x; elim: l => [x | a l Hind x Hnl Hl].
    by inversion 2.
  inversion Hnl; subst.
  inversion Hl; subst => //=.
  by apply (Hind x).
Qed.


Inductive Bad : seq X -> Prop :=
   | badempty : Bad nil
   | badextend (x:X) (l:list X) : Bad l -> Not_larger x l -> Bad (x::l).


Fixpoint Good (l : seq X) : Prop :=
  match l with
  | nil => False
  | [::x] => False
  | x::l' => (Good l') \/ (Larger x l')
   end.



Lemma Good_and_Bad (l : seq X) :
Good l -> Bad l -> False.
Proof.
  elim : l => //= a.
  case => //= a0 l Hind Hgood.
  inversion 1; subst.
  case : Hgood => [Hgood| Hl].
    by apply Hind.
  by apply (Larger_and_not_Larger H3 Hl ).
Qed.


Definition R_extended (x:  X) : relation X :=
  (fun y z => R y z \/ R x y ).

(** The relation of extension on the sequence **)

Inductive Bad_extension : relation (seq X) :=
  | extra x l: Bad (x::l) -> Bad_extension (x::l) l.


Section Dec_Bad_sequence.

Context {HdecR : (forall x y, {R x y} + { ~ R x y})}.

Lemma Dec_nlarger :
forall l x, {Not_larger  x l} + {~ Not_larger x l}.
Proof.
  elim => //= [ x | a l Hind x].
    left; by constructor.
  case : (Hind x); case : (HdecR a x) => Hr Hnl .
    2:left; by constructor.
  all:right; by inversion 1.
Qed.


Lemma Dec_Bad :
forall l, {Bad l} + {~ Bad l} .
Proof.
  elim  => //= [|x l Hind].
    left; by apply badempty.
  case :Hind; last first.
    move => Hgood; right => Hbad; apply Hgood; by inversion Hbad.
  case: (Dec_nlarger l x) => Hnl Hbad.
      left; by constructor.
  right => Habs; by inversion Habs.
Qed.


Lemma Dec_larger :
forall l x, {Larger x l} + {~ Larger x l}.
Proof.
  elim => [ x | a l Hind x] .
    right => H; by inversion H.
  case : (Hind x); case : (HdecR a x) => Hr Hnl .
    4: right => H; by inversion H.
  all:left; by constructor.
Qed.

Lemma Dec_Good :
forall l , {Good l} + {~ Good l}.
Proof.
  elim => //= [| a ].
    by right.
  case => //= a0 l Hind.
  case : Hind => Hgood.
    by left;left.
  case : (@Dec_larger (a0::l) a) => Hl.
    by left;right.
  right => H; apply Hgood.
  by case H.
Qed.



Lemma Larger_or_not_Larger (x : X) (l : seq X):
 ~Not_larger x l -> ~Larger x l -> False.
Proof.
  move : x; elim: l => [x Hnl H | a l Hind x Hnl Hl].
    by apply Hnl; constructor.
  case : (HdecR a x)=> H.
    by apply Hl; constructor.
  apply Hnl; constructor => //=.
  case : (Dec_nlarger l x); case (Dec_larger l x) => //= Hl' Hnl'.
    by exfalso; apply Hl; apply lextend.
  by exfalso; apply (Hind x).
Qed.



Lemma Good_or_Bad (l : seq X) :
~ Good l ->  ~ Bad l -> False.
Proof.
  elim : l => //= [ Hgood Hbad |a].
    by apply Hbad; constructor.
  case => //= [ Hind Hgood Hbad | a0 l Hind Hgood Hbad ].
    by apply Hbad; constructor; constructor.
  apply Hind => H.
    by apply Hgood;left.
  case : (Dec_larger (a0::l) a); case (Dec_nlarger (a0::l) a) => Hnl Hl.
    by apply (Larger_and_not_Larger Hnl Hl).
    by apply Hgood; right.
    by apply Hbad; constructor.
    by apply (Larger_or_not_Larger Hnl Hl).
Qed.



End Dec_Bad_sequence.


End Bad_sequence.


Section Almost_full_to_well_founded.


Lemma ZT_wf_bad_extension (X : Type) ( R : relation X):
  SecureBy R (ZT X) -> well_founded (Bad_extension R) .
Proof.
  move => Hsec.
  elim =>[| a l Hind].
  1,2 : constructor => l'; inversion 1; subst.
    constructor => l; inversion 1; subst; move : H H0 H1 H2  => _ _ _ Hbad.
    2: move : H H0 => _ Hbad.
  1,2 :inversion Hbad ; inversion H2; by apply H5 in Hsec.
Qed.



Lemma Bad_to_nl (X: Type) (R: relation X) (a x : X) (l : seq X) :
Bad R (rcons (a :: l) x) ->
Not_larger (R_extended R x) a l.
Proof.
  elim: l => [ Hbad|y l Hind Hbad].
    by constructor.
  inversion_clear Hbad.
  constructor; last first.
    apply Hind.
    constructor => //=.
      by inversion H.
    by inversion H0.
  move => HR.
  inversion_clear H0.
  case : HR => //=.
    inversion_clear H.
  by apply (Not_larger_spec H3).
Qed.


Lemma extension_inclusion (X: Type) (R : relation X) (x: X) (l : seq X):
  Bad R (rcons l x) -> Bad (R_extended R x) l.
Proof.
  elim : l =>[_ | a l Hind].
    by constructor.
  inversion_clear 1.
  constructor.
    by apply Hind.
  apply Bad_to_nl.
  by constructor.
Qed.


Lemma Lem_Acc_bad_extension (X: Type) (R : relation X) (x: X) (l : seq X):
 Acc (Bad_extension (R_extended R x)) l ->  Acc (Bad_extension R) (rcons l x).
Proof.
  elim => l0 _ Hind.
  constructor => y; inversion_clear 1.
  rewrite -rcons_cons in H0 |- *.
  apply Hind.
  constructor.
  by apply extension_inclusion.
Qed.


Lemma Lem_wf_bad_extension (X: Type) (R : relation X) :
  (∀ x : X, well_founded (Bad_extension (R_extended R x))) ->  well_founded (Bad_extension R) .
Proof.
  move => Hwf l.
  constructor; inversion_clear 1.
  rewrite (lastI x l ).
  apply Lem_Acc_bad_extension.
  apply Hwf.
Qed.



Theorem wf_bad_extension_from_af (X: Type) (R : relation X) :
  almost_full R ->  well_founded (Bad_extension R) .
Proof.
  move => [T Hsecure].
  generalize dependent R ; elim : T => [ R Hsec | p Hind R Hsec ].
    by apply ZT_wf_bad_extension.
  enough (forall x,well_founded (Bad_extension (R_extended R x))).
    by apply Lem_wf_bad_extension.
  move=> x;apply (Hind x).
  by move/(_ x): Hsec.
Qed.


End Almost_full_to_well_founded.


Section well_founded_to_almost_full.


Fixpoint R_extended_from_l (X : Type) ( R: relation X) (l : seq X) : relation X :=
  match l with
  | nil => R
  | x::l' => R_extended (R_extended_from_l R l') x
  end.


Lemma  Larger_to_full  (X : Type) (a:X) (R : relation X) (l : seq X) :
Larger R a l -> forall x y, R_extended_from_l R (a::l) x y .
Proof.
  move : a.
  elim : l => [| a0 l Hind].
    by inversion 1.
  inversion_clear 1 => x y; last first.
    move/(_ a H0 x y) : Hind; case => //= ; rewrite -/R_extended_from_l.
      by left; left.
    by right;left.
  clear Hind; right;right; elim : l => //= a1 l Hind;by  left.
Qed.


Lemma Good_to_full (X : Type) (R : relation X) (l : seq X) :
  Good R l -> forall x y, R_extended_from_l R l x y .
Proof.
  move : R; elim : l => //= a.
  case => //= a0 l Hind R.
  rewrite -/(Good R (a0::l)).
  move/(_ R) in Hind.
  case.
    move/Hind => Hdec x y.
    by left.
  by apply (Larger_to_full).
Qed.


Section WFT_tree.

Variable (X: Type).
Variable (R: relation X).


Context {HdecR : (forall x y, {R x y} + { ~ R x y})}.
Context { Hwf : well_founded (Bad_extension R)}.


Fixpoint bad_to_wft (l: seq X)  (H: Acc (Bad_extension R) l) : WFT X :=
SUP (fun x =>
match (@Dec_Bad X R HdecR (x::l)) with
  |left b => bad_to_wft (Acc_inv H (extra b))
  |right _ => ZT X
end).

Lemma bad_to_wft_unfold l H :
bad_to_wft H =
SUP (fun x =>
match (@Dec_Bad X R HdecR (x::l)) with
  |left b => bad_to_wft (Acc_inv H (extra b))
  |right g => ZT X
end).
Proof.
  unfold bad_to_wft at 1.
  destruct H; reflexivity.
Qed.

Lemma canonic_Acc l : Acc (Bad_extension R) l.
Proof.
  elim: l => [|x l Hind].
    exact (Hwf [::]).
  case: (@Dec_Bad X R HdecR (x::l)) => Hbad.
    apply (Acc_inv Hind (extra Hbad)).
  apply Hwf.
Defined.


Lemma af_from_acc_bad_extension (l : seq X) :
  Acc (Bad_extension R) l -> SecureBy (R_extended_from_l R l) (bad_to_wft (canonic_Acc l)) .
Proof.
  elim => l0 _ Hind.
  rewrite bad_to_wft_unfold => /= x.
  move/(_ (x::l0)) : Hind.
  rewrite /canonic_Acc; cbn.
  case: (@Dec_Bad _ _ HdecR (x::l0)) => //= Hbad .
    by move => /(_ (extra Hbad)) Hind.
  case (@Dec_Good _ _ HdecR (x::l0)) => Hgood; last first.
    exfalso; by apply (@Good_or_Bad _ _ HdecR _ Hgood Hbad).
  by have H:=(Good_to_full Hgood).
Qed.



Theorem af_from_wf_bad_extension :
  almost_full R.
Proof.
  exists (bad_to_wft (canonic_Acc [::])).
  by apply (@af_from_acc_bad_extension  nil).
Qed.

End WFT_tree.
End well_founded_to_almost_full.
End WBR_prop.