(**************************************************************)
(*   Copyright (c) 2024 Thibault Hilaire                      *)
(*                      David Ilcinkas                        *)
(*                      Jerome Leroux                         *)                           
(*   Authors : Thibault Hilaire [*]                           *)
(*             David Ilcinkas [+,*]                           *)
(*             Jerome Leroux  [+,*]                           *)
(*                                                            *)
(*                             [*] Affiliation Univ. Bordeaux *)
(*                             [+] Affiliation CNRS           *)
(**************************************************************)
(*      This file is distributed under the terms of the       *)
(*                      MIT License                           *)
(**************************************************************)


From mathcomp Require Import all_ssreflect .
From AlmostFull.PropBounded Require Import AlmostFull AFConstructions.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import Utf8.


Require Import Wellfounded.
Require Import Relation_Definitions.


Section WBR_prop.


Section Bad_sequence.


Variables (X: Type) (R: relation X).


(** x is not larger than any element of the list **)
Definition neverLarger x := List.Forall (fun y => ~ R y x).

Lemma neverLargerE x y s :
  neverLarger x (y::s) <-> neverLarger x s /\ ~ R y x.
Proof.
  split => [| []].
    by inversion 1.
  by constructor.
Qed.

(** x is larger than an element of the list **)
Definition larger x := List.Exists (fun y => R y x).

(** A bad sequence is a sequence without any increasing pair **)
Inductive Bad : seq X -> Prop :=
   | badEmpty : Bad nil
   | badExtend (x:X) (s:seq X) : Bad s -> neverLarger x s -> Bad (x::s).

Lemma BadE x s : Bad (x::s) <-> Bad s /\ neverLarger x s.
Proof.
  split => [| []].
    by inversion 1.
  by constructor.
Qed.

(** A good sequence is a sequence with an increasing pair *)
Inductive Good : seq X -> Prop :=
  goodC x s : (Good s) \/ (larger x s) -> Good (x::s).

Lemma nGood_Bad: forall s, ~ Good s -> Bad s.
Proof.
  elim => [|x s Hind HnGood].
  all: constructor.
    apply: Hind.
    contradict HnGood.
    by repeat constructor.
  elim: s {Hind} HnGood => [|y s Hind] HnGood.
  all: constructor.
    contradict HnGood.
    by constructor; right; constructor.
  apply Hind.
  contradict HnGood.
  inversion_clear HnGood.
  constructor.
  case: H; [left | right].
  all: by repeat constructor.
Qed.


(** The relation of extension of sequences that stay bad **)
Inductive Bad_extension : relation (seq X) :=
  | extra x s: Bad (x::s) -> Bad_extension (x::s) s.


(** The relation modification used in almost-full *)
Definition R_extended (x:  X) : relation X :=
  (fun y z => R y z \/ R x y ).

End Bad_sequence.


Section Almost_full_to_well_founded.

Lemma full_wf_bad_extension (X : Type) ( R : relation X):
  (forall x y : X, R x y) -> well_founded (Bad_extension R) .
Proof.
  do 2 (constructor; inversion_clear 1).
  inversion_clear H2.
  inversion_clear H3.
  firstorder.
Qed.

Lemma Bad_R_extended_E (X: Type) (R : relation X) (x y: X) (s : seq X):
  Bad R (y :: rcons s x) <->
  Bad (R_extended R x) (y::s) /\ ~ R x y.
Proof.
  split.
  all: elim : s y => /= [y | z s Hind y]; first by
    rewrite ?BadE neverLargerE; repeat constructor; tauto.
  all: move: Hind => /[dup] /(_ y) + /(_ z).
  all: rewrite !BadE !neverLargerE.
  all: by firstorder.
Qed.

Lemma Acc_bad_extension (X: Type) (R : relation X) (x: X) (s : seq X):
 Acc (Bad_extension (R_extended R x)) s ->
 Acc (Bad_extension R) (rcons s x).
Proof.
  elim => {}s _ Hind.
  constructor => t; inversion_clear 1.
  apply: (Hind (x0::s)).
  constructor.
  by apply Bad_R_extended_E.
Qed.

Lemma wf_bad_extension_extended (X: Type) (R : relation X) :
  (∀ x : X, well_founded (Bad_extension (R_extended R x))) ->
  well_founded (Bad_extension R) .
Proof.
  move => Hwf s.
  constructor => t; inversion_clear 1.
  rewrite (lastI x s).
  apply: Acc_bad_extension.
  exact: Hwf.
Qed.


Theorem wf_bad_extension_from_af (X: Type) (R : relation X) : 
  almost_full R ->  well_founded (Bad_extension R) .
Proof.
  elim => [| {}R _ Hind].
    exact: full_wf_bad_extension.
  exact: wf_bad_extension_extended.
Qed.

End Almost_full_to_well_founded.


Section well_founded_to_almost_full.

Definition R_extended_from_s (X : Type) ( R: relation X) :=
  foldr (fun x R0 => R_extended R0 x) R.

Lemma  larger_to_full  (X : Type) (x:X) (R : relation X) (s : seq X) :
larger R x s -> forall y z, R_extended_from_s R (x::s) y z.
Proof.
  elim: s => [|y s Hind].
  all: inversion_clear 1 => a b.
    right; right.
    by elim: s {Hind}; firstorder.
  by case (Hind H0 a b); [left|right]; left.
Qed.

Lemma Good_to_full (X : Type) (R : relation X) (s : seq X) :
  Good R s -> forall x y, R_extended_from_s R s x y.
Proof.
  elim : s => [|x s Hind].
  all: inversion_clear 1.
  case: H0 Hind => [HGood /(_ HGood) Hind y z| Hlarger _].
    by right.
  exact: larger_to_full.
Qed.


Section almostfull.

Variable (X: Type).
Variable (R: relation X).

Context {HdecG: forall s, Good R s \/ ~ Good R s}.

Context {Hwf: well_founded (Bad_extension R)}.


Lemma af_from_acc_bad_extension (s : seq X) :
  Acc (Bad_extension R) s -> almost_full (R_extended_from_s R s).
Proof.
  elim => {}s _ Hind.
  apply AF_SUP => x.
  rewrite -[X in almost_full X]/(R_extended_from_s R (x::s)).
  case: (HdecG (x::s)) => HGood.
    apply: AF_ZT.
    exact: Good_to_full.
  apply: Hind.
  constructor.
  exact: nGood_Bad.
Qed.

Theorem af_from_wf_bad_extension :
  almost_full R.
Proof.
  exact: (@af_from_acc_bad_extension nil).
Qed.

End almostfull.
End well_founded_to_almost_full.
End WBR_prop.