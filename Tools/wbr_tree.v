(**************************************************************)
(*   Copyright (c) 2024 Thibault Hilaire                      *)
(*                      David Ilcinkas                        *)
(*                      Jerome Leroux                         *)                           
(*   Authors : Thibault Hilaire [*]                           *)
(*             David Ilcinkas [+,*]                           *)
(*             Jerome Leroux  [+,*]                           *)
(*                                                            *)
(*                             [*] Affiliation Univ. Bordeaux *)
(*                             [+] Affiliation CNRS           *)
(**************************************************************)
(*      This file is distributed under the terms of the       *)
(*                      MIT License                           *)
(**************************************************************)


From mathcomp Require Import all_ssreflect.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import Utf8.

Require Import  Wellfounded Relation_Definitions Relation_Operators.


(*
  A relation on lists that will come in handy for our proof
  with results of accessibility on it
*)

Section R_seq_one.

Variables (T : Type) (R: relation T).


(*
  The two lists are identical except in one place,
  where the respective elements satisfy the relation R.
*)
Inductive R_seq_one: relation (seq T) :=
| Here a b s : R a b -> R_seq_one (a::s) (b::s)
| Further a s t : R_seq_one s t -> R_seq_one (a::s) (a::t).


Lemma R_seq_one_size s t: R_seq_one s t -> size s = size t.
Proof. elim => {s t} [//|a s t _ /= <- //]. Qed.

Lemma Acc_R_seq_one s : List.Forall (Acc R) s -> Acc R_seq_one s.
Proof.
  elim => {s} [| a s HRa _ Hind].
    by constructor; inversion 1.
  elim: Hind a HRa => {}s _ Hind a.
  elim => {}a /(Acc_intro a)-HaccA Hinda.
  by constructor; inversion_clear 1; auto.
Qed.

(*
  The two lists are identical except in at least one place,
  where the respective elements satisfy the relation R.
  It is less powerfull than the transitive closure of R_seq_one
  because we can't decrease more than once with R at one given
  place in the sequence and that is possible with the transitive closure.
*)
Inductive R_seq: relation (seq T) :=
| One_here a b s s' : R a b -> R_seq s s' -> R_seq (a::s) (b::s')
| No_more a b s : R a b -> R_seq (a::s) (b::s)
| Only_further a s s' : R_seq s s' -> R_seq (a::s) (a::s').


Lemma R_seq_size s t: R_seq s t -> size s = size t.
Proof. by elim => {s t} [//=| |a s t _ /= <- // ];firstorder. Qed.

Lemma Acc_R_seq s: List.Forall (Acc R) s -> Acc R_seq s.
Proof.
  elim => {s} [| a s HRa _ Hind].
    by constructor; inversion 1.
  elim: Hind a HRa => {}s _ Hind a.
  elim => {}a /(Acc_intro a)-HaccA Hinda.
  constructor; inversion_clear 1; auto.
  apply:(Hind s0) => [//=|].
  exact: (Acc_inv HaccA).
Qed.

Lemma R_seq_one_inc_R_seq s s':
  R_seq_one s s' ->
  R_seq s s'.
Proof.
  elim  => {s s'} [t t' s HR | t s s' _ Hseq].
    exact: No_more.
  exact: Only_further.
Qed.



End R_seq_one.

Section R_seq_one_aux.

Variables (T: Type) (R R': relation T).

Lemma to_R_seq_one s1:
  (forall t1, List.In t1 s1 → forall t2, R t1 t2 → R' t1 t2) ->
  forall s2, R_seq_one R s1 s2 -> R_seq_one R' s1 s2.
Proof.
  move => /[swap] s2 /[swap].
  by elim; [left|right]; firstorder.
Qed.

Lemma to_R_seq s1:
  (forall t1, List.In t1 s1 → forall t2, R t1 t2 → R' t1 t2) ->
  forall s2, R_seq R s1 s2 -> R_seq R' s1 s2.
Proof.
  move => /[swap] s2 /[swap].
  elim; firstorder.
  - apply: One_here; firstorder.
  - apply: No_more; firstorder.
  - apply: Only_further; firstorder.
Qed.


End R_seq_one_aux.




(*
  Definition of non-empty trees as a root node
  with a sequence of trees for the children,
  associated with a useful induction principle.
*)

Section TreeSeq.

Variable (A: Type).

Unset Elimination Schemes.

Inductive Tree :=
| Node of A & (seq Tree).

Lemma Tree_ind t (P : Tree -> Prop)
  (Pind: forall a s, (forall c, List.In c s -> P c) -> P (Node a s)) : P t.
Proof.
  move: t.
  fix Hindt 1.
  case => [a s].
  apply: Pind.
  elim: s => [T []|] t s Hinds T.
  case => [<-|Hin].
    exact: Hindt.
  exact: Hinds.
Qed.

Set Elimination Schemes.

Definition hd (T :Tree) : A := match T with
  | Node a _ => a
end.

Definition tl (T : Tree) : seq Tree := match T with
  | Node _ s => s
end.


(*
  A predicate on Tree that will allow us to select the trees
  on which the studied relation will be well-founded.
*)

Section Good.

Variable (R : relation A).

(*  This property states that the labels of the nodes decrease toward the leaves. *)
Inductive good: Tree -> Prop :=
| good_leaf a: good (Node a nil)
| good_br a t s : good (Node a s) -> R (hd t) a -> good t
  -> good (Node a (t::s)).

Lemma good_In :
  forall s a t, List.In t s -> good (Node a s) -> good t.
Proof.
  elim => //= t1 s Hind a t [-> | Hin] Hgood.
    by inversion Hgood.
  apply (Hind a) => //=.
  by inversion Hgood.
Qed.

End Good.


(*
  In the next sections, we define some rewriting relations on our Trees
  that are well-founded on good Trees.
*)


Section Rtp.

Variable (R_br: relation A).

(*
  The relation uses a global function from Trees to a type B equipped
  with a well-founded relation R_B.
*)
Variables (B: Type) (R_B: relation B) (f: Tree -> B).

Hypothesis (HwfA: well_founded R_br).
Hypothesis (HwfB: well_founded R_B).

(*
  This relation either decreases the value obtained by f,
  or does not modify it but recursively operates on one subtree.
*)
Inductive Rtp: relation Tree :=
| Rtp_hd t' t: R_B (f t') (f t) -> good R_br t' -> Rtp t' t
| Rtp_child a s' s: f (Node a s') = f (Node a s)
  -> R_seq Rtp s' s
  -> good R_br (Node a s') -> Rtp (Node a s') (Node a s).


(*
  A second relation that will help us to show that the first one is well-founded.
  This relation corresponds to Rtp when the first constructor is initially forbidden.
*)
Inductive Rtp_further: relation Tree :=
| Rtp_further_child a s' s: f (Node a s') = f (Node a s)
  -> R_seq Rtp s' s
  -> good R_br (Node a s') -> Rtp_further (Node a s') (Node a s).

Lemma Rtp_further_Rtp t' t: Rtp t' t ->
  R_B (f t') (f t) \/ (f t' = f t /\ Rtp_further t' t).
Proof.
  case => //= [t0 t0' HRB _| a s' s Hf HRseq Hgood].
    by left.
  by right.
Qed.

Lemma R_seq_Rtp_further_Rtp s' s: R_seq Rtp s' s ->
  R_seq R_B (map f s') (map f s)
    \/
  (map f s') = (map f s) /\ R_seq Rtp_further s' s.
Proof.
  elim => {s' s} [t t' s s' HR HR_seq | t t' s HR | t s s' HR_seq].
  - case => [Hind| [Hmap Hind]].
      left.
      case: (Rtp_further_Rtp HR) => [Ht_t'| [Ht_t' Hfurther]].
        exact: One_here.
      rewrite !map_cons Ht_t'.
      exact: Only_further.
    case: (Rtp_further_Rtp HR) => [Ht_t'| [Ht_t' Hfurther]].
      left.
      rewrite !map_cons Hmap.
      by apply R_seq_one_inc_R_seq, Here .
    right; rewrite !map_cons Ht_t' Hmap; split => //=.
    exact: One_here.
  - case: (Rtp_further_Rtp HR) => [Ht_t'| [Ht_t' Hfurther]].
      by left;rewrite !map_cons; apply: No_more.
    right; rewrite !map_cons Ht_t'; split => //=.
    exact: No_more.
  - case => [Hind| [Hmap Hind]].
      by left; apply: Only_further.
    by right; rewrite !map_cons Hmap; split => //=; apply: Only_further.
Qed.


Lemma Acc_good_Rtp_further:
forall t, good R_br t -> Acc Rtp_further t .
Proof.
  (* Induction on the label of the root *)
  suff: forall a, forall t , hd t = a -> good R_br t -> Acc Rtp_further t.
    by move => + t; apply.
  move => a.
  elim: (HwfA a) => {}a _ HindA.
  case => a0 s /= ->.
  (* Induction on (map f s : seq B) *)
  move HeqsB: (map f s) => sB.
  have Acc_sB: Acc (R_seq R_B) sB.
    apply: Acc_R_seq.
    by rewrite List.Forall_forall.
  elim: Acc_sB s HeqsB => {}sB _ HindsB s HeqsB Hgood.
  (* Induction on the children *)
  have Acc_s: Acc (R_seq Rtp_further) s.
    apply: Acc_R_seq.
    elim: s Hgood {HeqsB} => // t s Hinds Hgood.
    inversion_clear Hgood.
    by constructor; [exact: (HindA (hd t))|exact: Hinds].
  elim: Acc_s HeqsB Hgood => {}s _ Hinds HeqsB Hgood.
  (* Now ready to conclude with the different induction hypotheses *)
  constructor => t; inversion_clear 1 => {t}.
  rewrite -HeqsB in HindsB Hinds.
  move:H1 => /R_seq_Rtp_further_Rtp [HRB|[Heq HRf]].
    exact: (HindsB _ HRB).
  exact: (Hinds s' HRf).
Qed.


Theorem Acc_good_Rtp:
forall t, good R_br t -> Acc Rtp t.
Proof.
  (* Induction on (Acc R_B (f t)) *)
  suff: forall b t, (f t) = b -> good R_br t -> Acc Rtp t.
    by move => + t; apply.
  move => b; elim (HwfB b) => {}b _ HindB.
  (* Induction on (Acc Rtp_further t) *)
  move => t Heq Hgood.
  have Hacc := Acc_good_Rtp_further Hgood.
  elim :Hacc Heq Hgood => {}t _ Hind Heq Hgood.
  (* Now ready to conclude with the different induction hypotheses *)
  constructor; inversion 1; subst.
    exact: (HindB (f y)).
  exact: Hind.
Qed.

End Rtp.

Section Rtg.

Variable (R_br: relation A).
Variable (R_mod: relation A).
Variable (R_forest : relation nat).

Hypothesis (HwfA: well_founded R_br).
Hypothesis (HwfM: well_founded R_mod).
Hypothesis (HwfF: well_founded R_forest).

(*
  This relation either decreases (using R_mod) the value of the root,
  decreases (using R_forest) the arity of the tree,
  or recursively operates on one subtree.
*)
Inductive Rtg: relation Tree :=
| Rtg_hd a a' s s'  : R_mod a' a -> good R_br (Node a' s') -> Rtg (Node a' s') (Node a s)
| Rtg_tl a s s' : R_forest (size s') (size s) -> good R_br (Node a s') ->  Rtg (Node a s') (Node a s)
| Rtg_child a s s':  R_seq Rtg s' s -> good R_br (Node a s')
  -> Rtg (Node a s') (Node a s).


Definition f (T : Tree): (A * nat) := (hd T, size (tl T)).

Definition R_B := slexprod _ _ R_mod R_forest .

Lemma Incl_g_p: inclusion _  Rtg (Rtp R_br R_B f).
Proof.
  elim => a s Hind t.
  inversion_clear 1 => {t}.
  1,2: apply: Rtp_hd => //.
  - by apply left_slex.
  - by apply right_slex.
  - right => //.
      by rewrite /f /= (R_seq_size H0).
    exact: (to_R_seq Hind).
Qed.


Theorem Acc_good_Rtg:
forall t, good R_br t -> Acc Rtg t.
Proof.
  move => t Hgood.
  apply: (Acc_incl _ _ _ Incl_g_p).
  have HwfB := wf_slexprod _ _ _ _ HwfM HwfF.
  exact: Acc_good_Rtp.
Qed.

End Rtg.


Section Rt.

Variable (R_br: relation A).
Variable (R_mod: relation A).

Hypothesis (HwfA: well_founded R_br).
Hypothesis (HwfM: well_founded R_mod).


(*
  This relation either decreases (using R_mod) the value of the root,
  removes any (positive) number of children,
  or recursively operates on one subtree.
*)
Inductive Rt: relation Tree :=
| Rt_mod a a' s s': R_mod a' a -> good R_br (Node a' s') -> Rt (Node a' s') (Node a s)
| Rt_del  a s s': size s < size s' -> good R_br (Node a s)
  -> Rt (Node a s) (Node a s')
| Rt_child a s s':  R_seq Rt s' s -> good R_br (Node a s')
  -> Rt (Node a s') (Node a s).

Inductive Rt': relation Tree :=
| Rt'_mod a a' s s': R_mod a' a -> Rt' (Node a' s') (Node a s)
| Rt'_del  a s s': size s < size s' -> Rt' (Node a s) (Node a s')
| Rt'_child a s s':  R_seq Rt' s' s -> Rt' (Node a s') (Node a s).

Lemma Rt'_to_Rt:
  forall t1 t2, good R_br t1 -> Rt' t1 t2 -> Rt t1 t2 .
Proof.
  elim => // a1 s1 Hind [a2 s2] Hgood.
  inversion 1 as [a a' s s' HR [Ha' Hs'] [Ha Hs] |
   a s s' Hsize |
   a s s' HR [Ha Hs'] [Ha' Hs]]; subst .
  - by apply Rt_mod.
  - by apply Rt_del.
  - apply Rt_child => //.
    move: HR Hgood Hind.
    elim => [t t' s s' HR HR_seq Hind' | t t' s HR | t s s' HR_seq Hind' ] Hgood Hind.
    + apply: One_here.
        apply: Hind => //=.
          by left.
        by inversion Hgood.
      apply:Hind'.
        by inversion Hgood.
      by firstorder.
    + apply No_more, Hind => //=.
        by left.
      by inversion Hgood.
    + apply Only_further, Hind'.
        by inversion Hgood.
      by firstorder.
Qed.


Definition R_forest n1 n2 := lt n1 n2.

Lemma Incl_t_g: inclusion _ Rt (Rtg R_br R_mod R_forest).
Proof.
  elim => a s Hind t.
  inversion 1; subst.
  - by constructor 1.
  - constructor 2; last by [].
    by apply/ltP.
  - constructor 3; last by [].
    exact: (to_R_seq Hind).
Qed.


Theorem Acc_good_Rt :
  forall T, good R_br T -> Acc Rt T.
Proof.
  move => t Hgood.
  apply: (Acc_incl _ _ _ Incl_t_g).
  have HwfF := Wf_nat.lt_wf.
  exact: Acc_good_Rtg.
Qed.


End Rt.


Section coRt.

Variable (R_br: relation A).
Variable (R_mod: relation A).

Hypothesis (HwfA: well_founded R_br).
Hypothesis (HwfM: well_founded R_mod).

(*
  This relation either decreases (using R_mod) the value of the root,
  removes any (positive) number of children leaving at least one,
  adds any (positive) number of children if none before,
  or recursively operates on one subtree.
*)
Inductive coRt: relation Tree :=
| coRt_mod a a' s s': R_mod a' a -> good R_br (Node a' s') -> coRt (Node a' s') (Node a s)
| coRt_add a s: 0 < size s -> good R_br (Node a s) -> coRt (Node a s) (Node a nil)
| coRt_del a s1 s2 s3 : 0 < size s2 -> 0 < size (s1++s3)
  -> good R_br (Node a (s1++s3)) -> coRt (Node a (s1++s3)) (Node a (s1++s2++s3))
| coRt_child a s s': R_seq coRt s' s
  -> good R_br (Node a s') -> coRt (Node a s') (Node a s).


Definition coR_forest (n1 n2: nat) : Prop := (0 < n1) && ((n1 < n2) || (n2 == 0)).



Lemma wf_coR_forest : well_founded coR_forest.
Proof.
  suff Hgt0: forall n, n <> 0 -> Acc coR_forest n.
  {
    move => [|k].
      constructor => k /andP [] /lt0n_neq0 /eqP Hk _.
    all: exact: Hgt0.
  }
  move => n.
  elim: n.+1 {-2}n (ltnSn n) => {n} [//| n Hind k Hk Hgt0].
  constructor => m /andP [] /lt0n_neq0 /eqP Hm0 /orP [Hm|/eqP //].
  apply: Hind => //.
  exact: (@leq_trans k).
Qed.

Lemma Incl_cot_g: inclusion _ coRt (Rtg R_br R_mod coR_forest).
Proof.
  elim => a s Hind t.
  inversion 1; subst; last 1 first.
  - constructor 3 => //.
    exact: (to_R_seq Hind).
  - by constructor 1.
  all: constructor 2 => //=.
  - by rewrite /coR_forest H2.
  - by rewrite /coR_forest H3 /= !size_cat ltn_add2l -[size s3]/(0+_) ltn_add2r H2.
Qed.


Theorem Acc_good_coRt :
forall T,good R_br T -> Acc coRt T.
Proof.
  move => t Hgood.
  apply: (Acc_incl _ _ _ Incl_cot_g).
  have HwfF := wf_coR_forest.
  exact: Acc_good_Rtg.
Qed.

End coRt.

End TreeSeq.



(*
  Definition of trees as a node with a finfun that depends on the label
  in order to give the children, associated with a good induction principle.
*)

Section FinfunTree.

Variable (A: Type).

(*
  The variable F is the one that will give the arity of the nodes of our trees.
  In order to make finite trees we will need an arity 0 to terminate; another way to do
  it would be to add Empty as a constructor for our trees that would correspond to an empty
  tree.
*)
Variable (F : A -> finType).


Unset Elimination Schemes.

Inductive FTree :=
| Br (a:A) : {ffun (F a) -> FTree} -> FTree.


Lemma FTree_ind P
      (PBr: forall a (f : {ffun (F a) -> FTree}),
          (forall (t : (F a)), P (f t)) -> P (Br f)) : forall t, P t.
Proof.
  fix IHf 1 => -[a f].
  by apply:PBr => t; apply: IHf.
Qed.

Set Elimination Schemes.

Definition Fhd (T :FTree) : A := match T with
  | Br a f => a
end.

Definition Ftl (T : FTree) : seq FTree := match T with
  | Br a f => map f (enum (F a))
end .

(* The function that transforms a Tree with finfun into one with sequence *)

Fixpoint FT_to_T  T : (Tree A) := match T with
|Br a f => Node a ( codom  (fun t: (F a) => FT_to_T (f t)) )
end.

(* Some lemmas to show that the projection is the right one *)
Lemma Keep_hd :
forall T, Fhd T = hd (FT_to_T T).
Proof.
  move => [ a f] //=.
Qed.

Lemma Keep_tl :
forall T, map (FT_to_T) (Ftl T) = tl ( FT_to_T T).
Proof.
  move => [ a f] /=.
  rewrite -map_comp   //=.
Qed.

Definition Fgood (R :relation A) T := good R (FT_to_T T).

(*
  With the projection from FTrees to Trees and Fgood we can extend all the previous
  relation to FTrees and show that they are wellfounded on Fgood Ftrees thanks to
  the inverse image. We do an instance below.
*)

Section FRt.


Variable (R_br: relation A).

Variable (R_mod: relation A).

Hypothesis (HwfA: well_founded R_br).
Hypothesis (HwfM: well_founded R_mod).

Definition FRt T1 T2: Prop := Rt R_br R_mod (FT_to_T T1) (FT_to_T T2).


Theorem Acc_good_FRt :
forall T, Fgood R_br T -> Acc FRt T.
Proof.
  move => t Hgood.
  rewrite /FRt.
  apply Acc_inverse_image.
  by apply Acc_good_Rt.
Qed.

End FRt.
End FinfunTree.