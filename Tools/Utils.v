(**************************************************************)
(*   Copyright (c) 2024 Thibault Hilaire                      *)
(*                      David Ilcinkas                        *)
(*                      Jerome Leroux                         *)                           
(*   Authors : Thibault Hilaire [*]                           *)
(*             David Ilcinkas [+,*]                           *)
(*             Jerome Leroux  [+,*]                           *)
(*                                                            *)
(*                             [*] Affiliation Univ. Bordeaux *)
(*                             [+] Affiliation CNRS           *)
(**************************************************************)
(*      This file is distributed under the terms of the       *)
(*                      MIT License                           *)
(**************************************************************)


From mathcomp Require Import all_ssreflect zify.
Require Import Relations.
Import Order.TTheory.
Require Import orderext petrinet pnkmaccel. 
Require Import wbr_tree.
From Coq Require Import Arith Lia.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope order_scope.
Local Open Scope petri_net_scope.

Section Gadgets.

(* Some lemmas about closure of relation *)

Section Rel.

Context (A: Type).

Lemma Rel_clos_trans_inc (R1 R2 : relation A):
  inclusion _ R1 R2 ->
  inclusion _ (clos_refl_trans _ R1) (clos_refl_trans _ R2) .
Proof.
  move => Hinc a1 a2.
  elim => {a1 a2} [x y HR | x | x y z _ HR2xy _ HR2yz].
  - by apply rt_step, Hinc.
  - exact:rt_refl.
  - exact:(@rt_trans _ _ _ y).
Qed.

Lemma clos_t_rt (R: relation A) : forall x y z,
  clos_trans _ R x y ->
  clos_refl_trans _ R y z ->
  clos_trans _ R x z.
Proof.
  move => x y z Hr H; move: H Hr.
  elim => [a b HR Htrans | a //=| a b c Htrans1 Hind1 Htrans2 Hind2 H].
    apply: (@t_trans _ _ _ a) => //=.
    exact: t_step.
  by apply Hind2, Hind1.
Qed.


End Rel.

Section Seq.

(* Some lemmas about sequence with use of the List.In of the standard library *)

Context (T: Type).
Context (E: eqType).


Lemma  abs_cons_eq  (t: T) (s: seq T):
  s ++ [::t] = s ->
  False .
Proof.
  elim :s  => //= a s Hind.
  by inversion 1.
Qed.


Lemma allP' (s : seq T) (a : pred T) : reflect (forall x, List.In x s -> a x) (all a s).
Proof.
elim: s => [|/= y s IHs]; first by left.
apply : (iffP idP); last first.
  move => H.
  apply/andP; split => //=.
    by apply H; left.
  apply/IHs => x Hin.
  by apply: H; right.
move => /andP [Ha Hall] x.
case.
  by move => <-.
by apply/IHs.
Qed.

Lemma  Forall_inc_predicate (s : seq T) (P1 P2 : T -> Prop):
  (forall t:T, List.In t s -> P1 t -> P2 t) ->
  List.Forall P1 s ->
  List.Forall P2 s.
Proof.
  move => Hinc HP1.
  apply/List.Forall_forall => t Hin.
  move/(_ t Hin):Hinc; apply.
  by move/List.Forall_forall: HP1; apply.
Qed.

Lemma cat1  (s1 s2: seq T) (x: T) :
  s1++s2 = [::x] ->
  (s1=[::x] /\ s2 = nil) \/
  (s1= nil /\ s2 = [::x]).
Proof.
  case: s1 => [|t l].
    by rewrite cat0s => ->; right.
  rewrite cat_cons; inversion 1; subst.
  move/nilP: H2; rewrite cat_nilp => /andP [] /nilP -> /nilP ->.
  by left.
Qed.

Lemma cat_prefix  (s1 s2 s3 s4 : seq T) :
  s1 ++ s2 = s3 ++ s4 ->
  exists s, s1++s = s3 \/ s3++s=s1.
Proof.
  elim: s1 s2 s3 s4 => [//=|t s1 Hind] s2 [|t' s3] s4.
    1,2 :by move => _; eexists; left.
    by move => _; eexists;right.
  rewrite !cat_cons.
  inversion 1 as [[Ht Heq]];subst.
  move: (Hind s2 s3 s4 Heq) => [] s .
  elim => Hs; subst.
    by eexists; left.
  by eexists; right.
Qed.  
  
Lemma catL  (s1 s2 s3: seq T):
  s1 ++ s2 = s1 ++s3 -> s2 = s3.
Proof.
  elim : s1 => //= t s Hind.
  inversion 1.
  by apply Hind.
Qed.

Lemma nil_or_rcons  (s : seq T):
  (s=nil) \/ (exists t s', s= s'++[::t] ).
Proof.
  elim : s => //=.
    by left.
  move => a s.
  elim => [->|].
    by right; exists a,nil.
  move => [] t [] s' ->.
  by right; exists t, (a::s').
Qed.

Lemma neq_nil  (s s' : seq E):
  s != (s++s') ->
  s' != nil.
Proof.
  elim : s s' => [| t s Hind] [//=|t' s'] //=.
  by rewrite cats0 eqxx.
Qed.

Lemma flatten0 (s: seq (seq E)):
  nil = flatten s ->
  forall s', s' \in s -> s' = nil  .
Proof.
  elim: s => //= s' s Hind H.
  symmetry in H; move/nilP: H; rewrite cat_nilp => /andP [] /nilP -> /nilP Hs s''.
  rewrite in_cons => /orP.
  elim => //=.
    by move/eqP => ->. 
  by apply:Hind.
Qed.



Lemma subseqE  (x: E) (s1 s2 : seq E):
  subseq (x::s1) (x::s2) = subseq s1 s2.
Proof.
  by rewrite /subseq eqxx.
Qed.
  
End Seq.


Section FinType_seq.
(* 
  Lemmas about sequence with a finType, to be more precise about interactions
  between codom, flatten and in or List.In
*)
Context (F: finType).
Context (E: eqType).
Context (X: Type).


Lemma flatten_codom_nil (f:  F -> seq E):
  (forall x:F, f x = nil) ->
  flatten (codom f) = nil.
Proof.
  move => Hf.
  rewrite codomE.
  elim (enum F) => [//=| t l Hind].
  by rewrite map_cons //= Hind Hf.
Qed.


Lemma subseq_codom (s1 s2 : seq (seq E)):
  size s1 = size s2 -> 
  (forall i , subseq (nth nil s1 i) (nth nil s2 i)) ->  
  subseq (flatten s1) (flatten s2).
Proof.
  elim: s1 s2 => //= [s2 _ _ | a1 l1 Hind].
    by rewrite sub0seq.
  case => //= a2 l2.
  rewrite -![(size _).+1]/(1+(size _)) => /addnI Heq Hsub.
  apply cat_subseq.
    by apply (Hsub 0).
  apply Hind => //= i.
  by apply (Hsub i.+1).
Qed.

Lemma In_flatten_codom  (e: E) (f:  F -> seq E):
  e \in flatten (codom f) ->
  exists t:F, e \in (f t).
Proof.
  rewrite codomE.
  elim : (enum F) => [//=| t s Hind].
  rewrite map_cons /flatten /foldr mem_cat => Hor.
  elim: (orP Hor) => //= Hin.
  by exists t.
Qed.

Lemma In_codom_flatten (e: E) (t: F) (f:  F -> seq E):
  e \in (f t) ->
  e \in flatten (codom f) .
Proof.
  rewrite codomE.
  have Hin:( t \in enum F).
    by apply/mem_enum.
  elim : (enum F) Hin => //= t' l Hind.
  rewrite in_cons mem_cat => /orP Hor.
  elim : Hor.
    by move/eqP => -> ->.
  move => Hin Hy.
  by rewrite Hind //= orbT.
Qed.

Lemma codom_flatten1 (e: E)  (f:  F -> seq E): 
  flatten ( codom f) = [::e] ->
  exists t, (f t) = [::e] /\
  forall t0, t0 \in (enum F) -> t != t0 -> (f t0) = nil .
Proof.
  rewrite codomE.
  move: (enum_uniq F).
  elim : (enum F) => //= e0 s Hind /andP [] Hnotin Huniq H.
  move: (cat1 H).
  elim; last first.
    move => [] He0 /(Hind Huniq) [] t [] Hft Hnil; exists t; split => //=.
    move => t0; rewrite in_cons => /orP; elim => //=; last first.
      apply:Hnil.
    by move/eqP => Hty0; subst; move: He0 Hft => ->.
  move => [] He0 {}H.
  symmetry in H.
  move/flatten0: H => H.
  exists e0; split => //= t; rewrite in_cons => /orP.
  elim. 
    by move/eqP => ->; rewrite eqxx.
  move => Hin Hneq.
  by apply H, map_f. 
Qed.


Lemma L_In_flatten_codom  (x: X) (f:  F -> seq X):
  List.In x (flatten (codom f)) ->
  exists t:F, List.In x (f t).
Proof.
  rewrite codomE.
  elim : (enum F) => [//=| t s Hind].
  rewrite map_cons /flatten /foldr List.in_app_iff.
  case => //= Hin.
  by exists t.
Qed.

Lemma L_In_codom_flatten (x: X) (t: F) (f:  F -> seq X):
  List.In x (f t) ->
  List.In x (flatten (codom f)) .
Proof.
  rewrite codomE.
  have Hin:( t \in enum F).
    by apply/mem_enum.
  elim : (enum F) Hin => //= t' l Hind.
  rewrite in_cons List.in_app_iff => /orP Hor.
  elim : Hor.
    by move/eqP => ->; left.
  move => Hin Hy.
  by right; apply:Hind.
Qed.


Lemma subseq_flatten :
  forall l (s: seq E), s \in l -> subseq s (flatten l).
Proof.
  elim => //= t l Hind s.
  rewrite in_cons.
  move/orP; case.
    by move/eqP => ->; rewrite prefix_subseq.
  move/Hind.
  rewrite -[in subseq s (t++ flatten l)](cat0s s).
  by apply cat_subseq; rewrite sub0seq.
Qed.


Lemma fun_R_seq_one (f1 f2 : F ->  X ) (t : F) (P1 P2 : F -> bool) (R : relation X) :
  (forall t0, t0!=t -> f1 t0 = f2 t0 ) -> 
  P1 =1 P2 -> 
  P1 t ->
  P2 t -> 
  R (f1 t) (f2 t) -> 
  R_seq_one R [seq (f1 t0) | t0 <- enum F & (P1 t0)] [seq (f2 t0) | t0 <- enum F & (P2 t0)].
Proof.
  have Hin:( t \in enum F).
    by apply/mem_enum.
  move : Hin (enum_uniq F) .
  elim : (enum F) => //= a l Hind Hin /andP [Hnotin Huniq] Heq HP HP1 HP2 HR.
  move: Hin; rewrite in_cons => Hor.
  elim : (orP Hor) => [/eqP|] {}Hor.
    subst; rewrite HP1 HP2 !map_cons.
    suff Hseq: ([seq f2 t | t <- l & P2 t] = [seq f1 t | t <- l & P1 t]).
      rewrite Hseq.
      by apply Here.
    move:  (eq_filter HP) => ->.
    apply eq_in_map => t0 Hin.
    rewrite Heq => //=.
    by apply/eqP => Ht0; move :Ht0 Hin Hnotin => ->; rewrite mem_filter => /andP [_ ->].
  rewrite HP.
  case :(P2 a); last first.
    exact:Hind.
  rewrite !map_cons Heq; last first.
    by apply/eqP=> Ha; move : Hor Hnotin; rewrite Ha => ->.
  by apply Further; apply Hind.
Qed.


Lemma fun_R_seq_aux  (f1 f2 : F ->  X ) (s : seq F) (P1 P2 : F -> bool) (R : relation X) :
  P1 =1 P2 -> 
  (forall t0, t0 \in s-> f1 t0 = f2 t0 \/ R (f1 t0) (f2 t0)  ) -> 
  R_seq R [seq (f1 t0) | t0 <- s & (P1 t0)] [seq (f2 t0) | t0 <- s & (P2 t0)] \/ 
  [seq (f1 t0) | t0 <- s & (P1 t0)]=[seq (f2 t0) | t0 <- s & (P2 t0)].
Proof.
  move => HP.
  elim:s => //= [| t s Hind] Heq.
    by right.
  move: (HP t) => ->.
  case: (P2 t) => //=; last first.
    apply Hind; firstorder.
    by apply: Heq; rewrite in_cons H orbT.
  case: Hind => [|HR_seq| ->].
  - by firstorder;apply: Heq; rewrite in_cons H orbT.
  all: case:(Heq t).
  1,4: by rewrite in_cons eqxx orTb.
  1,3: move => ->.
  2: by right.
  1: by left;apply: Only_further.
  all: move => HR; left.
  - exact: One_here.
  - exact: No_more.
Qed.


Lemma fun_R_seq (f1 f2 : F ->  X) (t : F) (P1 P2 : F -> bool) (R : relation X) :
  (forall t0, t0!=t -> f1 t0 = f2 t0 \/ R (f1 t0) (f2 t0)  ) -> 
  P1 =1 P2 -> 
  P1 t ->
  P2 t -> 
  R (f1 t) (f2 t) -> 
  R_seq R [seq (f1 t0) | t0 <- enum F & (P1 t0)] [seq (f2 t0) | t0 <- enum F & (P2 t0)].
Proof.
  have Hin:( t \in enum F).
    by apply/mem_enum.
  move : Hin (enum_uniq F) .
  elim : (enum F) => //= a l Hind Hin /andP [Hnotin Huniq] Heq HP HP1 HP2 HR.
  move: Hin; rewrite in_cons => Hor.
  elim : (orP Hor) => [/eqP|] {}Hor; last first.
    case Ha_t:(a==t).
      by move/eqP: Ha_t Hor Hnotin => -> ->.
    move/eqP/eqP:Ha_t.
    move: (HP a) => ->.
    case: (P2 a) => //=.
    2: by move => _;apply Hind.
    move/Heq; case => Ha.
    1:rewrite Ha; apply: Only_further.
    2: apply: One_here => //=.
    1,2: by apply Hind.
  subst;rewrite HP1 HP2 !map_cons {Hind}.
  case: (@fun_R_seq_aux f1 f2 l P1 P2 R) => //=.
  - move => t Hin; apply Heq.
    by apply/negP => /eqP H;subst; move: Hin Hnotin => ->.
  - exact: One_here.
  - move => ->; exact: No_more.
Qed.

Lemma Dec_enum (t :F): 
  exists l1 l2, (forall t', t' \in l1 -> t' != t) /\
 (forall t', t' \in l2 -> t' != t) /\ 
 (enum F = l1 ++ t::l2)  .
Proof.
  have Hin:( t \in enum F).
    by apply/mem_enum.
  move : Hin (enum_uniq F) .
  elim : (enum F) => // a l Hind.
  rewrite in_cons.
  case/orP => [/eqP |] Hin; subst; rewrite cons_uniq => /andP [Hnotin Huniq].
    exists nil, l; split => //=; split => //= t Hin.
    by apply/eqP => Heq; subst; move/negP : Hnotin; move: Hin => ->.
  move/(_ Hin Huniq): Hind => [l1 [l2 [Hl1 [ Hl2 Heq]]]].
  exists (a::l1), l2; rewrite Heq => //=; split => //=.
  move => t'; rewrite in_cons.
  case/orP => [/eqP |] Hin'; apply/eqP => Ht; subst.
    by rewrite Hin in Hnotin.
  by move/(_ t Hin'): Hl1; rewrite eqxx.
Qed.

End FinType_seq.

Section pn.

Variable pn : petri_net.


Notation place := (place pn).
Notation marking := (marking pn).
Notation markingc := (markingc pn).
Notation transition := (transition pn).

Section antichain. 
(* Some lemmas about antichain of markings and subseq *)
  

Definition antichain (s : seq markingc) : bool :=
  pairwise (fun m1 m2 => ~~((m1 <= m2 )||(m2 <= m1 )) ) s.

Lemma In_antichain s1 s2 (mc1 mc2: markingc):
  mc1 \in s1 ->
  mc2 \in s2 ->
  antichain (s1 ++ s2) ->
  ~~((mc1 <= mc2 )||(mc2 <= mc1 )) .
Proof.
  elim: s1 s2 mc1 mc2 => // mc s1 Hind s2 mc1 mc2.
  rewrite in_cons => /orP Hor.
  case: Hor; last first.
    move => Hmc1 Hmc2 Hac.
    apply:(Hind s2) => //=.
    by move: Hac; rewrite cat_cons /antichain //= => /andP [_ ->].
  move/eqP => ->.
  rewrite cat_cons => Hin.
  rewrite /antichain //= => /andP [H _].
  move/allP/(_ mc2): H => -> //=.
  by rewrite mem_cat Hin orbT.
Qed.


Lemma In_same_antichain s (mc1 mc2: markingc):
  mc1 \in s ->
  mc2 \in s ->
  mc1 != mc2 ->
  antichain (s) ->
  ~~((mc1 <= mc2 )||(mc2 <= mc1 )) .
Proof.
  elim: s mc1 mc2 => // mc s1 Hind mc1 mc2.
  rewrite !in_cons => /orP Hor /orP Hor'.
  case: Hor; case Hor'.
    - by move/eqP => -> /eqP => ->; rewrite eqxx.
    - move => Hin /eqP -> Hneq; rewrite -cat1s => Hac.
      apply: (In_antichain _ Hin Hac).
      by rewrite in_cons eqxx.
    - move =>/eqP -> Hin Hneq; rewrite -cat1s => Hac.
      rewrite orbC.
      apply: (In_antichain _ Hin Hac).
      by rewrite in_cons eqxx.
    - move => Hin1 Hin2 Hneq /andP [_ H].
      exact:Hind.
Qed.

Lemma antichain_uniq s: antichain s -> uniq s.
Proof.
  elim : s => //= mc s Hind /andP [] Hmc /Hind -> .
  rewrite andbT.
  case Hin: (mc \in s) => //=.
  move/allP/(_ _ Hin):Hmc.
  by rewrite (Order.POrderTheory.le_refl mc).
Qed.

Lemma antichain_subseq:
  forall (s1 s2 : seq markingc), subseq s1 s2 -> antichain s2 -> antichain s1.
Proof.
  move => s1 s2.
  exact: subseq_pairwise.
Qed.


Lemma rev_antichain:
  forall s, antichain s -> antichain (rev s).
Proof.
  elim => //= a s Hind /andP [Ha /Hind Hs].
  rewrite rev_cons /antichain pairwise_rcons.
  apply/andP; split => //=.
  apply/allP => mc Hin.
  rewrite mem_rev in Hin.
  move/allP/(_ mc): Ha => Ha.
  move/Ha: Hin.
  apply contra.
  by rewrite orbC.
Qed.


Lemma in_middle_antichain:
  forall s s' mc, antichain ((mc::s)++s') -> antichain (s++(mc::s')).
Proof.
  move => s s' mc.
  rewrite /antichain !pairwise_cat.
  rewrite allrel_consl allrel_consr =>/andP [] /andP [] Hall HallRel /andP [] Hac_mc_s Hac_s'.
  apply/andP; split.
    apply/andP; split => //.
    move: Hac_mc_s => //= /andP [] H _.
    apply/allP => mc' Hin.
    by move/allP/(_ mc' Hin): H; rewrite orbC.
  apply/andP; split => //=.
    by move: Hac_mc_s => //= /andP [].
  by apply/andP.
Qed.

End antichain.

Section Marking_seq.

Lemma has_subset_leq (mc1 mc2 : markingc) (s1 s2 : seq markingc):
  subseq s1 s2 ->
  mc1 <= mc2 ->
  ~~  has (fun m => mc1 <= m) (s2) ->
  ~~  has (fun m => mc2 <= m) (s1).
Proof.
  elim : s1 s2 mc1 mc2 => [//=| m1 s1 Hind] s2 mc1 mc2 Hsub  Hleq Hs2 //=.
  have Hsub':= (cons_subseq Hsub).
  apply/norP; split.
  2: by apply:(Hind s2 mc1 mc2).
  apply/negP => Hleq'.
  move/negP:Hs2; apply.
  apply/hasP; exists m1.
    move/mem_subseq/(_ m1): Hsub; rewrite in_cons eqxx //=.
    by move/(_ isT).
  by apply (Order.POrderTheory.le_trans Hleq Hleq'). 
Qed.

  
End Marking_seq.
End pn.
End Gadgets.